/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.repository;

import java.util.UUID;
import org.junit.runner.RunWith;
import org.openlmis.equipment.domain.EquipmentOrderable;
import org.openlmis.equipment.util.EquipmentOrderableDataBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Transactional
@SuppressWarnings({"PMD.UnusedPrivateField"})
public class EquipmentOrderableRepositoryIntegrationTest
    extends BaseCrudRepositoryIntegrationTest<EquipmentOrderable> {

  @Autowired
  private EquipmentOrderableRepository equipmentOrderableRepository;

  @Override
  CrudRepository<EquipmentOrderable, UUID> getRepository() {
    return this.equipmentOrderableRepository;
  }

  @Override
  EquipmentOrderable generateInstance() {
    return new EquipmentOrderableDataBuilder().buildAsNew();
  }
}
