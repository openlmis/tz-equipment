/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.repository;

import static org.assertj.core.api.Assertions.assertThat;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openlmis.equipment.domain.Discipline;
import org.openlmis.equipment.domain.Equipment;
import org.openlmis.equipment.domain.EquipmentCategory;
import org.openlmis.equipment.domain.EquipmentEnergyType;
import org.openlmis.equipment.domain.EquipmentFundSource;
import org.openlmis.equipment.domain.EquipmentInventory;
import org.openlmis.equipment.domain.EquipmentModel;
import org.openlmis.equipment.domain.EquipmentType;
import org.openlmis.equipment.domain.FunctionalStatus;
import org.openlmis.equipment.domain.Utilization;
import org.openlmis.equipment.dto.EquipmentDto;
import org.openlmis.equipment.dto.EquipmentFundSourceDto;
import org.openlmis.equipment.repository.custom.EquipmentInventoryRepositoryCustom.RepositorySearchParams;
import org.openlmis.equipment.service.EquipmentFundSourceService;
import org.openlmis.equipment.service.EquipmentService;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.DisciplineDataBuilder;
import org.openlmis.equipment.util.EquipmentCategoryDataBuilder;
import org.openlmis.equipment.util.EquipmentDataBuilder;
import org.openlmis.equipment.util.EquipmentEnergyTypeDataBuilder;
import org.openlmis.equipment.util.EquipmentFundSourceDataBuilder;
import org.openlmis.equipment.util.EquipmentInventoryDataBuilder;
import org.openlmis.equipment.util.EquipmentModelDataBuilder;
import org.openlmis.equipment.util.EquipmentTypeDataBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Transactional
@SuppressWarnings({"PMD.TooManyMethods", "PMD.UnusedPrivateField"})
public class EquipmentInventoryRepositoryIntegrationTest
    extends BaseCrudRepositoryIntegrationTest<EquipmentInventory> {

  @Autowired
  private EquipmentInventoryRepository equipmentInventoryRepository;

  @Autowired
  private EquipmentFundSourceService equipmentFundSourceService;

  @Autowired
  private EquipmentService equipmentService;

  @MockBean
  private PermissionService permissionService;

  @Autowired
  private EquipmentTypeRepository equipmentTypeRepository;

  @Autowired
  private EquipmentRepository equipmentRepository;

  @Autowired
  private EquipmentEnergyTypeRepository equipmentEnergyTypeRepository;

  @Autowired
  private EquipmentModelRepository equipmentModelRepository;

  @Autowired
  private EquipmentCategoryRepository equipmentCategoryRepository;

  @Autowired
  private DisciplineRepository disciplineRepository;

  private Equipment equipment1;

  private Equipment equipment2;

  private EquipmentFundSource fundSource;

  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    if (equipment1 == null) {
      initEquipments();
    }

    if (fundSource == null) {
      fundSource = equipmentFundSourceService.create(
          EquipmentFundSourceDto.newInstance(
              new EquipmentFundSourceDataBuilder()
                  .buildAsNew()
          )
      );
    }
  }

  @Test
  public void shouldSearchEquipmentInventoryByUtilization() {

    createEquipmentInventory(
        equipment1,
        FunctionalStatus.FUNCTIONING,
        Utilization.ACTIVE
    );
    createEquipmentInventory(
        equipment1,
        FunctionalStatus.FUNCTIONING,
        Utilization.ACTIVE
    );
    createEquipmentInventory(
        equipment1,
        FunctionalStatus.FUNCTIONING,
        Utilization.NOT_IN_USE
    );

    RepositorySearchParams searchParams = new RepositorySearchParams();
    searchParams.setUtilizations(Arrays.asList(Utilization.ACTIVE));

    Page<EquipmentInventory> equipmentInventories = equipmentInventoryRepository.search(
        searchParams,
        pageable
    );
    assertThat(equipmentInventories).allMatch(
        inventory -> inventory.getUtilization().equals(Utilization.ACTIVE)
    );
  }

  @Test
  public void shouldSearchEquipmentInventoryByFunctionalStatus() {

    createEquipmentInventory(
        equipment1,
        FunctionalStatus.FUNCTIONING,
        Utilization.ACTIVE
    );
    createEquipmentInventory(
        equipment1,
        FunctionalStatus.AWAITING_REPAIR,
        Utilization.ACTIVE
    );
    createEquipmentInventory(
        equipment1,
        FunctionalStatus.UNSERVICEABLE,
        Utilization.NOT_IN_USE
    );

    List<FunctionalStatus> statusQuery = Arrays.asList(
        FunctionalStatus.UNSERVICEABLE,
        FunctionalStatus.AWAITING_REPAIR
    );
    RepositorySearchParams searchParams = new RepositorySearchParams();
    searchParams.setFunctionalStatuses(statusQuery);

    Page<EquipmentInventory> equipmentInventories = equipmentInventoryRepository.search(
        searchParams,
        pageable
    );

    assertThat(equipmentInventories).hasSize(2);
    assertThat(equipmentInventories).allMatch(
        inventory -> statusQuery.contains(inventory.getFunctionalStatus())
    );
  }

  @Test
  public void shouldSearchEquipmentInventoryByFacilityIds() {

    EquipmentInventory inv1 = createEquipmentInventory();
    EquipmentInventory inv2 = createEquipmentInventory();

    List<UUID> searchFacilityIds = Arrays.asList(inv1.getFacilityId(), inv2.getFacilityId());

    RepositorySearchParams searchParams = new RepositorySearchParams();
    searchParams.setProgramIds(searchFacilityIds);

    Page<EquipmentInventory> equipmentInventories = equipmentInventoryRepository.search(
        searchParams,
        pageable
    );
    assertThat(equipmentInventories).allMatch(
        equipmentInventory -> searchFacilityIds.contains(equipmentInventory.getFacilityId())
    );
  }

  @Test
  public void shouldSearchEquipmentInventoryByProgramIds() {

    EquipmentInventory inv1 = createEquipmentInventory();
    EquipmentInventory inv2 = createEquipmentInventory();

    List<UUID> searchProgramIds = Arrays.asList(inv1.getProgramId(), inv2.getProgramId());

    RepositorySearchParams searchParams = new RepositorySearchParams();
    searchParams.setProgramIds(searchProgramIds);

    Page<EquipmentInventory> inventories = equipmentInventoryRepository.search(
        searchParams, pageable
    );

    assertThat(inventories).allMatch(
        equipmentInventory -> searchProgramIds.contains(equipmentInventory.getProgramId())
    );
  }

  @Test
  public void shouldSearchEquipmentInventoryByEquipmentId() {

    createEquipmentInventory(equipment1);
    createEquipmentInventory(equipment1);
    createEquipmentInventory(equipment2);
    createEquipmentInventory(equipment2);

    RepositorySearchParams searchParams = new RepositorySearchParams();
    searchParams.setEquipmentIds(Arrays.asList(equipment1.getId()));

    Page<EquipmentInventory> inventories = equipmentInventoryRepository.search(
        searchParams,
        pageable
    );

    assertThat(inventories).allMatch(
        inventory -> inventory.getEquipment().getId().equals(equipment1.getId())
    );
  }

  @Test
  public void shouldSearchEquipmentInventoryByEquipmentTypeId() {

    createEquipmentInventory(equipment1);
    createEquipmentInventory(equipment1);
    createEquipmentInventory(equipment2);
    createEquipmentInventory(equipment2);

    Set<UUID> equipmentTypeIds = Sets.newHashSet(equipment1.getModel().getEquipmentType().getId());
    RepositorySearchParams searchParams = new RepositorySearchParams();
    searchParams.setEquipmentTypeIds(equipmentTypeIds);

    Page<EquipmentInventory> inventories = equipmentInventoryRepository.search(
        searchParams,
        pageable
    );

    assertThat(inventories).allMatch(
        inventory -> {
          UUID typeId = inventory.getEquipment().getModel().getEquipmentType().getId();
          return equipmentTypeIds.contains(typeId);
        }
    );
  }

  @Test
  public void shouldSearchEquipmentInventoryByIds() {
    EquipmentInventory inventory1 = createEquipmentInventory();
    EquipmentInventory inventory2 = createEquipmentInventory();
    EquipmentInventory inventory3 = createEquipmentInventory();

    Set<UUID> searchIds = Sets.newHashSet(inventory1.getId(), inventory2.getId());
    RepositorySearchParams searchParams = new RepositorySearchParams();
    searchParams.setIds(searchIds);

    Page<EquipmentInventory> inventories = equipmentInventoryRepository.search(
        searchParams,
        pageable
    );

    assertThat(inventories).allMatch(
        equipmentInventory -> searchIds.contains(equipmentInventory.getId())
    );
    assertThat(inventories).allMatch(
        equipmentInventory -> !equipmentInventory.getId().equals(inventory3.getId())
    );
  }

  @Test
  public void shouldReturnAllEquipmentInventoryWhenNoParameterIsProvided() {

    List<EquipmentInventory> expectedEquipmentInventories = Lists.newArrayList(
        createEquipmentInventory(),
        createEquipmentInventory(),
        createEquipmentInventory(),
        createEquipmentInventory()
    );

    Page<EquipmentInventory> foundEquipmentInventories = equipmentInventoryRepository.search(
        new RepositorySearchParams(),
        pageable);

    assertThat(expectedEquipmentInventories).allMatch(
        equipmentInventory -> foundEquipmentInventories.getContent().contains(equipmentInventory)
    );
  }


  private EquipmentInventory createEquipmentInventory() {
    return createEquipmentInventory(equipment1);
  }

  private EquipmentInventory createEquipmentInventory(Equipment equipment) {
    return createEquipmentInventory(
        new EquipmentInventoryDataBuilder()
            .withEquipment(equipment)
            .withFundSource(fundSource)
            .build()
    );
  }

  private EquipmentInventory createEquipmentInventory(
      Equipment equipment,
      FunctionalStatus status,
      Utilization utilization) {
    return createEquipmentInventory(
        new EquipmentInventoryDataBuilder()
            .withEquipment(equipment)
            .withFundSource(fundSource)
            .withStatus(status)
            .withUtilization(utilization)
            .build()
    );
  }

  private EquipmentInventory createEquipmentInventory(EquipmentInventory equipmentInventory) {
    EquipmentInventory createdEquipmentInventory = equipmentInventoryRepository.save(
        equipmentInventory
    );
    return createdEquipmentInventory;
  }

  @Override
  CrudRepository<EquipmentInventory, UUID> getRepository() {
    return this.equipmentInventoryRepository;
  }

  @Override
  EquipmentInventory generateInstance() {
    return new EquipmentInventoryDataBuilder()
        .withEquipment(equipment1)
        .withFundSource(fundSource)
        .withId(null).build();
  }

  private void initEquipments() {
    equipmentRepository.deleteAll();
    equipmentEnergyTypeRepository.deleteAll();
    equipmentModelRepository.deleteAll();
    equipmentCategoryRepository.deleteAll();
    disciplineRepository.deleteAll();
    equipmentTypeRepository.deleteAll();
    equipmentInventoryRepository.deleteAll();

    EquipmentType equipmentType1 = equipmentTypeRepository.save(
        new EquipmentTypeDataBuilder().buildAsNew()
    );

    EquipmentType equipmentType2 = equipmentTypeRepository.save(
        new EquipmentTypeDataBuilder().buildAsNew()
    );

    EquipmentModel equipmentModel1 = equipmentModelRepository.save(
        new EquipmentModelDataBuilder()
            .withEquipmentType(equipmentType1)
            .build()
    );

    EquipmentModel equipmentModel2 = equipmentModelRepository.save(
        new EquipmentModelDataBuilder()
            .withEquipmentType(equipmentType2)
            .build()
    );

    EquipmentEnergyType equipmentEnergyType = equipmentEnergyTypeRepository.save(
        new EquipmentEnergyTypeDataBuilder().buildAsNew()
    );

    Discipline discipline = disciplineRepository.save(
        new DisciplineDataBuilder().build()
    );

    EquipmentCategory equipmentCategory1 = equipmentCategoryRepository.save(
        new EquipmentCategoryDataBuilder()
            .withEquipmentType(equipmentType1)
            .withDisciplines(Sets.newHashSet(discipline))
            .build()
    );

    EquipmentCategory equipmentCategory2 = equipmentCategoryRepository.save(
        new EquipmentCategoryDataBuilder()
            .withEquipmentType(equipmentType2)
            .withDisciplines(Sets.newHashSet(discipline))
            .build()
    );

    equipment1 = equipmentService.create(EquipmentDto.newInstance(
        new EquipmentDataBuilder()
            .withEquipmentCategory(equipmentCategory1)
            .withEquipmentEnergyType(equipmentEnergyType)
            .withEquipmentModel(equipmentModel1)
            .buildAsNew()
    ));

    equipment2 = equipmentService.create(EquipmentDto.newInstance(
        new EquipmentDataBuilder()
            .withEquipmentCategory(equipmentCategory2)
            .withEquipmentEnergyType(equipmentEnergyType)
            .withEquipmentModel(equipmentModel2)
            .buildAsNew()
    ));
  }
}
