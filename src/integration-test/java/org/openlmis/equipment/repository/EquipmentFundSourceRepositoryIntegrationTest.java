/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.repository;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.assertj.core.api.Assertions.assertThat;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openlmis.equipment.domain.EquipmentFundSource;
import org.openlmis.equipment.dto.EquipmentFundSourceDto;
import org.openlmis.equipment.service.EquipmentFundSourceService;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.EquipmentFundSourceDataBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Transactional
@SuppressWarnings({"PMD.UnusedPrivateField"})
public class EquipmentFundSourceRepositoryIntegrationTest
    extends BaseCrudRepositoryIntegrationTest<EquipmentFundSource> {

  private static final String TEST_NAME = "foo";
  private static final String TEST_CODE = "bar";

  @MockBean
  private PermissionService permissionService;
  @Autowired
  private EquipmentFundSourceService equipmentFundSourceService;
  @Autowired
  private EquipmentFundSourceRepository equipmentFundSourceRepository;

  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    equipmentFundSourceRepository.deleteAll();
  }

  @Test
  public void shouldSearchEquipmentFundSourceByName() {
    createEquipmentFundSource();
    createEquipmentFundSource();
    createEquipmentFundSource(TEST_NAME, TEST_CODE);

    Page<EquipmentFundSource> fundSources = equipmentFundSourceRepository
        .findAllByNameContainingIgnoreCase("f", pageable);

    assertThat(fundSources).allMatch(fundSource -> fundSource.getName().contains("f"));
  }

  @Test
  public void shouldSearchEquipmentFundSourceByNameIgnoringCase() {
    createEquipmentFundSource();
    createEquipmentFundSource();
    createEquipmentFundSource(TEST_NAME, TEST_CODE);

    Page<EquipmentFundSource> fundSources = equipmentFundSourceRepository
        .findAllByNameContainingIgnoreCase("F", pageable);

    assertThat(fundSources).allMatch(fundSource -> containsIgnoreCase(fundSource.getName(), "F"));
  }

  @Test
  public void shouldSearchEquipmentFundSourceByCode() {
    createEquipmentFundSource();
    createEquipmentFundSource();
    createEquipmentFundSource(TEST_NAME, TEST_CODE);

    Page<EquipmentFundSource> fundSources = equipmentFundSourceRepository
        .findAllByCodeContainingIgnoreCase("f", pageable);

    assertThat(fundSources).allMatch(fundSource -> fundSource.getCode().contains("f"));
  }

  @Test
  public void shouldSearchEquipmentFundSourceByCodeIgnoringCase() {
    createEquipmentFundSource();
    createEquipmentFundSource();
    createEquipmentFundSource(TEST_NAME, TEST_CODE);

    Page<EquipmentFundSource> fundSources = equipmentFundSourceRepository
        .findAllByCodeContainingIgnoreCase("F", pageable);

    assertThat(fundSources).allMatch(fundSource -> fundSource.getCode().contains("F"));
  }

  @Test
  public void shouldReturnAllEquipmentFundSourceWhenNoParameterIsProvided() {

    List<EquipmentFundSource> expectedFundSources = Lists.newArrayList(
        createEquipmentFundSource(),
        createEquipmentFundSource(),
        createEquipmentFundSource(),
        createEquipmentFundSource()
    );

    Page<EquipmentFundSource> foundFundSources = equipmentFundSourceRepository.findAll(pageable);

    assertThat(expectedFundSources)
        .allMatch(fundSource -> foundFundSources.getContent().contains(fundSource));
  }

  private EquipmentFundSource createEquipmentFundSource() {
    return createEquipmentFundSource(null, null);
  }

  private EquipmentFundSource createEquipmentFundSource(String name, String code) {
    EquipmentFundSourceDataBuilder fundSourceDataBuilder = new EquipmentFundSourceDataBuilder();

    if (isNotBlank(name)) {
      fundSourceDataBuilder = fundSourceDataBuilder.withName(name);
    }
    if (isNotBlank(code)) {
      fundSourceDataBuilder = fundSourceDataBuilder.withCode(code);
    }

    EquipmentFundSourceDto fundSourceDto = EquipmentFundSourceDto.newInstance(
        fundSourceDataBuilder.buildAsNew()
    );
    EquipmentFundSource createdFundSource = equipmentFundSourceService.create(fundSourceDto);
    return createdFundSource;
  }

  @Override
  CrudRepository<EquipmentFundSource, UUID> getRepository() {
    return this.equipmentFundSourceRepository;
  }

  @Override
  EquipmentFundSource generateInstance() {
    return new EquipmentFundSourceDataBuilder().buildAsNew();
  }
}
