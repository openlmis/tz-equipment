/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.repository;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.assertj.core.api.Assertions.assertThat;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openlmis.equipment.domain.EquipmentEnergyType;
import org.openlmis.equipment.dto.EquipmentEnergyTypeDto;
import org.openlmis.equipment.service.EquipmentEnergyTypeService;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.EquipmentEnergyTypeDataBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Transactional
@SuppressWarnings({"PMD.UnusedPrivateField"})
public class EquipmentEnergyTypeRepositoryIntegrationTest
    extends BaseCrudRepositoryIntegrationTest<EquipmentEnergyType> {

  private static final String TEST_NAME = "foo";
  private static final String TEST_CODE = "bar";

  @MockBean
  private PermissionService permissionService;
  @Autowired
  private EquipmentEnergyTypeService equipmentEnergyTypeService;
  @Autowired
  private EquipmentEnergyTypeRepository equipmentEnergyTypeRepository;

  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    equipmentEnergyTypeRepository.deleteAll();
  }

  @Test
  public void shouldSearchEquipmentEnergyTypeByName() {
    createEquipmentEnergyType();
    createEquipmentEnergyType();
    createEquipmentEnergyType(TEST_NAME, TEST_CODE);

    Page<EquipmentEnergyType> energyTypes = equipmentEnergyTypeRepository
        .findAllByNameContainingIgnoreCase("f", pageable);

    assertThat(energyTypes).allMatch(energyType -> energyType.getName().contains("f"));
  }

  @Test
  public void shouldSearchEquipmentEnergyTypeByNameIgnoringCase() {
    createEquipmentEnergyType();
    createEquipmentEnergyType();
    createEquipmentEnergyType(TEST_NAME, TEST_CODE);

    Page<EquipmentEnergyType> energyTypes = equipmentEnergyTypeRepository
        .findAllByNameContainingIgnoreCase("F", pageable);

    assertThat(energyTypes).allMatch(energyType -> containsIgnoreCase(energyType.getName(), "F"));
  }

  @Test
  public void shouldSearchEquipmentEnergyTypeByCode() {
    createEquipmentEnergyType();
    createEquipmentEnergyType();
    createEquipmentEnergyType(TEST_NAME, TEST_CODE);

    Page<EquipmentEnergyType> energyTypes = equipmentEnergyTypeRepository
        .findAllByCodeContainingIgnoreCase("f", pageable);

    assertThat(energyTypes).allMatch(energyType -> energyType.getCode().contains("f"));
  }

  @Test
  public void shouldSearchEquipmentEnergyTypeByCodeIgnoringCase() {
    createEquipmentEnergyType();
    createEquipmentEnergyType();
    createEquipmentEnergyType(TEST_NAME, TEST_CODE);

    Page<EquipmentEnergyType> energyTypes = equipmentEnergyTypeRepository
        .findAllByCodeContainingIgnoreCase("F", pageable);

    assertThat(energyTypes).allMatch(energyType -> energyType.getCode().contains("F"));
  }

  @Test
  public void shouldReturnAllEquipmentEnergyTypeWhenNoParameterIsProvided() {

    List<EquipmentEnergyType> expectedEnergyTypes = Lists.newArrayList(
        createEquipmentEnergyType(),
        createEquipmentEnergyType(),
        createEquipmentEnergyType(),
        createEquipmentEnergyType()
    );

    Page<EquipmentEnergyType> foundEnergyTypes = equipmentEnergyTypeRepository.findAll(pageable);

    assertThat(expectedEnergyTypes)
        .allMatch(energyType -> foundEnergyTypes.getContent().contains(energyType));
  }

  private EquipmentEnergyType createEquipmentEnergyType() {
    return createEquipmentEnergyType(null, null);
  }

  private EquipmentEnergyType createEquipmentEnergyType(String name, String code) {
    EquipmentEnergyTypeDataBuilder energyTypeDataBuilder = new EquipmentEnergyTypeDataBuilder();

    if (isNotBlank(name)) {
      energyTypeDataBuilder = energyTypeDataBuilder.withName(name);
    }
    if (isNotBlank(code)) {
      energyTypeDataBuilder = energyTypeDataBuilder.withCode(code);
    }

    EquipmentEnergyTypeDto energyTypeDto = EquipmentEnergyTypeDto.newInstance(
        energyTypeDataBuilder.buildAsNew()
    );
    EquipmentEnergyType createdEnergyType = equipmentEnergyTypeService.create(energyTypeDto);
    return createdEnergyType;
  }

  @Override
  CrudRepository<EquipmentEnergyType, UUID> getRepository() {
    return this.equipmentEnergyTypeRepository;
  }

  @Override
  EquipmentEnergyType generateInstance() {
    return new EquipmentEnergyTypeDataBuilder().buildAsNew();
  }
}
