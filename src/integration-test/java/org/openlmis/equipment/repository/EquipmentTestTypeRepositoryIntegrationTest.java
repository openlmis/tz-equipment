/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.google.common.collect.Sets;
import java.util.List;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import org.openlmis.equipment.domain.Discipline;
import org.openlmis.equipment.domain.EquipmentCategory;
import org.openlmis.equipment.domain.EquipmentTestType;
import org.openlmis.equipment.domain.EquipmentType;
import org.openlmis.equipment.util.DisciplineDataBuilder;
import org.openlmis.equipment.util.EquipmentCategoryDataBuilder;
import org.openlmis.equipment.util.EquipmentTestTypeDataBuilder;
import org.openlmis.equipment.util.EquipmentTypeDataBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@SuppressWarnings("PMD.TooManyMethods")
public class EquipmentTestTypeRepositoryIntegrationTest extends
        BaseCrudRepositoryIntegrationTest<EquipmentTestType> {

  @Autowired
  private EquipmentTestTypeRepository repository;

  @Autowired
  private EquipmentCategoryRepository equipmentCategoryRepository;

  @Autowired
  private EquipmentTypeRepository equipmentTypeRepository;

  @Autowired
  private DisciplineRepository disciplineRepository;

  private Pageable pageable;

  Discipline discipline = new DisciplineDataBuilder().build();

  EquipmentType equipmentType = new EquipmentTypeDataBuilder().build();

  EquipmentCategory equipmentCategory;

  private EquipmentTestType equipmentTestType;

  private static final String EQUIPMENT_TEST_TYPE_NAME = "Name";
  private static final String EQUIPMENT_TEST_TYPE_CODE = "Code";

  @Override
  EquipmentTestTypeRepository getRepository() {
    return this.repository;
  }

  @Before
  public void setUp() {
    equipmentTypeRepository.save(equipmentType);
    disciplineRepository.save(discipline);
  }

  @Override
  EquipmentTestType generateInstance() {
    pageable = PageRequest.of(0, 10);

    equipmentCategory = new EquipmentCategoryDataBuilder()
        .withEquipmentType(equipmentType)
        .withDisciplines(Sets.newHashSet(discipline))
                        .build();

    equipmentCategoryRepository.save(equipmentCategory);

    return new EquipmentTestTypeDataBuilder()
            .withEnabled(true)
            .withCode("code")
            .withName("name")
            .withDisplayOrder(1)
            .withEquipmentCategory(equipmentCategory)
            .buildAsNew();
  }

  private EquipmentTestType generateEquipmentTestType() {
    equipmentCategory = new EquipmentCategoryDataBuilder()
        .withEquipmentType(equipmentType)
        .withDisciplines(Sets.newHashSet(discipline))
            .build();

    equipmentCategoryRepository.save(equipmentCategory);

    return new EquipmentTestTypeDataBuilder()
            .withName(EQUIPMENT_TEST_TYPE_NAME)
            .withCode(EQUIPMENT_TEST_TYPE_CODE)
            .withEnabled(true)
            .withDisplayOrder(1)
            .withEquipmentCategory(equipmentCategory)
            .buildAsNew();
  }


  @Test
  public void shouldFindByName() {
    equipmentTestType = generateEquipmentTestType();
    repository.save(equipmentTestType);

    List<EquipmentTestType> equipmentTestTypes = repository.findByName(
            EQUIPMENT_TEST_TYPE_NAME);

    assertEquals(1, equipmentTestTypes.size());
    assertTrue(equipmentTestTypes.stream().allMatch(result -> equipmentTestTypes
            .contains(equipmentTestType)));
  }

  @Test
  public void shouldFindByCode() {

    equipmentTestType = generateEquipmentTestType();
    repository.save(equipmentTestType);

    List<EquipmentTestType> equipmentTestTypes = repository.findByCode(EQUIPMENT_TEST_TYPE_CODE);
    assertEquals(1, equipmentTestTypes.size());
    assertTrue(equipmentTestTypes.stream().allMatch(result -> equipmentTestTypes
            .contains(equipmentTestType)));
  }


  @Test
  public void shouldFindEnabled() {

    equipmentTestType = generateEquipmentTestType();
    repository.save(equipmentTestType);

    Set<EquipmentTestType> equipmentTestTypes = repository.findByEnabled(true);
    assertEquals(1, equipmentTestTypes.size());
    assertTrue(equipmentTestTypes.stream().allMatch(result -> equipmentTestTypes
            .contains(equipmentTestType)));
  }


  @Test
  public void shouldReturnAll() {
    equipmentTestType = generateEquipmentTestType();
    repository.save(equipmentTestType);

    Page equipmentTestTypes = repository.findAllWithoutSnapshots(pageable);

    assertEquals(1, equipmentTestTypes.getContent().size());
    assertEquals(1, equipmentTestTypes.getContent().size());
  }

}
