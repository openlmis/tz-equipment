/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.google.common.collect.Sets;
import java.util.List;
import java.util.Set;
import org.junit.Test;
import org.openlmis.equipment.domain.Discipline;
import org.openlmis.equipment.domain.EquipmentCategory;
import org.openlmis.equipment.domain.EquipmentType;
import org.openlmis.equipment.util.DisciplineDataBuilder;
import org.openlmis.equipment.util.EquipmentCategoryDataBuilder;
import org.openlmis.equipment.util.EquipmentTypeDataBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class EquipmentCategoryRepositoryIntegrationTest extends
        BaseCrudRepositoryIntegrationTest<EquipmentCategory> {

  @Autowired
  private EquipmentCategoryRepository repository;

  @Autowired
  private EquipmentTypeRepository equipmentTypeRepository;

  @Autowired
  private DisciplineRepository disciplineRepository;

  private Pageable pageable;

  Discipline discipline = new DisciplineDataBuilder().build();

  EquipmentType equipmentType = new EquipmentTypeDataBuilder().build();

  private EquipmentCategory equipmentCategory;

  private static final String EQUIPMENT_CATEGORY_NAME = "Name";
  private static final String EQUIPMENT_CATEGORY_CODE = "Code";

  @Override
  EquipmentCategoryRepository getRepository() {
    return this.repository;
  }

  @Override
  EquipmentCategory generateInstance() {
    pageable = PageRequest.of(0, 10);

    equipmentTypeRepository.save(equipmentType);
    disciplineRepository.save(discipline);

    return new EquipmentCategoryDataBuilder()
        .withEnabled(true)
        .withCode("code")
        .withName("name")
        .withDisplayOrder(1)
        .withDisciplines(Sets.newHashSet(discipline))
            .withEquipmentType(equipmentType)
            .buildAsNew();
  }

  private EquipmentCategory generateEquipmentCategory() {
    equipmentTypeRepository.save(equipmentType);
    disciplineRepository.save(discipline);

    return new EquipmentCategoryDataBuilder()
        .withName(EQUIPMENT_CATEGORY_NAME)
        .withCode(EQUIPMENT_CATEGORY_CODE)
        .withEnabled(true)
        .withDisplayOrder(1)
        .withEquipmentType(equipmentType)
        .withDisciplines(Sets.newHashSet(discipline))
            .buildAsNew();
  }


  @Test
  public void shouldFindByName() {
    equipmentCategory = generateEquipmentCategory();
    repository.save(equipmentCategory);

    List<EquipmentCategory> equipmentCategories = repository.findByName(
            EQUIPMENT_CATEGORY_NAME);

    assertEquals(1, equipmentCategories.size());
    assertTrue(equipmentCategories.stream().allMatch(result -> equipmentCategories
            .contains(equipmentCategory)));
  }

  @Test
  public void shouldFindByCode() {

    equipmentCategory = generateEquipmentCategory();
    repository.save(equipmentCategory);

    List<EquipmentCategory> equipmentCategories = repository.findByCode(EQUIPMENT_CATEGORY_CODE);
    assertEquals(1, equipmentCategories.size());
    assertTrue(equipmentCategories.stream().allMatch(result -> equipmentCategories
            .contains(equipmentCategory)));
  }



  @Test
  public void shouldFindEnabled() {

    equipmentCategory = generateEquipmentCategory();
    repository.save(equipmentCategory);

    Set<EquipmentCategory> equipmentCategories = repository.findByEnabled(true);
    assertEquals(1, equipmentCategories.size());
    assertTrue(equipmentCategories.stream().allMatch(result -> equipmentCategories
            .contains(equipmentCategory)));
  }


  @Test
  public void shouldReturnAll() {
    equipmentCategory = generateEquipmentCategory();
    repository.save(equipmentCategory);

    Page equipmentCategories = repository.findAllWithoutSnapshots(pageable);

    assertEquals(1, equipmentCategories.getContent().size());
    assertEquals(1, equipmentCategories.getContent().size());
  }

}
