/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.repository;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.openlmis.equipment.domain.EquipmentModel;
import org.openlmis.equipment.domain.EquipmentType;
import org.openlmis.equipment.dto.EquipmentModelDto;
import org.openlmis.equipment.service.EquipmentModelService;
import org.openlmis.equipment.service.EquipmentModelService.SearchParams;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.EquipmentModelDataBuilder;
import org.openlmis.equipment.util.EquipmentTypeDataBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Transactional
@SuppressWarnings({"PMD.UnusedPrivateField"})
public class EquipmentModelRepositoryIntegrationTest
    extends BaseCrudRepositoryIntegrationTest<EquipmentModel> {

  private static final String TEST_NAME = "foo";
  private static final String TEST_CODE = "bar";
  @MockBean
  private PermissionService permissionService;
  @Autowired
  private EquipmentModelService equipmentModelService;
  @Autowired
  private EquipmentModelRepository equipmentModelRepository;
  @Autowired
  private EquipmentTypeRepository equipmentTypeRepository;

  private EquipmentType equipmentType;
  private EquipmentModelDataBuilder modelDataBuilder;
  private EquipmentTypeDataBuilder typeDataBuilder;
  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    equipmentModelRepository.deleteAll();
    equipmentTypeRepository.deleteAll();

    typeDataBuilder = new EquipmentTypeDataBuilder();
    equipmentType = equipmentTypeRepository.save(typeDataBuilder.buildAsNew());
    modelDataBuilder = new EquipmentModelDataBuilder().withEquipmentType(equipmentType);
  }

  @Test
  public void shouldSearchEquipmentModelByName() {
    createEquipmentModel(TEST_NAME, TEST_NAME, equipmentType);
    createEquipmentModel(TEST_NAME, TEST_CODE, equipmentType);

    SearchParams searchParams = Mockito.mock(SearchParams.class);
    when(searchParams.getName()).thenReturn("f");

    Page<EquipmentModel> models = equipmentModelRepository.search(searchParams, pageable);

    assertThat(models).allMatch(model -> model.getName().contains("f"));
  }

  @Test
  public void shouldSearchEquipmentModelByNameIgnoringCase() {
    createEquipmentModel(TEST_NAME, TEST_NAME, equipmentType);
    createEquipmentModel(TEST_NAME, TEST_CODE, equipmentType);

    SearchParams searchParams = Mockito.mock(SearchParams.class);
    when(searchParams.getName()).thenReturn("F");

    Page<EquipmentModel> models = equipmentModelRepository.search(searchParams, pageable);

    assertThat(models).allMatch(model -> containsIgnoreCase(model.getName(), "F"));
  }

  @Test
  public void shouldSearchEquipmentModelByCode() {
    createEquipmentModel(TEST_NAME, TEST_NAME, equipmentType);
    createEquipmentModel(TEST_CODE, TEST_CODE, equipmentType);

    SearchParams searchParams = Mockito.mock(SearchParams.class);
    when(searchParams.getCode()).thenReturn("f");

    Page<EquipmentModel> models = equipmentModelRepository.search(searchParams, pageable);

    assertThat(models).allMatch(model -> model.getCode().contains("f"));
  }

  @Test
  public void shouldSearchEquipmentModelByCodeIgnoringCase() {
    createEquipmentModel(TEST_NAME, TEST_NAME, equipmentType);
    createEquipmentModel(TEST_CODE, TEST_CODE, equipmentType);

    SearchParams searchParams = Mockito.mock(SearchParams.class);
    when(searchParams.getCode()).thenReturn("F");

    Page<EquipmentModel> models = equipmentModelRepository.search(searchParams, pageable);

    assertThat(models).allMatch(model -> containsIgnoreCase(model.getCode(), "F"));
  }

  @Test
  public void shouldSearchEquipmentModelByEquipmentType() {
    EquipmentType anotherType = equipmentTypeRepository.save(typeDataBuilder.buildAsNew());

    createEquipmentModel(equipmentType);
    createEquipmentModel(equipmentType);

    createEquipmentModel(anotherType);
    createEquipmentModel(anotherType);

    SearchParams searchParams = Mockito.mock(SearchParams.class);
    when(searchParams.getEquipmentTypeIds())
        .thenReturn(Sets.newHashSet(anotherType.getId()));

    Page<EquipmentModel> models = equipmentModelRepository.search(searchParams, pageable);

    assertThat(models).allMatch(model -> model.getEquipmentType().equals(anotherType));
  }

  @Test
  public void shouldSearchEquipmentModelByIds() {
    EquipmentModel model1 = createEquipmentModel(equipmentType);
    EquipmentModel model2 = createEquipmentModel(equipmentType);
    EquipmentModel model3 = createEquipmentModel(equipmentType);

    Set<UUID> searchIds = Sets.newHashSet(model1.getId(), model2.getId());
    SearchParams searchParams = Mockito.mock(SearchParams.class);
    when(searchParams.getIds()).thenReturn(searchIds);

    Page<EquipmentModel> models = equipmentModelRepository.search(searchParams, pageable);

    assertThat(models).allMatch(model -> searchIds.contains(model.getId()));
    assertThat(models).allMatch(model -> !model.getId().equals(model3.getId()));
  }

  @Test
  public void shouldReturnAllEquipmentModelWhenNoParameterIsProvided() {

    List<EquipmentModel> expectedModels = Lists.newArrayList(
        createEquipmentModel(equipmentType),
        createEquipmentModel(equipmentType),
        createEquipmentModel(equipmentType),
        createEquipmentModel(equipmentType)
    );

    SearchParams searchParams = Mockito.mock(SearchParams.class);
    Page<EquipmentModel> foundModels = equipmentModelRepository.search(searchParams, pageable);

    assertThat(expectedModels).allMatch(model -> foundModels.getContent().contains(model));
  }

  private EquipmentModel createEquipmentModel(EquipmentType type) {
    return createEquipmentModel(null, null, type);
  }

  private EquipmentModel createEquipmentModel(String name, String code, EquipmentType type) {
    if (isNotBlank(name)) {
      modelDataBuilder = modelDataBuilder.withName(name);
    }
    if (isNotBlank(code)) {
      modelDataBuilder = modelDataBuilder.withCode(code);
    }
    if (type != null) {
      modelDataBuilder = modelDataBuilder.withEquipmentType(type);
    }

    EquipmentModelDto modelDto = EquipmentModelDto.newInstance(
        modelDataBuilder.buildAsNew()
    );
    EquipmentModel createdModel = equipmentModelService.create(modelDto);
    return createdModel;
  }

  @Override
  CrudRepository<EquipmentModel, UUID> getRepository() {
    return this.equipmentModelRepository;
  }

  @Override
  EquipmentModel generateInstance() {
    return modelDataBuilder.buildAsNew();
  }
}
