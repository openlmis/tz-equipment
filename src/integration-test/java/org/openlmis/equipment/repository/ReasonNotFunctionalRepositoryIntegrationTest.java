/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.repository;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.openlmis.equipment.domain.ReasonNotFunctional;
import org.openlmis.equipment.dto.ReasonNotFunctionalDto;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.service.ReasonNotFunctionalService;
import org.openlmis.equipment.service.ReasonNotFunctionalService.SearchParams;
import org.openlmis.equipment.util.ReasonNotFunctionalDataBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Transactional
@SuppressWarnings({"PMD.UnusedPrivateField"})
public class ReasonNotFunctionalRepositoryIntegrationTest
    extends BaseCrudRepositoryIntegrationTest<ReasonNotFunctional> {

  private static final String TEST_NAME = "foo";
  @MockBean
  private PermissionService permissionService;
  @Autowired
  private ReasonNotFunctionalService reasonNotFunctionalService;
  @Autowired
  private ReasonNotFunctionalRepository reasonNotFunctionalRepository;

  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    reasonNotFunctionalRepository.deleteAll();
  }

  @Test
  public void shouldSearchReasonNotFunctionalByName() {
    createReasonNotFunctional(TEST_NAME);

    SearchParams searchParams = Mockito.mock(SearchParams.class);
    when(searchParams.getName()).thenReturn("f");

    Page<ReasonNotFunctional> reasons = reasonNotFunctionalRepository.search(
        searchParams,
        pageable
    );

    assertThat(reasons).allMatch(reason -> reason.getName().contains("f"));
  }

  @Test
  public void shouldSearchReasonNotFunctionalByNameIgnoringCase() {
    createReasonNotFunctional(TEST_NAME);

    SearchParams searchParams = Mockito.mock(SearchParams.class);
    when(searchParams.getName()).thenReturn("F");

    Page<ReasonNotFunctional> reasons = reasonNotFunctionalRepository.search(
        searchParams,
        pageable
    );

    assertThat(reasons).allMatch(reason -> containsIgnoreCase(reason.getName(), "F"));
  }

  @Test
  public void shouldSearchReasonNotFunctionalByIds() {
    ReasonNotFunctional reason1 = createReasonNotFunctional();
    ReasonNotFunctional reason2 = createReasonNotFunctional();
    ReasonNotFunctional reason3 = createReasonNotFunctional();

    Set<UUID> searchIds = Sets.newHashSet(reason1.getId(), reason2.getId());
    SearchParams searchParams = Mockito.mock(SearchParams.class);
    when(searchParams.getIds()).thenReturn(searchIds);

    Page<ReasonNotFunctional> reasons = reasonNotFunctionalRepository.search(
        searchParams,
        pageable
    );

    assertThat(reasons).allMatch(reason -> searchIds.contains(reason.getId()));
    assertThat(reasons).allMatch(reason -> !reason.getId().equals(reason3.getId()));
  }

  @Test
  public void shouldReturnAllReasonNotFunctionalWhenNoParameterIsProvided() {

    List<ReasonNotFunctional> expectedReasons = Lists.newArrayList(
        createReasonNotFunctional(),
        createReasonNotFunctional(),
        createReasonNotFunctional(),
        createReasonNotFunctional()
    );

    SearchParams searchParams = Mockito.mock(SearchParams.class);
    Page<ReasonNotFunctional> foundReasons = reasonNotFunctionalRepository.search(
        searchParams,
        pageable
    );

    assertThat(expectedReasons).allMatch(reason -> foundReasons.getContent().contains(reason));
  }

  private ReasonNotFunctional createReasonNotFunctional() {
    return createReasonNotFunctional("");
  }

  private ReasonNotFunctional createReasonNotFunctional(String name) {
    ReasonNotFunctionalDataBuilder reasonDataBuilder = new ReasonNotFunctionalDataBuilder();
    if (isNotBlank(name)) {
      reasonDataBuilder = reasonDataBuilder.withName(name);
    }

    ReasonNotFunctionalDto reasonDto = ReasonNotFunctionalDto.newInstance(
        reasonDataBuilder.buildAsNew()
    );
    ReasonNotFunctional createdReason = reasonNotFunctionalService.create(reasonDto);
    return createdReason;
  }

  @Override
  CrudRepository<ReasonNotFunctional, UUID> getRepository() {
    return this.reasonNotFunctionalRepository;
  }

  @Override
  ReasonNotFunctional generateInstance() {
    return new ReasonNotFunctionalDataBuilder().buildAsNew();
  }
}
