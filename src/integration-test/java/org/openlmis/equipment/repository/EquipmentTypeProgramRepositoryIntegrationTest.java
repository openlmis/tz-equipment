/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.UUID;
import org.junit.Test;

import org.openlmis.equipment.domain.EquipmentType;
import org.openlmis.equipment.domain.EquipmentTypeProgram;
import org.openlmis.equipment.util.EquipmentTypeDataBuilder;
import org.openlmis.equipment.util.EquipmentTypeProgramDataBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class EquipmentTypeProgramRepositoryIntegrationTest extends
        BaseCrudRepositoryIntegrationTest<EquipmentTypeProgram> {

  @Autowired
  private EquipmentTypeProgramRepository repository;

  @Autowired
  private EquipmentTypeRepository equipmentTypeRepository;

  private EquipmentTypeProgram equipmentTypeProgram;


  private Pageable pageable;

  private UUID programId = UUID.fromString("466b2e7f-5798-4027-a2ca-373627748ea6");

  EquipmentType equipmentType = new EquipmentTypeDataBuilder().build();

  @Override
  EquipmentTypeProgramRepository getRepository() {
    return this.repository;
  }

  @Override
  EquipmentTypeProgram generateInstance() {
    pageable = PageRequest.of(0, 10);

    equipmentTypeRepository.save(equipmentType);

    return new EquipmentTypeProgramDataBuilder()
            .withEquipmentType(equipmentType)
            .withProgramId(programId)
            .buildAsNew();
  }

  private EquipmentTypeProgram generateEquipmentTypeProgram() {
    equipmentTypeRepository.save(equipmentType);

    return new EquipmentTypeProgramDataBuilder()
            .withEquipmentType(equipmentType)
            .withProgramId(programId)
            .buildAsNew();
  }

  @Test
  public void shouldFindByProgramId() {
    equipmentTypeProgram = generateEquipmentTypeProgram();
    repository.save(equipmentTypeProgram);

    List<EquipmentTypeProgram> equipmentTypesProgram = repository.findByProgramId(
            programId);

    assertEquals(1, equipmentTypesProgram.size());
    assertTrue(equipmentTypesProgram.stream().allMatch(result -> equipmentTypesProgram
            .contains(equipmentTypeProgram)));
  }

  @Test
  public void shouldFindByEquipmentType() {
    equipmentTypeProgram = generateEquipmentTypeProgram();
    repository.save(equipmentTypeProgram);

    List<EquipmentTypeProgram> equipmentTypesProgram = repository.findByEquipmentType(
            equipmentType);

    assertEquals(1, equipmentTypesProgram.size());
    assertTrue(equipmentTypesProgram.stream().allMatch(result -> equipmentTypesProgram
            .contains(equipmentTypeProgram)));
  }

  @Test
  public void shouldReturnAll() {
    equipmentTypeProgram = generateEquipmentTypeProgram();
    repository.save(equipmentTypeProgram);

    Page equipmentTypes = repository.findAllWithoutSnapshots(pageable);

    assertEquals(1, equipmentTypes.getContent().size());
    assertEquals(1, equipmentTypes.getContent().size());
  }

}
