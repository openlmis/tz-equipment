/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.repository;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.openlmis.equipment.domain.Discipline;
import org.openlmis.equipment.domain.Equipment;
import org.openlmis.equipment.domain.EquipmentCategory;
import org.openlmis.equipment.domain.EquipmentEnergyType;
import org.openlmis.equipment.domain.EquipmentModel;
import org.openlmis.equipment.domain.EquipmentType;
import org.openlmis.equipment.dto.EquipmentDto;
import org.openlmis.equipment.service.EquipmentService;
import org.openlmis.equipment.service.EquipmentService.SearchParams;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.DisciplineDataBuilder;
import org.openlmis.equipment.util.EquipmentCategoryDataBuilder;
import org.openlmis.equipment.util.EquipmentDataBuilder;
import org.openlmis.equipment.util.EquipmentEnergyTypeDataBuilder;
import org.openlmis.equipment.util.EquipmentModelDataBuilder;
import org.openlmis.equipment.util.EquipmentTypeDataBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Transactional
@SuppressWarnings({"PMD.TooManyMethods", "PMD.UnusedPrivateField"})
public class EquipmentRepositoryIntegrationTest
    extends BaseCrudRepositoryIntegrationTest<Equipment> {

  private static final String TEST_NAME = "foo";
  private static final String TEST_MANUFACTURER = "bar";
  @MockBean
  private PermissionService permissionService;
  @Autowired
  private EquipmentService equipmentService;
  @Autowired
  private EquipmentTypeRepository equipmentTypeRepository;
  @Autowired
  private EquipmentRepository equipmentRepository;
  @Autowired
  private EquipmentEnergyTypeRepository equipmentEnergyTypeRepository;
  @Autowired
  private EquipmentModelRepository equipmentModelRepository;
  @Autowired
  private EquipmentCategoryRepository equipmentCategoryRepository;
  @Autowired
  private DisciplineRepository disciplineRepository;

  private EquipmentType equipmentType;
  private EquipmentTypeDataBuilder equipmentTypeDataBuilder;

  private EquipmentEnergyType equipmentEnergyType;
  private EquipmentEnergyTypeDataBuilder equipmentEnergyTypeDataBuilder;

  private EquipmentModel equipmentModel;
  private EquipmentModelDataBuilder equipmentModelDataBuilder;

  private EquipmentCategory equipmentCategory;
  private EquipmentCategoryDataBuilder equipmentCategoryDataBuilder;

  private EquipmentDataBuilder equipmentDataBuilder;
  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    equipmentRepository.deleteAll();
    equipmentEnergyTypeRepository.deleteAll();
    equipmentModelRepository.deleteAll();
    equipmentCategoryRepository.deleteAll();
    disciplineRepository.deleteAll();
    equipmentTypeRepository.deleteAll();

    equipmentTypeDataBuilder = new EquipmentTypeDataBuilder();
    equipmentType = equipmentTypeRepository.save(equipmentTypeDataBuilder.buildAsNew());

    equipmentModelDataBuilder = new EquipmentModelDataBuilder();
    equipmentModel = equipmentModelRepository.save(
        equipmentModelDataBuilder
            .withEquipmentType(equipmentType)
            .build()
    );

    equipmentEnergyTypeDataBuilder = new EquipmentEnergyTypeDataBuilder();
    equipmentEnergyType = equipmentEnergyTypeRepository.save(
        equipmentEnergyTypeDataBuilder.buildAsNew()
    );

    Discipline discipline = disciplineRepository.save(
        new DisciplineDataBuilder().build()
    );

    equipmentCategoryDataBuilder = new EquipmentCategoryDataBuilder();
    equipmentCategory = equipmentCategoryRepository.save(
        equipmentCategoryDataBuilder
            .withEquipmentType(equipmentType)
            .withDisciplines(Sets.newHashSet(discipline))
            .build()
    );

    equipmentDataBuilder = new EquipmentDataBuilder()
        .withEquipmentCategory(equipmentCategory)
        .withEquipmentEnergyType(equipmentEnergyType)
        .withEquipmentModel(equipmentModel);
  }

  @Test
  public void shouldSearchEquipmentByName() {
    createEquipment(TEST_NAME, TEST_MANUFACTURER);
    createEquipment(TEST_NAME, TEST_MANUFACTURER);

    SearchParams searchParams = Mockito.mock(SearchParams.class);
    when(searchParams.getName()).thenReturn("f");

    Page<Equipment> equipments = equipmentRepository.search(searchParams, pageable);

    assertThat(equipments).allMatch(equipment -> equipment.getName().contains("f"));
  }

  @Test
  public void shouldSearchEquipmentByNameIgnoringCase() {
    createEquipment(TEST_NAME, TEST_MANUFACTURER);
    createEquipment(TEST_NAME, TEST_MANUFACTURER);

    SearchParams searchParams = Mockito.mock(SearchParams.class);
    when(searchParams.getName()).thenReturn("F");

    Page<Equipment> equipments = equipmentRepository.search(searchParams, pageable);

    assertThat(equipments).allMatch(equipment -> containsIgnoreCase(equipment.getName(), "F"));
  }

  @Test
  public void shouldSearchEquipmentByManufacturer() {
    createEquipment(TEST_NAME, TEST_MANUFACTURER);
    createEquipment(TEST_NAME, TEST_MANUFACTURER);

    SearchParams searchParams = Mockito.mock(SearchParams.class);
    when(searchParams.getManufacturer()).thenReturn("b");

    Page<Equipment> equipments = equipmentRepository.search(searchParams, pageable);

    assertThat(equipments).allMatch(equipment -> equipment.getManufacturer().contains("b"));
  }

  @Test
  public void shouldSearchEquipmentByManufacturerIgnoringCase() {
    createEquipment(TEST_NAME, TEST_MANUFACTURER);
    createEquipment(TEST_NAME, TEST_MANUFACTURER);

    SearchParams searchParams = Mockito.mock(SearchParams.class);
    when(searchParams.getManufacturer()).thenReturn("B");

    Page<Equipment> equipments = equipmentRepository.search(searchParams, pageable);

    assertThat(equipments).allMatch(
        equipment -> containsIgnoreCase(equipment.getManufacturer(), "B")
    );
  }

  @Test
  public void shouldSearchEquipmentByEquipmentEnergyType() {
    EquipmentEnergyType anotherEnergyType = equipmentEnergyTypeRepository.save(
        equipmentEnergyTypeDataBuilder.buildAsNew()
    );

    createEquipment();
    createEquipment();

    createEquipment(null, null, anotherEnergyType, null, null);
    createEquipment(null, null, anotherEnergyType, null, null);


    SearchParams searchParams = Mockito.mock(SearchParams.class);
    when(searchParams.getEquipmentEnergyTypeIds())
        .thenReturn(Sets.newHashSet(anotherEnergyType.getId()));

    Page<Equipment> equipments = equipmentRepository.search(searchParams, pageable);

    assertThat(equipments).allMatch(
        equipment -> equipment.getEnergyType().equals(anotherEnergyType)
    );
  }

  @Test
  public void shouldSearchEquipmentByEquipmentModel() {
    EquipmentModel anotherModel = equipmentModelRepository.save(
        equipmentModelDataBuilder
            .withEquipmentType(equipmentType)
            .build()
    );

    createEquipment();
    createEquipment();

    createEquipment(null, null, null, anotherModel, null);
    createEquipment(null, null, null, anotherModel, null);


    SearchParams searchParams = Mockito.mock(SearchParams.class);
    when(searchParams.getEquipmentModelIds())
        .thenReturn(Sets.newHashSet(anotherModel.getId()));

    Page<Equipment> equipments = equipmentRepository.search(searchParams, pageable);

    assertThat(equipments).allMatch(equipment -> equipment.getModel().equals(anotherModel));
  }


  @Test
  public void shouldSearchEquipmentByEquipmentCategory() {
    EquipmentCategory anotherCategory = equipmentCategoryRepository.save(
        equipmentCategoryDataBuilder.withEquipmentType(equipmentType).build()
    );

    createEquipment();
    createEquipment();

    createEquipment(null, null, null, null, anotherCategory);
    createEquipment(null, null, null, null, anotherCategory);


    SearchParams searchParams = Mockito.mock(SearchParams.class);
    when(searchParams.getEquipmentCategoryIds())
        .thenReturn(Sets.newHashSet(anotherCategory.getId()));

    Page<Equipment> equipments = equipmentRepository.search(searchParams, pageable);

    assertThat(equipments).allMatch(equipment -> equipment.getCategory().equals(anotherCategory));
  }

  @Test
  public void shouldSearchEquipmentByIds() {
    Equipment equipment1 = createEquipment();
    Equipment equipment2 = createEquipment();
    Equipment equipment3 = createEquipment();

    Set<UUID> searchIds = Sets.newHashSet(equipment1.getId(), equipment2.getId());
    SearchParams searchParams = Mockito.mock(SearchParams.class);
    when(searchParams.getIds()).thenReturn(searchIds);

    Page<Equipment> equipments = equipmentRepository.search(searchParams, pageable);

    assertThat(equipments).allMatch(equipment -> searchIds.contains(equipment.getId()));
    assertThat(equipments).allMatch(equipment -> !equipment.getId().equals(equipment3.getId()));
  }

  @Test
  public void shouldReturnAllEquipmentWhenNoParameterIsProvided() {

    List<Equipment> expectedEquipments = Lists.newArrayList(
        createEquipment(),
        createEquipment(),
        createEquipment(),
        createEquipment()
    );

    SearchParams searchParams = Mockito.mock(SearchParams.class);
    Page<Equipment> foundEquipments = equipmentRepository.search(searchParams, pageable);

    assertThat(expectedEquipments).allMatch(
        equipment -> foundEquipments.getContent().contains(equipment)
    );
  }

  private Equipment createEquipment() {
    return createEquipment(null, null, null, null, null);
  }

  private Equipment createEquipment(String name, String manufacturer) {
    return createEquipment(name, manufacturer, null, null, null);
  }

  private Equipment createEquipment(String name, String manufacturer,
                                    EquipmentEnergyType equipmentEnergyType,
                                    EquipmentModel equipmentModel,
                                    EquipmentCategory equipmentCategory) {
    if (isNotBlank(name)) {
      equipmentDataBuilder = equipmentDataBuilder.withName(name);
    }
    if (isNotBlank(manufacturer)) {
      equipmentDataBuilder = equipmentDataBuilder.withManufacturer(manufacturer);
    }
    if (equipmentEnergyType != null) {
      equipmentDataBuilder = equipmentDataBuilder.withEquipmentEnergyType(equipmentEnergyType);
    }
    if (equipmentModel != null) {
      equipmentDataBuilder = equipmentDataBuilder.withEquipmentModel(equipmentModel);
    }
    if (equipmentCategory != null) {
      equipmentDataBuilder = equipmentDataBuilder.withEquipmentCategory(equipmentCategory);
    }
    EquipmentDto equipmentDto = EquipmentDto.newInstance(
        equipmentDataBuilder.withoutId().build()
    );
    Equipment createdEquipment = equipmentService.create(equipmentDto);
    return createdEquipment;
  }

  @Override
  CrudRepository<Equipment, UUID> getRepository() {
    return this.equipmentRepository;
  }

  @Override
  Equipment generateInstance() {
    return equipmentDataBuilder.buildAsNew();
  }
}
