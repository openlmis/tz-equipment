/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;
import lombok.Getter;
import org.apache.commons.collections4.CollectionUtils;
import org.javers.common.collections.Sets;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.openlmis.equipment.domain.BaseEntity;
import org.openlmis.equipment.domain.Contract;
import org.openlmis.equipment.domain.Discipline;
import org.openlmis.equipment.domain.Equipment;
import org.openlmis.equipment.domain.EquipmentCategory;
import org.openlmis.equipment.domain.EquipmentEnergyType;
import org.openlmis.equipment.domain.EquipmentModel;
import org.openlmis.equipment.domain.EquipmentType;
import org.openlmis.equipment.domain.ServiceType;
import org.openlmis.equipment.domain.Vendor;
import org.openlmis.equipment.service.ContractService;
import org.openlmis.equipment.util.ContractDataBuilder;
import org.openlmis.equipment.util.DisciplineDataBuilder;
import org.openlmis.equipment.util.EquipmentCategoryDataBuilder;
import org.openlmis.equipment.util.EquipmentDataBuilder;
import org.openlmis.equipment.util.EquipmentEnergyTypeDataBuilder;
import org.openlmis.equipment.util.EquipmentModelDataBuilder;
import org.openlmis.equipment.util.EquipmentTypeDataBuilder;
import org.openlmis.equipment.util.ServiceTypeDataBuilder;
import org.openlmis.equipment.util.VendorDataBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@SuppressWarnings("PMD.TooManyMethods")
public class ContractRepositoryIntegrationTest
    extends BaseCrudRepositoryIntegrationTest<Contract> {

  @Getter
  @Autowired
  private ContractRepository repository;

  @Autowired
  private VendorRepository vendorRepository;

  @Autowired
  private ServiceTypeRepository serviceTypeRepository;

  @Autowired
  private EquipmentTypeRepository equipmentTypeRepository;

  @Autowired
  private EquipmentRepository equipmentRepository;

  @Autowired
  private EquipmentEnergyTypeRepository equipmentEnergyTypeRepository;

  @Autowired
  private EquipmentModelRepository equipmentModelRepository;

  @Autowired
  private EquipmentCategoryRepository equipmentCategoryRepository;

  @Autowired
  private DisciplineRepository disciplineRepository;

  private Vendor vendor;

  private Vendor vendor1;

  private Vendor vendor2;

  private ServiceType serviceType;

  private ServiceType serviceType1;

  private ServiceType serviceType2;

  private Equipment equipment;

  private Equipment equipment1;

  private Equipment equipment2;

  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    vendorRepository.deleteAll();
    serviceTypeRepository.deleteAll();
    equipmentRepository.deleteAll();
    equipmentEnergyTypeRepository.deleteAll();
    equipmentModelRepository.deleteAll();
    equipmentCategoryRepository.deleteAll();
    disciplineRepository.deleteAll();
    equipmentTypeRepository.deleteAll();

    vendor = vendorRepository.save(
        new VendorDataBuilder().withoutId().build()
    );

    vendor1 = vendorRepository.save(
        new VendorDataBuilder().withoutId().build()
    );

    vendor2 = vendorRepository.save(
        new VendorDataBuilder().withoutId().build()
    );

    serviceType = serviceTypeRepository.save(
        new ServiceTypeDataBuilder().withoutId().build()
    );

    serviceType1 = serviceTypeRepository.save(
        new ServiceTypeDataBuilder().withoutId().build()
    );

    serviceType2 = serviceTypeRepository.save(
        new ServiceTypeDataBuilder().withoutId().build()
    );

    EquipmentType equipmentType = equipmentTypeRepository.save(
        new EquipmentTypeDataBuilder().buildAsNew()
    );

    EquipmentModel equipmentModel = equipmentModelRepository.save(
        new EquipmentModelDataBuilder()
            .withEquipmentType(equipmentType)
            .build()
    );

    EquipmentEnergyType equipmentEnergyType = equipmentEnergyTypeRepository.save(
        new EquipmentEnergyTypeDataBuilder().buildAsNew()
    );

    Discipline discipline = disciplineRepository.save(
        new DisciplineDataBuilder().build()
    );

    EquipmentCategory equipmentCategory = equipmentCategoryRepository.save(
        new EquipmentCategoryDataBuilder()
            .withEquipmentType(equipmentType)
            .withDisciplines(Sets.asSet(discipline))
            .build()
    );

    equipment = equipmentRepository.save(
        new EquipmentDataBuilder()
            .withEquipmentCategory(equipmentCategory)
            .withEquipmentEnergyType(equipmentEnergyType)
            .withEquipmentModel(equipmentModel)
            .buildAsNew()
    );

    equipment1 = equipmentRepository.save(
        new EquipmentDataBuilder()
            .withEquipmentCategory(equipmentCategory)
            .withEquipmentEnergyType(equipmentEnergyType)
            .withEquipmentModel(equipmentModel)
            .buildAsNew()
    );

    equipment2 = equipmentRepository.save(
        new EquipmentDataBuilder()
            .withEquipmentCategory(equipmentCategory)
            .withEquipmentEnergyType(equipmentEnergyType)
            .withEquipmentModel(equipmentModel)
            .buildAsNew()
    );
  }

  @Test
  public void shouldSearchContractByIdentifier() {
    createContract("test124");
    createContract("test345");
    createContract("another789");

    ContractService.SearchParams searchParams = Mockito.mock(ContractService.SearchParams.class);
    when(searchParams.getIdentifier()).thenReturn("test");

    Page<Contract> contracts = repository.search(searchParams, pageable);

    assertThat(contracts).allMatch(contract -> contract.getIdentifier().contains("test"));
  }

  @Test
  public void shouldSearchContractByDateFrom() {
    createContract(LocalDate.of(2021, 1, 1));
    createContract(LocalDate.of(2022, 2, 1));
    createContract(LocalDate.of(2023, 3, 1));

    ContractService.SearchParams searchParams = Mockito.mock(ContractService.SearchParams.class);
    LocalDate dateFrom = LocalDate.of(2022, 1, 1);
    when(searchParams.getDateFrom()).thenReturn(dateFrom);

    Page<Contract> contracts = repository.search(searchParams, pageable);

    assertThat(contracts).allMatch(contract -> !contract.getDate().isBefore(dateFrom));
  }

  @Test
  public void shouldSearchContractByDateTo() {
    createContract(LocalDate.of(2021, 1, 1));
    createContract(LocalDate.of(2022, 2, 1));
    createContract(LocalDate.of(2023, 3, 1));

    ContractService.SearchParams searchParams = Mockito.mock(ContractService.SearchParams.class);
    LocalDate dateTo = LocalDate.of(2022, 1, 1);
    when(searchParams.getDateTo()).thenReturn(dateTo);

    Page<Contract> contracts = repository.search(searchParams, pageable);

    assertThat(contracts).allMatch(contract -> contract.getDate().isBefore(dateTo));
  }

  @Test
  public void shouldSearchContractByVendor() {
    createContractWithVendor(vendor);
    createContractWithVendor(vendor1);
    createContractWithVendor(vendor2);
    createContractWithVendor(vendor2);

    ContractService.SearchParams searchParams = Mockito.mock(ContractService.SearchParams.class);
    Set<UUID> vendorQuery = Sets.asSet(vendor.getId(), vendor1.getId());
    when(searchParams.getVendorIds()).thenReturn(vendorQuery);

    Page<Contract> contracts = repository.search(searchParams, pageable);

    assertThat(contracts).allMatch(contract -> vendorQuery.contains(contract.getVendor().getId()));
  }

  @Test
  public void shouldSearchContractByEquipment() {
    createContractWithEquipments(Sets.asSet(equipment, equipment1));
    createContractWithEquipments(Sets.asSet(equipment, equipment2));
    createContractWithEquipments(Sets.asSet(equipment1, equipment2));
    createContractWithEquipments(Sets.asSet(equipment2));

    ContractService.SearchParams searchParams = Mockito.mock(ContractService.SearchParams.class);
    Set<UUID> equipmentQuery = Sets.asSet(equipment.getId(), equipment1.getId());
    when(searchParams.getEquipmentIds()).thenReturn(equipmentQuery);

    Page<Contract> contracts = repository.search(searchParams, pageable);

    assertThat(contracts).allMatch(
        contract -> CollectionUtils.containsAny(
            BaseEntity.toIds(contract.getEquipments()),
            equipmentQuery
        )
    );
  }

  @Test
  public void shouldSearchContractByServiceType() {
    createContractWithServiceTypes(Sets.asSet(serviceType, serviceType1));
    createContractWithServiceTypes(Sets.asSet(serviceType, serviceType2));
    createContractWithServiceTypes(Sets.asSet(serviceType1, serviceType2));
    createContractWithServiceTypes(Sets.asSet(serviceType2));

    ContractService.SearchParams searchParams = Mockito.mock(ContractService.SearchParams.class);
    Set<UUID> serviceTypeQuery = Sets.asSet(serviceType.getId(), serviceType1.getId());
    when(searchParams.getServiceTypeIds()).thenReturn(serviceTypeQuery);

    Page<Contract> contracts = repository.search(searchParams, pageable);

    assertThat(contracts).allMatch(
        contract -> CollectionUtils.containsAny(
            BaseEntity.toIds(contract.getServiceTypes()),
            serviceTypeQuery
        )
    );
  }


  private Contract createContractWithServiceTypes(Set<ServiceType> serviceTypes) {
    return createContract(
        UUID.randomUUID().toString(),
        LocalDate.now().minusYears(1),
        vendor, serviceTypes, Sets.asSet(equipment), Sets.asSet(UUID.randomUUID())
    );
  }

  private Contract createContractWithEquipments(Set<Equipment> equipments) {
    return createContract(
        UUID.randomUUID().toString(),
        LocalDate.now().minusYears(1),
        vendor, Sets.asSet(serviceType), equipments, Sets.asSet(UUID.randomUUID())
    );
  }

  private Contract createContractWithVendor(Vendor vendor) {
    return createContract(
        UUID.randomUUID().toString(),
        LocalDate.now().minusYears(1),
        vendor, Sets.asSet(serviceType), Sets.asSet(equipment), Sets.asSet(UUID.randomUUID())
    );
  }

  private Contract createContract(LocalDate date) {
    return createContract(UUID.randomUUID().toString(), date, vendor,
        Sets.asSet(serviceType), Sets.asSet(equipment), Sets.asSet(UUID.randomUUID())
    );
  }

  private Contract createContract(String identifier) {
    return createContract(identifier, LocalDate.now(), vendor,
        Sets.asSet(serviceType), Sets.asSet(equipment), Sets.asSet(UUID.randomUUID())
    );
  }

  private Contract createContract(String identifier,
                                  LocalDate date,
                                  Vendor vendor,
                                  Set<ServiceType> serviceTypes,
                                  Set<Equipment> equipments,
                                  Set<UUID> facilities) {
    Contract contract = new ContractDataBuilder()
        .withIdentifier(identifier)
        .withDate(date)
        .withVendor(vendor)
        .withServiceTypes(serviceTypes)
        .withEquipments(equipments)
        .withFacilities(facilities)
        .withoutId()
        .build();

    return repository.save(contract);
  }

  @Override
  Contract generateInstance() throws Exception {
    return new ContractDataBuilder()
        .withVendor(vendor)
        .withServiceTypes(Sets.asSet(serviceType))
        .withEquipments(Sets.asSet(equipment))
        .withoutId()
        .build();
  }
}
