/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.openlmis.equipment.domain.Equipment.Importer;
import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import com.google.common.collect.Lists;
import guru.nidi.ramltester.junit.RamlMatchers;
import java.util.List;
import java.util.UUID;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openlmis.equipment.domain.Equipment;
import org.openlmis.equipment.domain.EquipmentCategory;
import org.openlmis.equipment.domain.EquipmentEnergyType;
import org.openlmis.equipment.domain.EquipmentModel;
import org.openlmis.equipment.dto.EquipmentDto;
import org.openlmis.equipment.exception.PermissionMessageException;
import org.openlmis.equipment.service.EquipmentService.SearchParams;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.EquipmentDataBuilder;
import org.openlmis.equipment.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class EquipmentControllerIntegrationTest extends BaseWebIntegrationTest {

  static final String RESOURCE_PATH = BASE_PATH + "/equipments";

  private static final String ID_PATH = RESOURCE_PATH + "/{id}";

  private static final String PARAM_EQUIPMENT_ENERGY_TYPE_ID = "equipmentEnergyTypeId";

  private static final String PARAM_EQUIPMENT_MODEL_ID = "equipmentModelId";

  private static final String PARAM_EQUIPMENT_CATEGORY_ID = "equipmentCategoryId";

  private static final String PARAM_MANUFACTURER = "manufacturer";

  private static final String PARAM_NAME = "name";

  private UUID equipmentId;

  private Equipment equipment;

  private UUID equipmentEnergyTypeId;

  private EquipmentEnergyType equipmentEnergyType;

  private UUID equipmentModelId;

  private EquipmentModel equipmentModel;

  private UUID equipmentCategoryId;

  private EquipmentCategory equipmentCategory;

  private EquipmentDto equipmentDto;

  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    equipment = new EquipmentDataBuilder().build();
    equipmentId = equipment.getId();

    equipmentEnergyType = equipment.getEnergyType();
    equipmentEnergyTypeId = equipmentEnergyType.getId();

    equipmentModel = equipment.getModel();
    equipmentModelId = equipmentModel.getId();

    equipmentCategory = equipment.getCategory();
    equipmentCategoryId = equipmentCategory.getId();

    equipmentDto = EquipmentDto.newInstance(equipment);
  }

  @Test
  public void shouldGetEquipmentById() {
    given(equipmentService.getEquipment(any()))
        .willReturn(equipment);

    EquipmentDto response = restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .pathParam("id", equipmentId)
        .when()
        .get(ID_PATH)
        .then()
        .statusCode(200)
        .extract()
        .as(EquipmentDto.class);

    assertEquals(equipment.getId(), response.getId());
    assertEquals(equipment.getName(), response.getName());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldPostEquipmentDto() {
    when(equipmentService.create(eq(equipmentDto))).thenReturn(equipment);
    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(equipmentDto)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_CREATED)
        .extract().as(EquipmentDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestOnInvalidPostRequest() {
    Equipment requestBody = new EquipmentDataBuilder()
        .withManufacturer(null)
        .withName(null)
        .withEquipmentEnergyType(null)
        .withEquipmentCategory(null)
        .withEquipmentModel(null)
        .buildAsNew();

    when(equipmentService.create(any(Importer.class))).thenReturn(equipment);
    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(requestBody)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST);
  }


  @Test
  public void shouldPutEquipmentDto() {
    when(equipmentService.update(
        eq(equipmentId), eq(equipmentDto))
    ).thenReturn(equipment);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam(ID, equipmentId)
        .body(equipmentDto)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .extract().as(EquipmentDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestOnInvalidPutRequest() {
    Equipment requestBody = new EquipmentDataBuilder()
        .withManufacturer(null)
        .withName(null)
        .withEquipmentEnergyType(null)
        .withEquipmentCategory(null)
        .withEquipmentModel(null)
        .buildAsNew();

    when(equipmentService.update(eq(equipmentId), eq(equipmentDto)))
        .thenReturn(equipment);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam(ID, equipmentId)
        .body(requestBody)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST);

  }

  @Test
  public void shouldGetAllEquipments() {
    List<Equipment> expectedEquipments = Lists.newArrayList(equipment);
    Page<Equipment> expectedEquipmentsPage = PaginationUtil.getPage(expectedEquipments, pageable);

    given(equipmentService.search(any(SearchParams.class), any(Pageable.class)))
        .willReturn(expectedEquipmentsPage);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .queryParam(PAGE, pageable.getPageNumber())
        .queryParam(SIZE, pageable.getPageSize())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .when()
        .get(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .body(CONTENT, hasSize(expectedEquipments.size()))
        .body(CONTENT_ID, hasSize(expectedEquipments.size()))
        .body(CONTENT_ID, hasItem(equipment.getId().toString()));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldSearchEquipments() {
    List<Equipment> expectedEquipments = Lists.newArrayList(equipment);
    Page<Equipment> expectedEquipmentsPage = PaginationUtil.getPage(expectedEquipments, pageable);

    given(equipmentService.search(any(SearchParams.class), any(Pageable.class)))
        .willReturn(expectedEquipmentsPage);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .queryParam(PAGE, pageable.getPageNumber())
        .queryParam(SIZE, pageable.getPageSize())
        .queryParam(ID, equipmentId)
        .queryParam(PARAM_MANUFACTURER, equipment.getManufacturer())
        .queryParam(PARAM_NAME, equipment.getName())
        .queryParam(PARAM_EQUIPMENT_ENERGY_TYPE_ID, equipmentEnergyTypeId)
        .queryParam(PARAM_EQUIPMENT_MODEL_ID, equipmentModelId)
        .queryParam(PARAM_EQUIPMENT_CATEGORY_ID, equipmentCategoryId)
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .when()
        .get(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .body(CONTENT, hasSize(expectedEquipments.size()))
        .body(CONTENT_ID, hasSize(expectedEquipments.size()))
        .body(CONTENT_ID, hasItem(equipment.getId().toString()));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenPostWhileUserHasNoManagePermission() {
    PermissionMessageException exception = mockPermissionException(
        PermissionService.EQUIPMENT_MANAGE
    );
    when(equipmentService.create(eq(equipmentDto)))
        .thenThrow(exception);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(equipmentDto)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(403);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenUpdateWhileUserHasNoManagePermission() {
    PermissionMessageException exception = mockPermissionException(
        PermissionService.EQUIPMENT_MANAGE
    );
    when(equipmentService.update(eq(equipmentId), eq(equipmentDto)))
        .thenThrow(exception);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam("id", equipmentId)
        .body(equipmentDto)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(403);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }
}
