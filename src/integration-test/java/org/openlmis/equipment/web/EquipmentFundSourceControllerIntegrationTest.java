/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.openlmis.equipment.domain.EquipmentFundSource.Importer;
import static org.openlmis.equipment.i18n.EquipmentFundSourceMessageKeys.ERROR_NOT_FOUND;
import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import com.google.common.collect.Lists;
import guru.nidi.ramltester.junit.RamlMatchers;
import java.util.List;
import java.util.UUID;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openlmis.equipment.domain.EquipmentFundSource;
import org.openlmis.equipment.dto.EquipmentFundSourceDto;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.exception.PermissionMessageException;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.EquipmentFundSourceDataBuilder;
import org.openlmis.equipment.util.Message;
import org.openlmis.equipment.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class EquipmentFundSourceControllerIntegrationTest extends BaseWebIntegrationTest {
  static final String RESOURCE_PATH = BASE_PATH + "/equipmentFundSources";
  private static final String ID_PATH = RESOURCE_PATH + "/{id}";
  private static final String PARAM_CODE = "code";
  private static final String PARAM_NAME = "name";

  private EquipmentFundSource equipmentFundSource;
  private EquipmentFundSourceDto equipmentFundSourceDto;
  private UUID equipmentFundSourceId;
  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    equipmentFundSource = new EquipmentFundSourceDataBuilder().build();
    equipmentFundSourceId = equipmentFundSource.getId();
    equipmentFundSourceDto = EquipmentFundSourceDto.newInstance(equipmentFundSource);
  }

  @Test
  public void shouldPostEquipmentFundSourceDto() {
    when(equipmentFundSourceService.create(eq(equipmentFundSourceDto)))
        .thenReturn(equipmentFundSource);
    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(equipmentFundSourceDto)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_CREATED)
        .extract().as(EquipmentFundSourceDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestOnInvalidPostRequest() {
    EquipmentFundSource requestBody = new EquipmentFundSourceDataBuilder()
        .withCode(null)
        .withName(null)
        .buildAsNew();

    when(equipmentFundSourceService.create(any(Importer.class))).thenReturn(equipmentFundSource);
    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(requestBody)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST);
  }


  @Test
  public void shouldPutEquipmentFundSourceDto() {
    when(equipmentFundSourceService.update(
        eq(equipmentFundSourceId), eq(equipmentFundSourceDto))
    ).thenReturn(equipmentFundSource);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam(ID, equipmentFundSourceId)
        .body(equipmentFundSourceDto)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .extract().as(EquipmentFundSourceDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestOnInvalidPutRequest() {
    EquipmentFundSource requestBody = new EquipmentFundSourceDataBuilder()
        .withCode(null)
        .withName(null)
        .buildAsNew();

    when(equipmentFundSourceService.update(eq(equipmentFundSourceId), eq(equipmentFundSourceDto)))
        .thenReturn(equipmentFundSource);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam(ID, equipmentFundSourceId)
        .body(requestBody)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST);

  }

  @Test
  public void shouldGetEquipmentFundSourceById() {
    given(equipmentFundSourceService.getEquipmentFundSource(any()))
        .willReturn(equipmentFundSource);

    EquipmentFundSourceDto response = restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .pathParam("id", equipmentFundSourceId)
        .when()
        .get(ID_PATH)
        .then()
        .statusCode(200)
        .extract()
        .as(EquipmentFundSourceDto.class);

    assertEquals(equipmentFundSource.getName(), response.getName());
    assertEquals(equipmentFundSource.getCode(), response.getCode());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void getShouldReturnNotFoundForNonExistingEquipmentFundSource() {
    given(equipmentFundSourceService.getEquipmentFundSource(any()))
        .willThrow(new NotFoundException(new Message(ERROR_NOT_FOUND)));

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .pathParam("id", equipmentFundSourceId)
        .when()
        .get(ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_NOT_FOUND);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldSearchEquipmentFundSources() {
    List<EquipmentFundSource> expectedFundSources = Lists.newArrayList(equipmentFundSource);
    Page<EquipmentFundSource> expectedFundSourcesPage = PaginationUtil.getPage(
        expectedFundSources,
        pageable
    );

    given(equipmentFundSourceService.search(any(), any(), any(Pageable.class)))
        .willReturn(expectedFundSourcesPage);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .queryParam(PAGE, pageable.getPageNumber())
        .queryParam(SIZE, pageable.getPageSize())
        .queryParam(PARAM_CODE, equipmentFundSource.getCode())
        .queryParam(PARAM_NAME, equipmentFundSource.getName())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .when()
        .get(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .body(CONTENT, hasSize(expectedFundSources.size()))
        .body(CONTENT_ID, hasSize(expectedFundSources.size()))
        .body(CONTENT_ID, hasItem(equipmentFundSource.getId().toString()));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenPostWhileUserHasNoManagePermission() {
    PermissionMessageException exception = mockPermissionException(
        PermissionService.EQUIPMENT_MANAGE
    );
    when(equipmentFundSourceService.create(eq(equipmentFundSourceDto)))
        .thenThrow(exception);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(equipmentFundSourceDto)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(403);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenUpdateWhileUserHasNoManagePermission() {
    PermissionMessageException exception = mockPermissionException(
        PermissionService.EQUIPMENT_MANAGE
    );
    when(equipmentFundSourceService.update(eq(equipmentFundSourceId), eq(equipmentFundSourceDto)))
        .thenThrow(exception);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam("id", equipmentFundSourceId)
        .body(equipmentFundSourceDto)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(403);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }
}
