/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import guru.nidi.ramltester.junit.RamlMatchers;
import java.util.UUID;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openlmis.equipment.domain.EquipmentMaintenanceRequest;
import org.openlmis.equipment.dto.EquipmentMaintenanceRequestDto;
import org.openlmis.equipment.dto.EquipmentMaintenanceRequestReasonDto;
import org.openlmis.equipment.exception.PermissionMessageException;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.EquipmentMaintenanceRequestDataBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

@SuppressWarnings("PMD.TooManyMethods")
public class EquipmentMaintenanceRequestControllerIntegrationTest
    extends org.openlmis.equipment.web.BaseWebIntegrationTest {

  private static final String RESOURCE_URL = "/api/equipmentMaintenanceRequests";

  private static final String ID_PATH = RESOURCE_URL + "/{id}";

  private EquipmentMaintenanceRequest equipmentMaintenanceRequest;

  private EquipmentMaintenanceRequestDto maintenanceRequestDto;

  private UUID requestId;

  @Before
  public void setUp() {
    equipmentMaintenanceRequest = new EquipmentMaintenanceRequestDataBuilder().build();
    requestId = equipmentMaintenanceRequest.getId();
    maintenanceRequestDto = EquipmentMaintenanceRequestDto
        .newInstance(equipmentMaintenanceRequest);
  }

  @Test
  public void shouldPostEquipmentMaintenanceRequestDto() {
    when(maintenanceRequestService.create(eq(maintenanceRequestDto)))
        .thenReturn(equipmentMaintenanceRequest);
    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(maintenanceRequestDto)
        .when()
        .post(RESOURCE_URL)
        .then()
        .statusCode(HttpStatus.SC_CREATED)
        .extract().as(EquipmentMaintenanceRequestDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenPostWhileUserHasNoManagePermission() {
    PermissionMessageException exception = mockPermissionException(
        PermissionService.EQUIPMENT_MANAGE
    );
    when(maintenanceRequestService.create(eq(maintenanceRequestDto)))
        .thenThrow(exception);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(maintenanceRequestDto)
        .when()
        .post(RESOURCE_URL)
        .then()
        .statusCode(403);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestOnInvalidPutRequest() {
    EquipmentMaintenanceRequestReasonDto requestBody = new EquipmentMaintenanceRequestReasonDto();

    when(maintenanceRequestService.update(eq(requestId), eq(maintenanceRequestDto)))
        .thenReturn(equipmentMaintenanceRequest);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam(ID, requestId)
        .body(requestBody)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST);

  }

}