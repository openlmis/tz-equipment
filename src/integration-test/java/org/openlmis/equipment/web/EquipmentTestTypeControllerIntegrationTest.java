/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import guru.nidi.ramltester.junit.RamlMatchers;

import java.util.Optional;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import org.openlmis.equipment.domain.EquipmentCategory;
import org.openlmis.equipment.domain.EquipmentTestType;
import org.openlmis.equipment.dto.EquipmentTestTypeDto;
import org.openlmis.equipment.i18n.DisciplineMessageKeys;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.EquipmentCategoryDataBuilder;
import org.openlmis.equipment.util.EquipmentTestTypeDataBuilder;
import org.openlmis.equipment.util.PageDto;
import org.openlmis.equipment.util.PaginationUtil;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class EquipmentTestTypeControllerIntegrationTest extends BaseWebIntegrationTest {
  static final String RESOURCE_PATH = BASE_PATH + "/equipmentTestTypes";
  private static final String ID_URL = RESOURCE_PATH + "/{id}";
  private static final String EQUIPMENT_TEST_TYPE_NAME = "name";
  private static final String EQUIPMENT_TEST_TYPE_CODE = "code";

  EquipmentCategory equipmentCategory = new EquipmentCategoryDataBuilder().build();
  private EquipmentTestType equipmentTestType;
  private  EquipmentTestTypeDto equipmentTestTypeDto;

  private  UUID equipmentTestTypeId;
  private  Pageable pageable;


  /**
   * Constructor for test class.
   */
  public EquipmentTestTypeControllerIntegrationTest() {

    equipmentTestType = new EquipmentTestTypeDataBuilder()
            .withCode(EQUIPMENT_TEST_TYPE_CODE)
            .withName(EQUIPMENT_TEST_TYPE_NAME)
            .withEnabled(true)
            .withDisplayOrder(1)
            .withEquipmentCategory(equipmentCategory)
            .buildAsNew();

    equipmentTestTypeDto = new EquipmentTestTypeDto();
    equipmentTestType.export(equipmentTestTypeDto);
    equipmentTestTypeId = UUID.randomUUID();
    pageable = PageRequest.of(0, 10);
  }

  @Test
  public void shouldReturnEquipmentTestTypePage() {
    doReturn(PaginationUtil.getPage(singletonList(equipmentTestType), pageable))
            .when(equipmentTestTypeRepository).findAll();

    PageDto resultPage = restAssured.given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .when()
            .get(RESOURCE_PATH)
            .then()
            .statusCode(200)
            .extract().as(PageDto.class);

    assertEquals(1, resultPage.getContent().size());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldGetEquipmentTestType() {
    given(equipmentTestTypeRepository.findById(equipmentTestTypeId))
            .willReturn(Optional.of(equipmentTestType));

    EquipmentTestTypeDto response = restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .pathParam("id", equipmentTestTypeId)
            .when()
            .get(ID_URL)
            .then()
            .statusCode(200)
            .extract().as(EquipmentTestTypeDto.class);

    assertEquals(EQUIPMENT_TEST_TYPE_NAME, response.getName());
    assertEquals(EQUIPMENT_TEST_TYPE_CODE, response.getCode());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }


  @Test
  public void getShouldReturnNotFoundForNonExistingEquipmentTestType() {
    given(equipmentTestTypeRepository.findById(equipmentTestTypeId)).willReturn(Optional.empty());

    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .pathParam("id", equipmentTestTypeId)
            .when()
            .get(ID_URL)
            .then()
            .statusCode(404);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldPostEquipmentTestType() {
    given(equipmentTestTypeRepository.save(any()))
            .willReturn(equipmentTestType);

    EquipmentTestTypeDto response = restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(equipmentTestTypeDto)
            .when()
            .post(RESOURCE_PATH)
            .then()
            .statusCode(201)
            .extract().as(EquipmentTestTypeDto.class);

    assertEquals(EQUIPMENT_TEST_TYPE_NAME, response.getName());
    assertEquals(EQUIPMENT_TEST_TYPE_CODE, response.getCode());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldUpdateEquipmentTestType() {
    given(equipmentTestTypeRepository.save(any()))
            .willReturn(equipmentTestType);

    EquipmentTestTypeDto response = restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(equipmentTestTypeDto)
            .pathParam("id", equipmentTestTypeId)
            .when()
            .put(ID_URL)
            .then()
            .statusCode(200)
            .extract().as(EquipmentTestTypeDto.class);


    assertEquals(EQUIPMENT_TEST_TYPE_NAME, response.getName());
    assertEquals(EQUIPMENT_TEST_TYPE_CODE, response.getCode());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestWhenPostEmptyFields() {
    EquipmentTestTypeDto invalidDto = new EquipmentTestTypeDto();
    invalidDto.setCode("code");
    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(invalidDto)
            .when()
            .post(RESOURCE_PATH)
            .then()
            .statusCode(400)
            .body(MESSAGE, equalTo(getMessage(
                    DisciplineMessageKeys.ERROR_MISSING_MANDATORY_ITEMS)));

  }

  @Test
  public void shouldReturnUnauthorizedWhenPostButUserHasNoManagePermission() {
    doThrow(mockPermissionException(PermissionService.EQUIPMENT_MANAGE))
            .when(permissionService).canManageCce();

    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(equipmentTestTypeDto)
            .when()
            .post(RESOURCE_PATH)
            .then()
            .statusCode(403);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
            RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenUpdateButUserHasNoManagePermission() {
    doThrow(mockPermissionException(PermissionService.EQUIPMENT_MANAGE))
            .when(permissionService).canManageCce();

    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .pathParam("id", equipmentTestTypeId)
            .body(equipmentTestTypeDto)
            .when()
            .put(ID_URL)
            .then()
            .statusCode(403);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
            RamlMatchers.hasNoViolations());
  }

}
