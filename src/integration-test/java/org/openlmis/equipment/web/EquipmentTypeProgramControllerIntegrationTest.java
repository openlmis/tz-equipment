/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import com.jayway.restassured.response.Response;
import guru.nidi.ramltester.junit.RamlMatchers;
import java.util.Optional;
import java.util.UUID;
import org.junit.Assert;
import org.junit.Test;

import org.openlmis.equipment.domain.EquipmentType;
import org.openlmis.equipment.domain.EquipmentTypeProgram;
import org.openlmis.equipment.dto.EquipmentTypeProgramDto;
import org.openlmis.equipment.i18n.DisciplineMessageKeys;
import org.openlmis.equipment.i18n.MessageKeys;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.EquipmentTypeDataBuilder;
import org.openlmis.equipment.util.EquipmentTypeProgramDataBuilder;
import org.openlmis.equipment.util.PageDto;
import org.openlmis.equipment.util.PaginationUtil;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class EquipmentTypeProgramControllerIntegrationTest extends BaseWebIntegrationTest {

  static final String RESOURCE_PATH = BASE_PATH + "/equipmentTypeProgram";
  private static final String ID_URL = RESOURCE_PATH + "/{id}";

  private EquipmentTypeProgram equipmentTypeProgram;
  private EquipmentTypeProgramDto equipmentTypeProgramDto;
  private UUID equipmentTypeProgramId;
  private Pageable pageable;

  EquipmentType equipmentType = new EquipmentTypeDataBuilder().build();
  private UUID programId = UUID.fromString("466b2e7f-5798-4027-a2ca-373627748ea6");

  /**
   * Constructor for test class.
   */
  public EquipmentTypeProgramControllerIntegrationTest() {

    equipmentTypeProgram = new EquipmentTypeProgramDataBuilder()
            .withEquipmentType(equipmentType)
            .withProgramId(programId)
            .withDisplayOrder(1)
            .withEnableTotalColumn(true)
            .withEnableTestCount(false)
            .buildAsNew();


    equipmentTypeProgramDto = new EquipmentTypeProgramDto();
    equipmentTypeProgram.export(equipmentTypeProgramDto);
    equipmentTypeProgramId = UUID.randomUUID();
    pageable = PageRequest.of(0, 10);

  }


  @Test
  public void shouldReturnEquipmentTypeProgramPage() {
    doReturn(PaginationUtil.getPage(singletonList(equipmentTypeProgram), pageable))
            .when(equipmentTypeProgramRepository).findAll();

    PageDto resultPage = restAssured.given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .when()
            .get(RESOURCE_PATH)
            .then()
            .statusCode(200)
            .extract().as(PageDto.class);

    assertEquals(1, resultPage.getContent().size());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldGetEquipmentTypeProgram() {

    given(equipmentTypeProgramRepository.findById(equipmentTypeProgramId))
            .willReturn(Optional.of(equipmentTypeProgram));

    EquipmentTypeProgramDto response = restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .pathParam("id", equipmentTypeProgramId)
            .when()
            .get(ID_URL)
            .then()
            .statusCode(200)
            .extract().as(EquipmentTypeProgramDto.class);

    assertEquals(equipmentType, response.getEquipmentType());
    assertEquals(programId, response.getProgramId());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestWhenPostEmptyFields() {
    EquipmentTypeProgramDto invalidDto = new EquipmentTypeProgramDto();
    invalidDto.setProgramId(programId);
    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(invalidDto)
            .when()
            .post(RESOURCE_PATH)
            .then()
            .statusCode(400)
            .body(MESSAGE, equalTo(getMessage(
                    DisciplineMessageKeys.ERROR_MISSING_MANDATORY_ITEMS)));

  }

  @Test
  public void shouldPutEquipmentTypeProgram() {

    when(equipmentTypeProgramRepository.findById(equipmentTypeProgramId))
            .thenReturn(Optional.of(equipmentTypeProgram));
    given(equipmentTypeProgramRepository.save(any()))
            .willReturn(equipmentTypeProgram);

    EquipmentTypeProgramDto response = restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .pathParam("id", equipmentTypeProgramId)
            .body(equipmentTypeProgramDto)
            .when()
            .put(ID_URL)
            .then()
            .statusCode(200)
            .extract().as(EquipmentTypeProgramDto.class);

    assertEquals(equipmentType, response.getEquipmentType());
    assertEquals(programId, response.getProgramId());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestWhenPutEmptyFields() {
    EquipmentTypeProgramDto invalidDto = new EquipmentTypeProgramDto();
    invalidDto.setProgramId(programId);
    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .pathParam("id", equipmentTypeProgramId)
            .body(invalidDto)
            .when()
            .put(ID_URL)
            .then()
            .statusCode(400)
            .body(MESSAGE, equalTo(getMessage(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS)));
  }

  @Test
  public void shouldReturnUnauthorizedWhenPostButUserHasNoManagePermission() {
    doThrow(mockPermissionException(PermissionService.EQUIPMENT_MANAGE))
            .when(permissionService).canManageCce();

    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(equipmentTypeProgramDto)
            .when()
            .post(RESOURCE_PATH)
            .then()
            .statusCode(403);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
            RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenUpdateButUserHasNoManagePermission() {
    doThrow(mockPermissionException(PermissionService.EQUIPMENT_MANAGE))
            .when(permissionService).canManageCce();

    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .pathParam("id", equipmentTypeProgramId)
            .body(equipmentTypeProgramDto)
            .when()
            .put(ID_URL)
            .then()
            .statusCode(403);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
            RamlMatchers.hasNoViolations());
  }

  @Test
  public void getShouldReturnNotFoundForNonExistingEquipmentPrograms() {
    given(equipmentTypeProgramRepository.findById(equipmentTypeProgramId))
            .willReturn(Optional.empty());

    restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .pathParam("id", equipmentTypeProgramId)
            .when()
            .get(ID_URL)
            .then()
            .statusCode(404);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturn404EquipmentProgramWhenNotFoundById() {
    deleteEquipmentProgram()
            .then()
            .statusCode(404);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldDeleteEquipmentProgramWhenFoundById() {
    when(equipmentTypeProgramRepository.findById(equipmentTypeProgramId))
            .thenReturn(Optional.of(equipmentTypeProgram));

    deleteEquipmentProgram()
            .then()
            .statusCode(204);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  private Response deleteEquipmentProgram() {
    return restAssured
            .given()
            .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
            .pathParam("id", equipmentTypeProgramId)
            .when()
            .delete(ID_URL);
  }


}
