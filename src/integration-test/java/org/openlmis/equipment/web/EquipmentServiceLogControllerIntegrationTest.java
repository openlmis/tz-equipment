/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import guru.nidi.ramltester.junit.RamlMatchers;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.openlmis.equipment.domain.EquipmentServiceLog;
import org.openlmis.equipment.dto.EquipmentDto;
import org.openlmis.equipment.dto.EquipmentServiceLogDto;
import org.openlmis.equipment.util.EquipmentServiceLogDataBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

@SuppressWarnings("PMD.TooManyMethods")
public class EquipmentServiceLogControllerIntegrationTest
    extends org.openlmis.equipment.web.BaseWebIntegrationTest {

  static final String RESOURCE_PATH = BASE_PATH + "/equipmentServiceLogs";
  private static final String ID_PATH = RESOURCE_PATH + "/{id}";
  private UUID equipmentId;
  private EquipmentServiceLogDto equipmentServiceLogDto;
  private EquipmentServiceLog serviceLog;

  @Before
  public void setUp() {
    serviceLog = new EquipmentServiceLogDataBuilder().build();
    equipmentId = serviceLog.getId();

    equipmentServiceLogDto = EquipmentServiceLogDto.newInstance(serviceLog);
  }

  @Test
  public void shouldPostEquipmentServiceLogDto() {

    when(equipmentServiceLogService.create(eq(equipmentServiceLogDto)))
        .thenReturn(serviceLog);
    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(equipmentServiceLogDto)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(org.apache.http.HttpStatus.SC_CREATED)
        .extract().as(EquipmentServiceLogDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldPutEquipmentServiceLogDto() {
    when(equipmentServiceLogService.update(
        eq(equipmentId), eq(equipmentServiceLogDto))
    ).thenReturn(serviceLog);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam("id", equipmentId)
        .body(equipmentServiceLogDto)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(org.apache.http.HttpStatus.SC_OK)
        .extract().as(EquipmentDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }
}

