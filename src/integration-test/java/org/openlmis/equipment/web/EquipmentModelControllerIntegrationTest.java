/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.openlmis.equipment.domain.EquipmentModel.Importer;
import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import com.google.common.collect.Lists;
import guru.nidi.ramltester.junit.RamlMatchers;
import java.util.List;
import java.util.UUID;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openlmis.equipment.domain.EquipmentModel;
import org.openlmis.equipment.domain.EquipmentType;
import org.openlmis.equipment.dto.EquipmentModelDto;
import org.openlmis.equipment.exception.PermissionMessageException;
import org.openlmis.equipment.service.EquipmentModelService.SearchParams;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.EquipmentModelDataBuilder;
import org.openlmis.equipment.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class EquipmentModelControllerIntegrationTest extends BaseWebIntegrationTest {
  static final String RESOURCE_PATH = BASE_PATH + "/equipmentModels";
  private static final String ID_PATH = RESOURCE_PATH + "/{id}";
  private static final String PARAM_EQUIPMENT_TYPE_ID = "equipmentTypeId";
  private static final String PARAM_CODE = "code";
  private static final String PARAM_NAME = "name";

  private EquipmentModel equipmentModel;
  private EquipmentType equipmentType;
  private EquipmentModelDto equipmentModelDto;
  private UUID equipmentModelId;
  private UUID equipmentTypeId;
  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    equipmentModel = new EquipmentModelDataBuilder().build();
    equipmentModelId = equipmentModel.getId();
    equipmentType = equipmentModel.getEquipmentType();
    equipmentTypeId = equipmentType.getId();
    equipmentModelDto = EquipmentModelDto.newInstance(equipmentModel);

  }

  @Test
  public void shouldPostEquipmentModelDto() {
    when(equipmentModelService.create(eq(equipmentModelDto))).thenReturn(equipmentModel);
    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(equipmentModelDto)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_CREATED)
        .extract().as(EquipmentModelDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestOnInvalidPostRequest() {
    EquipmentModel requestBody = new EquipmentModelDataBuilder()
        .withCode(null)
        .withName(null)
        .withEquipmentType(null)
        .buildAsNew();

    when(equipmentModelService.create(any(Importer.class))).thenReturn(equipmentModel);
    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(requestBody)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST);
  }


  @Test
  public void shouldPutEquipmentModelDto() {
    when(equipmentModelService.update(
        eq(equipmentModelId), eq(equipmentModelDto))
    ).thenReturn(equipmentModel);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam(ID, equipmentModelId)
        .body(equipmentModelDto)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .extract().as(EquipmentModelDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestOnInvalidPutRequest() {
    EquipmentModel requestBody = new EquipmentModelDataBuilder()
        .withCode(null)
        .withName(null)
        .withEquipmentType(null)
        .buildAsNew();

    when(equipmentModelService.update(eq(equipmentModelId), eq(equipmentModelDto)))
        .thenReturn(equipmentModel);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam(ID, equipmentModelId)
        .body(requestBody)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST);

  }

  @Test
  public void shouldGetAllEquipmentModels() {
    List<EquipmentModel> expectedModels = Lists.newArrayList(equipmentModel);
    Page<EquipmentModel> expectedModelsPage = PaginationUtil.getPage(expectedModels, pageable);

    given(equipmentModelService.search(any(SearchParams.class), any(Pageable.class)))
        .willReturn(expectedModelsPage);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .queryParam(PAGE, pageable.getPageNumber())
        .queryParam(SIZE, pageable.getPageSize())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .when()
        .get(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .body(CONTENT, hasSize(expectedModels.size()))
        .body(CONTENT_ID, hasSize(expectedModels.size()))
        .body(CONTENT_ID, hasItem(equipmentModel.getId().toString()));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldSearchEquipmentModels() {
    List<EquipmentModel> expectedModels = Lists.newArrayList(equipmentModel);
    Page<EquipmentModel> expectedModelsPage = PaginationUtil.getPage(expectedModels, pageable);

    given(equipmentModelService.search(any(SearchParams.class), any(Pageable.class)))
        .willReturn(expectedModelsPage);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .queryParam(PAGE, pageable.getPageNumber())
        .queryParam(SIZE, pageable.getPageSize())
        .queryParam(ID, equipmentModelId)
        .queryParam(PARAM_CODE, equipmentModel.getCode())
        .queryParam(PARAM_NAME, equipmentModel.getName())
        .queryParam(PARAM_EQUIPMENT_TYPE_ID, equipmentTypeId)
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .when()
        .get(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .body(CONTENT, hasSize(expectedModels.size()))
        .body(CONTENT_ID, hasSize(expectedModels.size()))
        .body(CONTENT_ID, hasItem(equipmentModel.getId().toString()));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenPostWhileUserHasNoManagePermission() {
    PermissionMessageException exception = mockPermissionException(
        PermissionService.EQUIPMENT_MANAGE
    );
    when(equipmentModelService.create(eq(equipmentModelDto)))
        .thenThrow(exception);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(equipmentModelDto)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(403);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenUpdateWhileUserHasNoManagePermission() {
    PermissionMessageException exception = mockPermissionException(
        PermissionService.EQUIPMENT_MANAGE
    );
    when(equipmentModelService.update(eq(equipmentModelId), eq(equipmentModelDto)))
        .thenThrow(exception);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam("id", equipmentModelId)
        .body(equipmentModelDto)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(403);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }
}
