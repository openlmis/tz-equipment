/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import com.google.common.collect.Lists;
import guru.nidi.ramltester.junit.RamlMatchers;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openlmis.equipment.domain.EquipmentOrderable;
import org.openlmis.equipment.dto.EquipmentOrderableDto;
import org.openlmis.equipment.exception.PermissionMessageException;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.EquipmentOrderableDataBuilder;
import org.openlmis.equipment.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class EquipmentOrderableControllerIntegrationTest extends BaseWebIntegrationTest {

  static final String RESOURCE_PATH = BASE_PATH + "/equipmentOrderables";

  private static final String ID_PATH = RESOURCE_PATH + "/{id}";

  private static final String PARAM_EQUIPMENT_ID = "equipmentId";

  private static final String PARAM_OREDERABLE_ID = "orderableId";

  private EquipmentOrderable equipmentOrderable;

  private EquipmentOrderableDto equipmentOrderableDto;

  private UUID equipmentOrderableId;

  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    equipmentOrderable = new EquipmentOrderableDataBuilder().build();
    equipmentOrderableId = equipmentOrderable.getId();
    equipmentOrderableDto = EquipmentOrderableDto.newInstance(equipmentOrderable);

    when(equipmentOrderableRepository.save(any()))
        .thenReturn(equipmentOrderable);
  }

  @Test
  public void shouldPostEquipmentOrderableDto() {
    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(equipmentOrderableDto)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_CREATED)
        .extract().as(EquipmentOrderableDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenPostWhileUserHasNoManagePermission() {
    PermissionMessageException exception = mockPermissionException(
        PermissionService.EQUIPMENT_MANAGE
    );
    doThrow(exception).when(permissionService).canManageCce();

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(equipmentOrderableDto)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_FORBIDDEN);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestOnInvalidPostRequest() {
    EquipmentOrderableDto requestBody = EquipmentOrderableDto.newInstance(
        new EquipmentOrderableDataBuilder()
            .withEquipment(null)
            .withOrderableId(null)
            .buildAsNew()
    );

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(requestBody)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST);
  }

  @Test
  public void shouldGetEquipmentOrderableById() {
    given(equipmentOrderableRepository.findById(eq(equipmentOrderableId)))
        .willReturn(Optional.of(equipmentOrderable));

    EquipmentOrderableDto response = restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .pathParam("id", equipmentOrderableId)
        .when()
        .get(ID_PATH)
        .then()
        .statusCode(200)
        .extract()
        .as(EquipmentOrderableDto.class);

    assertEquals(equipmentOrderable.getOrderableId(), response.getOrderableId());
    assertEquals(equipmentOrderable.getEquipment().getId(), response.getEquipment().getId());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldDeleteEquipmentOrderableById() {
    //given the id does not exists
    given(equipmentOrderableRepository.existsById(eq(equipmentOrderableId)))
        .willReturn(true);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .pathParam("id", equipmentOrderableId)
        .when()
        .delete(ID_PATH)
        .then()
        .statusCode(200);

    verify(equipmentOrderableRepository).deleteById(eq(equipmentOrderableId));
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }


  @Test
  public void shouldReturnNotFoundWhenDeleteEquipmentOrderableByIdNotExisting() {
    //given the id does not exists
    given(equipmentOrderableRepository.existsById(eq(equipmentOrderableId)))
        .willReturn(false);

    //when you delete
    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .pathParam("id", equipmentOrderableId)
        .when()
        .delete(ID_PATH)
        //then 404 is returned
        .then()
        .statusCode(HttpStatus.SC_NOT_FOUND);

    //the delete method on repository is never called
    verify(equipmentOrderableRepository, times(0))
        .deleteById(eq(equipmentOrderableId));
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenGetOrderableWhileUserHasNoManagePermission() {
    PermissionMessageException exception = mockPermissionException(
        PermissionService.EQUIPMENT_MANAGE
    );
    doThrow(exception).when(permissionService).canManageCce();

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .pathParam("id", equipmentOrderableId)
        .when()
        .get(ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_FORBIDDEN)
        .extract()
        .as(EquipmentOrderableDto.class);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }

  @Test
  public void getShouldReturnNotFoundForNonExistingEquipmentOrderable() {
    given(equipmentOrderableRepository.findById(eq(equipmentOrderableId)))
        .willReturn(Optional.empty());
    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .pathParam("id", UUID.randomUUID())
        .when()
        .get(ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_NOT_FOUND);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldRetrieveAllEquipmentOrderablesOnNoParameters() {
    List<EquipmentOrderable> expectedOrderables = Lists.newArrayList(equipmentOrderable);
    Page<EquipmentOrderable> expectedOrderablesPage = PaginationUtil.getPage(
        expectedOrderables,
        pageable
    );

    given(equipmentOrderableRepository.findAll(any(Pageable.class)))
        .willReturn(expectedOrderablesPage);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .queryParam(PAGE, pageable.getPageNumber())
        .queryParam(SIZE, pageable.getPageSize())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .when()
        .get(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .body(CONTENT, hasSize(expectedOrderables.size()))
        .body(CONTENT_ID, hasSize(expectedOrderables.size()))
        .body(CONTENT_ID, hasItem(equipmentOrderable.getId().toString()));

    verify(equipmentOrderableRepository).findAll(any(Pageable.class));
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldRetrieveAllEquipmentOrderablesOfSpecifiedOrderableIdParameter() {
    List<EquipmentOrderable> expectedOrderables = Lists.newArrayList(equipmentOrderable);
    Page<EquipmentOrderable> expectedOrderablesPage = PaginationUtil.getPage(
        expectedOrderables,
        pageable
    );

    given(equipmentOrderableRepository.findAllByOrderableId(
        eq(equipmentOrderableId),
        any(Pageable.class))
    ).willReturn(expectedOrderablesPage);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .queryParam(PAGE, pageable.getPageNumber())
        .queryParam(SIZE, pageable.getPageSize())
        .queryParam(PARAM_OREDERABLE_ID, equipmentOrderableId)
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .when()
        .get(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .body(CONTENT, hasSize(expectedOrderables.size()))
        .body(CONTENT_ID, hasSize(expectedOrderables.size()))
        .body(CONTENT_ID, hasItem(equipmentOrderable.getId().toString()));

    verify(equipmentOrderableRepository).findAllByOrderableId(
        eq(equipmentOrderableId),
        any(Pageable.class)
    );
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }


  @Test
  public void shouldRetrieveAllEquipmentOrderablesOfSpecifiedEquipmentIdParameter() {
    List<EquipmentOrderable> expectedOrderables = Lists.newArrayList(equipmentOrderable);
    Page<EquipmentOrderable> expectedOrderablesPage = PaginationUtil.getPage(
        expectedOrderables,
        pageable
    );

    given(equipmentOrderableRepository.findAllByEquipment_Id(
        eq(equipmentOrderable.getEquipment().getId()),
        any(Pageable.class))
    ).willReturn(expectedOrderablesPage);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .queryParam(PAGE, pageable.getPageNumber())
        .queryParam(SIZE, pageable.getPageSize())
        .queryParam(PARAM_EQUIPMENT_ID, equipmentOrderable.getEquipment().getId())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .when()
        .get(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .body(CONTENT, hasSize(expectedOrderables.size()))
        .body(CONTENT_ID, hasSize(expectedOrderables.size()))
        .body(CONTENT_ID, hasItem(equipmentOrderable.getId().toString()));

    verify(equipmentOrderableRepository).findAllByEquipment_Id(
        eq(equipmentOrderable.getEquipment().getId()),
        any(Pageable.class)
    );
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }
}
