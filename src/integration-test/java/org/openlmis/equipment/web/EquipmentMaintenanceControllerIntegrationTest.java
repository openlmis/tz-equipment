/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import com.google.common.collect.Lists;
import guru.nidi.ramltester.junit.RamlMatchers;
import java.util.List;
import java.util.UUID;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openlmis.equipment.domain.EquipmentMaintenanceRequestReason;
import org.openlmis.equipment.dto.EquipmentMaintenanceRequestReasonDto;
import org.openlmis.equipment.exception.PermissionMessageException;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.EquipmentMaintenanceRequestReasonDataBuilder;
import org.openlmis.equipment.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class EquipmentMaintenanceControllerIntegrationTest
    extends org.openlmis.equipment.web.BaseWebIntegrationTest {

  static final String RESOURCE_PATH = BASE_PATH + "/equipmentMaintenanceRequestReasons";

  private static final String ID_PATH = RESOURCE_PATH + "/{id}";

  private static final String PARAM_NAME = "name";

  private UUID reasonId;

  private EquipmentMaintenanceRequestReason maintenanceRequestReason;

  private EquipmentMaintenanceRequestReasonDto maintenanceRequestReasonDto;

  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    maintenanceRequestReason = new EquipmentMaintenanceRequestReasonDataBuilder().build();
    reasonId = maintenanceRequestReason.getId();
    maintenanceRequestReasonDto = EquipmentMaintenanceRequestReasonDto
        .newInstance(maintenanceRequestReason);
  }

  @Test
  public void shouldGetMaintenanceRequestReasonById() {
    given(requestReasonService.getEquipmentMaintenanceRequestReason(any()))
        .willReturn(maintenanceRequestReason);

    EquipmentMaintenanceRequestReasonDto response = restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .pathParam("id", reasonId)
        .when()
        .get(ID_PATH)
        .then()
        .statusCode(200)
        .extract()
        .as(EquipmentMaintenanceRequestReasonDto.class);

    assertEquals(maintenanceRequestReason.getId(), response.getId());
    assertEquals(maintenanceRequestReason.getName(), response.getName());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldPostMaintenanceRequestReasonDto() {
    when(requestReasonService.create(eq(maintenanceRequestReasonDto)))
        .thenReturn(maintenanceRequestReason);
    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(maintenanceRequestReasonDto)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_CREATED)
        .extract().as(EquipmentMaintenanceRequestReasonDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestOnInvalidPostRequest() {
    EquipmentMaintenanceRequestReasonDto requestBody =
        new EquipmentMaintenanceRequestReasonDto();

    when(requestReasonService
        .create(any(EquipmentMaintenanceRequestReason.Importer.class)))
        .thenReturn(maintenanceRequestReason);
    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(requestBody)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST);
  }


  @Test
  public void shouldPutMaintenanceRequestReasonDto() {
    when(requestReasonService.update(
        eq(reasonId), eq(maintenanceRequestReasonDto))
    ).thenReturn(maintenanceRequestReason);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam(ID, reasonId)
        .body(maintenanceRequestReasonDto)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .extract().as(EquipmentMaintenanceRequestReasonDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestOnInvalidPutRequest() {
    EquipmentMaintenanceRequestReasonDto requestBody = new EquipmentMaintenanceRequestReasonDto();

    when(requestReasonService.update(eq(reasonId), eq(maintenanceRequestReasonDto)))
        .thenReturn(maintenanceRequestReason);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam(ID, reasonId)
        .body(requestBody)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST);

  }

  @Test
  public void shouldGetAllMaintenanceRequestReason() {
    List<EquipmentMaintenanceRequestReason> expectedServiceTypes
        = Lists.newArrayList(maintenanceRequestReason);
    Page<EquipmentMaintenanceRequestReason> expectedReasonsPage = PaginationUtil.getPage(
        expectedServiceTypes, pageable
    );

    given(requestReasonService.search(any(String.class), any(Pageable.class)))
        .willReturn(expectedReasonsPage);

    given(requestReasonService.findAll(any(Pageable.class)))
        .willReturn(expectedReasonsPage);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .queryParam(PAGE, pageable.getPageNumber())
        .queryParam(SIZE, pageable.getPageSize())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .when()
        .get(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .body(CONTENT, hasSize(expectedServiceTypes.size()))
        .body(CONTENT_ID, hasSize(expectedServiceTypes.size()))
        .body(CONTENT_ID, hasItem(maintenanceRequestReason.getId().toString()));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldSearchMaintenanceRequestReasons() {
    List<EquipmentMaintenanceRequestReason> expectedReasons
        = Lists.newArrayList(maintenanceRequestReason);
    Page<EquipmentMaintenanceRequestReason> expectedServiceTypesPage = PaginationUtil.getPage(
        expectedReasons, pageable
    );

    given(requestReasonService.search(any(String.class), any(Pageable.class)))
        .willReturn(expectedServiceTypesPage);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .queryParam(PAGE, pageable.getPageNumber())
        .queryParam(SIZE, pageable.getPageSize())
        .queryParam(PARAM_NAME, maintenanceRequestReason.getName())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .when()
        .get(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .body(CONTENT, hasSize(expectedReasons.size()))
        .body(CONTENT_ID, hasSize(expectedReasons.size()))
        .body(CONTENT_ID, hasItem(maintenanceRequestReason.getId().toString()));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenPostWhileUserHasNoManagePermission() {
    PermissionMessageException exception = mockPermissionException(
        PermissionService.EQUIPMENT_MANAGE
    );
    when(requestReasonService.create(eq(maintenanceRequestReasonDto)))
        .thenThrow(exception);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(maintenanceRequestReasonDto)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(403);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenUpdateWhileUserHasNoManagePermission() {
    PermissionMessageException exception = mockPermissionException(
        PermissionService.EQUIPMENT_MANAGE
    );
    when(requestReasonService.update(eq(reasonId), eq(maintenanceRequestReasonDto)))
        .thenThrow(exception);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam("id", reasonId)
        .body(maintenanceRequestReasonDto)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(403);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }
}
