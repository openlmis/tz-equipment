/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.openlmis.equipment.domain.EquipmentEnergyType.Importer;
import static org.openlmis.equipment.i18n.EquipmentEnergyTypeMessageKeys.ERROR_NOT_FOUND;
import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import com.google.common.collect.Lists;
import guru.nidi.ramltester.junit.RamlMatchers;
import java.util.List;
import java.util.UUID;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openlmis.equipment.domain.EquipmentEnergyType;
import org.openlmis.equipment.dto.EquipmentEnergyTypeDto;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.exception.PermissionMessageException;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.EquipmentEnergyTypeDataBuilder;
import org.openlmis.equipment.util.Message;
import org.openlmis.equipment.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class EquipmentEnergyTypeControllerIntegrationTest extends BaseWebIntegrationTest {
  static final String RESOURCE_PATH = BASE_PATH + "/equipmentEnergyTypes";
  private static final String ID_PATH = RESOURCE_PATH + "/{id}";
  private static final String PARAM_CODE = "code";
  private static final String PARAM_NAME = "name";

  private EquipmentEnergyType equipmentEnergyType;
  private EquipmentEnergyTypeDto equipmentEnergyTypeDto;
  private UUID equipmentEnergyTypeId;
  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    equipmentEnergyType = new EquipmentEnergyTypeDataBuilder().build();
    equipmentEnergyTypeId = equipmentEnergyType.getId();
    equipmentEnergyTypeDto = EquipmentEnergyTypeDto.newInstance(equipmentEnergyType);

  }

  @Test
  public void shouldPostEquipmentEnergyTypeDto() {
    when(equipmentEnergyTypeService.create(eq(equipmentEnergyTypeDto)))
        .thenReturn(equipmentEnergyType);
    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(equipmentEnergyTypeDto)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_CREATED)
        .extract().as(EquipmentEnergyTypeDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestOnInvalidPostRequest() {
    EquipmentEnergyType requestBody = new EquipmentEnergyTypeDataBuilder()
        .withCode(null)
        .withName(null)
        .buildAsNew();

    when(equipmentEnergyTypeService.create(any(Importer.class))).thenReturn(equipmentEnergyType);
    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(requestBody)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST);
  }


  @Test
  public void shouldPutEquipmentEnergyTypeDto() {
    when(equipmentEnergyTypeService.update(
        eq(equipmentEnergyTypeId), eq(equipmentEnergyTypeDto))
    ).thenReturn(equipmentEnergyType);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam(ID, equipmentEnergyTypeId)
        .body(equipmentEnergyTypeDto)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .extract().as(EquipmentEnergyTypeDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestOnInvalidPutRequest() {
    EquipmentEnergyType requestBody = new EquipmentEnergyTypeDataBuilder()
        .withCode(null)
        .withName(null)
        .buildAsNew();

    when(equipmentEnergyTypeService.update(eq(equipmentEnergyTypeId), eq(equipmentEnergyTypeDto)))
        .thenReturn(equipmentEnergyType);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam(ID, equipmentEnergyTypeId)
        .body(requestBody)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST);

  }

  @Test
  public void shouldGetEquipmentEnergyTypeById() {
    given(equipmentEnergyTypeService.getEquipmentEnergyType(any()))
        .willReturn(equipmentEnergyType);

    EquipmentEnergyTypeDto response = restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .pathParam("id", equipmentEnergyTypeId)
        .when()
        .get(ID_PATH)
        .then()
        .statusCode(200)
        .extract()
        .as(EquipmentEnergyTypeDto.class);

    assertEquals(equipmentEnergyType.getName(), response.getName());
    assertEquals(equipmentEnergyType.getCode(), response.getCode());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void getShouldReturnNotFoundForNonExistingEquipmentEnergyType() {
    given(equipmentEnergyTypeService.getEquipmentEnergyType(any()))
        .willThrow(new NotFoundException(new Message(ERROR_NOT_FOUND)));

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .pathParam("id", equipmentEnergyTypeId)
        .when()
        .get(ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_NOT_FOUND);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldSearchEquipmentEnergyTypes() {
    List<EquipmentEnergyType> expectedEnergyTypes = Lists.newArrayList(equipmentEnergyType);
    Page<EquipmentEnergyType> expectedEnergyTypesPage = PaginationUtil.getPage(
        expectedEnergyTypes,
        pageable
    );

    given(equipmentEnergyTypeService.search(any(), any(), any(Pageable.class)))
        .willReturn(expectedEnergyTypesPage);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .queryParam(PAGE, pageable.getPageNumber())
        .queryParam(SIZE, pageable.getPageSize())
        .queryParam(PARAM_CODE, equipmentEnergyType.getCode())
        .queryParam(PARAM_NAME, equipmentEnergyType.getName())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .when()
        .get(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .body(CONTENT, hasSize(expectedEnergyTypes.size()))
        .body(CONTENT_ID, hasSize(expectedEnergyTypes.size()))
        .body(CONTENT_ID, hasItem(equipmentEnergyType.getId().toString()));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenPostWhileUserHasNoManagePermission() {
    PermissionMessageException exception = mockPermissionException(
        PermissionService.EQUIPMENT_MANAGE
    );
    when(equipmentEnergyTypeService.create(eq(equipmentEnergyTypeDto)))
        .thenThrow(exception);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(equipmentEnergyTypeDto)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(403);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenUpdateWhileUserHasNoManagePermission() {
    PermissionMessageException exception = mockPermissionException(
        PermissionService.EQUIPMENT_MANAGE
    );
    when(equipmentEnergyTypeService.update(eq(equipmentEnergyTypeId), eq(equipmentEnergyTypeDto)))
        .thenThrow(exception);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam("id", equipmentEnergyTypeId)
        .body(equipmentEnergyTypeDto)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(403);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }
}
