/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.openlmis.equipment.domain.Vendor.Importer;
import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import com.google.common.collect.Lists;
import guru.nidi.ramltester.junit.RamlMatchers;
import java.util.List;
import java.util.UUID;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openlmis.equipment.domain.Vendor;
import org.openlmis.equipment.dto.VendorDto;
import org.openlmis.equipment.exception.PermissionMessageException;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.PaginationUtil;
import org.openlmis.equipment.util.VendorDataBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class VendorControllerIntegrationTest extends BaseWebIntegrationTest {

  static final String RESOURCE_PATH = BASE_PATH + "/vendors";

  private static final String ID_PATH = RESOURCE_PATH + "/{id}";

  private static final String PARAM_NAME = "name";

  private UUID vendorId;

  private Vendor vendor;

  private VendorDto vendorDto;

  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    vendor = new VendorDataBuilder().build();
    vendorId = vendor.getId();
    vendorDto = VendorDto.newInstance(vendor);
  }

  @Test
  public void shouldGetVendorById() {
    given(vendorService.getVendor(any()))
        .willReturn(vendor);

    VendorDto response = restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .pathParam("id", vendorId)
        .when()
        .get(ID_PATH)
        .then()
        .statusCode(200)
        .extract()
        .as(VendorDto.class);

    assertEquals(vendor.getId(), response.getId());
    assertEquals(vendor.getName(), response.getName());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldPostVendorDto() {
    when(vendorService.create(eq(vendorDto))).thenReturn(vendor);
    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(vendorDto)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_CREATED)
        .extract().as(VendorDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestOnInvalidPostRequest() {
    VendorDto requestBody = new VendorDto();

    when(vendorService.create(any(Importer.class))).thenReturn(vendor);
    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(requestBody)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST);
  }


  @Test
  public void shouldPutVendorDto() {
    when(vendorService.update(
        eq(vendorId), eq(vendorDto))
    ).thenReturn(vendor);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam(ID, vendorId)
        .body(vendorDto)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .extract().as(VendorDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestOnInvalidPutRequest() {
    VendorDto requestBody = new VendorDto();

    when(vendorService.update(eq(vendorId), eq(vendorDto)))
        .thenReturn(vendor);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam(ID, vendorId)
        .body(requestBody)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST);

  }

  @Test
  public void shouldGetAllVendors() {
    List<Vendor> expectedVendors = Lists.newArrayList(vendor);
    Page<Vendor> expectedVendorsPage = PaginationUtil.getPage(expectedVendors, pageable);

    given(vendorService.search(any(String.class), any(Pageable.class)))
        .willReturn(expectedVendorsPage);

    given(vendorService.findAll(any(Pageable.class)))
        .willReturn(expectedVendorsPage);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .queryParam(PAGE, pageable.getPageNumber())
        .queryParam(SIZE, pageable.getPageSize())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .when()
        .get(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .body(CONTENT, hasSize(expectedVendors.size()))
        .body(CONTENT_ID, hasSize(expectedVendors.size()))
        .body(CONTENT_ID, hasItem(vendor.getId().toString()));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldSearchVendors() {
    List<Vendor> expectedVendors = Lists.newArrayList(vendor);
    Page<Vendor> expectedVendorsPage = PaginationUtil.getPage(expectedVendors, pageable);

    given(vendorService.search(any(String.class), any(Pageable.class)))
        .willReturn(expectedVendorsPage);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .queryParam(PAGE, pageable.getPageNumber())
        .queryParam(SIZE, pageable.getPageSize())
        .queryParam(PARAM_NAME, vendor.getName())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .when()
        .get(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .body(CONTENT, hasSize(expectedVendors.size()))
        .body(CONTENT_ID, hasSize(expectedVendors.size()))
        .body(CONTENT_ID, hasItem(vendor.getId().toString()));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenPostWhileUserHasNoManagePermission() {
    PermissionMessageException exception = mockPermissionException(
        PermissionService.EQUIPMENT_MANAGE
    );
    when(vendorService.create(eq(vendorDto)))
        .thenThrow(exception);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(vendorDto)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(403);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenUpdateWhileUserHasNoManagePermission() {
    PermissionMessageException exception = mockPermissionException(
        PermissionService.EQUIPMENT_MANAGE
    );
    when(vendorService.update(eq(vendorId), eq(vendorDto)))
        .thenThrow(exception);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam("id", vendorId)
        .body(vendorDto)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(403);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }
}
