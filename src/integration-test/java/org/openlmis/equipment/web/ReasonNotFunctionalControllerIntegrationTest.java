/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.openlmis.equipment.domain.ReasonNotFunctional.Importer;
import static org.openlmis.equipment.i18n.ReasonNotFunctionalMessageKeys.ERROR_NOT_FOUND;
import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import com.google.common.collect.Lists;
import guru.nidi.ramltester.junit.RamlMatchers;
import java.util.List;
import java.util.UUID;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openlmis.equipment.domain.ReasonNotFunctional;
import org.openlmis.equipment.dto.ReasonNotFunctionalDto;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.exception.PermissionMessageException;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.Message;
import org.openlmis.equipment.util.PaginationUtil;
import org.openlmis.equipment.util.ReasonNotFunctionalDataBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class ReasonNotFunctionalControllerIntegrationTest extends BaseWebIntegrationTest {
  static final String RESOURCE_PATH = BASE_PATH + "/reasonsNotFunctional";
  private static final String ID_PATH = RESOURCE_PATH + "/{id}";
  private static final String PARAM_NAME = "name";

  private ReasonNotFunctional reasonNotFunctional;
  private ReasonNotFunctionalDto reasonNotFunctionalDto;
  private UUID reasonNotFunctionalId;
  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    reasonNotFunctional = new ReasonNotFunctionalDataBuilder().build();
    reasonNotFunctionalId = reasonNotFunctional.getId();
    reasonNotFunctionalDto = ReasonNotFunctionalDto.newInstance(reasonNotFunctional);

  }

  @Test
  public void shouldPostReasonNotFunctionalDto() {
    when(reasonNotFunctionalService.create(eq(reasonNotFunctionalDto)))
        .thenReturn(reasonNotFunctional);
    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(reasonNotFunctionalDto)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_CREATED)
        .extract().as(ReasonNotFunctionalDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestOnInvalidPostRequest() {
    ReasonNotFunctional requestBody = new ReasonNotFunctionalDataBuilder()
        .withName(null)
        .buildAsNew();

    when(reasonNotFunctionalService.create(any(Importer.class))).thenReturn(reasonNotFunctional);
    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(requestBody)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST);
  }


  @Test
  public void shouldPutReasonNotFunctionalDto() {
    when(reasonNotFunctionalService.update(
        eq(reasonNotFunctionalId), eq(reasonNotFunctionalDto))
    ).thenReturn(reasonNotFunctional);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam(ID, reasonNotFunctionalId)
        .body(reasonNotFunctionalDto)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .extract().as(ReasonNotFunctionalDto.class);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnBadRequestOnInvalidPutRequest() {
    ReasonNotFunctional requestBody = new ReasonNotFunctionalDataBuilder()
        .withName(null)
        .buildAsNew();

    when(reasonNotFunctionalService.update(eq(reasonNotFunctionalId), eq(reasonNotFunctionalDto)))
        .thenReturn(reasonNotFunctional);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam(ID, reasonNotFunctionalId)
        .body(requestBody)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_BAD_REQUEST);

  }

  @Test
  public void shouldGetReasonNotFunctionalById() {
    given(reasonNotFunctionalService.getReasonNotFunctional(any()))
        .willReturn(reasonNotFunctional);

    ReasonNotFunctionalDto response = restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .pathParam("id", reasonNotFunctionalId)
        .when()
        .get(ID_PATH)
        .then()
        .statusCode(200)
        .extract()
        .as(ReasonNotFunctionalDto.class);

    assertEquals(reasonNotFunctional.getName(), response.getName());
    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void getShouldReturnNotFoundForNonExistingReasonNotFunctional() {
    given(reasonNotFunctionalService.getReasonNotFunctional(any()))
        .willThrow(new NotFoundException(new Message(ERROR_NOT_FOUND)));

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .pathParam("id", reasonNotFunctionalId)
        .when()
        .get(ID_PATH)
        .then()
        .statusCode(HttpStatus.SC_NOT_FOUND);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldSearchReasonNotFunctional() {
    List<ReasonNotFunctional> expectedReasons = Lists.newArrayList(reasonNotFunctional);
    Page<ReasonNotFunctional> expectedReasonsPage = PaginationUtil.getPage(
        expectedReasons,
        pageable
    );

    given(reasonNotFunctionalService.search(any(), any(Pageable.class)))
        .willReturn(expectedReasonsPage);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .queryParam(PAGE, pageable.getPageNumber())
        .queryParam(SIZE, pageable.getPageSize())
        .queryParam(PARAM_NAME, reasonNotFunctional.getName())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .when()
        .get(RESOURCE_PATH)
        .then()
        .statusCode(HttpStatus.SC_OK)
        .body(CONTENT, hasSize(expectedReasons.size()))
        .body(CONTENT_ID, hasSize(expectedReasons.size()))
        .body(CONTENT_ID, hasItem(reasonNotFunctional.getId().toString()));

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenPostWhileUserHasNoManagePermission() {
    PermissionMessageException exception = mockPermissionException(
        PermissionService.EQUIPMENT_MANAGE
    );
    when(reasonNotFunctionalService.create(eq(reasonNotFunctionalDto)))
        .thenThrow(exception);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(reasonNotFunctionalDto)
        .when()
        .post(RESOURCE_PATH)
        .then()
        .statusCode(403);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }

  @Test
  public void shouldReturnUnauthorizedWhenUpdateWhileUserHasNoManagePermission() {
    PermissionMessageException exception = mockPermissionException(
        PermissionService.EQUIPMENT_MANAGE
    );
    when(reasonNotFunctionalService.update(eq(reasonNotFunctionalId), eq(reasonNotFunctionalDto)))
        .thenThrow(exception);

    restAssured
        .given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .pathParam("id", reasonNotFunctionalId)
        .body(reasonNotFunctionalDto)
        .when()
        .put(ID_PATH)
        .then()
        .statusCode(403);

    Assert.assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(),
        RamlMatchers.hasNoViolations());
  }
}
