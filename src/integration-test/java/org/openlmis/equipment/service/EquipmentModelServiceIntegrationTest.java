/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.service;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.google.common.collect.Sets;
import java.util.Set;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.openlmis.equipment.domain.EquipmentModel;
import org.openlmis.equipment.domain.EquipmentType;
import org.openlmis.equipment.dto.EquipmentModelDto;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.repository.EquipmentModelRepository;
import org.openlmis.equipment.repository.EquipmentTypeRepository;
import org.openlmis.equipment.service.EquipmentModelService.SearchParams;
import org.openlmis.equipment.util.EquipmentModelDataBuilder;
import org.openlmis.equipment.util.EquipmentTypeDataBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Transactional
@SuppressWarnings({"PMD.UnusedPrivateField"})
public class EquipmentModelServiceIntegrationTest {
  @MockBean
  private PermissionService permissionService;
  @Autowired
  private EquipmentModelService equipmentModelService;
  @Autowired
  private EquipmentModelRepository equipmentModelRepository;
  @Autowired
  private EquipmentTypeRepository equipmentTypeRepository;

  private EquipmentType equipmentType;
  private EquipmentModelDataBuilder modelDataBuilder;
  private EquipmentTypeDataBuilder typeDataBuilder;
  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    equipmentModelRepository.deleteAll();
    equipmentTypeRepository.deleteAll();

    typeDataBuilder = new EquipmentTypeDataBuilder();
    equipmentType = equipmentTypeRepository.save(typeDataBuilder.buildAsNew());
    modelDataBuilder = new EquipmentModelDataBuilder().withEquipmentType(equipmentType);
  }

  @Test
  public void shouldCreateNewEquipmentModel() {
    long originalCount = equipmentModelRepository.count();

    EquipmentModel createdModel = createEquipmentModel();

    long newCount = equipmentModelRepository.count();
    assertThat(newCount).isGreaterThan(originalCount);
    assertThat(createdModel.getId()).isNotNull();
  }

  @Test
  public void shouldUpdateExistingEquipmentModel() {
    EquipmentModel createdModel = createEquipmentModel();

    EquipmentModelDto updateDto = EquipmentModelDto.newInstance(createdModel);
    updateDto.setName(createdModel.getName() + " Updated");
    updateDto.setCode(createdModel.getCode() + " Updated");

    EquipmentModel updatedModel = equipmentModelService.update(
        createdModel.getId(),
        updateDto
    );

    updatedModel = equipmentModelService.getEquipmentModel(createdModel.getId());

    Assertions.assertThat(updateDto.getName()).isEqualTo(updatedModel.getName());
    Assertions.assertThat(updateDto.getCode()).isEqualTo(updatedModel.getCode());
  }

  @Test
  public void shouldGetEquipmentModelWhenIdExists() {
    EquipmentModel createdModel = createEquipmentModel();
    EquipmentModel foundModel = equipmentModelService.getEquipmentModel(createdModel.getId());
    assertThat(createdModel).isEqualTo(foundModel);
  }

  @Test
  public void shouldSearchEquipmentModels() {
    EquipmentModel model1 = createEquipmentModel(equipmentType);
    EquipmentModel model2 = createEquipmentModel(equipmentType);
    EquipmentModel model3 = createEquipmentModel(equipmentType);

    Set<UUID> searchIds = Sets.newHashSet(model1.getId(), model2.getId());
    SearchParams searchParams = Mockito.mock(SearchParams.class);
    when(searchParams.getIds()).thenReturn(searchIds);

    Page<EquipmentModel> models = equipmentModelService.search(searchParams, pageable);

    assertThat(models).allMatch(model -> searchIds.contains(model.getId()));
    assertThat(models).allMatch(model -> !model.getId().equals(model3.getId()));
  }

  @Test(expected = NotFoundException.class)
  public void shouldFailGettingEquipmentModelWhenIdNotExists() {
    equipmentModelService.getEquipmentModel(UUID.randomUUID());
  }

  private EquipmentModel createEquipmentModel() {
    return createEquipmentModel(equipmentType);
  }

  private EquipmentModel createEquipmentModel(EquipmentType type) {
    return createEquipmentModel(null, null, type);
  }

  private EquipmentModel createEquipmentModel(String name, String code, EquipmentType type) {
    if (isNotBlank(name)) {
      modelDataBuilder = modelDataBuilder.withName(name);
    }
    if (isNotBlank(code)) {
      modelDataBuilder = modelDataBuilder.withCode(code);
    }
    if (type != null) {
      modelDataBuilder = modelDataBuilder.withEquipmentType(type);
    }

    EquipmentModelDto modelDto = EquipmentModelDto.newInstance(
        modelDataBuilder.buildAsNew()
    );
    EquipmentModel createdModel = equipmentModelService.create(modelDto);
    return createdModel;
  }

}
