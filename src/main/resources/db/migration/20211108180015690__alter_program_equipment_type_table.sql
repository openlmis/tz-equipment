
ALTER TABLE ONLY equipment_type_programs
    ADD COLUMN IF NOT EXISTS enabletestcount BOOLEAN default false;

ALTER TABLE ONLY equipment_type_programs
    ADD COLUMN IF NOT EXISTS enabletotalcolumn BOOLEAN default false;


ALTER TABLE ONLY equipment_type_programs
    ADD COLUMN IF NOT EXISTS displayorder integer NOT NULL;