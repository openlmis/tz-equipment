CREATE TABLE equipment_inventory
(
    id uuid NOT NULL,
    equipmentId uuid NOT NULL,
    facilityId uuid NOT NULL,
    programId uuid NOT NULL,
    functionalStatus     text NOT NULL,
    reasonNotFunctionalId uuid,
    utilization          text NOT NULL,
    additionalNotes      text,
    decommissionDate     date,
    modifiedDate         timestamp with time zone,
    createdDate          timestamp with time zone,
    equipmentTrackingId  text NOT NULL,
    source               text,
    dateOfInstallation   date NOT NULL,
    dateOfWarrantyExpiry date,
    purchasePrice        double precision,
    registrationStatus   text NOT NULL
);

ALTER TABLE ONLY equipment_inventory
    ADD CONSTRAINT equipment_inventory_pkey PRIMARY KEY (id);

ALTER TABLE ONLY equipment_inventory
    ADD CONSTRAINT frk_equipment_id
    FOREIGN KEY (equipmentId)
    REFERENCES equipments (id);

ALTER TABLE ONLY equipment_inventory
    ADD CONSTRAINT frk_reason_not_functional_id
    FOREIGN KEY (reasonNotFunctionalId)
    REFERENCES reasons_not_functional (id);