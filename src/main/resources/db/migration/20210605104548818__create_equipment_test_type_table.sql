CREATE TABLE equipment_test_types
(
    id             uuid    NOT NULL,
    code           text    NOT NULL,
    name           text    NOT NULL,
    displayOrder integer NOT NULL,
    enabled         boolean NOT NULL,
    modifiedDate   timestamp with time zone,
    equipmentCategoryId uuid NOT NULL,
    lastModifier   uuid
);

ALTER TABLE ONLY equipment_test_types
    ADD CONSTRAINT frk_equipment_category_id FOREIGN KEY (equipmentCategoryId)
    REFERENCES equipment_categories(id);