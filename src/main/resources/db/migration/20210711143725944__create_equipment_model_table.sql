CREATE TABLE equipment_models
(
    id                uuid NOT NULL,
    code              text NOT NULL,
    name              text NOT NULL,
    equipmentTypeId uuid NOT NULL,
    modifiedDate      timestamp with time zone,
    createdDate timestamp with time zone,
    lastModifier      uuid
);

ALTER TABLE ONLY equipment_models
    ADD CONSTRAINT frk_equipment_type_id
        FOREIGN KEY (equipmentTypeId)
            REFERENCES equipment_types (id);