alter table equipment_maintenance_requests
    drop column approved;

alter table equipment_service_logs
    add approved boolean default null;

