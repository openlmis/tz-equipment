ALTER TABLE ONLY equipment_inventory
    RENAME TO equipment_inventory_items;

ALTER TABLE ONLY equipment_catalog
    RENAME TO equipment_catalog_items;

ALTER TABLE ONLY equipment_inventory_items
    ADD CONSTRAINT fk_inventory_catalog FOREIGN KEY (catalogitemid) REFERENCES equipment_catalog_items(id);

ALTER TABLE ONLY equipment_inventory_items
    ADD CONSTRAINT unq_inventory_catalog_eqid UNIQUE (catalogitemid, equipmenttrackingid);
