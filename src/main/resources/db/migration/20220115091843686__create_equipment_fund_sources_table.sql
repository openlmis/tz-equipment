CREATE TABLE equipment_fund_sources
(
    id                uuid NOT NULL,
    code              text NOT NULL,
    name              text NOT NULL,
    modifiedDate      timestamp with time zone,
    createdDate timestamp with time zone,
    lastModifier      uuid
);