ALTER TABLE ONLY equipment_models
    ADD CONSTRAINT equipment_model_pkey PRIMARY KEY (id);

ALTER TABLE ONLY equipment_energy_types
    ADD CONSTRAINT equipment_energy_type_pkey PRIMARY KEY (id);