ALTER TABLE ONLY equipment_type_programs
    ADD CONSTRAINT unq_equipment_type_programs UNIQUE (equipmenttypeid, programid);