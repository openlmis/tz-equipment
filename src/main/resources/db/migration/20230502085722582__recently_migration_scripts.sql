DROP table if exists equipment.contract_service_types;
DROP TABLE IF EXISTS equipment.contract_equipments;
DROP TABLE IF EXISTS equipment.contract_facilities;
DROP TABLE IF EXISTS equipment.contracts;
DROP TABLE IF EXISTS equipment.service_types;
DROP TABLE IF EXISTS equipment.equipment_service_logs;
DROP TABLE IF EXISTS equipment.equipment_maintenance_requests CASCADE;
DROP TABLE IF EXISTS equipment.equipment_maintenance_request_reasons CASCADE;
DROP TABLE IF EXISTS equipment.vendor_associated_users;
DROP TABLE IF EXISTS equipment.vendor;
CREATE TABLE equipment.vendor
(
    id uuid NOT NULL,
    modifiedDate         timestamp with time zone,
    createdDate          timestamp with time zone,
    name     text NOT NULL,
    website     text NOT NULL,
    contactPerson     text NOT NULL,
    primaryPhone     text NOT NULL,
    email     text NOT NULL,
    description     text NOT NULL,
    specialization     text NOT NULL,
    geographicalCoverage     text NOT NULL
);

ALTER TABLE ONLY equipment.vendor
    ADD CONSTRAINT vendor_pkey PRIMARY KEY (id);

CREATE TABLE equipment.vendor_associated_users
(
    vendorId uuid NOT NULL,
    associatedUser uuid NOT NULL
);

ALTER TABLE ONLY equipment.vendor_associated_users
    ADD CONSTRAINT fk_user_vendor
    foreign key (vendorid) references equipment.vendor (id);


--second script

CREATE TABLE equipment.service_types
(
    id uuid NOT NULL,
    name     text NOT NULL,
    description     text NOT NULL
);

ALTER TABLE ONLY equipment.service_types
    ADD CONSTRAINT service_types_pkey PRIMARY KEY (id);

--third script

create table equipment.equipment_maintenance_request_reasons
(
    id   UUID not null,
    code text NOT NULL ,
    name text not null
);

ALTER TABLE ONLY equipment.equipment_maintenance_request_reasons
    ADD CONSTRAINT equipment_maintenance_request_reasons_pkey PRIMARY KEY (id);

--forth
create table equipment.equipment_maintenance_requests (
                                                id UUID NOT NULL,
                                                inventoryId UUID not null,
                                                vendorId UUID NOT NULL,
                                                requestDate date,
                                                recommendedDate date,
                                                comment text,
                                                resolved boolean default false,
                                                vendorComment text,
                                                createdDate timestamp default now(),
                                                modifiedDate timestamp default now(),
                                                breakDownDate date,
                                                approved boolean default false,
                                                reasonId UUID NOT NULL
);

ALTER TABLE ONLY equipment.equipment_maintenance_requests
    ADD CONSTRAINT equipment_maintenance_requests_pkey PRIMARY KEY (id);

ALTER TABLE ONLY equipment.equipment_maintenance_requests
    ADD CONSTRAINT fk_reason_maintainance_request
    foreign key (reasonId) references equipment.equipment_maintenance_request_reasons (id);

ALTER TABLE ONLY equipment.equipment_maintenance_requests
    ADD CONSTRAINT fk_maintenance_requests_vendor
    foreign key (vendorid) references equipment.vendor (id);

ALTER TABLE ONLY equipment.equipment_maintenance_requests
    ADD CONSTRAINT fk_maintenance_requests_equipment_inventory
    foreign key (inventoryId) references equipment.equipment_inventory (id);

--Fifth

create table if not exists equipment.equipment_service_logs
(
    id UUID NOT NULL,
    requestId UUID NOT NULL,
    maintenancedate  date,
    serviceperformed TEXT  NOT NULL,
    finding          TEXT NOT NULL,
    recommendation  TEXT,
    nextvisitdate    date,
    createddate      timestamp default now(),
    modifieddate     timestamp default now()
    );

ALTER TABLE ONLY equipment.equipment_service_logs
    ADD CONSTRAINT equipment_service_logs_pkey PRIMARY KEY (id);

ALTER TABLE ONLY equipment.equipment_service_logs
    ADD CONSTRAINT fk_equipment_service_logs_requestId
    foreign key (requestId) references equipment.equipment_maintenance_requests(id);


--additional queries

CREATE TABLE equipment.contracts
(
    id uuid NOT NULL,
    modifiedDate timestamp with time zone,
    createdDate  timestamp with time zone,
    vendorid uuid NOT NULL,
    contractdate date NOT NULL,
    endDate      date NOT NULL,
    startDate    date NOT NULL,
    identifier   text NOT NULL,
    coverage     text NOT NULL,
    description  text NOT NULL,
    terms        text NOT NULL
);

CREATE TABLE equipment.contract_facilities
(
    contractid uuid NOT NULL,
    facilityid uuid NOT NULL
);

CREATE TABLE equipment.contract_service_types
(
    contractid uuid NOT NULL,
    servicetypeid uuid NOT NULL
);

CREATE TABLE equipment.contract_equipments
(
    contractid uuid NOT NULL,
    equipmentid uuid NOT NULL
);

ALTER TABLE ONLY equipment.contracts
    ADD CONSTRAINT contract_pkey PRIMARY KEY (id);

ALTER TABLE ONLY equipment.contract_facilities
    ADD CONSTRAINT fk_facility_contract
    foreign key (contractid) references equipment.contracts (id);


ALTER TABLE ONLY equipment.contract_service_types
    ADD CONSTRAINT fk_service_type_contract
    foreign key (contractid) references equipment.contracts (id);

ALTER TABLE ONLY equipment.contract_service_types
    ADD CONSTRAINT fk_contract_service_type
    foreign key (servicetypeid) references equipment.service_types (id);

ALTER TABLE ONLY equipment.contract_equipments
    ADD CONSTRAINT fk_equipment_contract
    foreign key (contractid) references equipment.contracts (id);

ALTER TABLE ONLY equipment.contract_equipments
    ADD CONSTRAINT fk_contract_equipment
    foreign key (equipmentid) references equipment.equipments (id);
