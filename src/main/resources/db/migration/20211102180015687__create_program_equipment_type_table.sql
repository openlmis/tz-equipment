CREATE TABLE equipment_type_programs
(
    id             uuid NOT NULL,
    equipmenttypeid uuid NOT NULL,
    programid uuid NOT NULL,
    modifieddate   timestamp with time zone,
    lastModifier   uuid
);

ALTER TABLE ONLY equipment_type_programs
    ADD CONSTRAINT equipment_type_program_pkey PRIMARY KEY (id);

ALTER TABLE ONLY equipment_type_programs
    ADD CONSTRAINT frk_equipment_type_program_id FOREIGN KEY (equipmenttypeid)
    REFERENCES equipment_types(id);
