DROP TABLE IF EXISTS equipment_orderables;

CREATE TABLE equipment_orderables
(
    id                 uuid NOT NULL,
    equipmentid        uuid NOT NULL,
    orderableid        uuid NOT NULL
);

ALTER TABLE ONLY equipment_orderables
    DROP CONSTRAINT if exists equipment_orderables_pkey;

ALTER TABLE ONLY equipment_orderables
    ADD CONSTRAINT equipment_orderables1_pkey PRIMARY KEY (id);



--
-- Name: equipment_orderables_unique_equipmentid_orderableid; Type: CONSTRAINT; Schema: cce; Owner: postgres; Tablespace:
--

ALTER TABLE ONLY equipment_orderables
    ADD CONSTRAINT equipment_orderables_unique_equipmentid_orderableid UNIQUE (equipmentid, orderableid);