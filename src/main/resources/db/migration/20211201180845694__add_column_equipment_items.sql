
ALTER TABLE ONLY equipment_inventory_items
    ADD COLUMN IF NOT EXISTS reasonnotfunctional text;
