CREATE TABLE equipments
(
    id                uuid NOT NULL,
    name              text NOT NULL,
    manufacturer      text NOT NULL,
    energyTypeId      uuid NOT NULL,
    modelId           uuid NOT NULL,
    categoryId        uuid NOT NULL,
    modifiedDate      timestamp with time zone,
    createdDate       timestamp with time zone,
    lastModifier      uuid
);

ALTER TABLE ONLY equipments
    ADD CONSTRAINT equipments_pkey PRIMARY KEY (id);

ALTER TABLE ONLY equipments
    ADD CONSTRAINT frk_equipment_energy_type_id
    FOREIGN KEY (energyTypeId)
    REFERENCES equipment_energy_types (id);

ALTER TABLE ONLY equipments
    ADD CONSTRAINT frk_equipment_category_id
    FOREIGN KEY (categoryId)
    REFERENCES equipment_categories (id);

ALTER TABLE ONLY equipments
    ADD CONSTRAINT frk_equipment_model_id
    FOREIGN KEY (modelId)
    REFERENCES equipment_models (id);