CREATE TABLE equipment_category_disciplines
(
    equipmentcategoryid uuid NOT NULL,
    disciplineid uuid NOT NULL
);

ALTER TABLE ONLY equipment_category_disciplines
    ADD CONSTRAINT fk_discipline_equipment_category
    foreign key (equipmentcategoryid) references equipment_categories (id);

ALTER TABLE ONLY equipment_category_disciplines
    ADD CONSTRAINT fk_equipment_category_discipline
    foreign key (disciplineid) references equipment_disciplines (id);

alter table equipment_categories
    drop column disciplineid;