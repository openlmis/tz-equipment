ALTER TABLE ONLY equipment_fund_sources
    ADD CONSTRAINT equipment_fund_sources_pkey PRIMARY KEY (id);

ALTER TABLE ONLY equipment_inventory
    ADD COLUMN IF NOT EXISTS fundSourceId uuid;

ALTER TABLE ONLY equipment_inventory
    ADD CONSTRAINT frk_fund_source_id
    FOREIGN KEY (fundSourceId)
    REFERENCES equipment_fund_sources (id);