CREATE TABLE reasons_not_functional
(
    id uuid NOT NULL,
    name         text    NOT NULL,
    displayorder integer NOT NULL,
    modifiedDate timestamp with time zone,
    createdDate  timestamp with time zone,
    lastModifier uuid
);

ALTER TABLE ONLY reasons_not_functional
    ADD CONSTRAINT reasons_not_functional_pkey PRIMARY KEY (id);

ALTER TABLE equipment_inventory_items
    DROP COLUMN reasonNotWorkingOrNotInUse;

ALTER TABLE equipment_inventory_items
    ADD COLUMN reasonNotFunctionalId uuid NULL;

ALTER TABLE equipment_inventory_items
    ADD CONSTRAINT frk_reason_not_functional_id
    FOREIGN KEY (reasonNotFunctionalId)
    REFERENCES reasons_not_functional (id);