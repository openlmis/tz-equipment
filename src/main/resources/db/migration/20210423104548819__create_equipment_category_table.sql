CREATE TABLE equipment_categories
(
    id             uuid    NOT NULL,
    code           text    NOT NULL,
    name           text,
    displayorder integer NOT NULL,
    enabled         boolean NOT NULL,
    modifieddate   timestamp with time zone,
    lastModifier   uuid
);