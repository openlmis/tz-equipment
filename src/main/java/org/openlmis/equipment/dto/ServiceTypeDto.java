/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.dto;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.equipment.domain.ServiceType;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ServiceTypeDto extends BaseDto
    implements ServiceType.Importer, ServiceType.Exporter {

  @NotBlank
  private String name;

  @NotBlank
  private String description;

  /**
   * Creates a new ServiceType DTO from the entity.
   *
   * @param serviceType the service type to export into DTO
   *
   * @return created ServiceType DTO
   */
  public static ServiceTypeDto newInstance(ServiceType serviceType) {
    ServiceTypeDto serviceTypeDto = new ServiceTypeDto();
    serviceType.export(serviceTypeDto);
    return serviceTypeDto;
  }

}
