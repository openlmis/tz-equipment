/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.dto;

import java.time.LocalDate;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.equipment.domain.EquipmentInventory;
import org.openlmis.equipment.domain.EquipmentMaintenanceRequest;
import org.openlmis.equipment.domain.EquipmentMaintenanceRequestReason;
import org.openlmis.equipment.domain.Vendor;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class EquipmentMaintenanceRequestDto
    extends BaseDto
    implements EquipmentMaintenanceRequest.Exporter, EquipmentMaintenanceRequest.Importer {

  @NotNull
  private EquipmentInventoryDto inventory;

  @NotNull
  private VendorDto vendor;

  private LocalDate requestDate;

  private LocalDate recommendedDate;

  private String comment;

  private Boolean resolved;

  private String vendorComment;

  private LocalDate breakDownDate;

  @NotNull
  private EquipmentMaintenanceRequestReasonDto reason;

  /**
   * Creates a new equipment maintenance request DTO from the entity.
   *
   * @param request the equipment maintenance request to export into DTO
   *
   * @return created equipment maintenance request DTO
   */
  public static EquipmentMaintenanceRequestDto newInstance(EquipmentMaintenanceRequest request) {
    EquipmentMaintenanceRequestDto requestDto = new EquipmentMaintenanceRequestDto();
    request.export(requestDto);
    return requestDto;
  }

  @Override
  public void setInventory(EquipmentInventory inventory) {
    if (inventory != null) {
      this.inventory = EquipmentInventoryDto.newInstance(inventory);
    } else {
      this.inventory = null;
    }
  }

  @Override
  public void setVendor(Vendor vendor) {
    if (vendor != null) {
      this.vendor = VendorDto.newInstance(vendor);
    } else {
      this.vendor = null;
    }
  }

  @Override
  public void setReason(EquipmentMaintenanceRequestReason reason) {
    if (reason != null) {
      this.reason = EquipmentMaintenanceRequestReasonDto.newInstance(reason);
    } else {
      this.reason = null;
    }
  }
}


