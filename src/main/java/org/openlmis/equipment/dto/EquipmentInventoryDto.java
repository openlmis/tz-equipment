/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.dto;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.equipment.domain.Equipment;
import org.openlmis.equipment.domain.EquipmentFundSource;
import org.openlmis.equipment.domain.EquipmentInventory;
import org.openlmis.equipment.domain.FunctionalStatus;
import org.openlmis.equipment.domain.ReasonNotFunctional;
import org.openlmis.equipment.domain.RegistrationStatus;
import org.openlmis.equipment.domain.Utilization;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class EquipmentInventoryDto
    extends BaseDto
    implements EquipmentInventory.Exporter, EquipmentInventory.Importer {

  @NotNull
  private EquipmentDto equipment;

  @NotNull
  private UUID facilityId;

  @NotNull
  private UUID programId;

  @NotNull
  private EquipmentFundSourceDto fundSource;

  @NotNull
  private FunctionalStatus functionalStatus;

  private ReasonNotFunctionalDto reasonNotFunctional;

  @NotNull
  private Utilization utilization;

  private String additionalNotes;

  private LocalDate decommissionDate;

  @NotNull
  private String equipmentTrackingId;

  private String source;

  @NotNull
  private LocalDate dateOfInstallation;

  private LocalDate dateOfWarrantyExpiry;

  private double purchasePrice;

  @NotNull
  private RegistrationStatus registrationStatus;

  /**
   * Creates a new equipment inventory DTO from the entity.
   *
   * @param equipment the equipment inventory to export into DTO
   *
   * @return created Equipment inventory DTO
   */
  public static EquipmentInventoryDto newInstance(EquipmentInventory equipment) {
    EquipmentInventoryDto equipmentInventoryDto = new EquipmentInventoryDto();
    equipment.export(equipmentInventoryDto);
    return equipmentInventoryDto;
  }

  /**
   * Creates a list of equipment inventory DTO from the entities.
   *
   * @param equipments the equipment inventory list to export into DTOs
   *
   * @return created Equipment inventory DTO list
   */
  public static List<EquipmentInventoryDto> toList(List<EquipmentInventory> equipments) {
    return equipments.stream()
        .map(e -> newInstance(e))
        .collect(Collectors.toList());
  }

  @Override
  public void setFundSource(EquipmentFundSource fundSource) {
    if (fundSource != null) {
      this.fundSource = EquipmentFundSourceDto.newInstance(fundSource);
    } else {
      this.fundSource = null;
    }
  }

  @Override
  public void setEquipment(Equipment equipment) {
    if (equipment != null) {
      this.equipment = EquipmentDto.newInstance(equipment);
    } else {
      this.equipment = null;
    }
  }

  @Override
  public void setReasonNotFunctional(ReasonNotFunctional reason) {
    if (reason != null) {
      this.reasonNotFunctional = ReasonNotFunctionalDto.newInstance(reason);
    } else {
      this.reasonNotFunctional = null;
    }
  }
}
