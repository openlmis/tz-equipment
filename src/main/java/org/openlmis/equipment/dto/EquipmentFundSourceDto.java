/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.dto;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.equipment.domain.EquipmentFundSource;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class EquipmentFundSourceDto extends BaseDto
    implements EquipmentFundSource.Importer, EquipmentFundSource.Exporter {

  @NotBlank
  private String name;

  @NotBlank
  private String code;

  /**
   * Creates a new equipment fund source DTO from the entity.
   *
   * @param fundSource the equipment fund source to export into DTO
   * @return created equipment fund source DTO
   */
  public static EquipmentFundSourceDto newInstance(EquipmentFundSource fundSource) {
    EquipmentFundSourceDto fundSourceDto = new EquipmentFundSourceDto();
    fundSource.export(fundSourceDto);
    return fundSourceDto;
  }
}
