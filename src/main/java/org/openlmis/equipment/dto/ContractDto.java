/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.dto;

import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.equipment.domain.Contract;
import org.openlmis.equipment.domain.Equipment;
import org.openlmis.equipment.domain.ServiceType;
import org.openlmis.equipment.domain.Vendor;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ContractDto extends BaseDto
    implements Contract.Importer, Contract.Exporter {

  @NotNull
  private VendorDto vendor;

  @NotBlank
  private String identifier;

  @NotNull
  private LocalDate startDate;

  @NotNull
  private LocalDate endDate;

  @NotBlank
  private String description;


  @NotBlank
  private String terms;

  @NotBlank
  private String coverage;

  @NotNull
  private LocalDate date;

  @NotEmpty
  private Set<ServiceTypeDto> serviceTypes;

  @NotEmpty
  private Set<EquipmentDto> equipments;

  @NotEmpty
  private Set<UUID> facilities;

  /**
   * Creates a new contract DTO from the entity.
   *
   * @param contract the contract to export into DTO
   *
   * @return created contract DTO
   */
  public static ContractDto newInstance(Contract contract) {
    ContractDto contractDto = new ContractDto();
    contract.export(contractDto);
    return contractDto;
  }


  @Override
  public void setServiceTypes(Set<ServiceType> serviceTypes) {
    this.serviceTypes = serviceTypes.stream()
        .map(serviceType -> {
          ServiceTypeDto serviceTypeDto = new ServiceTypeDto();
          serviceType.export(serviceTypeDto);
          return serviceTypeDto;
        }).collect(Collectors.toSet());
  }

  @Override
  public void setEquipments(Set<Equipment> equipments) {
    this.equipments = equipments.stream()
        .map(equipment -> {
          EquipmentDto equipmentDto = new EquipmentDto();
          equipment.export(equipmentDto);
          return equipmentDto;
        }).collect(Collectors.toSet());
  }

  @Override
  public void setVendor(Vendor vendor) {
    VendorDto vendorDto = new VendorDto();
    vendor.export(vendorDto);
    this.vendor = vendorDto;
  }
}
