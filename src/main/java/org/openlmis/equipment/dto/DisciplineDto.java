/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.openlmis.equipment.domain.Discipline;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class DisciplineDto extends BaseDto implements Discipline.Importer,Discipline.Exporter {
  private String name;

  private String code;

  private Integer displayOrder;

  private Boolean enabled;

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public String getCode() {
    return this.code;
  }

  @Override
  public Integer getDisplayOrder() {
    return this.displayOrder;
  }

  @Override
  public Boolean getEnabled() {
    return this.enabled;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public void setCode(String code) {
    this.code = code;
  }

  @Override
  public void setDisplayOrder(Integer displayOrder) {
    this.displayOrder = displayOrder;
  }

  @Override
  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }
}
