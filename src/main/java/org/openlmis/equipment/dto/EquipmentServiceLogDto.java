/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.dto;

import java.time.LocalDate;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.equipment.domain.EquipmentMaintenanceRequest;
import org.openlmis.equipment.domain.EquipmentServiceLog;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class EquipmentServiceLogDto extends BaseDto
    implements EquipmentServiceLog.Importer, EquipmentServiceLog.Exporter {

  @NotNull
  private EquipmentMaintenanceRequestDto request;

  private LocalDate maintenanceDate;

  @NotNull
  private String servicePerformed;

  @NotNull
  private String finding;

  @NotNull
  private String recommendation;

  private LocalDate nextVisitDate;

  private Boolean approved;

  /**
   * Creates a new equipment service log DTO from the entity.
   *
   * @param log the equipment  service log to export into DTO
   *
   * @return created Equipment Service Log Dto
   */
  public static EquipmentServiceLogDto newInstance(EquipmentServiceLog log) {
    EquipmentServiceLogDto serviceLogDto = new EquipmentServiceLogDto();
    log.export(serviceLogDto);
    return serviceLogDto;
  }

  @Override
  public void setRequest(EquipmentMaintenanceRequest request) {
    EquipmentMaintenanceRequestDto requestDto = new EquipmentMaintenanceRequestDto();
    request.export(requestDto);
    this.request = requestDto;
  }

  @Getter
  @Setter
  @AllArgsConstructor
  @NoArgsConstructor
  @EqualsAndHashCode
  public static class ApproveDto {

    private boolean approved;
  }
}
