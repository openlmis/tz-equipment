/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.dto;

import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.equipment.domain.EquipmentEnergyType;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class EquipmentEnergyTypeDto extends BaseDto
    implements EquipmentEnergyType.Importer, EquipmentEnergyType.Exporter {

  @NotBlank
  private String name;

  @NotBlank
  private String code;

  /**
   * Creates a new Equipment Energy Type DTO from the entity.
   *
   * @param energyType the equipment energy type to export into DTO
   * @return created Equipment Energy Type DTO
   */
  public static EquipmentEnergyTypeDto newInstance(EquipmentEnergyType energyType) {
    EquipmentEnergyTypeDto energyTypeDto = new EquipmentEnergyTypeDto();
    energyType.export(energyTypeDto);
    return energyTypeDto;
  }
}
