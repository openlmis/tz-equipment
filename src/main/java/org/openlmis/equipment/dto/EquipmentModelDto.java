/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.equipment.domain.EquipmentModel;
import org.openlmis.equipment.domain.EquipmentType;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class EquipmentModelDto extends BaseDto
    implements EquipmentModel.Importer, EquipmentModel.Exporter {

  @NotBlank
  private String name;

  @NotBlank
  private String code;

  @NotNull
  private EquipmentTypeDto equipmentType;

  /**
   * Creates a new Equipment Model DTO from the entity.
   * @param model the equipment model to export into DTO
   * @return created Equipment Model DTO
   */
  public static EquipmentModelDto newInstance(EquipmentModel model) {
    EquipmentModelDto modelDto = new EquipmentModelDto();
    model.export(modelDto);
    return modelDto;
  }

  @Override
  public void setEquipmentType(EquipmentType type) {
    EquipmentTypeDto typeDto = new EquipmentTypeDto();
    type.export(typeDto);
    this.equipmentType = typeDto;
  }
}
