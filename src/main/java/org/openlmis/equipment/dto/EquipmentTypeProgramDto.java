/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.dto;

import static org.openlmis.equipment.service.ResourceNames.PROGRAMS;

import java.util.UUID;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import org.openlmis.equipment.domain.EquipmentType;
import org.openlmis.equipment.domain.EquipmentTypeProgram;

@EqualsAndHashCode(callSuper = true, exclude = "serviceUrl")
public class EquipmentTypeProgramDto extends BaseDto implements EquipmentTypeProgram.Exporter,
        EquipmentTypeProgram.Importer {

  @Setter
  private String serviceUrl;

  private ObjectReferenceDto programId;

  @Getter
  @Setter
  private EquipmentType equipmentType;

  @Getter
  @Setter
  private Integer displayOrder;

  @Getter
  @Setter
  private Boolean enableTestCount;

  @Getter
  @Setter
  private Boolean enableTotalColumn;


  @Override
  public void setProgramId(UUID programId) {
    this.programId = ObjectReferenceDto.create(programId, serviceUrl, PROGRAMS);
  }

  @Override
  public UUID getProgramId() {
    return programId.getId();
  }
}
