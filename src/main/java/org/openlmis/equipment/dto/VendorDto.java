/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.dto;

import java.util.Set;
import java.util.UUID;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.equipment.domain.Vendor;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class VendorDto extends BaseDto
    implements Vendor.Importer, Vendor.Exporter {

  @NotBlank
  private String name;


  @NotBlank
  private String website;


  @NotBlank
  private String contactPerson;


  @NotBlank
  private String primaryPhone;

  @Email
  @NotBlank
  private String email;


  @NotBlank
  private String description;


  @NotBlank
  private String specialization;

  @NotBlank
  private String geographicalCoverage;

  private Set<UUID> associatedUsers;

  /**
   * Creates a new Vendor DTO from the entity.
   *
   * @param vendor the vendor to export into DTO
   *
   * @return created Vendor DTO
   */
  public static VendorDto newInstance(Vendor vendor) {
    VendorDto vendorDto = new VendorDto();
    vendor.export(vendorDto);
    return vendorDto;
  }

}
