/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.equipment.domain.Equipment;
import org.openlmis.equipment.domain.EquipmentCategory;
import org.openlmis.equipment.domain.EquipmentEnergyType;
import org.openlmis.equipment.domain.EquipmentModel;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class EquipmentDto extends BaseDto
    implements Equipment.Importer, Equipment.Exporter {

  @NotBlank
  private String name;

  @NotBlank
  private String manufacturer;

  @NotNull
  private EquipmentEnergyTypeDto energyType;

  @NotNull
  private EquipmentModelDto model;

  @NotNull
  private EquipmentCategoryDto category;

  /**
   * Creates a new Equipment DTO from the entity.
   *
   * @param equipment the equipment to export into DTO
   * @return created Equipment DTO
   */
  public static EquipmentDto newInstance(Equipment equipment) {
    EquipmentDto equipmentDto = new EquipmentDto();
    equipment.export(equipmentDto);
    return equipmentDto;
  }

  @Override
  public void setEnergyType(EquipmentEnergyType energyType) {
    EquipmentEnergyTypeDto energyTypeDto = new EquipmentEnergyTypeDto();
    energyType.export(energyTypeDto);
    this.energyType = energyTypeDto;
  }

  @Override
  public void setModel(EquipmentModel model) {
    EquipmentModelDto modelDto = new EquipmentModelDto();
    model.export(modelDto);
    this.model = modelDto;
  }

  @Override
  public void setCategory(EquipmentCategory category) {
    EquipmentCategoryDto categoryDto = new EquipmentCategoryDto();
    category.export(categoryDto);
    this.category = categoryDto;
  }
}
