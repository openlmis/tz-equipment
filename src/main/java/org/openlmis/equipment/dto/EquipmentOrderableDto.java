/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.dto;

import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.equipment.domain.Equipment;
import org.openlmis.equipment.domain.EquipmentOrderable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class EquipmentOrderableDto extends BaseDto
    implements EquipmentOrderable.Importer, EquipmentOrderable.Exporter {

  @NotNull
  private UUID orderableId;

  @NotNull
  private EquipmentDto equipment;

  /**
   * Creates a new EquipmentOrderable DTO from the entity.
   *
   * @param equipmentOrderable the EquipmentOrdeable to export into DTO
   *
   * @return created EquipmentOrderable DTO
   */
  public static EquipmentOrderableDto newInstance(EquipmentOrderable equipmentOrderable) {
    EquipmentOrderableDto equipmentOrderableDto = new EquipmentOrderableDto();
    equipmentOrderable.export(equipmentOrderableDto);
    return equipmentOrderableDto;
  }

  @Override
  public void setEquipment(Equipment equipment) {
    if (equipment != null) {
      this.equipment = EquipmentDto.newInstance(equipment);
    } else {
      this.equipment = null;
    }
  }

}
