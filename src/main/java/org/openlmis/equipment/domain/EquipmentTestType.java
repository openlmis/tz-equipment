/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.domain;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.javers.core.metamodel.annotation.TypeName;

@Entity
@Table(name = "equipment_test_types")
@NoArgsConstructor
@TypeName("EquipmentTestType")
public class EquipmentTestType extends BaseEntity {
  private static final String TEXT = "text";
  @Column(nullable = false)
  @Getter
  @Setter
  Integer displayOrder;

  @Column(nullable = false, unique = true, columnDefinition = TEXT)
  @Getter
  @Setter
  private String name;

  @Column(nullable = false, unique = true, columnDefinition = TEXT)
  @Getter
  @Setter
  private String code;

  @Column(nullable = false)
  @Getter
  @Setter
  private Boolean enabled;

  @ManyToOne
  @Type(type = UUID)
  @JoinColumn(name = "equipmentCategoryId", nullable = false)
  @Getter
  @Setter
  private EquipmentCategory equipmentCategory;


  /**
   * Equipment Test Type constructor.
   *
   * @param name              of Equipment Test Type
   * @param code              unique code of Equipment Test Type
   * @param displayOrder      display order
   * @param enabled           enabled
   * @param equipmentCategory Equipment category
   */
  public EquipmentTestType(
          String name, String code, Integer displayOrder, Boolean enabled,
          EquipmentCategory equipmentCategory) {
    this.name = name;
    this.code = code;
    this.displayOrder = displayOrder;
    this.enabled = enabled;
    this.equipmentCategory = equipmentCategory;
  }

  /**
   * Static factory method for constructing a new equipment test Type.
   *
   * @param name         name
   * @param code         code
   * @param displayOrder displayOrder
   * @param enabled      enabled
   * @return new equipment test type
   */

  public static EquipmentTestType newEquipmentTestType(
          String name, String code, Integer displayOrder, Boolean enabled,
          EquipmentCategory equipmentCategory) {
    return new EquipmentTestType(name, code, displayOrder, enabled, equipmentCategory);
  }

  /**
   * static factory method for constructing a new equipment test Type using an importer (DTO).
   *
   * @param importer the equipment test type importer (DTO)
   * @return new equipment test type
   */
  public static EquipmentTestType newEquipmentTestType(EquipmentTestType.Importer importer) {
    EquipmentTestType newEquipmentTestType = new EquipmentTestType(importer.getName(),
            importer.getCode(), importer.getDisplayOrder(),
            importer.getEnabled(), importer.getEquipmentCategory());
    newEquipmentTestType.id = importer.getId();
    newEquipmentTestType.code = importer.getCode();
    newEquipmentTestType.displayOrder = importer.getDisplayOrder();
    newEquipmentTestType.enabled = importer.getEnabled();
    newEquipmentTestType.equipmentCategory = importer.getEquipmentCategory();
    return newEquipmentTestType;
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof EquipmentTestType)) {
      return false;
    }
    EquipmentTestType equipmentCategory = (EquipmentTestType) obj;
    return Objects.equals(name, equipmentCategory.name);
  }

  /**
   * Export this object to the specified exporter (DTO).
   *
   * @param exporter exporter to export to
   */
  public void export(EquipmentTestType.Exporter exporter) {
    exporter.setId(id);
    exporter.setName(name);
    exporter.setCode(code);
    exporter.setDisplayOrder(displayOrder);
    exporter.setEnabled(enabled);
    exporter.setEquipmentCategory(equipmentCategory);
  }

  /**
   * Static factory method for constructing a new right with a name and type.
   *
   * @param equipmentTestType equipment Test type
   */

  public void updateFrom(EquipmentTestType equipmentTestType) {
    this.name = equipmentTestType.getName();
    this.code = equipmentTestType.getCode();
    this.enabled = equipmentTestType.getEnabled();
    this.displayOrder = equipmentTestType.getDisplayOrder();
    this.equipmentCategory = equipmentTestType.getEquipmentCategory();
  }

  public interface Exporter {
    void setId(java.util.UUID id);

    void setName(String name);

    void setCode(String code);

    void setDisplayOrder(Integer displayOrder);

    void setEnabled(Boolean enabled);

    void setEquipmentCategory(EquipmentCategory equipmentCategory);


  }

  public interface Importer {
    java.util.UUID getId();

    String getName();

    String getCode();

    Integer getDisplayOrder();

    Boolean getEnabled();


    EquipmentCategory getEquipmentCategory();

  }
}
