/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.domain;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.javers.core.metamodel.annotation.TypeName;

@Entity
@TypeName("EquipmentServiceLog")
@Table(name = "equipment_service_logs")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class EquipmentServiceLog extends BaseTimestampedEntity {

  @Getter
  @Setter
  @ManyToOne
  @Type(type = UUID)
  @JoinColumn(name = "requestId", nullable = false)
  private EquipmentMaintenanceRequest request;

  @Getter
  @Setter
  private LocalDate maintenanceDate;

  @Getter
  @Setter
  @Column(columnDefinition = TEXT, nullable = false)
  private String servicePerformed;

  @Getter
  @Setter
  @Column(columnDefinition = TEXT, nullable = false)
  private String finding;

  @Getter
  @Setter
  @Column(columnDefinition = TEXT, nullable = false)
  private String recommendation;

  @Getter
  @Setter
  private LocalDate nextVisitDate;

  @Getter
  @Setter
  private Boolean approved;

  /**
   * Create new the equipment service log entity.
   *
   * @param request          request object of the equipment maintenance request data.
   * @param maintenanceDate  defines the equipment maintenance date of the machine.
   * @param servicePerformed defines the service performed vendor.
   * @param finding          defines the findings archived by vendor.
   * @param recommendation   defines the recommendation provided by vendor.
   * @param nextVisitDate    defines the next date of the equipment maintenance.
   */
  public EquipmentServiceLog(EquipmentMaintenanceRequest request,
                             LocalDate maintenanceDate,
                             String servicePerformed,
                             String finding,
                             String recommendation,
                             LocalDate nextVisitDate,
                             Boolean approved
  ) {
    this.request = request;
    this.maintenanceDate = maintenanceDate;
    this.servicePerformed = servicePerformed;
    this.finding = finding;
    this.recommendation = recommendation;
    this.nextVisitDate = nextVisitDate;
    this.approved = approved;
  }

  /**
   * Export the content of the equipment service log entity.
   *
   * @param exporter destination of the exported equipment service log data.
   */
  public void export(EquipmentServiceLog.Exporter exporter) {
    exporter.setId(this.id);
    exporter.setRequest(this.request);
    exporter.setMaintenanceDate(this.maintenanceDate);
    exporter.setServicePerformed(this.servicePerformed);
    exporter.setFinding(this.finding);
    exporter.setRecommendation(this.recommendation);
    exporter.setNextVisitDate(this.nextVisitDate);
    exporter.setApproved(this.approved);
  }

  public interface Importer extends BaseEntity.BaseImporter {

    EquipmentMaintenanceRequest.Importer getRequest();

    LocalDate getMaintenanceDate();

    String getServicePerformed();

    String getFinding();

    String getRecommendation();

    LocalDate getNextVisitDate();

    Boolean getApproved();
  }

  public interface Exporter extends BaseEntity.BaseExporter {

    void setRequest(EquipmentMaintenanceRequest request);

    void setMaintenanceDate(LocalDate maintenanceDate);

    void setServicePerformed(String servicePerformed);

    void setFinding(String finding);

    void setRecommendation(String recommendation);

    void setNextVisitDate(LocalDate nextVisitDate);

    void setApproved(Boolean approved);
  }

}
