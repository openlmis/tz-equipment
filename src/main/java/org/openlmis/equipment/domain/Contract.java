/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */


package org.openlmis.equipment.domain;

import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.javers.core.metamodel.annotation.TypeName;

@Entity
@Table(name = "contracts")
@TypeName("Contract")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"identifier", "date", "vendor"}, callSuper = true)
public class Contract extends BaseTimestampedEntity {

  @NotBlank
  @Column(unique = true)
  private String identifier;

  @NotNull
  private LocalDate startDate;

  @NotNull
  private LocalDate endDate;

  @NotBlank
  @Column(columnDefinition = TEXT)
  private String description;


  @NotBlank
  @Column(columnDefinition = TEXT)
  private String terms;

  @NotBlank
  @Column(columnDefinition = TEXT)
  private String coverage;

  @NotNull
  @Column(name = "contractdate")
  private LocalDate date;

  @NotNull
  @ManyToOne
  @JoinColumn(name = "vendorid")
  private Vendor vendor;

  @NotEmpty
  @ManyToMany
  @JoinTable(name = "contract_service_types",
      joinColumns = @JoinColumn(name = "contractid", nullable = false),
      inverseJoinColumns = @JoinColumn(name = "servicetypeid", nullable = false))

  private Set<ServiceType> serviceTypes;


  @NotEmpty
  @ManyToMany
  @JoinTable(name = "contract_equipments",
      joinColumns = @JoinColumn(name = "contractid", nullable = false),
      inverseJoinColumns = @JoinColumn(name = "equipmentid", nullable = false))
  private Set<Equipment> equipments;


  @NotEmpty
  @Type(type = UUID)
  @ElementCollection
  @CollectionTable(name = "contract_facilities", joinColumns = @JoinColumn(name = "contractid"))
  @Column(name = "facilityid")
  private Set<UUID> facilities;

  /**
   * Export the content of the contract entity.
   *
   * @param exporter destination of the exported contract data.
   */
  public void export(Contract.Exporter exporter) {
    exporter.setId(this.id);
    exporter.setVendor(this.vendor);
    exporter.setIdentifier(this.identifier);
    exporter.setStartDate(this.startDate);
    exporter.setEndDate(this.endDate);
    exporter.setDescription(this.description);
    exporter.setTerms(this.terms);
    exporter.setCoverage(this.coverage);
    exporter.setDate(this.date);
    exporter.setServiceTypes(this.serviceTypes);
    exporter.setEquipments(this.equipments);
    exporter.setFacilities(this.facilities);
  }

  public interface Importer extends BaseImporter {

    public Vendor.Importer getVendor();

    public String getIdentifier();

    public LocalDate getStartDate();

    public LocalDate getEndDate();


    public String getDescription();


    public String getTerms();

    public String getCoverage();

    public LocalDate getDate();

    public Set<? extends ServiceType.Importer> getServiceTypes();


    public Set<? extends Equipment.Importer> getEquipments();


    public Set<UUID> getFacilities();
  }

  public interface Exporter extends BaseExporter {

    public void setTerms(String terms);

    public void setCoverage(String coverage);

    public void setDate(LocalDate date);

    public void setServiceTypes(Set<ServiceType> serviceTypes);

    public void setEquipments(Set<Equipment> equipments);

    public void setIdentifier(String identifier);

    public void setVendor(Vendor vendor);

    public void setStartDate(LocalDate startDate);

    public void setEndDate(LocalDate endDate);

    public void setDescription(String description);

    public void setFacilities(Set<java.util.UUID> facilities);
  }
}
