/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.domain;

import java.time.LocalDate;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.javers.core.metamodel.annotation.TypeName;


@Entity
@TypeName("EquipmentInventory")
@Table(name = "equipment_inventory")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class EquipmentInventory extends BaseTimestampedEntity {

  @Getter
  @Setter
  @ManyToOne
  @Type(type = UUID)
  @JoinColumn(name = "equipmentId", nullable = false)
  private Equipment equipment;

  @Getter
  @Setter
  @Type(type = UUID)
  @Column(nullable = false)
  private UUID facilityId;

  @Getter
  @Setter
  @Type(type = UUID)
  @Column(nullable = false)
  private UUID programId;

  @Getter
  @Setter
  @NotNull
  @ManyToOne
  @Type(type = UUID)
  @JoinColumn(name = "fundSourceId", nullable = false)
  private EquipmentFundSource fundSource;

  @Getter
  @Setter
  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private FunctionalStatus functionalStatus;

  @Getter
  @ManyToOne
  @Type(type = UUID)
  @JoinColumn(name = "reasonNotFunctionalId")
  private ReasonNotFunctional reasonNotFunctional;

  @Getter
  @Setter
  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private Utilization utilization;

  @Getter
  @Setter
  @Column(columnDefinition = TEXT)
  private String additionalNotes;

  @Getter
  @Setter
  private LocalDate decommissionDate;

  @Getter
  @Setter
  @Column(columnDefinition = TEXT, nullable = false)
  private String equipmentTrackingId;

  @Getter
  @Setter
  @Column(columnDefinition = TEXT)
  private String source;

  @Getter
  @Setter
  @Column(nullable = false)
  private LocalDate dateOfInstallation;

  @Getter
  @Setter
  private LocalDate dateOfWarrantyExpiry;

  @Getter
  @Setter
  private double purchasePrice;

  @Getter
  @Setter
  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private RegistrationStatus registrationStatus;

  /**
   * Create new equipment inventory.
   *
   * @param equipment        the associated equipment catalog.
   * @param facilityId       the id of the facility for the equipment.
   * @param programId        the program associated with the equipment.
   * @param functionalStatus the equipment functional status.
   * @param utilization      the utilization state of the equipment.
   */
  public EquipmentInventory(Equipment equipment, java.util.UUID facilityId,
                            java.util.UUID programId, FunctionalStatus functionalStatus,
                            Utilization utilization) {
    this.equipment = equipment;
    this.facilityId = facilityId;
    this.programId = programId;
    this.functionalStatus = functionalStatus;
    this.utilization = utilization;
  }

  /**
   * Export the content of the equipment inventory entity.
   *
   * @param exporter destination of the exported equipment inventory data.
   */
  public void export(EquipmentInventory.Exporter exporter) {
    exporter.setId(this.id);
    exporter.setEquipment(this.equipment);
    exporter.setFacilityId(this.facilityId);
    exporter.setProgramId(this.programId);
    exporter.setFundSource(this.fundSource);
    exporter.setFunctionalStatus(this.functionalStatus);
    exporter.setReasonNotFunctional(this.reasonNotFunctional);
    exporter.setUtilization(this.utilization);
    exporter.setAdditionalNotes(this.additionalNotes);
    exporter.setDecommissionDate(this.decommissionDate);
    exporter.setEquipmentTrackingId(this.equipmentTrackingId);
    exporter.setSource(this.source);
    exporter.setDateOfInstallation(this.dateOfInstallation);
    exporter.setDateOfWarrantyExpiry(this.dateOfWarrantyExpiry);
    exporter.setPurchasePrice(this.purchasePrice);
    exporter.setRegistrationStatus(this.registrationStatus);
  }

  /**
   * Update details of the equipment inventory from another inventory instance.
   *
   * @param inventory the inventory whose details will be used to update this inventory.
   */
  public void updateFrom(EquipmentInventory inventory) {
    this.functionalStatus = inventory.functionalStatus;
    this.fundSource = inventory.getFundSource();
    this.reasonNotFunctional = inventory.getReasonNotFunctional();
    this.utilization = inventory.utilization;
    this.additionalNotes = inventory.additionalNotes;
    this.decommissionDate = inventory.decommissionDate;
    this.equipmentTrackingId = inventory.equipmentTrackingId;
    this.source = inventory.source;
    this.dateOfInstallation = inventory.dateOfInstallation;
    this.dateOfWarrantyExpiry = inventory.dateOfWarrantyExpiry;
    this.purchasePrice = inventory.purchasePrice;
    this.registrationStatus = inventory.registrationStatus;
  }

  public interface Importer extends BaseTimestampedEntity.BaseImporter {

    Equipment.Importer getEquipment();

    UUID getFacilityId();

    UUID getProgramId();

    EquipmentFundSource.Importer getFundSource();

    FunctionalStatus getFunctionalStatus();

    ReasonNotFunctional.Importer getReasonNotFunctional();

    Utilization getUtilization();

    String getAdditionalNotes();

    LocalDate getDecommissionDate();

    String getEquipmentTrackingId();

    String getSource();

    LocalDate getDateOfInstallation();

    LocalDate getDateOfWarrantyExpiry();

    double getPurchasePrice();

    RegistrationStatus getRegistrationStatus();
  }

  public interface Exporter extends BaseTimestampedEntity.BaseExporter {

    void setEquipment(Equipment equipment);

    void setProgramId(UUID programId);

    void setFacilityId(UUID facilityId);

    void setFundSource(EquipmentFundSource fundSource);

    void setFunctionalStatus(FunctionalStatus functionalStatus);

    void setReasonNotFunctional(ReasonNotFunctional reasonNotFunctional);

    void setUtilization(Utilization utilization);

    void setAdditionalNotes(String additionalNotes);

    void setDecommissionDate(LocalDate decommissionDate);

    void setEquipmentTrackingId(String equipmentTrackingId);

    void setSource(String source);

    void setDateOfInstallation(LocalDate dateOfInstallation);

    void setDateOfWarrantyExpiry(LocalDate dateOfWarrantyExpiry);

    void setPurchasePrice(double purchasePrice);

    void setRegistrationStatus(RegistrationStatus registrationStatus);
  }
}
