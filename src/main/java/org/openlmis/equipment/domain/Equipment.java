/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.javers.core.metamodel.annotation.TypeName;

@Entity
@Table(name = "equipments")
@TypeName("Equipment")
@NoArgsConstructor
@EqualsAndHashCode(of = {"name", "manufacturer"}, callSuper = true)
public class Equipment extends BaseTimestampedEntity {

  @Getter
  @Setter
  @NotBlank
  @Column(nullable = false, unique = true, columnDefinition = TEXT)
  private String name;

  @Getter
  @Setter
  @NotBlank
  @Column(nullable = false, columnDefinition = TEXT)
  private String manufacturer;

  @Getter
  @Setter
  @NotNull
  @ManyToOne
  @JoinColumn(name = "energytypeid", nullable = false)
  private EquipmentEnergyType energyType;

  @Getter
  @Setter
  @NotNull
  @ManyToOne
  @JoinColumn(name = "modelid", nullable = false)
  private EquipmentModel model;

  @Getter
  @Setter
  @NotNull
  @ManyToOne
  @JoinColumn(name = "categoryid", nullable = false)
  private EquipmentCategory category;

  /**
   * Create new the equipment entity.
   *
   * @param name destination of the exported equipment data.
   * @param manufacturer defines the manufacturer of the equipment data.
   * @param energyType defines the energyType of the equipment data.
   * @param model defines the model of the equipment data.
   * @param category defines the category of the equipment data.
   */
  public Equipment(String name, String manufacturer, EquipmentEnergyType energyType,
                   EquipmentModel model, EquipmentCategory category) {
    this.name = name;
    this.manufacturer = manufacturer;
    this.energyType = energyType;
    this.model = model;
    this.category = category;
  }

  /**
   * Export the content of the equipment entity.
   *
   * @param exporter destination of the exported equipment data.
   */
  public void export(Exporter exporter) {
    exporter.setId(this.id);
    exporter.setName(this.name);
    exporter.setManufacturer(this.manufacturer);
    exporter.setEnergyType(this.energyType);
    exporter.setModel(this.model);
    exporter.setCategory(this.category);
  }

  public interface Importer extends BaseEntity.BaseImporter {
    String getName();

    String getManufacturer();

    EquipmentEnergyType.Importer getEnergyType();

    EquipmentModel.Importer getModel();

    EquipmentCategory.Importer getCategory();
  }

  public interface Exporter extends BaseEntity.BaseExporter {
    void setName(String name);

    void setManufacturer(String manufacturer);

    void setEnergyType(EquipmentEnergyType energyType);

    void setModel(EquipmentModel model);

    void setCategory(EquipmentCategory category);
  }
}
