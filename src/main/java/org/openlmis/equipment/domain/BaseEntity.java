/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.domain;

import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@MappedSuperclass
@EqualsAndHashCode
@ToString
public abstract class BaseEntity {

  protected static final String UUID = "pg-uuid";

  protected static final String TEXT = "text";

  protected static final String NUMERIC = "big_decimal";

  @Id
  @GeneratedValue(generator = "uuid-gen")
  @GenericGenerator(name = "uuid-gen",
      strategy = "org.openlmis.equipment.util.ConditionalUuidGenerator")
  @Type(type = UUID)
  @Getter
  @Setter
  protected UUID id;

  public interface BaseExporter {

    void setId(UUID id);
  }

  public interface BaseImporter {

    UUID getId();
  }

  /**
   * Map entities to respective ids.
   *
   * @param entities entities to convert.
   *
   * @return list of entities ids.
   */
  public static List<UUID> toIds(Collection<? extends BaseEntity> entities) {
    return entities.stream()
        .map(baseEntity -> baseEntity.getId())
        .collect(Collectors.toList());
  }
}
