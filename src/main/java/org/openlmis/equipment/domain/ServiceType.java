/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */


package org.openlmis.equipment.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.javers.core.metamodel.annotation.TypeName;

@Entity
@Table(name = "service_types")
@TypeName("ServiceType")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"name"}, callSuper = true)
public class ServiceType extends BaseEntity {

  @NotBlank
  @Column(unique = true)
  private String name;

  @NotBlank
  @Column(columnDefinition = TEXT)
  private String description;

  /**
   * Create new service type from the provided service type details.
   *
   * @param importer provided service type details
   *
   * @return new instance of service type.
   */
  public static ServiceType newInstance(Importer importer) {
    ServiceType serviceType = new ServiceType();
    serviceType.setId(importer.getId());
    serviceType.setName(importer.getName());
    serviceType.setDescription(importer.getDescription());

    return serviceType;
  }

  /**
   * Export the content of the service type entity.
   *
   * @param exporter destination of the exported service type data.
   */
  public void export(ServiceType.Exporter exporter) {
    exporter.setId(this.id);
    exporter.setName(this.name);
    exporter.setDescription(this.description);
  }

  public interface Importer extends BaseImporter {

    String getName();

    String getDescription();
  }

  public interface Exporter extends BaseExporter {

    void setName(String name);

    void setDescription(String description);
  }
}
