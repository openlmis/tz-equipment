/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.domain;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.javers.core.metamodel.annotation.TypeName;

@Entity
@TypeName("EquipmentMaintenanceRequest")
@Table(name = "equipment_maintenance_requests")
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class EquipmentMaintenanceRequest extends BaseTimestampedEntity {

  @Getter
  @Setter
  @ManyToOne
  @Type(type = UUID)
  @JoinColumn(name = "inventoryId", nullable = false)
  private EquipmentInventory inventory;

  @Getter
  @Setter
  @ManyToOne
  @Type(type = UUID)
  @JoinColumn(name = "vendorId", nullable = false)
  private Vendor vendor;

  @Getter
  @Setter
  private LocalDate requestDate;

  @Getter
  @Setter
  private LocalDate recommendedDate;

  @Getter
  @Setter
  @Column(columnDefinition = TEXT, nullable = false)
  private String comment;

  @Getter
  @Setter
  private Boolean resolved;

  @Getter
  @Setter
  @Column(columnDefinition = TEXT, nullable = false)
  private String vendorComment;

  @Getter
  @Setter
  private LocalDate breakDownDate;

  @Getter
  @Setter
  @ManyToOne
  @Type(type = UUID)
  @JoinColumn(name = "reasonId", nullable = false)
  private EquipmentMaintenanceRequestReason reason;

  /**
   * Create new equipment maintenance request.
   *
   * @param inventory       the associated equipment inventory catalog.
   * @param vendor          the vendor associated with the equipment.
   * @param requestDate     the requestDate of equipment maintenance.
   * @param recommendedDate the recommendedDate for equipment maintenance.
   * @param comment         the comment for the equipment maintenance.
   * @param resolved        the resolved flag if the equipment problem has been resolved.
   * @param vendorComment   the comment of the vendor.
   * @param breakDownDate   the breakDownDate the date the equipment breakdown.
   * @param reason          the reasonId for the equipment maintenance request reason.
   */
  public EquipmentMaintenanceRequest(EquipmentInventory inventory,
                                     Vendor vendor,
                                     LocalDate requestDate,
                                     LocalDate recommendedDate,
                                     String comment,
                                     Boolean resolved,
                                     String vendorComment,
                                     LocalDate breakDownDate,
                                     EquipmentMaintenanceRequestReason reason) {
    this.inventory = inventory;
    this.vendor = vendor;
    this.requestDate = requestDate;
    this.recommendedDate = recommendedDate;
    this.comment = comment;
    this.resolved = resolved;
    this.vendorComment = vendorComment;
    this.breakDownDate = breakDownDate;
    this.reason = reason;

  }

  /**
   * Export the content of the equipment maintenance request entity.
   *
   * @param exporter destination of the exported equipment maintenance request data.
   */
  public void export(EquipmentMaintenanceRequest.Exporter exporter) {

    exporter.setId(this.id);
    exporter.setInventory(this.inventory);
    exporter.setVendor(this.vendor);
    exporter.setRequestDate(this.requestDate);
    exporter.setRecommendedDate(this.recommendedDate);
    exporter.setComment(this.comment);
    exporter.setResolved(this.resolved);
    exporter.setVendorComment(this.vendorComment);
    exporter.setBreakDownDate(this.breakDownDate);
    exporter.setReason(this.reason);
  }

  /**
   * Update details of the equipment maintenance request from another inventory instance.
   *
   * @param request the request whose details will be
   *                used to update this equipment maintenance request.
   */
  public void updateFrom(EquipmentMaintenanceRequest request) {

    this.inventory = request.inventory;
    this.vendor = request.vendor;
    this.requestDate = request.getRequestDate();
    this.recommendedDate = request.getRecommendedDate();
    this.comment = request.getComment();
    this.resolved = request.getResolved();
    this.vendorComment = request.getVendorComment();
    this.breakDownDate = request.getBreakDownDate();
    this.reason = request.getReason();

  }

  public interface Importer
      extends BaseTimestampedEntity.BaseImporter {

    EquipmentInventory.Importer getInventory();

    Vendor.Importer getVendor();

    LocalDate getRequestDate();

    LocalDate getRecommendedDate();

    String getComment();

    Boolean getResolved();

    String getVendorComment();

    LocalDate getBreakDownDate();

    EquipmentMaintenanceRequestReason.Importer getReason();

  }

  public interface Exporter
      extends BaseTimestampedEntity.BaseExporter {

    void setInventory(EquipmentInventory inventory);

    void setVendor(Vendor vendor);

    void setRequestDate(LocalDate requestDate);

    void setRecommendedDate(LocalDate recommendedDate);

    void setComment(String comment);

    void setResolved(Boolean resolved);

    void setVendorComment(String vendorComment);

    void setBreakDownDate(LocalDate breakDownDate);

    void setReason(EquipmentMaintenanceRequestReason reason);
  }

}
