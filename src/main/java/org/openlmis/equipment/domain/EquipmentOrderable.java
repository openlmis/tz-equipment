/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.domain;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.javers.core.metamodel.annotation.TypeName;

@Entity
@Table(name = "equipment_orderables",
    uniqueConstraints = @UniqueConstraint(
        name = "equipment_orderables_unique_equipmentid_orderableid",
        columnNames = {"equipmentid", "orderableid"})
)
@NoArgsConstructor
@AllArgsConstructor
@TypeName("EquipmentOrderable")
public class EquipmentOrderable extends BaseEntity {

  @Column(nullable = false, unique = true)
  @Getter
  @Setter
  @Type(type = UUID)
  private UUID orderableId;


  @Getter
  @Setter
  @Type(type = UUID)
  @ManyToOne
  @JoinColumn(name = "equipmentId", nullable = false)
  private Equipment equipment;

  public static EquipmentOrderable newEquipmentOrderable(Equipment equipment,
                                                         UUID orderableId) {
    return new EquipmentOrderable(orderableId, equipment);
  }

  /**
   * Export the content of the of the EquipmentOrderable entity.
   *
   * @param exporter destination of the exported equipment orderable data.
   */
  public void export(EquipmentOrderable.Exporter exporter) {
    exporter.setId(this.id);
    exporter.setEquipment(this.equipment);
    exporter.setOrderableId(this.orderableId);
  }


  public interface Importer extends BaseEntity.BaseImporter {

    UUID getOrderableId();

    Equipment.Importer getEquipment();

  }

  public interface Exporter extends BaseEntity.BaseExporter {

    void setEquipment(Equipment equipment);

    void setOrderableId(UUID orderableId);

  }
}
