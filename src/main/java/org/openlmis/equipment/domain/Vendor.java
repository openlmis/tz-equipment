/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */


package org.openlmis.equipment.domain;

import java.util.Set;
import java.util.UUID;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.javers.core.metamodel.annotation.TypeName;

@Entity
@Table(name = "vendor")
@TypeName("Vendor")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"name"}, callSuper = true)
public class Vendor extends BaseTimestampedEntity {

  @NotBlank
  @Column(unique = true)
  private String name;


  @NotBlank
  private String website;


  @NotBlank
  private String contactPerson;


  @NotBlank
  private String primaryPhone;

  @Email
  @NotBlank
  private String email;


  @NotBlank
  @Column(columnDefinition = TEXT)
  private String description;


  @NotBlank
  @Column(columnDefinition = TEXT)
  private String specialization;

  @NotBlank
  @Column(columnDefinition = TEXT)
  private String geographicalCoverage;


  @Type(type = UUID)
  @ElementCollection
  @CollectionTable(name = "vendor_associated_users", joinColumns = @JoinColumn(name = "vendor_id"))
  @Column(name = "associated_user")
  private Set<UUID> associatedUsers;

  /**
   * Create new vendor from the provided vendor details.
   *
   * @param importer provided vendor details
   *
   * @return new instance of Vendor.
   */
  public static Vendor newInstance(Vendor.Importer importer) {
    Vendor vendor = new Vendor();

    vendor.setId(importer.getId());
    vendor.setName(importer.getName());
    vendor.setWebsite(importer.getWebsite());
    vendor.setContactPerson(importer.getContactPerson());
    vendor.setPrimaryPhone(importer.getPrimaryPhone());
    vendor.setEmail(importer.getEmail());
    vendor.setDescription(importer.getDescription());
    vendor.setSpecialization(importer.getSpecialization());
    vendor.setGeographicalCoverage(importer.getGeographicalCoverage());
    vendor.setAssociatedUsers(importer.getAssociatedUsers());

    return vendor;
  }

  /**
   * Export the content of the vendor entity.
   *
   * @param exporter destination of the exported vendor data.
   */
  public void export(Vendor.Exporter exporter) {
    exporter.setId(this.id);
    exporter.setName(this.name);
    exporter.setWebsite(this.website);
    exporter.setContactPerson(this.contactPerson);
    exporter.setPrimaryPhone(this.primaryPhone);
    exporter.setEmail(this.email);
    exporter.setDescription(this.description);
    exporter.setSpecialization(this.specialization);
    exporter.setGeographicalCoverage(this.geographicalCoverage);
    exporter.setAssociatedUsers(this.associatedUsers);
  }

  public interface Importer extends BaseEntity.BaseImporter {

    String getName();

    String getWebsite();

    String getContactPerson();

    String getPrimaryPhone();

    String getEmail();

    String getDescription();

    String getSpecialization();

    String getGeographicalCoverage();

    Set<UUID> getAssociatedUsers();
  }

  public interface Exporter extends BaseEntity.BaseExporter {

    void setName(String name);

    void setWebsite(String website);

    void setContactPerson(String contactPerson);

    void setPrimaryPhone(String primaryPhone);

    void setEmail(String email);

    void setDescription(String description);

    void setSpecialization(String specialization);

    void setGeographicalCoverage(String geographicalCoverage);

    void setAssociatedUsers(Set<UUID> associatedUsers);
  }
}
