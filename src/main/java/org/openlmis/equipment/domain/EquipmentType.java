/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.domain;

import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.javers.core.metamodel.annotation.TypeName;

@Entity
@Table(name = "equipment_types")
@NoArgsConstructor
@TypeName("EquipmentType")
@SuppressWarnings({"PMD.UnusedPrivateField"})
public class EquipmentType extends BaseEntity {
  private static final String TEXT = "text";

  @Column(nullable = false, unique = true, columnDefinition = TEXT)
  @Getter
  @Setter
  private String name;

  @Column(nullable = false, unique = true, columnDefinition = TEXT)
  @Getter
  @Setter
  private String code;

  @Getter
  @Setter
  private Boolean isColdChain;

  @Getter
  @Setter
  private Boolean isBioChemistry;

  @Getter
  @Setter
  private Boolean active;

  private EquipmentType(String name, String code,
                        Boolean isColdChain, Boolean isBioChemistry, Boolean active) {
    this.name = name;
    this.code = code;
    this.isColdChain = isColdChain;
    this.isBioChemistry = isBioChemistry;
    this.active = active;
  }

  /**
   * Static factory method for constructing a new right with a name and type.
   *
   * @param equipmentType equipment type
   */
  public void updateFrom(EquipmentType equipmentType) {
    this.name = equipmentType.getName();
    this.code = equipmentType.getCode();
    this.active = equipmentType.getActive();
    this.isBioChemistry = equipmentType.getIsBioChemistry();
    this.isColdChain = equipmentType.getIsColdChain();
  }

  /**
   * Static factory method for constructing a new equipment type.
   *
   * @param name name
   * @param code code
   * @param isBioChemistry isBioChemistry
   * @param isColdChain isColdChain
   * @param active active
   */
  public static EquipmentType newEquipmentType(
          String name, String code,
          Boolean isBioChemistry, Boolean isColdChain, Boolean active) {
    return new EquipmentType(name, code, isBioChemistry, isColdChain, active);
  }

  /**
   * Static factory method for constructing a new equipment type using an importer (DTO).
   *
   * @param importer the equipment type importer (DTO)
   */
  public static EquipmentType newEquipmentType(EquipmentType.Importer importer) {
    EquipmentType newEquipmentType = new EquipmentType(importer.getName(),
            importer.getCode(), importer.getIsColdChain(), importer.getIsBioChemistry(),
            importer.getActive());
    newEquipmentType.id = importer.getId();
    newEquipmentType.code = importer.getCode();
    newEquipmentType.isColdChain = importer.getIsColdChain();
    newEquipmentType.isBioChemistry = importer.getIsBioChemistry();
    newEquipmentType.active = importer.getActive();
    return newEquipmentType;
  }

  /**
   * Export this object to the specified exporter (DTO).
   *
   * @param exporter exporter to export to
   */
  public void export(Exporter exporter) {
    exporter.setId(id);
    exporter.setName(name);
    exporter.setCode(code);
    exporter.setIsBioChemistry(isBioChemistry);
    exporter.setIsColdChain(isColdChain);
    exporter.setActive(active);
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof EquipmentType)) {
      return false;
    }
    EquipmentType equipmentType = (EquipmentType) obj;
    return Objects.equals(name, equipmentType.name);
  }

  public interface Exporter {
    void setId(UUID id);

    void setName(String name);

    void setCode(String code);

    void setIsBioChemistry(Boolean isBioChemistry);

    void setIsColdChain(Boolean isColdChain);

    void setActive(Boolean active);
  }

  public interface Importer {
    UUID getId();

    String getName();

    String getCode();

    Boolean getIsColdChain();

    Boolean getIsBioChemistry();

    Boolean getActive();
  }
}