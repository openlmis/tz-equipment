/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
@Table(name = "reasons_not_functional")
public class ReasonNotFunctional extends BaseEntity {

  @NotBlank
  @Column(nullable = false, unique = true, columnDefinition = TEXT)
  private String code;

  @NotBlank
  @Column(nullable = false, unique = true, columnDefinition = TEXT)
  private String name;

  @NotNull
  @Column(nullable = false)
  private Integer displayOrder;

  /**
   * Creates new instance based on data from {@link ReasonNotFunctional.Importer}.
   *
   * @param importer instance of {@link ReasonNotFunctional.Importer}
   * @return new instance of ReasonNotFunctional.
   */
  public static ReasonNotFunctional newInstance(ReasonNotFunctional.Importer importer) {
    ReasonNotFunctional reasonNotFunctional = new ReasonNotFunctional(
        importer.getCode(),
        importer.getName(),
        importer.getDisplayOrder()
    );
    reasonNotFunctional.id = importer.getId();
    return reasonNotFunctional;
  }

  /**
   * Export this object to the specified exporter (DTO).
   *
   * @param exporter exporter to export to
   */
  public void export(ReasonNotFunctional.Exporter exporter) {
    exporter.setId(id);
    exporter.setName(name);
    exporter.setCode(code);
    exporter.setDisplayOrder(displayOrder);
  }

  public interface Exporter {

    void setId(java.util.UUID id);

    void setCode(String code);

    void setName(String name);

    void setDisplayOrder(Integer displayOrder);
  }

  public interface Importer {

    java.util.UUID getId();

    String getCode();

    String getName();

    Integer getDisplayOrder();
  }
}
