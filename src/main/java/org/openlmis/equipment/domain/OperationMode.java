/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode (callSuper = true)
@Table (name = "equipment_operation_modes")
public class OperationMode extends BaseEntity {

  @Column (nullable = false, unique = true)
  private String name;

  @Column (nullable = false, unique = true)
  private String code;

  @Column (nullable = false)
  private Integer displayOrder;

  @Column (columnDefinition = TEXT)
  private String description;

  @Column (nullable = false)
  private Boolean active;

  public interface Exporter {
    void setId(java.util.UUID id);

    void setName(String name);

    void setCode(String code);

    void setDisplayOrder(Integer displayOrder);

    void setDescription(String description);

    void setActive(Boolean active);
  }

  public interface Importer {
    java.util.UUID getId();

    String getName();

    String getCode();

    Integer getDisplayOrder();

    String getDescription();

    Boolean getActive();
  }

  /**
   * Export this object to the specified exporter (DTO).
   *
   * @param exporter exporter to export to
   */
  public void export(Exporter exporter) {
    exporter.setId(id);

    exporter.setName(name);

    exporter.setCode(code);

    exporter.setDisplayOrder(displayOrder);

    exporter.setDescription(description);

    exporter.setActive(active);
  }

  /**
   * Creates new instance based on data from {@link OperationMode.Importer}.
   *
   * @param importer instance of {@link OperationMode.Importer}
   * @return new instance of OperationMode.
   */
  public static OperationMode newInstance(Importer importer) {
    OperationMode operationMode = new OperationMode(
        importer.getName(), importer.getCode(), importer.getDisplayOrder(),
        importer.getDescription(), importer.getActive()
    );
    operationMode.id = importer.getId();
    return operationMode;
  }

}
