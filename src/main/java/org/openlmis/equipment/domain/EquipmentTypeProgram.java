/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.javers.core.metamodel.annotation.TypeName;

@Entity
@Table(name = "equipment_type_programs")
@NoArgsConstructor
@TypeName("EquipmentTypeProgram")
@SuppressWarnings({"PMD.UnusedPrivateField"})
public class EquipmentTypeProgram extends BaseEntity {

  @ManyToOne
  @Type(type = UUID)
  @JoinColumn(name = "equipmentTypeId", nullable = false)
  @Getter
  @Setter
  private EquipmentType equipmentType;

  @Getter
  @Setter
  @Type(type = UUID)
  @Column(nullable = false)
  private  UUID programId;

  @Column(nullable = false)
  private Integer displayOrder;

  @Getter
  @Setter
  private Boolean enableTestCount;

  @Getter
  @Setter
  private Boolean enableTotalColumn;

  private EquipmentTypeProgram(EquipmentType equipmentType, UUID programId, Boolean enableTestCount,
                               Boolean enableTotalColumn, Integer displayOrder) {
    this.equipmentType = equipmentType;
    this.programId = programId;
    this.displayOrder = displayOrder;
    this.enableTestCount = enableTestCount;
    this.enableTotalColumn = enableTotalColumn;
  }

  /**
   * Static factory method for constructing a new equipment type program with a program
   * and equipment type.
   *
   * @param equipmentTypeProgram equipment type program
   */
  public void updateFrom(EquipmentTypeProgram equipmentTypeProgram) {
    this.programId = equipmentTypeProgram.getProgramId();
    this.equipmentType = equipmentTypeProgram.getEquipmentType();
  }

  /**
   *  Static method for constructing new Equipment Type program.
   * @param equipmentType equipment type id
   * @param programId program id
   * @return
   */
  public static EquipmentTypeProgram newEquipmentTypeProgram(EquipmentType equipmentType,
                                                             UUID programId,
                                                             Boolean enableTestCount,
                                                             Boolean enableTotalColumn,
                                                             Integer displayOrder) {
    return new EquipmentTypeProgram(equipmentType, programId, enableTestCount,
            enableTotalColumn, displayOrder);
  }

  /**
   * Static factory method for constructing a new equipment type program using an importer (DTO).
   * @param importer the equipment type program importer (DTO)
   * @return
   */
  public static EquipmentTypeProgram newEquipmentTypeProgram(
          EquipmentTypeProgram.Importer importer) {
    EquipmentTypeProgram equipmentTypeProgram = new EquipmentTypeProgram(
            importer.getEquipmentType(), importer.getProgramId(),importer.getEnableTestCount(),
            importer.getEnableTotalColumn(), importer.getDisplayOrder()
    );
    equipmentTypeProgram.id = importer.getId();
    equipmentTypeProgram.equipmentType = importer.getEquipmentType();
    equipmentTypeProgram.programId = importer.getProgramId();
    equipmentTypeProgram.enableTestCount = importer.getEnableTestCount();
    equipmentTypeProgram.enableTotalColumn = importer.getEnableTotalColumn();
    equipmentTypeProgram.displayOrder =  importer.getDisplayOrder();
    return equipmentTypeProgram;
  }

  /**
   * Export this object to the specified exporter (DTO).
   *
   * @param exporter exporter to export to
   */
  public void export(EquipmentTypeProgram.Exporter exporter) {
    exporter.setId(id);
    exporter.setEquipmentType(equipmentType);
    exporter.setProgramId(programId);
    exporter.setDisplayOrder(displayOrder);
    exporter.setEnableTestCount(enableTestCount);
    exporter.setEnableTotalColumn(enableTotalColumn);
  }

  public interface Exporter {
    void setId(UUID id);

    void setEquipmentType(EquipmentType equipmentType);

    void setProgramId(UUID programId);

    void setDisplayOrder(Integer displayOrder);

    void  setEnableTestCount(Boolean enableTestCount);

    void  setEnableTotalColumn(Boolean enableTotalColumn);

  }

  public interface Importer {

    UUID getId();

    EquipmentType getEquipmentType();

    UUID getProgramId();

    Boolean getEnableTestCount();

    Boolean getEnableTotalColumn();

    Integer getDisplayOrder();

  }


}
