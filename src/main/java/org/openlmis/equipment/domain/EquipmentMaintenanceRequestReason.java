/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.javers.core.metamodel.annotation.TypeName;

@Entity
@Table(name = "equipment_maintenance_request_reasons")
@TypeName("EquipmentMaintenanceRequestReason")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"name"}, callSuper = true)
public class EquipmentMaintenanceRequestReason extends BaseEntity {

  @NotBlank
  @Column(unique = true)
  private String code;

  @NotBlank
  @Column(columnDefinition = TEXT)
  private String name;

  /**
   * Create new equipment maintenance reason .
   *
   * @param importer provided equipment maintenance reasons
   *
   * @return new instance of equipment maintenance reason.
   */
  public static EquipmentMaintenanceRequestReason newInstance(Importer importer) {
    EquipmentMaintenanceRequestReason reason = new EquipmentMaintenanceRequestReason();
    reason.setId(importer.getId());
    reason.setCode(importer.getCode());
    reason.setName(importer.getName());

    return reason;
  }

  /**
   * Export the content of the equipment maintenanceRequest reason entity.
   *
   * @param exporter destination of the exported equipment maintenance requestReason data.
   */
  public void export(EquipmentMaintenanceRequestReason.Exporter exporter) {
    exporter.setId(this.id);
    exporter.setCode(this.code);
    exporter.setName(this.name);
  }

  public interface Importer extends BaseImporter {

    String getCode();

    String getName();
  }

  public interface Exporter extends BaseExporter {

    void setCode(String code);

    void setName(String name);
  }
}
