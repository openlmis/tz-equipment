/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.i18n;

import java.util.Arrays;

public abstract class MessageKeys {
  public static final String EQUIPMENT_MAINTENANCE_REQUEST = "equipmentMaintenanceRequest";
  public static final String EQUIPMENT_SERVICE_LOG = "equipmentServiceLog";

  //ENTITIES
  public static final String SERVICE_TYPE = "serviceType";
  public static final String EQUIPMENT_INVENTORY = "equipmentInventory";
  public static final String EQUIPMENT = "equipment";
  public static final String EQUIPMENT_ENERGY_TYPE = "equipmentEnergyType";
  public static final String EQUIPMENT_FUND_SOURCE = "equipmentFundSource";
  public static final String EQUIPMENT_MODEL = "equipmentModel";
  public static final String EQUIPMENT_TYPE = "equipmentType";
  public static final String ERROR_NOT_FOUND = "Not Found";
  public static final String ERROR_MISSING_MANDATORY_ITEMS = "Missing Mandatory Items";
  protected static final String SERVICE_PREFIX = "equipment";
  protected static final String REQUIRED = "required";
  protected static final String SEARCH = "search";
  protected static final String WITH = "with";
  protected static final String NULL = "null";
  protected static final String NAME = "name";
  protected static final String TYPE = "type";
  protected static final String CODE = "code";
  protected static final String AND = "and";
  protected static final String ID = "id";
  protected static final String FORMAT = "format";
  protected static final String UUID = "uuid";
  protected static final String DATE = "date";
  protected static final String BOOLEAN = "boolean";
  protected static final String INVALID = "invalid";
  protected static final String UNAUTHORIZED = "unauthorized";
  protected static final String NOT_FOUND = "notFound";
  protected static final String ID_MISMATCH = "idMismatch";
  protected static final String LACKS_PARAMETERS = "lacksParameters";
  protected static final String INVALID_PARAMS = "invalidParams";
  protected static final String NOT_UNIQUE = "notUnique";
  protected static final String MISMATCH = "mismatch";
  protected static final String JAVERS = "javers";
  private static final String DELIMITER = ".";
  static final String SERVICE_ERROR_PREFIX = join(SERVICE_PREFIX, "error");
  public static final String ERROR_IO = SERVICE_ERROR_PREFIX + ".io";
  public static final String ERROR_FILE_IS_EMPTY = SERVICE_ERROR_PREFIX + ".file.empty";
  public static final String ERROR_INCORRECT_FILE_FORMAT = SERVICE_ERROR_PREFIX
      + ".file.format.incorrect";
  public static final String ERROR_JAVERS_EXISTING_ENTRY =
      join(SERVICE_ERROR_PREFIX, JAVERS, "entryAlreadyExists");
  private static final String ERROR_DTO_EXPANSION = join(SERVICE_ERROR_PREFIX, "dtoExpansion");
  public static final String ERROR_DTO_EXPANSION_CAST = join(ERROR_DTO_EXPANSION, "cast");
  public static final String ERROR_DTO_EXPANSION_HREF = join(ERROR_DTO_EXPANSION, "href");
  public static final String ERROR_DTO_EXPANSION_ASSIGNMENT = join(ERROR_DTO_EXPANSION,
      "assignment");
  public static final String CAN_NOT_FIND_PROGRAM_DETAILS_FROM_ORDERABLE =
          SERVICE_ERROR_PREFIX + ".canNotFindProgramDetailsFromOrderable";

  MessageKeys() {
    throw new UnsupportedOperationException();
  }

  protected static String join(String... params) {
    return String.join(DELIMITER, Arrays.asList(params));
  }
}
