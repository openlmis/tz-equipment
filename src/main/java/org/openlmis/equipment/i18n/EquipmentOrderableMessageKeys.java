/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.i18n;

public class EquipmentOrderableMessageKeys extends MessageKeys {
  private static final String ERROR_PREFIX
      = SERVICE_ERROR_PREFIX + ".equipmentOrderable";

  public static final String ORDERABLE_ID_REQUIRED
      = join(ERROR_PREFIX, "orderableId.required");

  public static final String EQUIPMENT_ID_REQUIRED
      = join(ERROR_PREFIX,"equipmentId.required");

  public static final String ERROR_EQUIPMENT_ID_NOT_FOUND
      = join(ERROR_PREFIX,"notFound");

}


