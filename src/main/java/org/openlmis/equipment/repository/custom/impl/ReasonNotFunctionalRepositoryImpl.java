/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.repository.custom.impl;

import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.openlmis.equipment.domain.ReasonNotFunctional;
import org.openlmis.equipment.repository.custom.ReasonNotFunctionalRepositoryCustom;
import org.openlmis.equipment.service.ReasonNotFunctionalService;
import org.openlmis.equipment.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;

public class ReasonNotFunctionalRepositoryImpl implements ReasonNotFunctionalRepositoryCustom {
  private static final String HQL_COUNT = "SELECT DISTINCT COUNT(*)"
      + " FROM ReasonNotFunctional AS f";

  private static final String HQL_SELECT = "SELECT DISTINCT f"
      + " FROM ReasonNotFunctional AS f";

  private static final String WHERE = "WHERE";
  private static final String AND = " AND ";
  private static final String DEFAULT_SORT = "f.name ASC";
  private static final String ORDER_BY = "ORDER BY";

  private static final String WITH_NAME = "UPPER(f.name) LIKE :name";
  private static final String WITH_IDS = "f.id IN (:ids)";

  @PersistenceContext
  private EntityManager entityManager;

  /**
   * Search reason not functional based on the provided parameters.
   *
   * @param searchParams Search parameters containing id, or name.
   * @param pageable     pagination information.
   *
   * @return a page of found reason not functional which conforms the search parameters.
   */
  public Page<ReasonNotFunctional> search(
      ReasonNotFunctionalService.SearchParams searchParams,
      Pageable pageable) {

    Pair<String, Map<String, Object>> countSql = prepareQuery(HQL_COUNT, searchParams);
    String countSqlStmnt = countSql.getFirst();
    Map<String, Object> countSqlParams = countSql.getSecond();

    Query countQuery = entityManager.createQuery(countSqlStmnt, Long.class);
    countSqlParams.forEach(countQuery::setParameter);

    Long count = (Long) countQuery.getSingleResult();

    if (count < 1) {
      return PaginationUtil.getPage(Collections.emptyList(), pageable, 0);
    }

    Pair<String, Map<String, Object>> selectSql = prepareQuery(HQL_SELECT, searchParams);
    String selectSqlStmnt = selectSql.getFirst();
    Map<String, Object> selectSqlParams = selectSql.getSecond();
    String hqlWithSort = Joiner.on(' ').join(
        Lists.newArrayList(
            selectSqlStmnt,
            ORDER_BY,
            PaginationUtil.getOrderPredicate(pageable, "f.", DEFAULT_SORT)
        )
    );

    Query searchQuery = entityManager.createQuery(hqlWithSort, ReasonNotFunctional.class);
    selectSqlParams.forEach(searchQuery::setParameter);
    List<ReasonNotFunctional> reasonNotFunctionals = searchQuery
        .setMaxResults(pageable.getPageSize())
        .setFirstResult(Math.toIntExact(pageable.getOffset()))
        .getResultList();

    return PaginationUtil.getPage(reasonNotFunctionals, pageable, count);
  }

  private Pair<String, Map<String, Object>> prepareQuery(
      String baseSql,
      ReasonNotFunctionalService.SearchParams searchParams) {

    List<String> sql = Lists.newArrayList(baseSql);
    List<String> where = Lists.newArrayList();
    Map<String, Object> params = new LinkedHashMap<>();

    if (isNotBlank(searchParams.getName())) {
      where.add(WITH_NAME);
      params.put("name", "%" + searchParams.getName().toUpperCase() + "%");
    }

    if (isNotEmpty(searchParams.getIds())) {
      where.add(WITH_IDS);
      params.put("ids", searchParams.getIds());
    }

    if (!where.isEmpty()) {
      sql.add(WHERE);
      sql.add(Joiner.on(AND).join(where));
    }

    String query = Joiner.on(' ').join(sql);
    return Pair.of(query, params);
  }
}
