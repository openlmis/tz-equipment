/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.repository;

import java.util.List;
import java.util.UUID;

import org.openlmis.equipment.domain.EquipmentType;
import org.openlmis.equipment.domain.EquipmentTypeProgram;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface EquipmentTypeProgramRepository extends
        PagingAndSortingRepository<EquipmentTypeProgram, UUID> {

  @Query("SELECT etp FROM EquipmentTypeProgram etp  WHERE etp.equipmentType = :equipmentType ")
  List<EquipmentTypeProgram> findByEquipmentType(@Param("equipmentType")
                                                         EquipmentType equipmentType);


  @Query("SELECT etp FROM EquipmentTypeProgram etp  WHERE etp.programId = :programId ")
  List<EquipmentTypeProgram> findByProgramId(@Param("programId") UUID programId);

  @Query("SELECT etp FROM EquipmentTypeProgram etp  WHERE etp.programId = :programId and "
          + " etp.equipmentType = :equipmentType")
  List<EquipmentTypeProgram> findByProgramIdAndType(@Param("programId") UUID programId,
                                                    @Param("equipmentType")
                                                            EquipmentType equipmentType);

  @Query(value = "SELECT\n"
          + "    r.*\n"
          + "FROM\n"
          + "    equipment.equipment_type_programs r\n"
          + " ",
          nativeQuery = true)
  Page<EquipmentTypeProgram> findAllWithoutSnapshots(Pageable pageable);
}
