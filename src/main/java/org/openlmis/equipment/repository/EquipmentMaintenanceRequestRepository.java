/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.repository;

import java.util.List;
import java.util.UUID;
import org.openlmis.equipment.domain.EquipmentMaintenanceRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface EquipmentMaintenanceRequestRepository
    extends PagingAndSortingRepository<EquipmentMaintenanceRequest, UUID> {
  @Query(value = "SELECT\n"
      + "    r.*\n"
      + "FROM\n"
      + "    equipment.equipment_maintenance_requests r\n"
      + " ",
      nativeQuery = true)
  Page<EquipmentMaintenanceRequest> findAllWithoutSnapshots(Pageable pageable);

  @Query(value = "SELECT * FROM equipment.equipment_maintenance_requests \n"
      + "left join equipment.equipment_inventory ei on "
      + "(ei.id = equipment_maintenance_requests.inventoryId) "
      + "JOIN equipment.vendor esv on (equipment_maintenance_requests.vendorId = esv.id) "
      + "JOIN equipment.equipment_maintenance_request_reasons fc on "
      + " (equipment_maintenance_requests.reasonId = fc.id) "
      + "where esv.id = :vendorId and resolved = false", nativeQuery = true)
  List<EquipmentMaintenanceRequest> findByVendorId(@Param("vendorId") UUID vendorId);

}
