/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.openlmis.equipment.i18n.MessageKeys.ERROR_NOT_FOUND;
import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;
import static org.openlmis.equipment.web.EquipmentTypeProgramController.RESOURCE_PATH;

import com.google.common.collect.Sets;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import lombok.NoArgsConstructor;
import org.openlmis.equipment.domain.EquipmentTypeProgram;
import org.openlmis.equipment.dto.EquipmentTypeProgramDto;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.EquipmentTypeProgramMessageKeys;
import org.openlmis.equipment.i18n.MessageKeys;
import org.openlmis.equipment.repository.EquipmentTypeProgramRepository;
import org.openlmis.equipment.service.PermissionService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@NoArgsConstructor(force = true)
@Controller
@Transactional
@RequestMapping(RESOURCE_PATH)
public class EquipmentTypeProgramController  extends BaseController {

  static final String RESOURCE_PATH = BASE_PATH + "/equipmentTypeProgram";

  private static final Logger LOGGER =
          LoggerFactory.getLogger(EquipmentTypeProgramController.class);

  @Autowired
  private EquipmentTypeProgramRepository equipmentTypeProgramRepository;

  @Autowired
  private PermissionService permissionService;

  public EquipmentTypeProgramController(EquipmentTypeProgramRepository repository) {
    this.equipmentTypeProgramRepository = Objects.requireNonNull(repository);
  }

  /**
   * Save a program equipment type using the provided program equipment type DTO
   * If the type does not exist, will create one. If it
   * does exist, will throw existing errors.
   *
   * @param equipmentTypeProgramDto provided equipment type DTO.
   * @return the saved program equipment type.
   */
  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public EquipmentTypeProgramDto create(
          @RequestBody EquipmentTypeProgramDto equipmentTypeProgramDto) {

    permissionService.canManageCce();

    if (equipmentTypeProgramDto.getProgramId() == null
            || equipmentTypeProgramDto.getEquipmentType() == null) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }
    System.out.println(equipmentTypeProgramDto.toString());

    List<EquipmentTypeProgram>  equipmentTypePrograms = equipmentTypeProgramRepository
             .findByProgramIdAndType(equipmentTypeProgramDto.getProgramId(),
                    equipmentTypeProgramDto.getEquipmentType());

    if (equipmentTypePrograms.size() > 0) {
      throw new ValidationMessageException(EquipmentTypeProgramMessageKeys.ERROR_EQUIPMENT_EXISTS);
    }
    EquipmentTypeProgram equipmentTypeProgramSave =
          EquipmentTypeProgram.newEquipmentTypeProgram(equipmentTypeProgramDto);
    equipmentTypeProgramSave = equipmentTypeProgramRepository.save(equipmentTypeProgramSave);
    LOGGER.debug("Saved equipment type program with id: " + equipmentTypeProgramSave.getId());

    return exportToDto(equipmentTypeProgramSave);
  }

  /**
   * Get all program equipment types in the system.
   * @return all equipment types program in the system.
   */
  @GetMapping()
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<EquipmentTypeProgramDto> findAll(@RequestParam(required = false) UUID programId,
                                        Pageable pageable) {

    Set<EquipmentTypeProgram> equipmentTypesProgram;
    if (programId != null) {
      equipmentTypesProgram = Sets.newHashSet(equipmentTypeProgramRepository
              .findByProgramId(programId));
    } else {
      equipmentTypesProgram = Sets.newHashSet(equipmentTypeProgramRepository.findAll());
    }

    Set<EquipmentTypeProgramDto> equipmentTypeProgramDtos =
            equipmentTypesProgram.stream().map(this::exportToDto).collect(Collectors.toSet());

    return toPage(equipmentTypeProgramDtos, pageable);
  }



  /**
   * Get chosen program equipment type.
   *
   * @param id id of the equipment type program to get.
   * @return the equipment type program.
   */
  @GetMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentTypeProgramDto get(@PathVariable("id") UUID id) {

    EquipmentTypeProgram equipmentTypeProgram =
            equipmentTypeProgramRepository.findById(id)
                    .orElseThrow(() -> new NotFoundException(ERROR_NOT_FOUND));
    return exportToDto(equipmentTypeProgram);
  }

  /**
   * Delete chosen program equipment type.
   *
   * @param id id of the equipment type program to get.
   */

  @DeleteMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void remove(@PathVariable("id") UUID id) {

    permissionService.canManageCce();

    equipmentTypeProgramRepository.findById(id)
                    .orElseThrow(() -> new NotFoundException(ERROR_NOT_FOUND));

    equipmentTypeProgramRepository.deleteById(id);
  }

  /**
   * Updating Program Equipment Type.
   *
   * @param equipmentTypeProgramDto a EquipmentTypeProgramDto bound to the request body.
   * @param equipmentTypeProgramId  the UUID of Program Equipment Type which we want to update.
   * @return the updated EquipmentTypeProgramDto.
   */

  @PutMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentTypeProgramDto update(
          @RequestBody EquipmentTypeProgramDto equipmentTypeProgramDto,
          @PathVariable("id") UUID equipmentTypeProgramId) {

    permissionService.canManageCce();

    if (equipmentTypeProgramDto.getProgramId() == null
            || equipmentTypeProgramDto.getEquipmentType() == null) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    EquipmentTypeProgram template =
            EquipmentTypeProgram.newEquipmentTypeProgram(equipmentTypeProgramDto);
    EquipmentTypeProgram equipmentTypeToUpdate =
            equipmentTypeProgramRepository.findById(equipmentTypeProgramId).orElse(null);

    EquipmentTypeProgram equipmentTypeToSave;
    if (equipmentTypeToUpdate == null) {
      equipmentTypeToSave = template;
      equipmentTypeToSave.setId(equipmentTypeProgramId);
    } else {
      equipmentTypeToSave = equipmentTypeToUpdate;
      equipmentTypeToSave.updateFrom(template);
    }

    equipmentTypeToSave = equipmentTypeProgramRepository.save(equipmentTypeToSave);

    return exportToDto(equipmentTypeToSave);
  }


  private EquipmentTypeProgramDto exportToDto(EquipmentTypeProgram equipmentTypeProgram) {
    EquipmentTypeProgramDto equipmentTypeProgramDto = new EquipmentTypeProgramDto();
    equipmentTypeProgram.export(equipmentTypeProgramDto);
    return equipmentTypeProgramDto;
  }

}
