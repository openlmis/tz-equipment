/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.openlmis.equipment.domain.Equipment;
import org.openlmis.equipment.domain.EquipmentOrderable;
import org.openlmis.equipment.dto.EquipmentOrderableDto;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.EquipmentOrderableMessageKeys;
import org.openlmis.equipment.i18n.MessageKeys;
import org.openlmis.equipment.repository.EquipmentOrderableRepository;
import org.openlmis.equipment.service.EquipmentService;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(EquipmentOrderableController.RESOURCE_PATH)
@RequiredArgsConstructor
public class EquipmentOrderableController extends BaseController {

  public static final String RESOURCE_PATH = BASE_PATH + "/equipmentOrderables";

  @Autowired
  private PermissionService permissionService;

  @Autowired
  private EquipmentOrderableRepository repository;

  @Autowired
  private EquipmentService equipmentService;

  /**
   * Get all Equipment Orderable base on optional equipment or orderable paramters.
   *
   * @return Equipment Orderable.
   */
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<EquipmentOrderableDto> findAll(
      @RequestParam(value = "equipmentId", required = false) Optional<UUID> equipmentId,
      @RequestParam(value = "orderableId", required = false) Optional<UUID> orderableId,
      Pageable pageable) {

    permissionService.canManageCce();

    Page<EquipmentOrderable> page;
    if (equipmentId.isPresent() && orderableId.isPresent()) {
      page = repository.findAllByEquipment_IdAndOrderableId(
          equipmentId.get(), orderableId.get(), pageable
      );
    } else if (equipmentId.isPresent()) {
      page = repository.findAllByEquipment_Id(equipmentId.get(), pageable);
    } else if (orderableId.isPresent()) {
      page = repository.findAllByOrderableId(orderableId.get(), pageable);
    } else {
      page = repository.findAll(pageable);
    }

    return page.map(equipmentOrderable -> toEquipmentOrderableDto(equipmentOrderable));
  }

  /**
   * Delete equipment orderable mapping of a certain Id.
   *
   * @param equipmentOrderableId id of the equipment orderable mapping to be deleted.
   *
   */
  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  public void delete(@PathVariable(value = "id") UUID equipmentOrderableId) {
    permissionService.canManageCce();

    if (!repository.existsById(equipmentOrderableId)) {
      throw new NotFoundException(EquipmentOrderableMessageKeys.ERROR_NOT_FOUND);
    }
    repository.deleteById(equipmentOrderableId);
  }

  /**
   * Save a program equipment type using the provided program equipment orderable DTO
   * If the type does not exist, will create one. If it
   * does exist, will throw existing errors.
   *
   * @param data provided equipment type DTO.
   *
   * @return the saved program equipment orderable.
   */
  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public EquipmentOrderableDto create(@RequestBody EquipmentOrderableDto data) {

    permissionService.canManageCce();

    if (data.getOrderableId() == null
        || data.getEquipment() == null) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    EquipmentOrderable equipmentOrderable = repository.save(toEquipmentOrderable(data));
    data = toEquipmentOrderableDto(equipmentOrderable);
    return data;
  }

  /**
   * Retrieve equipment orderable mapping using its Id.
   *
   * @param id id of the equipment orderable mapping to be retrieved.
   *
   * @return equipment orderable mapping.
   */
  @GetMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentOrderableDto getEquipmentOrderable(@PathVariable("id") UUID id) {
    permissionService.canManageCce();

    EquipmentOrderable orderable = repository.findById(id)
        .orElseThrow(
            () -> new NotFoundException(
                new Message(EquipmentOrderableMessageKeys.ERROR_NOT_FOUND)
            )
        );
    return EquipmentOrderableDto.newInstance(orderable);
  }


  private EquipmentOrderableDto toEquipmentOrderableDto(EquipmentOrderable equipmentOrderable) {
    EquipmentOrderableDto equipmentOrderableDto = new EquipmentOrderableDto();
    equipmentOrderable.export(equipmentOrderableDto);
    return equipmentOrderableDto;
  }

  private EquipmentOrderable toEquipmentOrderable(EquipmentOrderable.Importer importer) {
    EquipmentOrderable equipmentOrderable = new EquipmentOrderable();
    equipmentOrderable.setId(importer.getId());
    equipmentOrderable.setOrderableId(importer.getOrderableId());

    Equipment equipment = equipmentService.getEquipment(importer.getEquipment().getId());
    equipmentOrderable.setEquipment(equipment);

    return equipmentOrderable;
  }
}
