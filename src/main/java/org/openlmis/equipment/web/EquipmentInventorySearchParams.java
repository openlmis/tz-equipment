/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import java.util.Set;
import java.util.UUID;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.openlmis.equipment.domain.FunctionalStatus;
import org.openlmis.equipment.domain.Utilization;
import org.openlmis.equipment.service.EquipmentInventoryService;

@Getter
@Setter
@EqualsAndHashCode
public class EquipmentInventorySearchParams implements EquipmentInventoryService.SearchParams {

  private Set<UUID> ids;

  private Set<UUID> equipmentIds;

  private Set<UUID> equipmentTypeIds;

  private Set<UUID> orderableIds;

  private Set<Utilization> utilizations;

  private Set<FunctionalStatus> functionalStatuses;
}
