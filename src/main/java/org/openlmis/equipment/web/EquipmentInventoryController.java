/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;
import static org.openlmis.equipment.web.EquipmentInventoryController.RESOURCE_PATH;

import java.time.ZonedDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import javax.validation.Valid;
import org.openlmis.equipment.domain.Equipment;
import org.openlmis.equipment.domain.EquipmentFundSource;
import org.openlmis.equipment.domain.EquipmentInventory;
import org.openlmis.equipment.domain.ReasonNotFunctional;
import org.openlmis.equipment.dto.EquipmentInventoryDto;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.EquipmentInventoryMessageKeys;
import org.openlmis.equipment.repository.EquipmentInventoryRepository;
import org.openlmis.equipment.service.EquipmentFundSourceService;
import org.openlmis.equipment.service.EquipmentInventoryService;
import org.openlmis.equipment.service.EquipmentService;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.service.ReasonNotFunctionalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@Transactional
@RequestMapping(RESOURCE_PATH)
public class EquipmentInventoryController extends BaseController {

  static final String RESOURCE_PATH = BASE_PATH + "/equipmentInventories";

  @Autowired
  private EquipmentInventoryRepository inventoryRepository;

  @Autowired
  private EquipmentInventoryService equipmentInventoryService;

  @Autowired
  private EquipmentService equipmentService;

  @Autowired
  private EquipmentFundSourceService fundSourceService;

  @Autowired
  private ReasonNotFunctionalService reasonNotFunctionalService;

  @Autowired
  private PermissionService permissionService;

  /**
   * Allows creating new equipment inventory item. If the id is specified, it will be ignored.
   *
   * @param equipmentInventoryDto A equipment inventory item bound to the request body.
   *
   * @return created equipment inventory item.
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public EquipmentInventoryDto create(
      @RequestBody @Valid EquipmentInventoryDto equipmentInventoryDto) {

    permissionService.canEditInventory(
        equipmentInventoryDto.getProgramId(), equipmentInventoryDto.getFacilityId());
    equipmentInventoryDto.setId(null);
    EquipmentInventory equipmentInventory = newEquipmentInventory(equipmentInventoryDto);
    EquipmentInventoryDto dto = saveInventory(equipmentInventory);

    return dto;
  }

  /**
   * Get chosen equipment inventory item.
   *
   * @param equipmentInventoryId UUID of equipment inventory item which we want to get
   *
   * @return equipment nventory item.
   */
  @GetMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentInventoryDto getEquipmentInventory(
      @PathVariable("id") UUID equipmentInventoryId) {
    EquipmentInventory equipmentInventory = inventoryRepository.findById(equipmentInventoryId)
        .orElseThrow(() -> new NotFoundException(EquipmentInventoryMessageKeys.ERROR_NOT_FOUND));

    permissionService.canViewInventory(equipmentInventory);
    return EquipmentInventoryDto.newInstance(equipmentInventory);
  }

  /**
   * Search equipment inventory items that user has right for.
   *
   * @return equipment inventory items.
   */
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<EquipmentInventoryDto> searchEquipmentInventories(
      @RequestParam("facilityId") UUID facilityId,
      @RequestParam("programId") UUID programId,
      @Valid EquipmentInventorySearchParams params,
      Pageable pageable) {

    Page<EquipmentInventory> foundEquipments = equipmentInventoryService.searchEquipmentInventory(
        facilityId,
        programId,
        params,
        pageable
    );
    return foundEquipments.map(equipment -> EquipmentInventoryDto.newInstance(equipment));
  }

  /**
   * Search orderable equipment inventory items.
   *
   * @return orderable equipment inventory items map.
   */
  @GetMapping("/orderables")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Map<UUID, List<EquipmentInventoryDto>> searchOrderablesEquipmentInventories(
      @RequestParam("facilityId") UUID facilityId,
      @RequestParam("programId") UUID programId,
      EquipmentInventorySearchParams params) {


    Map<UUID, List<EquipmentInventory>> foundEquipments =
        equipmentInventoryService.searchOrderableEquipmentInventories(
            facilityId,
            programId,
            params
        );

    Map<UUID, List<EquipmentInventoryDto>> foundEquipmentDtos = new LinkedHashMap<>();
    for (UUID orderableId : foundEquipments.keySet()) {
      foundEquipmentDtos.put(
          orderableId,
          EquipmentInventoryDto.toList(foundEquipments.get(orderableId))
      );
    }

    return foundEquipmentDtos;
  }

  /**
   * Updates equipment inventory item.
   *
   * @param equipmentInventoryId  UUID of equipment inventory item which we want to update
   * @param equipmentInventoryDto equipment inventory that will be updated
   *
   * @return updated equipment inventory item.
   */
  @PutMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentInventoryDto updateEquipmentInventory(
      @RequestBody @Valid EquipmentInventoryDto equipmentInventoryDto,
      @PathVariable("id") UUID equipmentInventoryId) {
    if (!equipmentInventoryDto.getId().equals(equipmentInventoryId)) {
      throw new ValidationMessageException(EquipmentInventoryMessageKeys.ERROR_ID_MISMATCH);
    }
    Optional<EquipmentInventory> existingInventory = inventoryRepository.findById(
        equipmentInventoryId
    );

    if (existingInventory.isPresent()) {
      permissionService.canEditInventory(existingInventory.get());
    } else {
      permissionService.canEditInventory(
          equipmentInventoryDto.getProgramId(), equipmentInventoryDto.getFacilityId());
    }

    EquipmentInventoryDto dto;
    if (existingInventory.isPresent()) {
      dto = updateInventory(equipmentInventoryDto, existingInventory.get());
    } else {
      EquipmentInventory equipmentInventory = newEquipmentInventory(equipmentInventoryDto);
      dto = saveInventory(equipmentInventory);
    }

    return dto;
  }

  /**
   * Deletes equipment inventory item with the given id.
   */
  @DeleteMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteEquipmentInventory(@PathVariable("id") UUID id) {
    Optional<EquipmentInventory> equipmentInventory = inventoryRepository.findById(id);
    if (!equipmentInventory.isPresent()) {
      throw new NotFoundException(EquipmentInventoryMessageKeys.ERROR_NOT_FOUND);
    }

    permissionService.canEditInventory(equipmentInventory.get());
    inventoryRepository.delete(equipmentInventory.get());
  }

  private EquipmentInventoryDto updateInventory(EquipmentInventoryDto equipmentInventoryDto,
                                                EquipmentInventory existingInventory) {
    EquipmentInventory equipmentInventory = newEquipmentInventory(equipmentInventoryDto);
    existingInventory.updateFrom(equipmentInventory);
    EquipmentInventoryDto itemDto = saveInventory(existingInventory);
    return itemDto;
  }

  private EquipmentInventory newEquipmentInventory(EquipmentInventoryDto dto) {
    Equipment equipment = equipmentService.getEquipment(dto.getEquipment().getId());
    ReasonNotFunctional reasonNotFunctional = null;
    if (dto.getReasonNotFunctional() != null) {
      reasonNotFunctional = reasonNotFunctionalService.getReasonNotFunctional(
          dto.getReasonNotFunctional().getId()
      );
    }

    EquipmentFundSource fundSource = null;
    if (dto.getFundSource() != null) {
      fundSource = fundSourceService.getEquipmentFundSource(dto.getFundSource().getId());
    }

    return new EquipmentInventory(
        equipment,
        dto.getFacilityId(),
        dto.getProgramId(),
        fundSource,
        dto.getFunctionalStatus(),
        reasonNotFunctional,
        dto.getUtilization(),
        dto.getAdditionalNotes(),
        dto.getDecommissionDate(),
        dto.getEquipmentTrackingId(),
        dto.getSource(),
        dto.getDateOfInstallation(),
        dto.getDateOfWarrantyExpiry(),
        dto.getPurchasePrice(),
        dto.getRegistrationStatus()
    );
  }

  private EquipmentInventoryDto saveInventory(EquipmentInventory equipmentInventory) {
    equipmentInventory.setModifiedDate(ZonedDateTime.now());
    return EquipmentInventoryDto.newInstance(inventoryRepository.save(equipmentInventory));
  }
}
