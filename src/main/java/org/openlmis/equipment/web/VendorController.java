/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import java.util.Optional;
import java.util.UUID;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.openlmis.equipment.domain.Vendor;
import org.openlmis.equipment.dto.VendorDto;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.MessageKeys;
import org.openlmis.equipment.service.VendorService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(VendorController.RESOURCE_PATH)
@RequiredArgsConstructor
public class VendorController extends BaseController {

  public static final String RESOURCE_PATH = BASE_PATH + "/vendors";

  private final VendorService vendorService;


  /**
   * Create a new vendor.
   *
   * @param vendorDto     details of the vendor to be created.
   * @param bindingResult validation results of the vendor details.
   *
   * @return
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public VendorDto create(@RequestBody @Valid VendorDto vendorDto,
                          BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    Vendor vendor = vendorService.create(vendorDto);
    return VendorDto.newInstance(vendor);
  }

  /**
   * Update a specified vendor using its ID.
   *
   * @param vendorDto     details of the vendor to be updated.
   * @param bindingResult validation results of the vendor details.
   *
   * @return
   */
  @PutMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public VendorDto update(@PathVariable("id") UUID vendorId,
                          @RequestBody @Valid VendorDto vendorDto,
                          BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    vendorDto.setId(vendorId);
    Vendor vendor = vendorService.update(vendorId, vendorDto);
    return VendorDto.newInstance(vendor);
  }

  /**
   * Retrieve vendor using its Id.
   *
   * @param vendorId id of the vendor to be retrieved.
   *
   * @return vendor of specified id.
   */
  @GetMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public VendorDto getVendor(@PathVariable("id") UUID vendorId) {
    Vendor vendor = vendorService.getVendor(vendorId);
    return VendorDto.newInstance(vendor);
  }

  /**
   * Retrieves all vendors with name similar to name parameter.
   *
   * @param queryParams name query parameter.
   * @param pageable    object used to encapsulate the pagination related values:
   *                    page, size and sort.
   *
   * @return Page of vendors matching query parameters.
   */
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<VendorDto> searchVendors(
      @RequestParam(required = false, name = "name") Optional<String> queryParams,
      Pageable pageable) {

    return queryParams
        .map(nameFilter -> vendorService.search(nameFilter, pageable))
        .orElseGet(() -> vendorService.findAll(pageable))
        .map(vendor -> VendorDto.newInstance(vendor));
  }
}
