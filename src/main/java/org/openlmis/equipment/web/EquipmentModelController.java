/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import java.util.Map;
import java.util.UUID;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.openlmis.equipment.domain.EquipmentModel;
import org.openlmis.equipment.dto.EquipmentModelDto;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.MessageKeys;
import org.openlmis.equipment.service.EquipmentModelService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(EquipmentModelController.RESOURCE_PATH)
@RequiredArgsConstructor
public class EquipmentModelController extends BaseController {

  public static final String RESOURCE_PATH = BASE_PATH + "/equipmentModels";

  private final EquipmentModelService equipmentModelService;

  /**
   * Create a new equipment model.
   *
   * @param modelDto      details of the model to be created.
   * @param bindingResult validation results of the model details.
   * @return
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public EquipmentModelDto create(@RequestBody @Valid EquipmentModelDto modelDto,
                                  BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    EquipmentModel model = equipmentModelService.create(modelDto);
    return EquipmentModelDto.newInstance(model);
  }

  /**
   * Update a specified equipment model using its ID.
   *
   * @param modelDto      details of the model to be updated.
   * @param bindingResult validation results of the model details.
   * @return
   */
  @PutMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentModelDto update(@PathVariable("id") UUID modelId,
                                  @RequestBody @Valid EquipmentModelDto modelDto,
                                  BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    modelDto.setId(modelId);
    EquipmentModel model = equipmentModelService.update(modelId, modelDto);
    return EquipmentModelDto.newInstance(model);
  }

  /**
   * Retrieve equipment model using its Id.
   *
   * @param equipmentId id of the model to be retrieved.
   * @return
   */
  @GetMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentModelDto getEquipmentModel(@PathVariable("id") UUID equipmentId) {
    EquipmentModel model = equipmentModelService.getEquipmentModel(equipmentId);
    return EquipmentModelDto.newInstance(model);
  }

  /**
   * Retrieves all equipment models with code similar to code parameter or name similar to
   * name parameter or equipment type whose id is similar to equipmentTypeId parameter or.
   * whose id is among the ids parameters
   *
   * @param queryParams request parameters (code, name,  equipmentTypeId).
   * @param pageable    object used to encapsulate the pagination related values:
   *                    page, size and sort.
   * @return List of required equipment models matching query parameters.
   */
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<EquipmentModelDto> searchEquipmentModels(
      @RequestParam(required = false)
          Map<String, Object> queryParams,
      Pageable pageable) {

    MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
    if (!CollectionUtils.isEmpty(queryParams)) {
      queryParams.forEach(map::add);
    }

    EquipmentModelSearchParams params = new EquipmentModelSearchParams(map);

    Page<EquipmentModel> foundEquipmentModels = equipmentModelService.search(params, pageable);
    return foundEquipmentModels.map(model -> EquipmentModelDto.newInstance(model));
  }
}
