/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import java.util.Optional;
import java.util.UUID;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.openlmis.equipment.domain.EquipmentEnergyType;
import org.openlmis.equipment.dto.EquipmentEnergyTypeDto;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.MessageKeys;
import org.openlmis.equipment.service.EquipmentEnergyTypeService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(EquipmentEnergyTypeController.RESOURCE_PATH)
@RequiredArgsConstructor
public class EquipmentEnergyTypeController extends BaseController {

  public static final String RESOURCE_PATH = BASE_PATH + "/equipmentEnergyTypes";

  private final EquipmentEnergyTypeService equipmentEnergyTypeService;

  /**
   * Create a new equipment energy type.
   *
   * @param energyTypeDto details of the energy type to be created.
   * @param bindingResult validation results of the energy type details.
   * @return
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public EquipmentEnergyTypeDto create(@RequestBody @Valid EquipmentEnergyTypeDto energyTypeDto,
                                       BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    EquipmentEnergyType energyType = equipmentEnergyTypeService.create(energyTypeDto);
    return EquipmentEnergyTypeDto.newInstance(energyType);
  }

  /**
   * Update a specified equipment energy type using its ID.
   *
   * @param energyTypeDto details of the energy type to be updated.
   * @param bindingResult validation results of the energy type details.
   * @return
   */
  @PutMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentEnergyTypeDto update(@PathVariable("id") UUID energyTypeId,
                                       @RequestBody @Valid EquipmentEnergyTypeDto energyTypeDto,
                                       BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    energyTypeDto.setId(energyTypeId);
    EquipmentEnergyType energyType = equipmentEnergyTypeService.update(energyTypeId, energyTypeDto);
    return EquipmentEnergyTypeDto.newInstance(energyType);
  }

  /**
   * Retrieve equipment energy type using its Id.
   *
   * @param id id of the energy type to be retrieved.
   * @return
   */
  @GetMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentEnergyTypeDto getEquipmentEnergyType(@PathVariable("id") UUID id) {
    EquipmentEnergyType energyType = equipmentEnergyTypeService.getEquipmentEnergyType(id);
    return EquipmentEnergyTypeDto.newInstance(energyType);
  }

  /**
   * Retrieves all equipment energy types with code similar to code parameter or name similar to
   * name parameter.
   *
   * @param name     name search query parameter.
   * @param code     code search query parameter.
   * @param pageable object used to encapsulate the pagination related values:
   *                 page, size and sort.
   * @return List of required equipment energy types matching query parameters.
   */
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<EquipmentEnergyTypeDto> searchEquipmentEnergyTypes(
      @RequestParam(required = false) Optional<String> name,
      @RequestParam(required = false) Optional<String> code,
      Pageable pageable) {

    Page<EquipmentEnergyType> foundEnergyTypes = equipmentEnergyTypeService.search(
        name.orElse(""),
        code.orElse(""),
        pageable
    );
    return foundEnergyTypes.map(energyType -> EquipmentEnergyTypeDto.newInstance(energyType));
  }
}
