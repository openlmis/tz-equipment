/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import java.util.Map;
import java.util.UUID;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.openlmis.equipment.domain.Contract;
import org.openlmis.equipment.dto.ContractDto;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.MessageKeys;
import org.openlmis.equipment.service.ContractService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ContractController.RESOURCE_PATH)
@RequiredArgsConstructor
public class ContractController extends BaseController {

  public static final String RESOURCE_PATH = BASE_PATH + "/contracts";

  private final ContractService contractService;


  /**
   * Create a new contract.
   *
   * @param contractDto   details of the contract to be created.
   * @param bindingResult validation results of the contract details.
   *
   * @return
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public ContractDto create(@RequestBody @Valid ContractDto contractDto,
                            BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    Contract contract = contractService.create(contractDto);
    return ContractDto.newInstance(contract);
  }

  /**
   * Update a specified contract using its ID.
   *
   * @param contractDto   details of the contract to be updated.
   * @param bindingResult validation results of the contract details.
   *
   * @return
   */
  @PutMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public ContractDto update(@PathVariable("id") UUID contractId,
                            @RequestBody @Valid ContractDto contractDto,
                            BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    contractDto.setId(contractId);
    Contract contract = contractService.update(contractId, contractDto);
    return ContractDto.newInstance(contract);
  }

  /**
   * Retrieve contract using its Id.
   *
   * @param contractId id of the contract to be retrieved.
   *
   * @return contract of specified id.
   */
  @GetMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public ContractDto getContract(@PathVariable("id") UUID contractId) {
    Contract contract = contractService.getContract(contractId);
    return ContractDto.newInstance(contract);
  }


  /**
   * Retrieves all contracts which confirms to the specified query params.
   *
   * @param queryParams request parameters (vendorIds,serviceTypeIds,equipmentIds,dateFrom,dateTo,
   *                    facilityIds,identifier,ids).
   * @param pageable    object used to encapsulate the pagination related values:
   *                    page, size and sort.
   *
   * @return List of required contracts matching query parameters.
   */
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<ContractDto> searchContracts(
      @RequestParam(required = false)
      Map<String, Object> queryParams,
      Pageable pageable) {

    MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
    if (!CollectionUtils.isEmpty(queryParams)) {
      queryParams.forEach(map::add);
    }

    ContractSearchParams params = new ContractSearchParams(map);

    Page<Contract> foundContracts = contractService.search(params, pageable);
    return foundContracts.map(contract -> ContractDto.newInstance(contract));
  }
}
