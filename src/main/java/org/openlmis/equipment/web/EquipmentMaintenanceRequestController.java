/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;
import static org.openlmis.equipment.web.EquipmentMaintenanceRequestController.RESOURCE_PATH;

import com.google.common.collect.Sets;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.openlmis.equipment.domain.EquipmentMaintenanceRequest;
import org.openlmis.equipment.domain.Vendor;
import org.openlmis.equipment.dto.EquipmentMaintenanceRequestDto;
import org.openlmis.equipment.dto.UserDto;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.MessageKeys;
import org.openlmis.equipment.repository.EquipmentMaintenanceRequestRepository;
import org.openlmis.equipment.repository.VendorRepository;
import org.openlmis.equipment.service.EquipmentMaintenanceRequestService;
import org.openlmis.equipment.util.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(RESOURCE_PATH)
@RequiredArgsConstructor
public class EquipmentMaintenanceRequestController
    extends BaseController {

  static final String RESOURCE_PATH = BASE_PATH + "/equipmentMaintenanceRequests";

  @Autowired
  private final EquipmentMaintenanceRequestService service;

  @Autowired
  private EquipmentMaintenanceRequestRepository repository;

  @Autowired
  private VendorRepository vendorRepository;

  @Autowired
  private AuthenticationHelper authenticationHelper;

  /**
   * Allows creating new equipment maintenance request item.
   * If the id is specified, it will be ignored.
   *
   * @param requestDto    A equipment maintenance request bound to the request body.
   * @param bindingResult A bindingResult for equipment maintenance request.
   * @return created equipment maintenance request.
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public EquipmentMaintenanceRequestDto create(@RequestBody @Valid
                                               EquipmentMaintenanceRequestDto requestDto,
                                               BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    EquipmentMaintenanceRequest request = service.create(requestDto);
    return EquipmentMaintenanceRequestDto.newInstance(request);
  }

  /**
   * Update a specified maintenance request using its ID.
   *
   * @param requestDto    details of the maintenance request to be updated.
   * @param bindingResult validation results of the maintenance request details.
   * @return
   */
  @PutMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentMaintenanceRequestDto update(@PathVariable("id") UUID requestId,
                                               @RequestBody @Valid
                                               EquipmentMaintenanceRequestDto
                                                   requestDto,
                                               BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    requestDto.setId(requestId);
    EquipmentMaintenanceRequest request
        = service.update(requestId, requestDto);
    return EquipmentMaintenanceRequestDto.newInstance(request);
  }

  /**
   * Get all equipment maintenance requests in the system.
   *
   * @return all equipment maintenance requests by vendor in the system.
   */
  @GetMapping(value = "/byVendor")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<EquipmentMaintenanceRequestDto> findAllByVendor(
      Pageable pageable) {

    UserDto userDto = authenticationHelper.getCurrentUser();

    List<Vendor> vendorList = vendorRepository
        .findVendorByAssociatedUsersContaining(userDto.getId());

    if (!vendorList.isEmpty()) {

      Set<EquipmentMaintenanceRequest> equipment
          = Sets.newHashSet(repository
          .findByVendorId(vendorList.get(0).getId()));

      Set<EquipmentMaintenanceRequestDto> equipmentDtos =
          equipment.stream().map(this::exportToDto).collect(Collectors.toSet());
      return toPage(equipmentDtos, pageable);
    }

    return null;

  }

  private EquipmentMaintenanceRequestDto exportToDto(EquipmentMaintenanceRequest
                                                         request) {
    EquipmentMaintenanceRequestDto equipmentDto
        = new EquipmentMaintenanceRequestDto();
    request.export(equipmentDto);
    return equipmentDto;
  }

}
