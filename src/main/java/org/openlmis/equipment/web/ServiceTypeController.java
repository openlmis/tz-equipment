/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import java.util.Optional;
import java.util.UUID;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.openlmis.equipment.domain.ServiceType;
import org.openlmis.equipment.dto.ServiceTypeDto;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.MessageKeys;
import org.openlmis.equipment.service.ServiceTypeService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ServiceTypeController.RESOURCE_PATH)
@RequiredArgsConstructor
public class ServiceTypeController extends BaseController {

  public static final String RESOURCE_PATH = BASE_PATH + "/serviceTypes";

  private final ServiceTypeService serviceTypeService;


  /**
   * Create a new service type.
   *
   * @param serviceTypeDto details of the service type to be created.
   * @param bindingResult  validation results of the service type details.
   *
   * @return
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public ServiceTypeDto create(@RequestBody @Valid ServiceTypeDto serviceTypeDto,
                               BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    ServiceType serviceType = serviceTypeService.create(serviceTypeDto);
    return ServiceTypeDto.newInstance(serviceType);
  }

  /**
   * Update a specified service type using its ID.
   *
   * @param serviceTypeDto details of the service type to be updated.
   * @param bindingResult  validation results of the service type details.
   *
   * @return
   */
  @PutMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public ServiceTypeDto update(@PathVariable("id") UUID serviceTypeId,
                               @RequestBody @Valid ServiceTypeDto serviceTypeDto,
                               BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    serviceTypeDto.setId(serviceTypeId);
    ServiceType serviceType = serviceTypeService.update(serviceTypeId, serviceTypeDto);
    return ServiceTypeDto.newInstance(serviceType);
  }

  /**
   * Retrieve service type using its Id.
   *
   * @param serviceTypeId id of the service type to be retrieved.
   *
   * @return service type of specified id.
   */
  @GetMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public ServiceTypeDto getServiceType(@PathVariable("id") UUID serviceTypeId) {
    ServiceType serviceType = serviceTypeService.getServiceType(serviceTypeId);
    return ServiceTypeDto.newInstance(serviceType);
  }

  /**
   * Retrieves all service types with name similar to name parameter.
   *
   * @param queryParams name query parameter.
   * @param pageable    object used to encapsulate the pagination related values:
   *                    page, size and sort.
   *
   * @return Page of service types matching query parameter.
   */
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<ServiceTypeDto> searchServiceTypes(
      @RequestParam(required = false, name = "name") Optional<String> queryParams,
      Pageable pageable) {

    return queryParams
        .map(nameFilter -> serviceTypeService.search(nameFilter, pageable))
        .orElseGet(() -> serviceTypeService.findAll(pageable))
        .map(serviceType -> ServiceTypeDto.newInstance(serviceType));
  }
}
