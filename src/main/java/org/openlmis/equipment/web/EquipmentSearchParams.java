/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static java.util.Arrays.asList;
import static org.openlmis.equipment.i18n.EquipmentModelMessageKeys.ERROR_INVALID_PARAMS;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.service.EquipmentService;
import org.openlmis.equipment.util.Message;
import org.springframework.util.MultiValueMap;

@EqualsAndHashCode
@ToString
public final class EquipmentSearchParams implements EquipmentService.SearchParams {

  private static final String ID = "id";
  private static final String MANUFACTURER = "manufacturer";
  private static final String NAME = "name";
  private static final String EQUIPMENT_ENERGY_TYPE_ID = "equipmentEnergyTypeId";
  private static final String EQUIPMENT_MODEL_ID = "equipmentModelId";
  private static final String EQUIPMENT_CATEGORY_ID = "equipmentCategoryId";

  private static final List<String> ALL_PARAMETERS =
      asList(
          ID, MANUFACTURER, NAME, EQUIPMENT_ENERGY_TYPE_ID,
          EQUIPMENT_MODEL_ID, EQUIPMENT_CATEGORY_ID
      );

  private RequestParams queryParams;

  /**
   * Wraps map of query params into an object.
   */
  public EquipmentSearchParams(MultiValueMap<String, Object> queryMap) {
    queryParams = new RequestParams(queryMap);
    validate();
  }

  /**
   * Gets {@link String} for "manufacturer" key from params.
   *
   * @return String value of manufacturer or null if params doesn't contain "manufacturer" key.
   */
  public String getManufacturer() {
    if (!queryParams.containsKey(MANUFACTURER)) {
      return null;
    }
    return queryParams.getFirst(MANUFACTURER);
  }

  /**
   * Gets {@link String} for "name" key from params.
   *
   * @return String value of name or null if params doesn't contain "name" key.
   */
  public String getName() {
    if (!queryParams.containsKey(NAME)) {
      return null;
    }
    return queryParams.getFirst(NAME);
  }

  /**
   * Gets equipment energy type id.
   * If param value has incorrect format {@link ValidationMessageException} will be thrown.
   *
   * @return UUID value of equipment energy type id or null if
   *     params doesn't contain "equipmentEnergyTypeId" key.
   */
  public Set<UUID> getEquipmentEnergyTypeIds() {
    if (!queryParams.containsKey(EQUIPMENT_ENERGY_TYPE_ID)) {
      return Collections.emptySet();
    }
    return queryParams.getUuids(EQUIPMENT_ENERGY_TYPE_ID);
  }

  /**
   * Gets equipment model id.
   * If param value has incorrect format {@link ValidationMessageException} will be thrown.
   *
   * @return UUID value of equipment model id or null if
   *     params doesn't contain "equipmentEnergyTypeId" key.
   */
  public Set<UUID> getEquipmentModelIds() {
    if (!queryParams.containsKey(EQUIPMENT_MODEL_ID)) {
      return Collections.emptySet();
    }
    return queryParams.getUuids(EQUIPMENT_MODEL_ID);
  }

  /**
   * Gets equipment category id.
   * If param value has incorrect format {@link ValidationMessageException} will be thrown.
   *
   * @return UUID value of equipment category id or null if
   *     params doesn't contain "equipmentEnergyTypeId" key.
   */
  public Set<UUID> getEquipmentCategoryIds() {
    if (!queryParams.containsKey(EQUIPMENT_CATEGORY_ID)) {
      return Collections.emptySet();
    }
    return queryParams.getUuids(EQUIPMENT_CATEGORY_ID);
  }


  /**
   * Gets {@link Set} of {@link UUID} for "id" key from params.
   *
   * @return List of ids from params, empty if there is no "id" param
   */
  public Set<UUID> getIds() {
    if (!queryParams.containsKey(ID)) {
      return Collections.emptySet();
    }
    return queryParams.getUuids(ID);
  }

  /**
   * Checks if query params are valid. Returns false if any provided param is not on supported list.
   */
  public void validate() {
    if (!ALL_PARAMETERS.containsAll(queryParams.keySet())) {
      throw new ValidationMessageException(new Message(ERROR_INVALID_PARAMS));
    }
  }
}
