/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import com.google.common.collect.Sets;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.openlmis.equipment.domain.EquipmentServiceLog;
import org.openlmis.equipment.dto.EquipmentMaintenanceRequestDto;
import org.openlmis.equipment.dto.EquipmentServiceLogDto;
import org.openlmis.equipment.dto.EquipmentServiceLogDto.ApproveDto;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.MessageKeys;
import org.openlmis.equipment.service.EquipmentMaintenanceRequestService;
import org.openlmis.equipment.service.EquipmentServiceLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(EquipmentServiceLogController.RESOURCE_PATH)
@RequiredArgsConstructor
public class EquipmentServiceLogController extends BaseController {

  public static final String RESOURCE_PATH = BASE_PATH + "/equipmentServiceLogs";

  private final EquipmentServiceLogService service;

  @Autowired
  private final EquipmentMaintenanceRequestService requestService;

  /**
   * Create a new equipment service log.
   *
   * @param serviceLogDto details of the equipment to be created.
   * @param bindingResult validation results of the equipment details.
   * @return
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public EquipmentServiceLogDto create(@RequestBody
                                       @Valid EquipmentServiceLogDto serviceLogDto,
                                       BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    //Update request status
    if (serviceLogDto.getRequest() != null) {

      EquipmentMaintenanceRequestDto requestDto = serviceLogDto.getRequest();
      requestDto.setResolved(true);
      requestService.update(requestDto.getId(), requestDto);

    }

    EquipmentServiceLog serviceLog = service.create(serviceLogDto);
    return EquipmentServiceLogDto.newInstance(serviceLog);
  }

  /**
   * Approved specified service equipment maintenance log.
   *
   * @param logId                id of the equipment maintenance log.
   * @param serviceLogApproveDto approval details.
   * @param bindingResult        validation results of the equipment details.
   *
   * @return equipment maintenance log information after approval.
   */
  @PutMapping(value = "/{id}/approve")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentServiceLogDto approve(@PathVariable("id") UUID logId,
                                        @RequestBody
                                        @Valid ApproveDto serviceLogApproveDto,
                                        BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    EquipmentServiceLog serviceLog = service.approve(logId, serviceLogApproveDto);
    return EquipmentServiceLogDto.newInstance(serviceLog);
  }

  /**
   * Update a specified equipment using its ID.
   *
   * @param serviceLogDto details of the equipment to be updated.
   * @param bindingResult validation results of the equipment details.
   *
   * @return
   */
  @PutMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentServiceLogDto update(@PathVariable("id") UUID logId,
                                       @RequestBody
                                       @Valid EquipmentServiceLogDto serviceLogDto,
                                       BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    if (serviceLogDto.getRequest() != null) {

      EquipmentMaintenanceRequestDto requestDto = serviceLogDto.getRequest();
      requestService.update(requestDto.getId(), requestDto);

    }

    serviceLogDto.setId(logId);
    EquipmentServiceLog serviceLog = service.update(logId, serviceLogDto);
    return EquipmentServiceLogDto.newInstance(serviceLog);
  }

  /**
   * Get all equipment logs by  item id.
   *
   * @return all equipment logs from the system by item id.
   */
  @GetMapping(value = "/{id}/byItem")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<EquipmentServiceLogDto> findLogsByItem(
      @PathVariable("id") UUID itemId,
      Pageable pageable) {

    Set<EquipmentServiceLog> equipment
        = Sets.newHashSet(service
        .findByItem(itemId));

    Set<EquipmentServiceLogDto> equipmentDtos =
        equipment.stream().map(this::exportToDto).collect(Collectors.toSet());
    return toPage(equipmentDtos, pageable);

  }

  private EquipmentServiceLogDto exportToDto(EquipmentServiceLog
                                                 request) {
    EquipmentServiceLogDto equipmentDto
        = new EquipmentServiceLogDto();
    request.export(equipmentDto);
    return equipmentDto;
  }

}
