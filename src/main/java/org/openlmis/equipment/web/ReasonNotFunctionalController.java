/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import java.util.Map;
import java.util.UUID;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.openlmis.equipment.domain.ReasonNotFunctional;
import org.openlmis.equipment.dto.ReasonNotFunctionalDto;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.MessageKeys;
import org.openlmis.equipment.service.ReasonNotFunctionalService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ReasonNotFunctionalController.RESOURCE_PATH)
@RequiredArgsConstructor
public class ReasonNotFunctionalController extends BaseController {

  public static final String RESOURCE_PATH = BASE_PATH + "/reasonsNotFunctional";

  private final ReasonNotFunctionalService reasonNotFunctionalService;

  /**
   * Create a new reason not functional.
   *
   * @param reasonDto     details of the reason to be created.
   * @param bindingResult validation results of the reason details.
   *
   * @return
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public ReasonNotFunctionalDto create(@RequestBody @Valid ReasonNotFunctionalDto reasonDto,
                                       BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    ReasonNotFunctional reason = reasonNotFunctionalService.create(reasonDto);
    return ReasonNotFunctionalDto.newInstance(reason);
  }

  /**
   * Update a specified reason not functional using its ID.
   *
   * @param reasonDto     details of the reason to be updated.
   * @param bindingResult validation results of the reason details.
   *
   * @return
   */
  @PutMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public ReasonNotFunctionalDto update(@PathVariable("id") UUID reasonId,
                                       @RequestBody @Valid ReasonNotFunctionalDto reasonDto,
                                       BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    reasonDto.setId(reasonId);
    ReasonNotFunctional reason = reasonNotFunctionalService.update(reasonId, reasonDto);
    return ReasonNotFunctionalDto.newInstance(reason);
  }

  /**
   * Retrieve reason not functional using its Id.
   *
   * @param reasonId id of the reason to be retrieved.
   *
   * @return
   */
  @GetMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public ReasonNotFunctionalDto getReasonNotFunctional(@PathVariable("id") UUID reasonId) {
    ReasonNotFunctional reason = reasonNotFunctionalService.getReasonNotFunctional(reasonId);
    return ReasonNotFunctionalDto.newInstance(reason);
  }

  /**
   * Retrieves all reason not functional with name similar to name filter parameter
   * or id is within the list of id parameters.
   *
   * @param queryParams request parameters (name, id).
   * @param pageable    object used to encapsulate the pagination related values:
   *                    page, size and sort.
   *
   * @return List of required reason not functional matching query parameters.
   */
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<ReasonNotFunctionalDto> searchReasonNotFunctional(
      @RequestParam(required = false)
          Map<String, Object> queryParams,
      Pageable pageable) {

    MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
    if (!CollectionUtils.isEmpty(queryParams)) {
      queryParams.forEach(map::add);
    }

    ReasonNotFunctionalSearchParams params = new ReasonNotFunctionalSearchParams(map);

    Page<ReasonNotFunctional> foundReasonNotFunctional = reasonNotFunctionalService.search(
        params,
        pageable
    );
    return foundReasonNotFunctional.map(reason -> ReasonNotFunctionalDto.newInstance(reason));
  }

}
