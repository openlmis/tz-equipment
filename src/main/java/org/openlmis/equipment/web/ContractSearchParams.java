/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static java.util.Arrays.asList;
import static org.openlmis.equipment.i18n.EquipmentModelMessageKeys.ERROR_INVALID_PARAMS;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.service.ContractService;
import org.openlmis.equipment.util.Message;
import org.springframework.util.MultiValueMap;

@EqualsAndHashCode
@ToString
public final class ContractSearchParams implements ContractService.SearchParams {

  public static final String ID = "id";

  public static final String IDENTIFIER = "identifier";

  public static final String VENDOR_ID = "vendorId";

  public static final String SERVICE_TYPE_ID = "serviceTypeId";

  public static final String EQUIPMENT_ID = "equipmentId";

  public static final String FACILITY_ID = "facilityId";

  public static final String DATE_FROM = "dateFrom";

  public static final String DATE_TO = "dateTo";

  private static final List<String> ALL_PARAMETERS =
      asList(
          ID, IDENTIFIER, VENDOR_ID, SERVICE_TYPE_ID, EQUIPMENT_ID,
          FACILITY_ID, DATE_FROM, DATE_TO
      );

  private RequestParams queryParams;

  /**
   * Wraps map of query params into an object.
   */
  public ContractSearchParams(MultiValueMap<String, Object> queryMap) {
    queryParams = new RequestParams(queryMap);
    validate();
  }

  /**
   * Gets {@link String} for "identifier" key from params.
   *
   * @return String value of identifier or null if params doesn't contain "identifier" key.
   */
  public String getIdentifier() {
    if (!queryParams.containsKey(IDENTIFIER)) {
      return null;
    }
    return queryParams.getFirst(IDENTIFIER);
  }

  @Override
  public LocalDate getDateFrom() {
    if (!queryParams.containsKey(DATE_FROM)) {
      return null;
    }
    return queryParams.getLocalDate(DATE_FROM);
  }

  @Override
  public LocalDate getDateTo() {
    if (!queryParams.containsKey(DATE_TO)) {
      return null;
    }
    return queryParams.getLocalDate(DATE_TO);
  }

  /**
   * Gets {@link String} for "vendor" key from params.
   *
   * @return String value of vendor or null if params doesn't contain "vendor" key.
   */
  public Set<UUID> getVendorIds() {
    if (!queryParams.containsKey(VENDOR_ID)) {
      return Collections.emptySet();
    }
    return queryParams.getUuids(VENDOR_ID);
  }

  /**
   * Gets service type id.
   * If param value has incorrect format {@link ValidationMessageException} will be thrown.
   *
   * @return UUID value of service type id or null if
   *     params doesn't contain "serviceTypeId" key.
   */
  public Set<UUID> getServiceTypeIds() {
    if (!queryParams.containsKey(SERVICE_TYPE_ID)) {
      return Collections.emptySet();
    }
    return queryParams.getUuids(SERVICE_TYPE_ID);
  }

  /**
   * Gets equipment id.
   * If param value has incorrect format {@link ValidationMessageException} will be thrown.
   *
   * @return UUID value of equipment id or null if
   *     params doesn't contain "equipmentId" key.
   */
  public Set<UUID> getEquipmentIds() {
    if (!queryParams.containsKey(EQUIPMENT_ID)) {
      return Collections.emptySet();
    }
    return queryParams.getUuids(EQUIPMENT_ID);
  }

  /**
   * Gets facility id.
   * If param value has incorrect format {@link ValidationMessageException} will be thrown.
   *
   * @return UUID value of facility id or null if
   *     params doesn't contain "facilityId" key.
   */
  public Set<UUID> getFacilityIds() {
    if (!queryParams.containsKey(FACILITY_ID)) {
      return Collections.emptySet();
    }
    return queryParams.getUuids(FACILITY_ID);
  }


  /**
   * Gets {@link Set} of {@link UUID} for "id" key from params.
   *
   * @return List of ids from params, empty if there is no "id" param
   */
  public Set<UUID> getIds() {
    if (!queryParams.containsKey(ID)) {
      return Collections.emptySet();
    }
    return queryParams.getUuids(ID);
  }

  /**
   * Checks if query params are valid.
   * Returns false if any provided param is not on supported list.
   */
  public void validate() {
    if (!ALL_PARAMETERS.containsAll(queryParams.keySet())) {
      throw new ValidationMessageException(new Message(ERROR_INVALID_PARAMS));
    }
  }
}
