/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.openlmis.equipment.i18n.MessageKeys.ERROR_NOT_FOUND;
import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;
import static org.openlmis.equipment.web.EquipmentTestTypeController.RESOURCE_PATH;

import com.google.common.collect.Sets;

import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import lombok.NoArgsConstructor;
import org.openlmis.equipment.domain.EquipmentTestType;
import org.openlmis.equipment.dto.EquipmentTestTypeDto;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.MessageKeys;
import org.openlmis.equipment.repository.EquipmentTestTypeRepository;
import org.openlmis.equipment.service.PermissionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@NoArgsConstructor
@Controller
@Transactional
@RequestMapping(RESOURCE_PATH)
public class EquipmentTestTypeController extends BaseController {
  static final String RESOURCE_PATH = BASE_PATH + "/equipmentTestTypes";

  private static final Logger LOGGER = LoggerFactory.getLogger(EquipmentTestTypeController.class);

  @Autowired
  private PermissionService permissionService;

  @Autowired
  private EquipmentTestTypeRepository equipmentTestTypeRepository;

  public EquipmentTestTypeController(EquipmentTestTypeRepository repository) {
    this.equipmentTestTypeRepository = Objects.requireNonNull(repository);
  }


  /**
   * Get all equipment test type in the system.
   *
   * @param pageable pagination params such as sort, offset , pageNumber e.t.c.
   * @return all equipment test type list in the system.
   */
  @GetMapping()
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<EquipmentTestTypeDto> findAll(Pageable pageable) {

    Set<EquipmentTestType> equipmentTestTypes =
            Sets.newHashSet(equipmentTestTypeRepository.findAll());
    Set<EquipmentTestTypeDto> equipmentTestTypeDtos =
            equipmentTestTypes.stream().map(this::exportToDto).collect(Collectors.toSet());

    return toPage(equipmentTestTypeDtos, pageable);
  }

  /**
   * Save equipment Test Type using provided equipment Test Type DTO
   * If test type does not exist, will create new one and
   * If does exist will update it.
   *
   * @param equipmentTestTypeDto provide Equipment Test Type DTO.
   * @return saved equipment test type.
   */
  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public EquipmentTestTypeDto create(
          @RequestBody EquipmentTestTypeDto equipmentTestTypeDto) {

    permissionService.canManageCce();

    if (equipmentTestTypeDto.getCode() == null
            ||
            equipmentTestTypeDto.getName() == null
            ||
            equipmentTestTypeDto.getEquipmentCategory() == null
            ||
            equipmentTestTypeDto.getEnabled() == null
            ||
            equipmentTestTypeDto.getDisplayOrder() == null) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }
    EquipmentTestType equipmentTestTypeToSave =
            EquipmentTestType.newEquipmentTestType(equipmentTestTypeDto);
    equipmentTestTypeToSave = equipmentTestTypeRepository.save(equipmentTestTypeToSave);
    LOGGER.debug("Saved equipment test type with id: " + equipmentTestTypeToSave.getId());

    return exportToDto(equipmentTestTypeToSave);
  }

  /**
   * Get chosen equipment test type.
   *
   * @param id of equipment test type to get.
   * @return equipment test type.
   */

  @GetMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentTestTypeDto get(@PathVariable("id") UUID id) {

    EquipmentTestType equipmentTestType =
            equipmentTestTypeRepository.findById(id)
                    .orElseThrow(() -> new NotFoundException(ERROR_NOT_FOUND));
    return exportToDto(equipmentTestType);
  }


  /**
   * Updating equipment Test Type.
   *
   * @param equipmentTestTypeDto a EquipmentTestTypeDto bound to the request body.
   * @param equipmentTestTypeId  the UUID of Equipment Test Type which we want to update.
   * @return updated equipment test type DTO.
   */
  @PutMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentTestTypeDto update(
          @RequestBody EquipmentTestTypeDto equipmentTestTypeDto,
          @PathVariable("id") UUID equipmentTestTypeId) {
    permissionService.canManageCce();

    if (equipmentTestTypeDto.getCode() == null
            ||
            equipmentTestTypeDto.getEnabled() == null
            ||
            equipmentTestTypeDto.getEquipmentCategory() == null
            ||
            equipmentTestTypeDto.getDisplayOrder() == null
            ||
            equipmentTestTypeDto.getName() == null) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    EquipmentTestType template = EquipmentTestType.newEquipmentTestType(equipmentTestTypeDto);
    EquipmentTestType equipmentTestTypeToUpdate =
            equipmentTestTypeRepository.findById(equipmentTestTypeId).orElse(null);

    EquipmentTestType equipmentTestTypeToSave;
    if (equipmentTestTypeToUpdate == null) {
      equipmentTestTypeToSave = template;
      equipmentTestTypeToSave.setId(equipmentTestTypeId);
    } else {
      equipmentTestTypeToSave = equipmentTestTypeToUpdate;
      equipmentTestTypeToSave.updateFrom(template);
    }

    equipmentTestTypeToSave = equipmentTestTypeRepository.save(equipmentTestTypeToSave);

    return exportToDto(equipmentTestTypeToSave);
  }

  private EquipmentTestTypeDto exportToDto(EquipmentTestType equipmentTestType) {
    EquipmentTestTypeDto equipmentTestTypeDto = new EquipmentTestTypeDto();
    equipmentTestType.export(equipmentTestTypeDto);
    return equipmentTestTypeDto;
  }
}
