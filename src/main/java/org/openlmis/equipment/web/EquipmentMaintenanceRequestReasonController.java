/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import java.util.Optional;
import java.util.UUID;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.openlmis.equipment.domain.EquipmentMaintenanceRequestReason;
import org.openlmis.equipment.dto.EquipmentMaintenanceRequestReasonDto;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.MessageKeys;
import org.openlmis.equipment.service.EquipmentMaintenanceRequestReasonService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(EquipmentMaintenanceRequestReasonController.RESOURCE_PATH)
@RequiredArgsConstructor
public class EquipmentMaintenanceRequestReasonController extends BaseController {

  public static final String RESOURCE_PATH = BASE_PATH + "/equipmentMaintenanceRequestReasons";

  private final EquipmentMaintenanceRequestReasonService reasonService;

  /**
   * Create a new equipment maintenance request reason.
   *
   * @param requestReasonDto details of the maintenance request reason to be created.
   * @param bindingResult    validation results of the maintenance request reason details.
   * @return
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public EquipmentMaintenanceRequestReasonDto create(@RequestBody @Valid
                                                     EquipmentMaintenanceRequestReasonDto
                                                         requestReasonDto,
                                                     BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    EquipmentMaintenanceRequestReason requestReason = reasonService.create(requestReasonDto);
    return EquipmentMaintenanceRequestReasonDto.newInstance(requestReason);
  }

  /**
   * Update a specified maintenance request reason using its ID.
   *
   * @param requestReasonDto details of the maintenance request reason to be updated.
   * @param bindingResult    validation results of the maintenance request reason details.
   * @return
   */
  @PutMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentMaintenanceRequestReasonDto update(@PathVariable("id") UUID reasonId,
                                                     @RequestBody @Valid
                                                     EquipmentMaintenanceRequestReasonDto
                                                         requestReasonDto,
                                                     BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    requestReasonDto.setId(reasonId);
    EquipmentMaintenanceRequestReason requestReason
        = reasonService.update(reasonId, requestReasonDto);
    return EquipmentMaintenanceRequestReasonDto.newInstance(requestReason);
  }

  /**
   * Retrieve maintenance request reason using its Id.
   *
   * @param reasonId id of the maintenance request to be retrieved.
   * @return maintenance request of specified id.
   */
  @GetMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentMaintenanceRequestReasonDto
      getEquipmentMaintenanceRequestReason(@PathVariable("id")
                                           UUID reasonId) {
    EquipmentMaintenanceRequestReason requestReason = reasonService
        .getEquipmentMaintenanceRequestReason(reasonId);
    return EquipmentMaintenanceRequestReasonDto.newInstance(requestReason);
  }

  /**
   * Retrieves all equipment maintenance requestReasons with name similar to name parameter.
   *
   * @param queryParams name query parameter.
   * @param pageable    object used to encapsulate the pagination related values:
   *                    page, size and sort.
   * @return Page of maintenance requests matching query parameter.
   */
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<EquipmentMaintenanceRequestReasonDto> searchEquipmentMaintenanceRequestReasons(
      @RequestParam(required = false, name = "name") Optional<String> queryParams,
      Pageable pageable) {

    return queryParams
        .map(nameFilter -> reasonService.search(nameFilter, pageable))
        .orElseGet(() -> reasonService.findAll(pageable))
        .map(requestReason -> EquipmentMaintenanceRequestReasonDto.newInstance(requestReason));
  }
}
