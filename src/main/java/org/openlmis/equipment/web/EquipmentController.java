/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.openlmis.equipment.service.ResourceNames.BASE_PATH;

import com.google.common.collect.Sets;

import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.validation.Valid;

import lombok.RequiredArgsConstructor;
import org.openlmis.equipment.domain.Equipment;
import org.openlmis.equipment.domain.EquipmentType;
import org.openlmis.equipment.dto.EquipmentDto;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.MessageKeys;
import org.openlmis.equipment.repository.EquipmentRepository;
import org.openlmis.equipment.repository.EquipmentTypeRepository;
import org.openlmis.equipment.service.EquipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(EquipmentController.RESOURCE_PATH)
@RequiredArgsConstructor
public class EquipmentController extends BaseController {

  public static final String RESOURCE_PATH = BASE_PATH + "/equipments";

  private final EquipmentService equipmentService;

  @Autowired
  EquipmentRepository equipmentRepository;

  @Autowired
  EquipmentTypeRepository equipmentTypeRepository;


  /**
   * Create a new equipment.
   *
   * @param equipmentDto  details of the equipment to be created.
   * @param bindingResult validation results of the equipment details.
   * @return
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public EquipmentDto create(@RequestBody @Valid EquipmentDto equipmentDto,
                             BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    Equipment equipment = equipmentService.create(equipmentDto);
    return EquipmentDto.newInstance(equipment);
  }

  /**
   * Update a specified equipment using its ID.
   *
   * @param equipmentDto  details of the equipment to be updated.
   * @param bindingResult validation results of the equipment details.
   * @return
   */
  @PutMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentDto update(@PathVariable("id") UUID equipmentId,
                             @RequestBody @Valid EquipmentDto equipmentDto,
                             BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_ITEMS);
    }

    equipmentDto.setId(equipmentId);
    Equipment equipment = equipmentService.update(equipmentId, equipmentDto);
    return EquipmentDto.newInstance(equipment);
  }

  /**
   * Retrieve equipment equipment using its Id.
   *
   * @param equipmentId id of the equipment to be retrieved.
   * @return
   */
  @GetMapping(value = "/{id}")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public EquipmentDto getEquipment(@PathVariable("id") UUID equipmentId) {
    Equipment equipment = equipmentService.getEquipment(equipmentId);
    return EquipmentDto.newInstance(equipment);
  }

  /**
   * Get all equipment types in the system.
   * @return all equipment types program in the system.
   */
  @GetMapping(value = "byProgram")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<EquipmentDto> findAllProgramAndEquipmentType(
          @RequestParam UUID programId,
          @RequestParam UUID equipmentTypeId,
          Pageable pageable) {

    EquipmentType equipmentType = equipmentTypeRepository.findById(equipmentTypeId).orElse(null);
    Set<Equipment> equipment = Sets.newHashSet(equipmentRepository
            .findEquipmentByProgramIdAndType(programId,equipmentType));


    Set<EquipmentDto> equipmentDtos =
            equipment.stream().map(this::exportToDto).collect(Collectors.toSet());

    return toPage(equipmentDtos, pageable);
  }


  /**
   * Retrieves all equipments with code similar to code parameter or name similar to
   * name parameter or equipment energy type whose id is similar to equipmentEnergyTypeId
   * parameter or equipment model whose id is similar to equipmentModelId parameter or
   * equipment category whose id is similar to equipmentCategoryId parameter
   * whose id is among the ids parameters.
   *
   * @param queryParams request parameters (code, name, equipmentEnergyTypeId,
   *                   equipmentModelId, equipmentCategoryId ).
   * @param pageable    object used to encapsulate the pagination related values:
   *                    page, size and sort.
   * @return List of required equipments matching query parameters.
   */
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<EquipmentDto> searchEquipments(
      @RequestParam(required = false)
          Map<String, Object> queryParams,
      Pageable pageable) {

    MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
    if (!CollectionUtils.isEmpty(queryParams)) {
      queryParams.forEach(map::add);
    }

    EquipmentSearchParams params = new EquipmentSearchParams(map);

    Page<Equipment> foundEquipments = equipmentService.search(params, pageable);
    return foundEquipments.map(equipment -> EquipmentDto.newInstance(equipment));
  }

  private EquipmentDto exportToDto(Equipment equipment) {
    EquipmentDto equipmentDto = new EquipmentDto();
    equipment.export(equipmentDto);
    return equipmentDto;
  }
}
