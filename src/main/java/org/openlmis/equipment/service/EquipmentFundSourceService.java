/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.service;

import java.util.UUID;
import org.openlmis.equipment.domain.EquipmentFundSource;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.EquipmentFundSourceMessageKeys;
import org.openlmis.equipment.repository.EquipmentFundSourceRepository;
import org.openlmis.equipment.util.Message;
import org.openlmis.equipment.util.UuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Service
public class EquipmentFundSourceService {

  @Autowired
  private EquipmentFundSourceRepository equipmentFundSourceRepository;
  @Autowired
  private PermissionService permissionService;

  /**
   * Create a new fund source using the specified details.
   *
   * @param importer details of the new equipment fund source.
   *
   * @return
   */
  @Transactional
  public EquipmentFundSource create(EquipmentFundSource.Importer importer) {
    permissionService.canManageCce();

    EquipmentFundSource fundSource = newInstance(importer);
    fundSource.setId(null);
    fundSource = equipmentFundSourceRepository.save(fundSource);
    return fundSource;
  }

  /**
   * Update specified equipment fund source.
   *
   * @param fundSourceId the Id of the fund source to be updated.
   * @param importer     the imported with new details for the fund source.
   *
   * @return
   */
  @Transactional
  public EquipmentFundSource update(UUID fundSourceId, EquipmentFundSource.Importer importer) {
    permissionService.canManageCce();

    if (!UuidUtil.sameId(importer.getId(), fundSourceId)) {
      throw new ValidationMessageException(
          new Message(EquipmentFundSourceMessageKeys.ERROR_ID_MISMATCH)
      );
    }

    if (!equipmentFundSourceRepository.existsById(fundSourceId)) {
      return create(importer);
    } else {
      EquipmentFundSource fundSource = newInstance(importer);
      fundSource.setId(fundSourceId);
      fundSource = equipmentFundSourceRepository.save(fundSource);

      return fundSource;
    }
  }

  private EquipmentFundSource newInstance(EquipmentFundSource.Importer importer) {
    return new EquipmentFundSource(
        importer.getName(),
        importer.getCode()
    );
  }

  /**
   * Retrieve equipment fund source of specified Id.
   *
   * @param equipmentId id of the equipment fund source to retrieve.
   *
   * @return
   */
  public EquipmentFundSource getEquipmentFundSource(UUID equipmentId) {
    EquipmentFundSource fundSource = equipmentFundSourceRepository.findById(equipmentId)
        .orElseThrow(() -> new NotFoundException(
                new Message(EquipmentFundSourceMessageKeys.ERROR_NOT_FOUND)
            )
        );
    return fundSource;
  }

  /**
   * Search equipment fund sources based on the provided parameters.
   *
   * @param nameQuery the query string to be compared with name property.
   * @param codeQuery the query string to be compared with code property.
   * @param pageable  pagination information.
   *
   * @return a page of found equipment fund source which conforms the search parameters.
   */
  public Page<EquipmentFundSource> search(String nameQuery, String codeQuery, Pageable pageable) {
    if (!StringUtils.isEmpty(nameQuery) && StringUtils.isEmpty(codeQuery)) {
      return equipmentFundSourceRepository.findAllByNameContainingIgnoreCase(nameQuery, pageable);
    } else if (StringUtils.isEmpty(nameQuery) && !StringUtils.isEmpty(codeQuery)) {
      return equipmentFundSourceRepository.findAllByCodeContainingIgnoreCase(codeQuery, pageable);
    } else if (!StringUtils.isEmpty(nameQuery) && !StringUtils.isEmpty(codeQuery)) {
      return equipmentFundSourceRepository
          .findAllByNameContainingIgnoreCaseAndCodeContainingIgnoreCase(
              nameQuery,
              codeQuery,
              pageable
          );
    } else {
      return equipmentFundSourceRepository.findAll(pageable);
    }
  }

}
