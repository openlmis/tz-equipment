/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.service;

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.openlmis.equipment.domain.ServiceType;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.ServiceTypeMessageKeys;
import org.openlmis.equipment.repository.ServiceTypeRepository;
import org.openlmis.equipment.util.Message;
import org.openlmis.equipment.util.UuidUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class ServiceTypeService {

  private final ServiceTypeRepository serviceTypeRepository;

  private final PermissionService permissionService;

  /**
   * Create a new service type using the specified details.
   *
   * @param importer details of the new serviceType .
   *
   * @return
   */
  @Transactional
  public ServiceType create(ServiceType.Importer importer) {
    permissionService.canManageCce();

    ServiceType serviceType = ServiceType.newInstance(importer);
    serviceType.setId(null);
    serviceType = serviceTypeRepository.save(serviceType);
    return serviceType;
  }

  /**
   * Update specified service type.
   *
   * @param id       the id of the  to be updated.
   * @param importer the imported with new details for the .
   *
   * @return
   */
  @Transactional
  public ServiceType update(UUID id, ServiceType.Importer importer) {
    permissionService.canManageCce();

    if (!UuidUtil.sameId(importer.getId(), id)) {
      throw new ValidationMessageException(
          new Message(ServiceTypeMessageKeys.ERROR_ID_MISMATCH)
      );
    }

    if (!serviceTypeRepository.existsById(id)) {
      return create(importer);
    } else {
      ServiceType serviceType = ServiceType.newInstance(importer);
      serviceType.setId(id);
      serviceType = serviceTypeRepository.save(serviceType);

      return serviceType;
    }
  }


  /**
   * Retrieve service type of specified Id.
   *
   * @param serviceTypeId id of the service type to retrieve.
   *
   * @return
   */
  public ServiceType getServiceType(UUID serviceTypeId) {
    ServiceType serviceType = serviceTypeRepository.findById(serviceTypeId)
        .orElseThrow(
            () -> new NotFoundException(new Message(ServiceTypeMessageKeys.ERROR_NOT_FOUND))
        );
    return serviceType;
  }

  /**
   * Find all available service types paginated.
   *
   * @param pageable pagination details.
   *
   * @return
   */
  public Page<ServiceType> findAll(Pageable pageable) {
    return serviceTypeRepository.findAll(pageable);
  }

  /**
   * Search service types based on the name filter.
   *
   * @param name     name search parameter value.
   * @param pageable pagination information.
   *
   * @return a page of found service type which conforms the search parameters.
   */
  public Page<ServiceType> search(String name, Pageable pageable) {
    return serviceTypeRepository.findAllByNameContainingIgnoreCase(name, pageable);
  }

}
