/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.service;

import java.util.Set;
import java.util.UUID;
import org.openlmis.equipment.domain.EquipmentModel;
import org.openlmis.equipment.domain.EquipmentType;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.EquipmentModelMessageKeys;
import org.openlmis.equipment.i18n.EquipmentTypeMessageKeys;
import org.openlmis.equipment.repository.EquipmentModelRepository;
import org.openlmis.equipment.repository.EquipmentTypeRepository;
import org.openlmis.equipment.util.Message;
import org.openlmis.equipment.util.UuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EquipmentModelService {

  @Autowired
  private EquipmentModelRepository equipmentModelRepository;
  @Autowired
  private EquipmentTypeRepository equipmentTypeRepository;
  @Autowired
  private PermissionService permissionService;

  /**
   * Create a new model using the specified details.
   *
   * @param importer details of the new equipment model.
   *
   * @return
   */
  @Transactional
  public EquipmentModel create(EquipmentModel.Importer importer) {
    permissionService.canManageCce();

    EquipmentModel model = newInstance(importer);
    model.setId(null);
    model = equipmentModelRepository.save(model);
    return model;
  }

  /**
   * Update specified equipment model.
   *
   * @param modelId  the Id of the model to be updated.
   * @param importer the imported with new details for the model.
   *
   * @return
   */
  @Transactional
  public EquipmentModel update(UUID modelId, EquipmentModel.Importer importer) {
    permissionService.canManageCce();

    if (!UuidUtil.sameId(importer.getId(), modelId)) {
      throw new ValidationMessageException(
          new Message(EquipmentModelMessageKeys.ERROR_ID_MISMATCH)
      );
    }

    if (!equipmentModelRepository.existsById(modelId)) {
      return create(importer);
    } else {
      EquipmentModel model = newInstance(importer);
      model.setId(modelId);
      model = equipmentModelRepository.save(model);

      return model;
    }
  }

  private EquipmentModel newInstance(EquipmentModel.Importer importer) {
    EquipmentType type = equipmentTypeRepository
        .findById(importer.getEquipmentType().getId())
        .orElseThrow(
            () -> new NotFoundException(
                new Message(EquipmentTypeMessageKeys.ERROR_NOT_FOUND)
            )
        );
    return new EquipmentModel(
        importer.getName(),
        importer.getCode(),
        type
    );
  }

  /**
   * Retrieve equipment model of specified Id.
   *
   * @param equipmentId id of the equipment model to retrieve.
   *
   * @return
   */
  public EquipmentModel getEquipmentModel(UUID equipmentId) {
    EquipmentModel model = equipmentModelRepository.findById(equipmentId)
        .orElseThrow(() -> new NotFoundException(
                new Message(EquipmentModelMessageKeys.ERROR_NOT_FOUND)
            )
        );
    return model;
  }

  /**
   * Find all available equipment models paginated.
   *
   * @param pageable pagination details.
   *
   * @return
   */
  public Page<EquipmentModel> findAll(Pageable pageable) {
    return equipmentModelRepository.findAll(pageable);
  }

  /**
   * Search equipment models based on the provided parameters.
   *
   * @param searchParams Search parameters containing id, equipmentTypeId,code or name.
   * @param pageable     pagination information.
   *
   * @return a page of found equipment model which conforms the search parameters.
   */
  public Page<EquipmentModel> search(SearchParams searchParams, Pageable pageable) {
    return equipmentModelRepository.search(searchParams, pageable);
  }

  public interface SearchParams {

    String getCode();

    String getName();

    Set<UUID> getEquipmentTypeIds();

    Set<UUID> getIds();
  }
}
