/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.service;

import static org.openlmis.equipment.i18n.ReasonNotFunctionalMessageKeys.ERROR_REASON_NOT_FUNCTIONAL_CODE_NOT_UNIQUE;
import static org.openlmis.equipment.i18n.ReasonNotFunctionalMessageKeys.ERROR_REASON_NOT_FUNCTIONAL_NAME_NOT_UNIQUE;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import org.openlmis.equipment.domain.ReasonNotFunctional;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.ReasonNotFunctionalMessageKeys;
import org.openlmis.equipment.repository.ReasonNotFunctionalRepository;
import org.openlmis.equipment.util.Message;
import org.openlmis.equipment.util.UuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ReasonNotFunctionalService {

  @Autowired
  private ReasonNotFunctionalRepository reasonNotFunctionalRepository;

  @Autowired
  private PermissionService permissionService;

  /**
   * Create a new reason using the specified details.
   *
   * @param importer details of the new reason.
   *
   * @return
   */
  @Transactional
  public ReasonNotFunctional create(ReasonNotFunctional.Importer importer) {
    permissionService.canManageCce();

    Optional<ReasonNotFunctional> existing = reasonNotFunctionalRepository.findOneByName(
        importer.getName()
    );
    if (existing.isPresent()) {
      throw new ValidationMessageException(
          ERROR_REASON_NOT_FUNCTIONAL_NAME_NOT_UNIQUE, importer.getName()
      );
    }

    Optional<ReasonNotFunctional> existingCode = reasonNotFunctionalRepository.findOneByCode(
        importer.getCode()
    );
    if (existingCode.isPresent()) {
      throw new ValidationMessageException(
          ERROR_REASON_NOT_FUNCTIONAL_CODE_NOT_UNIQUE, importer.getCode()
      );
    }

    ReasonNotFunctional reason = ReasonNotFunctional.newInstance(importer);
    reason.setId(null);
    reason = reasonNotFunctionalRepository.save(reason);
    return reason;
  }

  /**
   * Update specified reason.
   *
   * @param reasonId the Id of the reason to be updated.
   * @param importer the imported with new details for the reason.
   *
   * @return
   */
  @Transactional
  public ReasonNotFunctional update(UUID reasonId, ReasonNotFunctional.Importer importer) {
    permissionService.canManageCce();

    if (!UuidUtil.sameId(importer.getId(), reasonId)) {
      throw new ValidationMessageException(
          new Message(ReasonNotFunctionalMessageKeys.ERROR_ID_MISMATCH)
      );
    }

    if (!reasonNotFunctionalRepository.existsById(reasonId)) {
      return create(importer);
    } else {

      Optional<ReasonNotFunctional> existing = reasonNotFunctionalRepository.findOneByName(
          importer.getName()
      );
      if (existing.isPresent() && !existing.get().getId().equals(reasonId)) {
        throw new ValidationMessageException(
            ERROR_REASON_NOT_FUNCTIONAL_NAME_NOT_UNIQUE, importer.getName()
        );
      }

      Optional<ReasonNotFunctional> existingCode = reasonNotFunctionalRepository.findOneByCode(
          importer.getCode()
      );
      if (existingCode.isPresent() && !existingCode.get().getId().equals(reasonId)) {
        throw new ValidationMessageException(
            ERROR_REASON_NOT_FUNCTIONAL_CODE_NOT_UNIQUE, importer.getName()
        );
      }

      ReasonNotFunctional reason = ReasonNotFunctional.newInstance(importer);
      reason.setId(reasonId);
      reason = reasonNotFunctionalRepository.save(reason);

      return reason;
    }
  }


  /**
   * Retrieve reason of specified Id.
   *
   * @param reasonId id of the reason to retrieve.
   *
   * @return
   */
  public ReasonNotFunctional getReasonNotFunctional(UUID reasonId) {
    ReasonNotFunctional reason = reasonNotFunctionalRepository.findById(reasonId)
        .orElseThrow(() -> new NotFoundException(
                new Message(ReasonNotFunctionalMessageKeys.ERROR_NOT_FOUND)
            )
        );
    return reason;
  }

  /**
   * Find all available reasons paginated.
   *
   * @param pageable pagination details.
   *
   * @return
   */
  public Page<ReasonNotFunctional> findAll(Pageable pageable) {
    return reasonNotFunctionalRepository.findAll(pageable);
  }

  /**
   * Search reasons not functional based on the provided parameters.
   *
   * @param searchParams Search parameters containing id, or name.
   * @param pageable     pagination information.
   *
   * @return a page of found reason not functional which conforms the search parameters.
   */
  public Page<ReasonNotFunctional> search(
      ReasonNotFunctionalService.SearchParams searchParams, Pageable pageable) {
    return reasonNotFunctionalRepository.search(searchParams, pageable);
  }

  public interface SearchParams {

    String getName();

    Set<UUID> getIds();
  }
}
