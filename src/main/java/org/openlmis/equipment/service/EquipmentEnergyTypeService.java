/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.service;

import java.util.UUID;
import org.openlmis.equipment.domain.EquipmentEnergyType;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.EquipmentEnergyTypeMessageKeys;
import org.openlmis.equipment.repository.EquipmentEnergyTypeRepository;
import org.openlmis.equipment.util.Message;
import org.openlmis.equipment.util.UuidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Service
public class EquipmentEnergyTypeService {

  @Autowired
  private EquipmentEnergyTypeRepository equipmentEnergyTypeRepository;
  @Autowired
  private PermissionService permissionService;

  /**
   * Create a new energy type using the specified details.
   *
   * @param importer details of the new equipment energy type.
   *
   * @return
   */
  @Transactional
  public EquipmentEnergyType create(EquipmentEnergyType.Importer importer) {
    permissionService.canManageCce();

    EquipmentEnergyType energyType = newInstance(importer);
    energyType.setId(null);
    energyType = equipmentEnergyTypeRepository.save(energyType);
    return energyType;
  }

  /**
   * Update specified equipment energy type.
   *
   * @param energyTypeId the Id of the energyType to be updated.
   * @param importer     the imported with new details for the energy type.
   *
   * @return
   */
  @Transactional
  public EquipmentEnergyType update(UUID energyTypeId, EquipmentEnergyType.Importer importer) {
    permissionService.canManageCce();

    if (!UuidUtil.sameId(importer.getId(), energyTypeId)) {
      throw new ValidationMessageException(
          new Message(EquipmentEnergyTypeMessageKeys.ERROR_ID_MISMATCH)
      );
    }

    if (!equipmentEnergyTypeRepository.existsById(energyTypeId)) {
      return create(importer);
    } else {
      EquipmentEnergyType energyType = newInstance(importer);
      energyType.setId(energyTypeId);
      energyType = equipmentEnergyTypeRepository.save(energyType);

      return energyType;
    }
  }

  private EquipmentEnergyType newInstance(EquipmentEnergyType.Importer importer) {
    return new EquipmentEnergyType(
        importer.getName(),
        importer.getCode()
    );
  }

  /**
   * Retrieve equipment energy type of specified Id.
   *
   * @param equipmentId id of the equipment energy type to retrieve.
   *
   * @return
   */
  public EquipmentEnergyType getEquipmentEnergyType(UUID equipmentId) {
    EquipmentEnergyType energyType = equipmentEnergyTypeRepository.findById(equipmentId)
        .orElseThrow(() -> new NotFoundException(
                new Message(EquipmentEnergyTypeMessageKeys.ERROR_NOT_FOUND)
            )
        );
    return energyType;
  }

  /**
   * Search equipment energy types based on the provided parameters.
   *
   * @param nameQuery the query string to be compared with name property.
   * @param codeQuery the query string to be compared with code property.
   * @param pageable  pagination information.
   *
   * @return a page of found equipment energy type which conforms the search parameters.
   */
  public Page<EquipmentEnergyType> search(String nameQuery, String codeQuery, Pageable pageable) {
    if (!StringUtils.isEmpty(nameQuery) && StringUtils.isEmpty(codeQuery)) {
      return equipmentEnergyTypeRepository.findAllByNameContainingIgnoreCase(nameQuery, pageable);
    } else if (StringUtils.isEmpty(nameQuery) && !StringUtils.isEmpty(codeQuery)) {
      return equipmentEnergyTypeRepository.findAllByCodeContainingIgnoreCase(codeQuery, pageable);
    } else if (!StringUtils.isEmpty(nameQuery) && !StringUtils.isEmpty(codeQuery)) {
      return equipmentEnergyTypeRepository
          .findAllByNameContainingIgnoreCaseAndCodeContainingIgnoreCase(
              nameQuery,
              codeQuery,
              pageable
          );
    } else {
      return equipmentEnergyTypeRepository.findAll(pageable);
    }
  }

}
