/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.service;

import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.openlmis.equipment.domain.Contract;
import org.openlmis.equipment.domain.Equipment;
import org.openlmis.equipment.domain.ServiceType;
import org.openlmis.equipment.domain.Vendor;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.ContractMessageKeys;
import org.openlmis.equipment.i18n.EquipmentMessageKeys;
import org.openlmis.equipment.i18n.ServiceTypeMessageKeys;
import org.openlmis.equipment.i18n.VendorMessageKeys;
import org.openlmis.equipment.repository.ContractRepository;
import org.openlmis.equipment.repository.EquipmentRepository;
import org.openlmis.equipment.repository.ServiceTypeRepository;
import org.openlmis.equipment.repository.VendorRepository;
import org.openlmis.equipment.util.Message;
import org.openlmis.equipment.util.UuidUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class ContractService {

  private final ContractRepository contractRepository;

  private final VendorRepository vendorRepository;

  private final ServiceTypeRepository serviceTypeRepository;

  private final EquipmentRepository equipmentRepository;

  private final PermissionService permissionService;


  /**
   * Create new contract from the provided contract details.
   *
   * @param importer provided contract details
   *
   * @return new instance of Contract.
   */
  private Contract newInstance(Contract.Importer importer) {

    Contract contract = new Contract();
    contract.setIdentifier(importer.getIdentifier());
    contract.setStartDate(importer.getStartDate());
    contract.setEndDate(importer.getEndDate());
    contract.setDate(importer.getDate());
    contract.setDescription(importer.getDescription());
    contract.setCoverage(importer.getCoverage());
    contract.setFacilities(importer.getFacilities());
    contract.setTerms(importer.getTerms());

    if (ObjectUtils.isNotEmpty(importer.getVendor())) {
      Vendor vendor = vendorRepository
          .findById(importer.getVendor().getId())
          .orElseThrow(
              () -> new NotFoundException(
                  new Message(VendorMessageKeys.ERROR_NOT_FOUND)
              )
          );
      contract.setVendor(vendor);
    }

    if (ObjectUtils.isNotEmpty(importer.getServiceTypes())) {
      Set<ServiceType> serviceTypes = importer.getServiceTypes()
          .stream()
          .map(serviceType -> {
            return serviceTypeRepository
                .findById(serviceType.getId())
                .orElseThrow(
                    () -> new NotFoundException(
                        new Message(ServiceTypeMessageKeys.ERROR_NOT_FOUND)
                    )
                );
          }).collect(Collectors.toSet());
      contract.setServiceTypes(serviceTypes);
    }

    if (ObjectUtils.isNotEmpty(importer.getEquipments())) {
      Set<Equipment> equipments = importer.getEquipments()
          .stream()
          .map(equipment -> {
            return equipmentRepository
                .findById(equipment.getId())
                .orElseThrow(
                    () -> new NotFoundException(
                        new Message(EquipmentMessageKeys.ERROR_NOT_FOUND)
                    )
                );
          }).collect(Collectors.toSet());
      contract.setEquipments(equipments);
    }

    return contract;
  }

  /**
   * Create a new  contract using the specified details.
   *
   * @param importer details of the new contract .
   *
   * @return
   */
  @Transactional
  public Contract create(Contract.Importer importer) {
    permissionService.canManageCce();

    String identifier = importer.getIdentifier();
    if (contractRepository.existsByIdentifier(identifier)) {
      throw new ValidationMessageException(
          new Message(ContractMessageKeys.ERROR_IDENTIFIER_NOT_UNIQUE)
      );
    }

    Contract contract = newInstance(importer);
    contract.setId(null);
    contract = contractRepository.save(contract);
    return contract;
  }

  /**
   * Update specified contract .
   *
   * @param id       the id of the  to be updated.
   * @param importer the imported with new details for the .
   *
   * @return
   */
  @Transactional
  public Contract update(UUID id, Contract.Importer importer) {
    permissionService.canManageCce();

    if (!UuidUtil.sameId(importer.getId(), id)) {
      throw new ValidationMessageException(
          new Message(ContractMessageKeys.ERROR_ID_MISMATCH)
      );
    }

    if (!contractRepository.existsById(id)) {
      return create(importer);
    } else {
      Contract contract = newInstance(importer);
      contract.setId(id);
      contract = contractRepository.save(contract);

      return contract;
    }
  }


  /**
   * Retrieve contract  of specified Id.
   *
   * @param contractId id of the contract  to retrieve.
   *
   * @return
   */
  public Contract getContract(UUID contractId) {
    Contract contract = contractRepository.findById(contractId)
        .orElseThrow(
            () -> new NotFoundException(new Message(ContractMessageKeys.ERROR_NOT_FOUND))
        );
    return contract;
  }

  /**
   * Find all available contract s paginated.
   *
   * @param pageable pagination details.
   *
   * @return
   */
  public Page<Contract> findAll(Pageable pageable) {
    return contractRepository.findAll(pageable);
  }

  /**
   * Search contracts based on the name filter.
   *
   * @param searchParams search parameters.
   * @param pageable     pagination information.
   *
   * @return a page of found contract which conforms the search parameters.
   */
  public Page<Contract> search(SearchParams searchParams, Pageable pageable) {
    return contractRepository.search(searchParams, pageable);
  }

  public interface SearchParams {

    Set<UUID> getIds();

    Set<UUID> getVendorIds();

    String getIdentifier();

    LocalDate getDateFrom();

    LocalDate getDateTo();

    Set<UUID> getServiceTypeIds();

    Set<UUID> getEquipmentIds();

    Set<UUID> getFacilityIds();
  }
}
