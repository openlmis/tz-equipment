/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.service;

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.openlmis.equipment.domain.Vendor;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.VendorMessageKeys;
import org.openlmis.equipment.repository.VendorRepository;
import org.openlmis.equipment.util.Message;
import org.openlmis.equipment.util.UuidUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class VendorService {

  private final VendorRepository vendorRepository;

  private final PermissionService permissionService;

  /**
   * Create a new  vendor using the specified details.
   *
   * @param importer details of the new vendor .
   *
   * @return
   */
  @Transactional
  public Vendor create(Vendor.Importer importer) {
    permissionService.canManageCce();

    Vendor vendor = Vendor.newInstance(importer);
    vendor.setId(null);
    vendor = vendorRepository.save(vendor);
    return vendor;
  }

  /**
   * Update specified vendor .
   *
   * @param id       the id of the  to be updated.
   * @param importer the imported with new details for the .
   *
   * @return
   */
  @Transactional
  public Vendor update(UUID id, Vendor.Importer importer) {
    permissionService.canManageCce();

    if (!UuidUtil.sameId(importer.getId(), id)) {
      throw new ValidationMessageException(
          new Message(VendorMessageKeys.ERROR_ID_MISMATCH)
      );
    }

    if (!vendorRepository.existsById(id)) {
      return create(importer);
    } else {
      Vendor vendor = Vendor.newInstance(importer);
      vendor.setId(id);
      vendor = vendorRepository.save(vendor);

      return vendor;
    }
  }


  /**
   * Retrieve vendor  of specified Id.
   *
   * @param vendorId id of the vendor  to retrieve.
   *
   * @return
   */
  public Vendor getVendor(UUID vendorId) {
    Vendor vendor = vendorRepository.findById(vendorId)
        .orElseThrow(
            () -> new NotFoundException(new Message(VendorMessageKeys.ERROR_NOT_FOUND))
        );
    return vendor;
  }

  /**
   * Find all available vendor s paginated.
   *
   * @param pageable pagination details.
   *
   * @return
   */
  public Page<Vendor> findAll(Pageable pageable) {
    return vendorRepository.findAll(pageable);
  }

  /**
   * Search vendors based on the name filter.
   *
   * @param name     name search parameter value.
   * @param pageable pagination information.
   *
   * @return a page of found vendor which conforms the search parameters.
   */
  public Page<Vendor> search(String name, Pageable pageable) {
    return vendorRepository.findAllByNameContainingIgnoreCase(name, pageable);
  }

}
