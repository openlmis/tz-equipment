/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.SetUtils;
import org.openlmis.equipment.domain.EquipmentInventory;
import org.openlmis.equipment.domain.EquipmentOrderable;
import org.openlmis.equipment.domain.FunctionalStatus;
import org.openlmis.equipment.domain.Utilization;
import org.openlmis.equipment.repository.EquipmentInventoryRepository;
import org.openlmis.equipment.repository.EquipmentOrderableRepository;
import org.openlmis.equipment.repository.custom.EquipmentInventoryRepositoryCustom.RepositorySearchParams;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
@RequiredArgsConstructor
public class EquipmentInventoryService {

  private final PermissionService permissionService;

  private final EquipmentInventoryRepository equipmentInventoryRepository;

  private final EquipmentOrderableRepository equipmentOrderableRepository;

  /**
   * Find all available equipment inventory paginated.
   *
   * @param pageable pagination details.
   *
   * @return
   */
  public Page<EquipmentInventory> findAll(Pageable pageable) {
    return equipmentInventoryRepository.findAll(pageable);
  }

  /**
   * Search equipment inventories associated with specified
   * orderable ids based on the provided parameters.
   *
   * @param params Search parameters containing name, id, facility id, program id,
   *               functional status, utilization and ids.
   *
   * @return a page of found equipmentInventory  which conforms the search parameters.
   */
  public Map<UUID, List<EquipmentInventory>> searchOrderableEquipmentInventories(
      UUID facilityId, UUID programId,
      EquipmentInventoryService.SearchParams params) {

    if (facilityId == null || programId == null) {
      throw new IllegalArgumentException(
          "You must specify facilityId and programId to search equipments for"
      );
    }

    Set<UUID> orderableIds = params.getOrderableIds();
    if (ObjectUtils.isEmpty(orderableIds)) {
      throw new IllegalArgumentException(
          "You must specify orderableId to search equipments for"
      );
    }

    permissionService.canSubmitRequisition(facilityId, programId);

    Map<UUID, List<EquipmentInventory>> orderableEquipments = new LinkedHashMap<>();
    RepositorySearchParams searchParams = toRepositorySearchParams(facilityId, programId, params);
    for (UUID orderableId : orderableIds) {
      Set<UUID> equipmentIds = toAssociatedEquipmentIds(SetUtils.hashSet(orderableId));

      if (!ObjectUtils.isEmpty(params.getEquipmentIds())) {
        equipmentIds = new HashSet<>(equipmentIds);
        equipmentIds.retainAll(params.getEquipmentIds());
      }

      if (ObjectUtils.isEmpty(equipmentIds)) {
        continue;
      }

      searchParams.setEquipmentIds(equipmentIds);

      orderableEquipments.put(
          orderableId,
          equipmentInventoryRepository.search(
              searchParams,
              PageRequest.of(0, Integer.MAX_VALUE)
          ).getContent()
      );
    }

    return orderableEquipments;
  }


  /**
   * Search equipment inventory s based on the provided parameters.
   *
   * @param params   Search parameters containing name, id, facility id, program id,
   *                 functional status, utilization and ids.
   * @param pageable pagination information.
   *
   * @return a page of found equipmentInventory  which conforms the search parameters.
   */
  public Page<EquipmentInventory> searchEquipmentInventory(UUID facilityId, UUID programId,
                                                           SearchParams params,
                                                           Pageable pageable) {

    if (facilityId == null || programId == null) {
      throw new IllegalArgumentException(
          "You must specify facility and program to search equipments for"
      );
    }

    permissionService.canViewInventory(facilityId, programId);

    RepositorySearchParams searchParams = toRepositorySearchParams(facilityId, programId, params);
    Set<UUID> orderableIds = params.getOrderableIds();

    //if the orderables are specified then return only equipments
    // which are associated with any of the specified orderables
    if (!ObjectUtils.isEmpty(orderableIds)) {
      Set<UUID> equipmentIds = toAssociatedEquipmentIds(orderableIds);

      if (!ObjectUtils.isEmpty(params.getEquipmentIds())) {
        equipmentIds = new HashSet<>(equipmentIds);
        equipmentIds.retainAll(params.getEquipmentIds());
      }
      searchParams.setEquipmentIds(equipmentIds);
    }

    Page<EquipmentInventory> equipmentInventories = equipmentInventoryRepository.search(
        searchParams,
        pageable
    );
    return equipmentInventories;
  }

  private RepositorySearchParams toRepositorySearchParams(
      UUID facilityId, UUID programId, SearchParams params) {
    RepositorySearchParams searchParams = new RepositorySearchParams();
    searchParams.setFacilityIds(Arrays.asList(facilityId));
    searchParams.setProgramIds(Arrays.asList(programId));
    searchParams.setUtilizations(params.getUtilizations());
    searchParams.setFunctionalStatuses(params.getFunctionalStatuses());
    searchParams.setIds(params.getIds());
    searchParams.setEquipmentIds(params.getEquipmentIds());
    searchParams.setEquipmentTypeIds(params.getEquipmentTypeIds());

    return searchParams;
  }

  private Set<UUID> toAssociatedEquipmentIds(Set<UUID> orderableIds) {
    return orderableIds.stream()
        .flatMap(id -> equipmentOrderableRepository.findAllByOrderableId(id).stream())
        .map(EquipmentOrderable::getEquipment)
        .map(equipment -> equipment.getId())
        .collect(Collectors.toSet());
  }

  public interface SearchParams {

    Set<UUID> getIds();

    Set<UUID> getEquipmentIds();

    Set<UUID> getEquipmentTypeIds();

    Set<UUID> getOrderableIds();

    Set<FunctionalStatus> getFunctionalStatuses();

    Set<Utilization> getUtilizations();
  }
}
