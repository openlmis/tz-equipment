/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */


package org.openlmis.equipment.service;

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.openlmis.equipment.domain.EquipmentMaintenanceRequestReason;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.ServiceTypeMessageKeys;
import org.openlmis.equipment.repository.EquipmentMaintenanceRequestReasonRepository;
import org.openlmis.equipment.util.Message;
import org.openlmis.equipment.util.UuidUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class EquipmentMaintenanceRequestReasonService {
  private final EquipmentMaintenanceRequestReasonRepository reasonRepository;

  private final PermissionService permissionService;

  /**
   * Create a new maintenance request reason using the specified details.
   *
   * @param importer details of the new maintenanceRequestReason.
   * @return
   */
  @Transactional
  public EquipmentMaintenanceRequestReason create(
      EquipmentMaintenanceRequestReason.Importer importer) {

    EquipmentMaintenanceRequestReason reason
        = EquipmentMaintenanceRequestReason.newInstance(importer);
    reason.setId(null);
    reason = reasonRepository.save(reason);
    return reason;
  }

  /**
   * Update specified equipment maintenance request reason.
   *
   * @param id       the id of the  to be updated.
   * @param importer the imported with new details for the maintenance reason.
   * @return
   */
  @Transactional
  public EquipmentMaintenanceRequestReason update(UUID id,
                                                  EquipmentMaintenanceRequestReason
                                                      .Importer importer) {
    permissionService.canManageCce();

    if (!UuidUtil.sameId(importer.getId(), id)) {
      throw new ValidationMessageException(
          new Message(ServiceTypeMessageKeys.ERROR_ID_MISMATCH)
      );
    }

    if (!reasonRepository.existsById(id)) {
      return create(importer);
    } else {
      EquipmentMaintenanceRequestReason reason
          = EquipmentMaintenanceRequestReason.newInstance(importer);
      reason.setId(id);
      reason = reasonRepository.save(reason);

      return reason;
    }
  }


  /**
   * Retrieve maintenance reason of specified Id.
   *
   * @param reasonId id of the maintenance reason to retrieve.
   * @return
   */
  public EquipmentMaintenanceRequestReason
      getEquipmentMaintenanceRequestReason(UUID reasonId) {

    EquipmentMaintenanceRequestReason requestReason = reasonRepository
        .findById(reasonId)
        .orElseThrow(
            () -> new NotFoundException(
                new Message(ServiceTypeMessageKeys.ERROR_NOT_FOUND))
        );
    return requestReason;
  }

  /**
   * Find all available reasons paginated.
   *
   * @param pageable pagination details.
   * @return
   */
  public Page<EquipmentMaintenanceRequestReason> findAll(Pageable pageable) {
    return reasonRepository.findAll(pageable);
  }

  /**
   * Search maintenance reason based on the name filter.
   *
   * @param name     name search parameter value.
   * @param pageable pagination information.
   * @return a page of found maintenance reason which conforms the search parameters.
   */
  public Page<EquipmentMaintenanceRequestReason> search(String name, Pageable pageable) {
    return reasonRepository.findAllByNameContainingIgnoreCase(name, pageable);
  }

}
