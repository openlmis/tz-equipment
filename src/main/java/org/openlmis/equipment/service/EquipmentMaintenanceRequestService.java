/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.service;

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.openlmis.equipment.domain.EquipmentInventory;
import org.openlmis.equipment.domain.EquipmentMaintenanceRequest;
import org.openlmis.equipment.domain.EquipmentMaintenanceRequestReason;
import org.openlmis.equipment.domain.Vendor;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.MaintenanceRequestMessageKeys;
import org.openlmis.equipment.repository.EquipmentInventoryRepository;
import org.openlmis.equipment.repository.EquipmentMaintenanceRequestReasonRepository;
import org.openlmis.equipment.repository.EquipmentMaintenanceRequestRepository;
import org.openlmis.equipment.repository.VendorRepository;
import org.openlmis.equipment.util.Message;
import org.openlmis.equipment.util.UuidUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class EquipmentMaintenanceRequestService {

  private final EquipmentInventoryRepository equipmentInventoryRepository;

  private final VendorRepository vendorRepository;

  private final EquipmentMaintenanceRequestReasonRepository maintenanceRequestReasonRepository;

  private final EquipmentMaintenanceRequestRepository repository;

  /**
   * Create a new equipment maintenance request using the specified details.
   *
   * @param importer details of the new equipment maintenance request.

   * @return
   */
  @Transactional
  public EquipmentMaintenanceRequest create(EquipmentMaintenanceRequest.Importer
                                                  importer) {

    EquipmentMaintenanceRequest maintenanceRequest = newInstance(importer);
    maintenanceRequest.setId(null);
    maintenanceRequest = repository.save(maintenanceRequest);
    return maintenanceRequest;
  }

  private EquipmentMaintenanceRequest newInstance(EquipmentMaintenanceRequest.Importer importer) {

    EquipmentInventory inventory = equipmentInventoryRepository
        .findById(importer.getInventory().getId())
        .orElseThrow(
            () -> new NotFoundException(
                new Message(MaintenanceRequestMessageKeys.ERROR_NOT_FOUND)
            )
        );

    Vendor vendor = vendorRepository
        .findById(importer.getVendor().getId())
        .orElseThrow(
            () -> new NotFoundException(
                new Message(MaintenanceRequestMessageKeys.ERROR_NOT_FOUND)
            )
        );

    EquipmentMaintenanceRequestReason reason = maintenanceRequestReasonRepository
        .findById(importer.getReason().getId())
        .orElseThrow(
            () -> new NotFoundException(
                new Message(MaintenanceRequestMessageKeys.ERROR_NOT_FOUND)
            )
        );
    return new EquipmentMaintenanceRequest(
        inventory,
        vendor,
        importer.getRequestDate(),
        importer.getRecommendedDate(),
        importer.getComment(),
        importer.getResolved(),
        importer.getVendorComment(),
        importer.getBreakDownDate(),
        reason
    );
  }

  /**
   * Update specified equipment .
   *
   * @param id       the id of the  to be updated.
   * @param importer the imported with new details for the .
   *
   * @return
   */
  @Transactional
  public EquipmentMaintenanceRequest update(UUID id,
                                            EquipmentMaintenanceRequest.Importer importer) {

    if (!UuidUtil.sameId(importer.getId(), id)) {
      throw new ValidationMessageException(
          new Message(MaintenanceRequestMessageKeys.ERROR_ID_MISMATCH)
      );
    }

    if (!repository.existsById(id)) {
      return create(importer);
    } else {
      EquipmentMaintenanceRequest maintenanceRequest = newInstance(importer);
      maintenanceRequest.setId(id);
      maintenanceRequest = repository.save(maintenanceRequest);

      return maintenanceRequest;
    }
  }
}
