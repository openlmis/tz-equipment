/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.openlmis.equipment.domain.EquipmentMaintenanceRequest;
import org.openlmis.equipment.domain.EquipmentServiceLog;
import org.openlmis.equipment.dto.EquipmentServiceLogDto;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.EquipmentServiceLogMessageKeys;
import org.openlmis.equipment.i18n.MaintenanceRequestMessageKeys;
import org.openlmis.equipment.repository.EquipmentMaintenanceRequestRepository;
import org.openlmis.equipment.repository.EquipmentServiceLogRepository;
import org.openlmis.equipment.util.Message;
import org.openlmis.equipment.util.UuidUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class EquipmentServiceLogService {

  private final EquipmentMaintenanceRequestRepository
      equipmentMaintenanceRequestRepository;
  private final EquipmentServiceLogRepository serviceLogRepository;

  /**
   * Create a new equipment service Log using the specified details.
   *
   * @param importer details of the new equipment service log .
   * @return
   */
  @Transactional
  public EquipmentServiceLog create(EquipmentServiceLog.Importer importer) {

    EquipmentServiceLog serviceLog = newInstance(importer);
    serviceLog.setId(null);
    serviceLog = serviceLogRepository.save(serviceLog);
    return serviceLog;
  }

  private EquipmentServiceLog newInstance(EquipmentServiceLog.Importer importer) {

    EquipmentMaintenanceRequest request = equipmentMaintenanceRequestRepository
        .findById(importer.getRequest().getId())
        .orElseThrow(
            () -> new NotFoundException(
                new Message(MaintenanceRequestMessageKeys.ERROR_NOT_FOUND)
            )
        );

    return new EquipmentServiceLog(
        request,
        importer.getMaintenanceDate(),
        importer.getServicePerformed(),
        importer.getFinding(),
        importer.getRecommendation(),
        importer.getNextVisitDate(),
        importer.getApproved()
    );
  }

  /**
   * Update specified equipment service log .
   *
   * @param id       the id of the  to be updated.
   * @param importer the imported with new details for the .
   *
   * @return
   */
  @Transactional
  public EquipmentServiceLog update(UUID id, EquipmentServiceLog.Importer importer) {

    if (!UuidUtil.sameId(importer.getId(), id)) {
      throw new ValidationMessageException(
          new Message(EquipmentServiceLogMessageKeys.ERROR_ID_MISMATCH)
      );
    }

    if (!serviceLogRepository.existsById(id)) {
      return create(importer);
    } else {
      EquipmentServiceLog equipment = newInstance(importer);
      equipment.setId(id);
      equipment = serviceLogRepository.save(equipment);

      return equipment;
    }
  }

  /**
   * Get service logs by specified equipment inventory id .
   *
   * @param itemId    the id of the  to be updated.
   *
   * @return
   */
  public List<EquipmentServiceLog> findByItem(UUID itemId) {

    if (itemId == null) {
      return null;
    }
    return serviceLogRepository.findEquipmentServiceLogsBy(itemId);
  }

  /**
   * Approve specified maintenance service log.
   *
   * @param logId      id of maintenance service log.
   * @param approveDto approval details.
   *
   * @return approved maintenance service log details.
   */
  @Transactional
  public EquipmentServiceLog approve(UUID logId, EquipmentServiceLogDto.ApproveDto approveDto) {
    Optional<EquipmentServiceLog> serviceLogCheck = serviceLogRepository.findById(logId);
    if (!serviceLogCheck.isPresent()) {
      throw new ValidationMessageException(
          new Message(EquipmentServiceLogMessageKeys.ERROR_ID_MISMATCH)
      );
    }
    EquipmentServiceLog serviceLog = serviceLogCheck.get();

    if (serviceLog.getApproved() != null && serviceLog.getApproved() == Boolean.FALSE) {
      throw new ValidationMessageException(
          new Message(EquipmentServiceLogMessageKeys.ERROR_APPROVAL_COMPLETED)
      );
    }

    Optional<EquipmentMaintenanceRequest> requestCheck =
        equipmentMaintenanceRequestRepository.findById(
            serviceLog.getRequest().getId()
        );
    if (!requestCheck.isPresent()) {
      throw new ValidationMessageException(
          new Message(EquipmentServiceLogMessageKeys.ERROR_ID_MISMATCH)
      );
    }
    EquipmentMaintenanceRequest request = requestCheck.get();

    request.setResolved(approveDto.isApproved());
    equipmentMaintenanceRequestRepository.save(request);

    serviceLog.setApproved(approveDto.isApproved());
    serviceLog = serviceLogRepository.save(serviceLog);

    return serviceLog;
  }
}
