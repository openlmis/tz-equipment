/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.service;

import java.util.Set;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.openlmis.equipment.domain.Equipment;
import org.openlmis.equipment.domain.EquipmentCategory;
import org.openlmis.equipment.domain.EquipmentEnergyType;
import org.openlmis.equipment.domain.EquipmentModel;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.i18n.EquipmentCategoryMessageKeys;
import org.openlmis.equipment.i18n.EquipmentEnergyTypeMessageKeys;
import org.openlmis.equipment.i18n.EquipmentMessageKeys;
import org.openlmis.equipment.i18n.EquipmentModelMessageKeys;
import org.openlmis.equipment.repository.EquipmentCategoryRepository;
import org.openlmis.equipment.repository.EquipmentEnergyTypeRepository;
import org.openlmis.equipment.repository.EquipmentModelRepository;
import org.openlmis.equipment.repository.EquipmentRepository;
import org.openlmis.equipment.util.Message;
import org.openlmis.equipment.util.UuidUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class EquipmentService {

  private final EquipmentRepository equipmentRepository;
  private final EquipmentEnergyTypeRepository equipmentEnergyTypeRepository;
  private final EquipmentModelRepository equipmentModelRepository;
  private final EquipmentCategoryRepository equipmentCategoryRepository;
  private final PermissionService permissionService;

  /**
   * Create a new  using the specified details.
   *
   * @param importer details of the new equipment .
   *
   * @return
   */
  @Transactional
  public Equipment create(Equipment.Importer importer) {
    permissionService.canManageCce();

    Equipment equipment = newInstance(importer);
    equipment.setId(null);
    equipment = equipmentRepository.save(equipment);
    return equipment;
  }

  /**
   * Update specified equipment .
   *
   * @param id       the id of the  to be updated.
   * @param importer the imported with new details for the .
   *
   * @return
   */
  @Transactional
  public Equipment update(UUID id, Equipment.Importer importer) {
    permissionService.canManageCce();

    if (!UuidUtil.sameId(importer.getId(), id)) {
      throw new ValidationMessageException(
          new Message(EquipmentMessageKeys.ERROR_ID_MISMATCH)
      );
    }

    if (!equipmentRepository.existsById(id)) {
      return create(importer);
    } else {
      Equipment equipment = newInstance(importer);
      equipment.setId(id);
      equipment = equipmentRepository.save(equipment);

      return equipment;
    }
  }

  private Equipment newInstance(Equipment.Importer importer) {
    EquipmentEnergyType energyType = equipmentEnergyTypeRepository
        .findById(importer.getEnergyType().getId())
        .orElseThrow(
            () -> new NotFoundException(
                new Message(EquipmentEnergyTypeMessageKeys.ERROR_NOT_FOUND)
            )
        );

    EquipmentModel model = equipmentModelRepository
        .findById(importer.getModel().getId())
        .orElseThrow(
            () -> new NotFoundException(
                new Message(EquipmentModelMessageKeys.ERROR_NOT_FOUND)
            )
        );


    EquipmentCategory category = equipmentCategoryRepository
        .findById(importer.getCategory().getId())
        .orElseThrow(
            () -> new NotFoundException(
                new Message(EquipmentCategoryMessageKeys.ERROR_NOT_FOUND)
            )
        );


    return new Equipment(
        importer.getName(),
        importer.getManufacturer(),
        energyType,
        model,
        category
    );
  }

  /**
   * Retrieve equipment  of specified Id.
   *
   * @param equipmentId id of the equipment  to retrieve.
   *
   * @return
   */
  public Equipment getEquipment(UUID equipmentId) {
    Equipment equipment = equipmentRepository.findById(equipmentId)
        .orElseThrow(() -> new NotFoundException(
                new Message(EquipmentMessageKeys.ERROR_NOT_FOUND)
            )
        );
    return equipment;
  }

  /**
   * Find all available equipment s paginated.
   *
   * @param pageable pagination details.
   *
   * @return
   */
  public Page<Equipment> findAll(Pageable pageable) {
    return equipmentRepository.findAll(pageable);
  }

  /**
   * Search equipment s based on the provided parameters.
   *
   * @param searchParams Search parameters containing id, equipmentTypeId,code or name.
   * @param pageable     pagination information.
   *
   * @return a page of found equipment  which conforms the search parameters.
   */
  public Page<Equipment> search(SearchParams searchParams, Pageable pageable) {
    return equipmentRepository.search(searchParams, pageable);
  }

  public interface SearchParams {

    String getName();

    String getManufacturer();

    Set<UUID> getEquipmentEnergyTypeIds();

    Set<UUID> getEquipmentModelIds();

    Set<UUID> getEquipmentCategoryIds();

    Set<UUID> getIds();
  }
}
