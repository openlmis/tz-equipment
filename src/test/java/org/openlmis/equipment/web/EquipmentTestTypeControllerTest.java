/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.openlmis.equipment.domain.EquipmentCategory;
import org.openlmis.equipment.domain.EquipmentTestType;
import org.openlmis.equipment.dto.EquipmentTestTypeDto;
import org.openlmis.equipment.repository.EquipmentTestTypeRepository;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.EquipmentCategoryDataBuilder;
import org.openlmis.equipment.util.EquipmentTestTypeDataBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@SuppressWarnings({"PMD.UnusedPrivateField", "PMD.TooManyMethods"})
public class EquipmentTestTypeControllerTest {
  @Mock
  Pageable pageable;

  @Mock
  EquipmentTestTypeRepository repository;

  @Mock
  private PermissionService permissionService;

  @InjectMocks
  EquipmentTestTypeController controller = new EquipmentTestTypeController();


  private final String name;
  private final String code;
  private final EquipmentTestType equipmentTestType;
  private final Set<EquipmentTestType> equipmentTestTypes;
  private final EquipmentTestTypeDto equipmentTestTypeDto;
  EquipmentCategory equipmentCategory = new EquipmentCategoryDataBuilder().build();

  /**
   * Constructor for test.
   */

  public EquipmentTestTypeControllerTest() {
    initMocks(this);
    name = "name1";
    code = "code1";
    equipmentTestType = new EquipmentTestTypeDataBuilder()
            .withName(name)
            .withCode(code)
            .withDisplayOrder(1)
            .withEnabled(true)
            .withEquipmentCategory(equipmentCategory)
            .build();
    equipmentTestTypeDto = new EquipmentTestTypeDto();
    equipmentTestType.export(equipmentTestTypeDto);
    equipmentTestTypes = Sets.newHashSet(equipmentTestType);
  }

  @Test
  public void shouldGetAllTestType() {
    List<EquipmentTestTypeDto> expected = Lists.newArrayList(equipmentTestTypeDto);
    when(repository.findAll()).thenReturn(equipmentTestTypes);

    Page<EquipmentTestTypeDto> equipmentTestTypeDtos = controller.findAll(pageable);

    assertThat(equipmentTestTypeDtos.getTotalElements())
            .isEqualTo(expected.size());
  }

  @Test
  public void shouldCreateNewEquipmentTestType() {
    when(repository.save(any())).thenReturn(equipmentTestType);
    controller.create(equipmentTestTypeDto);
    verify(repository).save(equipmentTestType);
  }

  @Test
  public void shouldUpdateEquipmentTestType() {
    when(repository.save(any())).thenReturn(equipmentTestType);
    controller.update(equipmentTestTypeDto, equipmentTestType.getId());
    verify(repository).save(equipmentTestType);
  }

  @Test
  public void shouldGetEquipmentTestType() {
    //given
    when(repository.findById(equipmentTestType.getId()))
            .thenReturn(Optional.of(equipmentTestType));
    EquipmentTestTypeDto equipmentTestTypeDto1 =
            controller.get(equipmentTestType.getId());
    assertEquals(equipmentTestTypeDto1, equipmentTestTypeDto);
  }


  @Test
  public void shouldNotCreateExistingEquipmentTestType() {
    when(repository.save(any())).thenReturn(equipmentTestType);
    controller.create(equipmentTestTypeDto);
  }

}
