/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.openlmis.equipment.domain.EquipmentEnergyType;
import org.openlmis.equipment.dto.EquipmentEnergyTypeDto;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.service.EquipmentEnergyTypeService;
import org.openlmis.equipment.util.EquipmentEnergyTypeDataBuilder;
import org.openlmis.equipment.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;

@SuppressWarnings("PMD.TooManyMethods")
public class EquipmentEnergyTypeControllerTest {
  private static final String TEST_CODE = "Code 1";
  private static final String TEST_NAME = "Name 1";

  @Mock
  private EquipmentEnergyTypeService equipmentEnergyTypeService;

  @InjectMocks
  private EquipmentEnergyTypeController equipmentEnergyTypeController;

  private EquipmentEnergyType equipmentEnergyType;
  private EquipmentEnergyTypeDto equipmentEnergyTypeDto;
  private UUID equipmentEnergyTypeId;
  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    equipmentEnergyType = new EquipmentEnergyTypeDataBuilder().build();
    equipmentEnergyTypeId = equipmentEnergyType.getId();
    equipmentEnergyTypeDto = EquipmentEnergyTypeDto.newInstance(equipmentEnergyType);
  }

  @Test
  public void shouldCreateNewEquipment() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    when(equipmentEnergyTypeService.create(equipmentEnergyTypeDto))
        .thenReturn(equipmentEnergyType);

    EquipmentEnergyTypeDto savedEnergyTypeDto = equipmentEnergyTypeController.create(
        equipmentEnergyTypeDto,
        bindingResult
    );
    verify(equipmentEnergyTypeService).create(equipmentEnergyTypeDto);
    Assertions.assertThat(savedEnergyTypeDto).isEqualTo(equipmentEnergyTypeDto);
  }

  @Test
  public void shouldUpdateExistingEquipment() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    EquipmentEnergyType energyTypeUpdate = new EquipmentEnergyTypeDataBuilder().build();
    energyTypeUpdate.setId(equipmentEnergyTypeId);
    EquipmentEnergyTypeDto energyTypeUpdateDto = EquipmentEnergyTypeDto.newInstance(
        energyTypeUpdate
    );

    when(equipmentEnergyTypeService.update(equipmentEnergyTypeId, energyTypeUpdateDto))
        .thenReturn(energyTypeUpdate);

    EquipmentEnergyTypeDto updatedEnergyTypeDto = equipmentEnergyTypeController.update(
        equipmentEnergyTypeId,
        energyTypeUpdateDto,
        bindingResult
    );
    verify(equipmentEnergyTypeService).update(equipmentEnergyTypeId, energyTypeUpdateDto);
    Assertions.assertThat(updatedEnergyTypeDto).isEqualTo(energyTypeUpdateDto);
  }

  @Test(expected = ValidationMessageException.class)
  public void shouldThrowExceptionWhenValidationFails() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    when(bindingResult.hasErrors()).thenReturn(true);
    when(equipmentEnergyTypeService.create(equipmentEnergyTypeDto))
        .thenReturn(equipmentEnergyType);
    equipmentEnergyTypeController.create(
        equipmentEnergyTypeDto,
        bindingResult
    );
  }

  @Test
  public void shouldGetEquipmentEnergyType() {
    when(equipmentEnergyTypeService.getEquipmentEnergyType(equipmentEnergyTypeId))
        .thenReturn(equipmentEnergyType);
    EquipmentEnergyTypeDto foundEnergyTypeDto =
        equipmentEnergyTypeController.getEquipmentEnergyType(equipmentEnergyTypeId);

    verify(equipmentEnergyTypeService).getEquipmentEnergyType(equipmentEnergyTypeId);
    Assertions.assertThat(foundEnergyTypeDto).isEqualTo(equipmentEnergyTypeDto);
  }

  @Test
  public void shouldSearchEquipmentEnergyTypes() {
    List<EquipmentEnergyType> expectedEnergyTypes = Lists.newArrayList(equipmentEnergyType);

    when(equipmentEnergyTypeService.search(
        any(),
        any(),
        eq(pageable))
    ).thenReturn(PaginationUtil.getPage(expectedEnergyTypes, pageable));

    Page<EquipmentEnergyTypeDto> foundEnergyTypeDtos =
        equipmentEnergyTypeController.searchEquipmentEnergyTypes(
            Optional.of(TEST_NAME),
            Optional.of(TEST_CODE),
            pageable
        );

    verify(equipmentEnergyTypeService).search(
        any(),
        any(),
        eq(pageable)
    );

    List<EquipmentEnergyTypeDto> expectedEnergyTypeDtos = Lists.newArrayList(
        equipmentEnergyTypeDto
    );
    Assertions.assertThat(foundEnergyTypeDtos.getContent()).isEqualTo(expectedEnergyTypeDtos);
  }
}
