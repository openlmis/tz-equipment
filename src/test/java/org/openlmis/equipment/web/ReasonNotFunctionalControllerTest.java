/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.openlmis.equipment.domain.ReasonNotFunctional;
import org.openlmis.equipment.dto.ReasonNotFunctionalDto;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.service.ReasonNotFunctionalService;
import org.openlmis.equipment.util.PaginationUtil;
import org.openlmis.equipment.util.ReasonNotFunctionalDataBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;

@SuppressWarnings("PMD.TooManyMethods")
public class ReasonNotFunctionalControllerTest {
  private static final String TEST_NAME = "Name 1";

  @Mock
  private ReasonNotFunctionalService reasonNotFunctionalService;

  @InjectMocks
  private ReasonNotFunctionalController reasonNotFunctionalController;

  private ReasonNotFunctional reasonNotFunctional;
  private ReasonNotFunctionalDto reasonNotFunctionalDto;
  private UUID reasonNotFunctionalId;
  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    reasonNotFunctional = new ReasonNotFunctionalDataBuilder().build();
    reasonNotFunctionalId = reasonNotFunctional.getId();
    reasonNotFunctionalDto = ReasonNotFunctionalDto.newInstance(reasonNotFunctional);
  }

  @Test
  public void shouldCreateNewEquipment() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    when(reasonNotFunctionalService.create(reasonNotFunctionalDto))
        .thenReturn(reasonNotFunctional);

    ReasonNotFunctionalDto savedModelDto = reasonNotFunctionalController.create(
        reasonNotFunctionalDto,
        bindingResult
    );
    verify(reasonNotFunctionalService).create(reasonNotFunctionalDto);
    Assertions.assertThat(savedModelDto).isEqualTo(reasonNotFunctionalDto);
  }

  @Test(expected = ValidationMessageException.class)
  public void shouldThrowExceptionWhenValidationFails() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    when(bindingResult.hasErrors()).thenReturn(true);
    when(reasonNotFunctionalService.create(reasonNotFunctionalDto))
        .thenReturn(reasonNotFunctional);
    reasonNotFunctionalController.create(
        reasonNotFunctionalDto,
        bindingResult
    );
  }

  @Test
  public void shouldGetReasonNotFunctional() {
    when(reasonNotFunctionalService.getReasonNotFunctional(reasonNotFunctionalId))
        .thenReturn(reasonNotFunctional);
    ReasonNotFunctionalDto foundModelDto = reasonNotFunctionalController.getReasonNotFunctional(
        reasonNotFunctionalId
    );

    verify(reasonNotFunctionalService).getReasonNotFunctional(reasonNotFunctionalId);
    Assertions.assertThat(foundModelDto).isEqualTo(reasonNotFunctionalDto);
  }

  @Test
  public void shouldSearchReasonNotFunctional() {
    List<ReasonNotFunctional> expectedModels = Lists.newArrayList(reasonNotFunctional);

    when(reasonNotFunctionalService.search(
        any(ReasonNotFunctionalService.SearchParams.class),
        eq(pageable))
    ).thenReturn(PaginationUtil.getPage(expectedModels, pageable));

    Map<String, Object> searchParams = new LinkedHashMap<>();
    searchParams.put("name", TEST_NAME);
    searchParams.put("id", reasonNotFunctionalId);

    Page<ReasonNotFunctionalDto> foundModelDtos = reasonNotFunctionalController
        .searchReasonNotFunctional(searchParams, pageable);

    verify(reasonNotFunctionalService).search(
        any(ReasonNotFunctionalService.SearchParams.class),
        eq(pageable)
    );

    List<ReasonNotFunctionalDto> expectedModelDtos = Lists.newArrayList(reasonNotFunctionalDto);
    Assertions.assertThat(foundModelDtos.getContent()).isEqualTo(expectedModelDtos);
  }
}
