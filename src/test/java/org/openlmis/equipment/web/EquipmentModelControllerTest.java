/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.openlmis.equipment.domain.EquipmentModel;
import org.openlmis.equipment.domain.EquipmentType;
import org.openlmis.equipment.dto.EquipmentModelDto;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.service.EquipmentModelService;
import org.openlmis.equipment.util.EquipmentModelDataBuilder;
import org.openlmis.equipment.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;

@SuppressWarnings("PMD.TooManyMethods")
public class EquipmentModelControllerTest {
  private static final String TEST_CODE = "Code 1";
  private static final String TEST_NAME = "Name 1";

  @Mock
  private EquipmentModelService equipmentModelService;

  @InjectMocks
  private EquipmentModelController equipmentModelController;

  private EquipmentModel equipmentModel;
  private EquipmentType equipmentType;
  private EquipmentModelDto equipmentModelDto;
  private UUID equipmentModelId;
  private UUID equipmentTypeId;
  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    equipmentModel = new EquipmentModelDataBuilder().build();
    equipmentModelId = equipmentModel.getId();
    equipmentType = equipmentModel.getEquipmentType();
    equipmentTypeId = equipmentType.getId();
    equipmentModelDto = EquipmentModelDto.newInstance(equipmentModel);
  }

  @Test
  public void shouldCreateNewEquipment() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    when(equipmentModelService.create(equipmentModelDto))
        .thenReturn(equipmentModel);

    EquipmentModelDto savedModelDto = equipmentModelController.create(
        equipmentModelDto,
        bindingResult
    );
    verify(equipmentModelService).create(equipmentModelDto);
    Assertions.assertThat(savedModelDto).isEqualTo(equipmentModelDto);
  }

  @Test(expected = ValidationMessageException.class)
  public void shouldThrowExceptionWhenValidationFails() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    when(bindingResult.hasErrors()).thenReturn(true);
    when(equipmentModelService.create(equipmentModelDto))
        .thenReturn(equipmentModel);
    equipmentModelController.create(
        equipmentModelDto,
        bindingResult
    );
  }

  @Test
  public void shouldGetEquipmentModel() {
    when(equipmentModelService.getEquipmentModel(equipmentModelId))
        .thenReturn(equipmentModel);
    EquipmentModelDto foundModelDto = equipmentModelController.getEquipmentModel(equipmentModelId);

    verify(equipmentModelService).getEquipmentModel(equipmentModelId);
    Assertions.assertThat(foundModelDto).isEqualTo(equipmentModelDto);
  }

  @Test
  public void shouldSearchEquipmentModels() {
    List<EquipmentModel> expectedModels = Lists.newArrayList(equipmentModel);

    when(equipmentModelService.search(
        any(EquipmentModelService.SearchParams.class),
        eq(pageable))
    ).thenReturn(PaginationUtil.getPage(expectedModels, pageable));

    Map<String, Object> searchParams = new LinkedHashMap<>();
    searchParams.put("code", TEST_CODE);
    searchParams.put("name", TEST_NAME);
    searchParams.put("id", equipmentModelId);
    searchParams.put("equipmentTypeId", equipmentTypeId);

    Page<EquipmentModelDto> foundModelDtos = equipmentModelController.searchEquipmentModels(
        searchParams,
        pageable
    );

    verify(equipmentModelService).search(
        any(EquipmentModelService.SearchParams.class),
        eq(pageable)
    );

    List<EquipmentModelDto> expectedModelDtos = Lists.newArrayList(equipmentModelDto);
    Assertions.assertThat(foundModelDtos.getContent()).isEqualTo(expectedModelDtos);
  }
}
