/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.openlmis.equipment.domain.EquipmentMaintenanceRequestReason;
import org.openlmis.equipment.dto.EquipmentMaintenanceRequestReasonDto;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.service.EquipmentMaintenanceRequestReasonService;
import org.openlmis.equipment.util.EquipmentMaintenanceRequestReasonDataBuilder;
import org.openlmis.equipment.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;

@SuppressWarnings("PMD.TooManyMethods")
public class EquipmentMaintenanceRequestReasonControllerTest {

  private static final String TEST_NAME = "Name 1";

  @Mock
  private EquipmentMaintenanceRequestReasonService requestReasonService;

  @InjectMocks
  private EquipmentMaintenanceRequestReasonController requestReasonController;

  private EquipmentMaintenanceRequestReason maintenanceRequestReason;

  private UUID reasonId;

  private EquipmentMaintenanceRequestReasonDto requestReasonDto;

  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    maintenanceRequestReason
        = new EquipmentMaintenanceRequestReasonDataBuilder()
        .build();
    reasonId = maintenanceRequestReason.getId();
    requestReasonDto = requestReasonDto.newInstance(maintenanceRequestReason);

  }

  @Test
  public void shouldCreateNewMaintenanceRequestReason() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    when(requestReasonService.create(requestReasonDto))
        .thenReturn(maintenanceRequestReason);

    EquipmentMaintenanceRequestReasonDto
        savedReasonDto = requestReasonController.create(
        requestReasonDto,
        bindingResult
    );
    verify(requestReasonService).create(requestReasonDto);
    Assertions.assertThat(savedReasonDto).isEqualTo(requestReasonDto);
  }

  @Test(expected = ValidationMessageException.class)
  public void shouldThrowExceptionWhenValidationFails() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    when(bindingResult.hasErrors()).thenReturn(true);
    when(requestReasonService.create(requestReasonDto))
        .thenReturn(maintenanceRequestReason);
    requestReasonController.create(
        requestReasonDto,
        bindingResult
    );
  }

  @Test
  public void shouldGetEquipmentMaintenanceRequestReason() {
    when(requestReasonService.getEquipmentMaintenanceRequestReason(reasonId))
        .thenReturn(maintenanceRequestReason);
    EquipmentMaintenanceRequestReasonDto foundServiceTypeDto
        = requestReasonController
        .getEquipmentMaintenanceRequestReason(reasonId);

    verify(requestReasonService).getEquipmentMaintenanceRequestReason(reasonId);
    Assertions.assertThat(foundServiceTypeDto).isEqualTo(requestReasonDto);
  }

  @Test
  public void shouldSearchEquipmentMaintenanceRequestReasons() {
    List<EquipmentMaintenanceRequestReason> expectedRequestReason
        = Lists.newArrayList(maintenanceRequestReason);

    when(requestReasonService.search(
        any(String.class),
        eq(pageable))
    ).thenReturn(PaginationUtil.getPage(expectedRequestReason, pageable));

    Page<EquipmentMaintenanceRequestReasonDto> foundServiceTypeDtos
        = requestReasonController
        .searchEquipmentMaintenanceRequestReasons(
            Optional.of(TEST_NAME),
            pageable
        );

    verify(requestReasonService).search(
        any(String.class),
        eq(pageable)
    );

    List<EquipmentMaintenanceRequestReasonDto> expectedReasonDtos
        = Lists.newArrayList(requestReasonDto);
    Assertions.assertThat(foundServiceTypeDtos.getContent()).isEqualTo(expectedReasonDtos);
  }

}
