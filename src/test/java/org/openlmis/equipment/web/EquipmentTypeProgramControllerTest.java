/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.openlmis.equipment.domain.EquipmentType;
import org.openlmis.equipment.domain.EquipmentTypeProgram;
import org.openlmis.equipment.dto.EquipmentTypeProgramDto;
import org.openlmis.equipment.repository.EquipmentTypeProgramRepository;
import org.openlmis.equipment.service.PermissionService;
import org.openlmis.equipment.util.EquipmentTypeDataBuilder;
import org.openlmis.equipment.util.EquipmentTypeProgramDataBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@SuppressWarnings({"PMD.UnusedPrivateField", "PMD.TooManyMethods"})
public class EquipmentTypeProgramControllerTest {

  @Mock
  private Pageable pageable;

  @Mock
  private EquipmentTypeProgramRepository repository;

  @Mock
  private PermissionService permissionService;

  @InjectMocks
  private EquipmentTypeProgramController controller = new EquipmentTypeProgramController();

  private EquipmentTypeProgram equipmentTypeProgram;
  private Set<EquipmentTypeProgram> equipmentTypePrograms;
  private EquipmentTypeProgramDto equipmentTypeProgramDto;

  EquipmentType equipmentType = new EquipmentTypeDataBuilder().build();
  private UUID programId = UUID.fromString("466b2e7f-5798-4027-a2ca-373627748ea6");

  /**
   * Constructor for test.
   */
  public EquipmentTypeProgramControllerTest() {
    initMocks(this);
    equipmentTypeProgram = new EquipmentTypeProgramDataBuilder()
            .withEquipmentType(equipmentType)
            .withProgramId(programId)
            .withDisplayOrder(1)
            .withEnableTestCount(true)
            .withEnableTotalColumn(false)
            .build();
    equipmentTypeProgramDto = new EquipmentTypeProgramDto();
    equipmentTypeProgram.export(equipmentTypeProgramDto);
    equipmentTypePrograms = Sets.newHashSet(equipmentTypeProgram);
  }

  @Test
  public void shouldGetAllTypesProgram() {
    List<EquipmentTypeProgramDto> expected = Lists.newArrayList(equipmentTypeProgramDto);
    when(repository.findAll()).thenReturn(equipmentTypePrograms);

    Page<EquipmentTypeProgramDto> equipmentTypeProgramDtos = controller.findAll(null,pageable);

    assertThat(equipmentTypeProgramDtos.getTotalElements())
            .isEqualTo(expected.size());
  }

  @Test
  public void shouldGetEquipmentTypeProgram() {
    //given
    when(repository.findById(equipmentTypeProgram.getId()))
            .thenReturn(Optional.of(equipmentTypeProgram));
    EquipmentTypeProgramDto equipmentTypeDto1 =
            controller.get(equipmentTypeProgram.getId());
    assertEquals(equipmentTypeDto1, equipmentTypeProgramDto);
  }

  @Test
  public void whenDeletingAnProgramTypeItShouldUseTheRepository() {
    when(repository.findById(equipmentTypeProgram.getId()))
            .thenReturn(Optional.of(equipmentTypeProgram));
    // Given that an item with equipment ID is removed
    controller.remove(equipmentTypeProgram.getId());
    // Verify that the repository is used to delete the item
    verify(repository,times(1)).deleteById(equipmentTypeProgram.getId());
  }


  @Test
  public void shouldCreateNewEquipmentTypeProgram() {
    when(repository.save(any())).thenReturn(equipmentTypeProgram);
    controller.create(equipmentTypeProgramDto);
    verify(repository).save(equipmentTypeProgram);
  }

}
