/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.openlmis.equipment.domain.Vendor;
import org.openlmis.equipment.dto.VendorDto;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.service.VendorService;
import org.openlmis.equipment.util.PaginationUtil;
import org.openlmis.equipment.util.VendorDataBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;

@SuppressWarnings("PMD.TooManyMethods")
public class VendorControllerTest {

  private static final String TEST_NAME = "Name 1";

  @Mock
  private VendorService vendorService;

  @InjectMocks
  private VendorController vendorController;

  private Vendor vendor;

  private UUID vendorId;

  private VendorDto vendorDto;

  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    vendor = new VendorDataBuilder().build();
    vendorId = vendor.getId();
    vendorDto = vendorDto.newInstance(vendor);

  }

  @Test
  public void shouldCreateNewVendor() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    when(vendorService.create(vendorDto))
        .thenReturn(vendor);

    VendorDto savedVendorDto = vendorController.create(
        vendorDto,
        bindingResult
    );
    verify(vendorService).create(vendorDto);
    Assertions.assertThat(savedVendorDto).isEqualTo(vendorDto);
  }

  @Test(expected = ValidationMessageException.class)
  public void shouldThrowExceptionWhenValidationFails() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    when(bindingResult.hasErrors()).thenReturn(true);
    when(vendorService.create(vendorDto))
        .thenReturn(vendor);
    vendorController.create(
        vendorDto,
        bindingResult
    );
  }

  @Test
  public void shouldGetVendor() {
    when(vendorService.getVendor(vendorId))
        .thenReturn(vendor);
    VendorDto foundVendorDto = vendorController.getVendor(vendorId);

    verify(vendorService).getVendor(vendorId);
    Assertions.assertThat(foundVendorDto).isEqualTo(vendorDto);
  }

  @Test
  public void shouldSearchVendors() {
    List<Vendor> expectedVendors = Lists.newArrayList(vendor);

    when(vendorService.search(
        any(String.class),
        eq(pageable))
    ).thenReturn(PaginationUtil.getPage(expectedVendors, pageable));

    Page<VendorDto> foundVendorDtos = vendorController.searchVendors(
        Optional.of(TEST_NAME),
        pageable
    );

    verify(vendorService).search(
        any(String.class),
        eq(pageable)
    );

    List<VendorDto> expectedVendorDtos = Lists.newArrayList(vendorDto);
    Assertions.assertThat(foundVendorDtos.getContent()).isEqualTo(expectedVendorDtos);
  }

}
