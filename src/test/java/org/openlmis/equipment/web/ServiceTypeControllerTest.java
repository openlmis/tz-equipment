/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.openlmis.equipment.domain.ServiceType;
import org.openlmis.equipment.dto.ServiceTypeDto;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.service.ServiceTypeService;
import org.openlmis.equipment.util.PaginationUtil;
import org.openlmis.equipment.util.ServiceTypeDataBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;

@SuppressWarnings("PMD.TooManyMethods")
public class ServiceTypeControllerTest {

  private static final String TEST_NAME = "Name 1";

  @Mock
  private ServiceTypeService serviceTypeService;

  @InjectMocks
  private ServiceTypeController serviceTypeController;

  private ServiceType serviceType;

  private UUID serviceTypeId;

  private ServiceTypeDto serviceTypeDto;

  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    serviceType = new ServiceTypeDataBuilder().build();
    serviceTypeId = serviceType.getId();
    serviceTypeDto = ServiceTypeDto.newInstance(serviceType);

  }

  @Test
  public void shouldCreateNewServiceType() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    when(serviceTypeService.create(serviceTypeDto))
        .thenReturn(serviceType);

    ServiceTypeDto savedServiceTypeDto = serviceTypeController.create(
        serviceTypeDto,
        bindingResult
    );
    verify(serviceTypeService).create(serviceTypeDto);
    Assertions.assertThat(savedServiceTypeDto).isEqualTo(serviceTypeDto);
  }

  @Test(expected = ValidationMessageException.class)
  public void shouldThrowExceptionWhenValidationFails() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    when(bindingResult.hasErrors()).thenReturn(true);
    when(serviceTypeService.create(serviceTypeDto))
        .thenReturn(serviceType);
    serviceTypeController.create(
        serviceTypeDto,
        bindingResult
    );
  }

  @Test
  public void shouldGetServiceType() {
    when(serviceTypeService.getServiceType(serviceTypeId))
        .thenReturn(serviceType);
    ServiceTypeDto foundServiceTypeDto = serviceTypeController.getServiceType(serviceTypeId);

    verify(serviceTypeService).getServiceType(serviceTypeId);
    Assertions.assertThat(foundServiceTypeDto).isEqualTo(serviceTypeDto);
  }

  @Test
  public void shouldSearchServiceTypes() {
    List<ServiceType> expectedServiceTypes = Lists.newArrayList(serviceType);

    when(serviceTypeService.search(
        any(String.class),
        eq(pageable))
    ).thenReturn(PaginationUtil.getPage(expectedServiceTypes, pageable));

    Page<ServiceTypeDto> foundServiceTypeDtos = serviceTypeController.searchServiceTypes(
        Optional.of(TEST_NAME),
        pageable
    );

    verify(serviceTypeService).search(
        any(String.class),
        eq(pageable)
    );

    List<ServiceTypeDto> expectedServiceTypeDtos = Lists.newArrayList(serviceTypeDto);
    Assertions.assertThat(foundServiceTypeDtos.getContent()).isEqualTo(expectedServiceTypeDtos);
  }

}
