/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.openlmis.equipment.domain.Equipment;
import org.openlmis.equipment.domain.EquipmentCategory;
import org.openlmis.equipment.domain.EquipmentEnergyType;
import org.openlmis.equipment.domain.EquipmentModel;
import org.openlmis.equipment.dto.EquipmentDto;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.service.EquipmentService;
import org.openlmis.equipment.util.EquipmentDataBuilder;
import org.openlmis.equipment.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;

@SuppressWarnings("PMD.TooManyMethods")
public class EquipmentControllerTest {
  private static final String TEST_MANUFACTURER = "Manufacturer 1";
  private static final String TEST_NAME = "Name 1";

  @Mock
  private EquipmentService equipmentService;

  @InjectMocks
  private EquipmentController equipmentController;

  private Equipment equipment;
  private UUID equipmentId;
  private EquipmentEnergyType equipmentEnergyType;
  private UUID equipmentEnergyTypeId;
  private EquipmentModel equipmentModel;
  private UUID equipmentModelId;
  private EquipmentCategory equipmentCategory;
  private UUID equipmentCategoryId;
  private EquipmentDto equipmentDto;
  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    equipment = new EquipmentDataBuilder().build();
    equipmentId = equipment.getId();
    equipmentEnergyType = equipment.getEnergyType();
    equipmentEnergyTypeId = equipmentEnergyType.getId();
    equipmentModel = equipment.getModel();
    equipmentModelId = equipmentModel.getId();
    equipmentCategory = equipment.getCategory();
    equipmentCategoryId = equipmentCategory.getId();
    equipmentDto = equipmentDto.newInstance(equipment);

  }

  @Test
  public void shouldCreateNewEquipment() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    when(equipmentService.create(equipmentDto))
        .thenReturn(equipment);

    EquipmentDto savedEquipmentDto = equipmentController.create(
        equipmentDto,
        bindingResult
    );
    verify(equipmentService).create(equipmentDto);
    Assertions.assertThat(savedEquipmentDto).isEqualTo(equipmentDto);
  }

  @Test(expected = ValidationMessageException.class)
  public void shouldThrowExceptionWhenValidationFails() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    when(bindingResult.hasErrors()).thenReturn(true);
    when(equipmentService.create(equipmentDto))
        .thenReturn(equipment);
    equipmentController.create(
        equipmentDto,
        bindingResult
    );
  }

  @Test
  public void shouldGetEquipment() {
    when(equipmentService.getEquipment(equipmentId))
        .thenReturn(equipment);
    EquipmentDto foundEquipmentDto = equipmentController.getEquipment(equipmentId);

    verify(equipmentService).getEquipment(equipmentId);
    Assertions.assertThat(foundEquipmentDto).isEqualTo(equipmentDto);
  }

  @Test
  public void shouldSearchEquipments() {
    List<Equipment> expectedEquipments = Lists.newArrayList(equipment);

    when(equipmentService.search(
        any(EquipmentService.SearchParams.class),
        eq(pageable))
    ).thenReturn(PaginationUtil.getPage(expectedEquipments, pageable));

    Map<String, Object> searchParams = new LinkedHashMap<>();
    searchParams.put("manufacturer", TEST_MANUFACTURER);
    searchParams.put("name", TEST_NAME);
    searchParams.put("id", equipmentId);
    searchParams.put("equipmentEnergyTypeId", equipmentEnergyTypeId);
    searchParams.put("equipmentModelId", equipmentModelId);
    searchParams.put("equipmentCategoryId", equipmentCategoryId);

    Page<EquipmentDto> foundEquipmentDtos = equipmentController.searchEquipments(
        searchParams,
        pageable
    );

    verify(equipmentService).search(
        any(EquipmentService.SearchParams.class),
        eq(pageable)
    );

    List<EquipmentDto> expectedEquipmentDtos = Lists.newArrayList(equipmentDto);
    Assertions.assertThat(foundEquipmentDtos.getContent()).isEqualTo(expectedEquipmentDtos);
  }

}
