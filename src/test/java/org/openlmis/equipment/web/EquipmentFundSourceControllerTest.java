/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.openlmis.equipment.domain.EquipmentFundSource;
import org.openlmis.equipment.dto.EquipmentFundSourceDto;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.service.EquipmentFundSourceService;
import org.openlmis.equipment.util.EquipmentFundSourceDataBuilder;
import org.openlmis.equipment.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;

@SuppressWarnings("PMD.TooManyMethods")
public class EquipmentFundSourceControllerTest {
  private static final String TEST_CODE = "Code 1";
  private static final String TEST_NAME = "Name 1";

  @Mock
  private EquipmentFundSourceService equipmentFundSourceService;

  @InjectMocks
  private EquipmentFundSourceController equipmentFundSourceController;

  private EquipmentFundSource equipmentFundSource;
  private EquipmentFundSourceDto equipmentFundSourceDto;
  private UUID equipmentFundSourceId;
  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    equipmentFundSource = new EquipmentFundSourceDataBuilder().build();
    equipmentFundSourceId = equipmentFundSource.getId();
    equipmentFundSourceDto = EquipmentFundSourceDto.newInstance(equipmentFundSource);
  }

  @Test
  public void shouldCreateNewEquipmentFundSource() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    when(equipmentFundSourceService.create(equipmentFundSourceDto))
        .thenReturn(equipmentFundSource);

    EquipmentFundSourceDto savedFundSourceDto = equipmentFundSourceController.create(
        equipmentFundSourceDto,
        bindingResult
    );
    verify(equipmentFundSourceService).create(equipmentFundSourceDto);
    Assertions.assertThat(savedFundSourceDto).isEqualTo(equipmentFundSourceDto);
  }

  @Test
  public void shouldUpdateExistingEquipmentFundSource() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    EquipmentFundSource fundSourceUpdate = new EquipmentFundSourceDataBuilder().build();
    fundSourceUpdate.setId(equipmentFundSourceId);
    EquipmentFundSourceDto fundSourceUpdateDto = EquipmentFundSourceDto.newInstance(
        fundSourceUpdate
    );

    when(equipmentFundSourceService.update(equipmentFundSourceId, fundSourceUpdateDto))
        .thenReturn(fundSourceUpdate);

    EquipmentFundSourceDto updatedFundSourceDto = equipmentFundSourceController.update(
        equipmentFundSourceId,
        fundSourceUpdateDto,
        bindingResult
    );
    verify(equipmentFundSourceService).update(equipmentFundSourceId, fundSourceUpdateDto);
    Assertions.assertThat(updatedFundSourceDto).isEqualTo(fundSourceUpdateDto);
  }

  @Test(expected = ValidationMessageException.class)
  public void shouldThrowExceptionWhenValidationFails() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    when(bindingResult.hasErrors()).thenReturn(true);
    when(equipmentFundSourceService.create(equipmentFundSourceDto))
        .thenReturn(equipmentFundSource);
    equipmentFundSourceController.create(
        equipmentFundSourceDto,
        bindingResult
    );
  }

  @Test
  public void shouldGetEquipmentFundSource() {
    when(equipmentFundSourceService.getEquipmentFundSource(equipmentFundSourceId))
        .thenReturn(equipmentFundSource);
    EquipmentFundSourceDto foundFundSourceDto =
        equipmentFundSourceController.getEquipmentFundSource(equipmentFundSourceId);

    verify(equipmentFundSourceService).getEquipmentFundSource(equipmentFundSourceId);
    Assertions.assertThat(foundFundSourceDto).isEqualTo(equipmentFundSourceDto);
  }

  @Test
  public void shouldSearchEquipmentFundSources() {
    List<EquipmentFundSource> expectedFundSources = Lists.newArrayList(equipmentFundSource);

    when(equipmentFundSourceService.search(
        any(),
        any(),
        eq(pageable))
    ).thenReturn(PaginationUtil.getPage(expectedFundSources, pageable));

    Page<EquipmentFundSourceDto> foundFundSourceDtos =
        equipmentFundSourceController.searchEquipmentFundSources(
            Optional.of(TEST_NAME),
            Optional.of(TEST_CODE),
            pageable
        );

    verify(equipmentFundSourceService).search(
        any(),
        any(),
        eq(pageable)
    );

    List<EquipmentFundSourceDto> expectedFundSourceDtos = Lists.newArrayList(
        equipmentFundSourceDto
    );
    Assertions.assertThat(foundFundSourceDtos.getContent()).isEqualTo(expectedFundSourceDtos);
  }
}
