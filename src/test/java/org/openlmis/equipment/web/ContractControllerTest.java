/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.web;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.openlmis.equipment.domain.Contract;
import org.openlmis.equipment.dto.ContractDto;
import org.openlmis.equipment.exception.ValidationMessageException;
import org.openlmis.equipment.service.ContractService;
import org.openlmis.equipment.util.ContractDataBuilder;
import org.openlmis.equipment.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;

@SuppressWarnings("PMD.TooManyMethods")
public class ContractControllerTest {

  private static final String TEST_IDENTIFIER = "12345";

  @Mock
  private ContractService contractService;

  @InjectMocks
  private ContractController contractController;

  private Contract contract;

  private UUID contractId;

  private ContractDto contractDto;

  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    contract = new ContractDataBuilder().build();
    contractId = contract.getId();
    contractDto = contractDto.newInstance(contract);
  }

  @Test
  public void shouldCreateNewContract() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    when(contractService.create(contractDto))
        .thenReturn(contract);

    ContractDto savedContractDto = contractController.create(
        contractDto,
        bindingResult
    );
    verify(contractService).create(contractDto);
    Assertions.assertThat(savedContractDto).isEqualTo(contractDto);
  }

  @Test(expected = ValidationMessageException.class)
  public void shouldThrowExceptionWhenValidationFails() {
    BindingResult bindingResult = Mockito.mock(BindingResult.class);
    when(bindingResult.hasErrors()).thenReturn(true);
    when(contractService.create(contractDto))
        .thenReturn(contract);
    contractController.create(
        contractDto,
        bindingResult
    );
  }

  @Test
  public void shouldGetContract() {
    when(contractService.getContract(contractId))
        .thenReturn(contract);
    ContractDto foundContractDto = contractController.getContract(contractId);

    verify(contractService).getContract(contractId);
    Assertions.assertThat(foundContractDto).isEqualTo(contractDto);
  }

  @Test
  public void shouldSearchContracts() {
    List<Contract> expectedContracts = Lists.newArrayList(contract);

    when(contractService.search(
        any(ContractService.SearchParams.class),
        eq(pageable))
    ).thenReturn(PaginationUtil.getPage(expectedContracts, pageable));

    Map<String, Object> searchParams = new LinkedHashMap<>();
    searchParams.put(ContractSearchParams.IDENTIFIER, TEST_IDENTIFIER);
    searchParams.put(ContractSearchParams.ID, contractId);
    searchParams.put(ContractSearchParams.VENDOR_ID, UUID.randomUUID());
    searchParams.put(ContractSearchParams.SERVICE_TYPE_ID, UUID.randomUUID());
    searchParams.put(ContractSearchParams.EQUIPMENT_ID, UUID.randomUUID());
    searchParams.put(ContractSearchParams.FACILITY_ID, UUID.randomUUID());
    searchParams.put(ContractSearchParams.DATE_FROM, "2022-05-06");
    searchParams.put(ContractSearchParams.DATE_TO, "2023-05-06");

    Page<ContractDto> foundContractDtos = contractController.searchContracts(
        searchParams,
        pageable
    );

    verify(contractService).search(
        any(ContractService.SearchParams.class),
        eq(pageable)
    );

    List<ContractDto> expectedContractDtos = Lists.newArrayList(contractDto);
    Assertions.assertThat(foundContractDtos.getContent()).isEqualTo(expectedContractDtos);
  }

}
