/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.util;

import java.util.UUID;

import org.openlmis.equipment.domain.EquipmentCategory;
import org.openlmis.equipment.domain.EquipmentTestType;

public class EquipmentTestTypeDataBuilder {
  private static int instanceNumber = 0;
  private UUID id;
  private String name;
  private String code;
  private Boolean enabled;
  private Integer displayOrder;
  private EquipmentCategory equipmentCategory;


  /**
   * Builds instance of {@link EquipmentTestTypeDataBuilder} with sample data.
   */
  public EquipmentTestTypeDataBuilder() {
    instanceNumber++;

    id = UUID.randomUUID();
    name = "name" + instanceNumber;
    code = "code" + instanceNumber;
    enabled = true;
    equipmentCategory = new EquipmentCategoryDataBuilder().build();

  }

  /**
   * Builds instance of {@link EquipmentTestType}.
   */
  public EquipmentTestType build() {
    EquipmentTestType equipmentTestType = EquipmentTestType.newEquipmentTestType(name, code,
            displayOrder, enabled, equipmentCategory);
    equipmentTestType.setId(id);
    return equipmentTestType;
  }

  /**
   * Builds instance of {@link EquipmentTestType} without id.
   */
  public EquipmentTestType buildAsNew() {
    return this.withoutId().build();
  }

  public EquipmentTestTypeDataBuilder withoutId() {
    this.id = null;
    return this;
  }

  public EquipmentTestTypeDataBuilder withDisplayOrder(Integer displayOrder) {
    this.displayOrder = displayOrder;
    return this;
  }

  public EquipmentTestTypeDataBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public EquipmentTestTypeDataBuilder withCode(String code) {
    this.code = code;
    return this;
  }

  public EquipmentTestTypeDataBuilder withEnabled(Boolean enabled) {
    this.enabled = enabled;
    return this;
  }

  public EquipmentTestTypeDataBuilder withEquipmentCategory(EquipmentCategory equipmentCategory) {
    this.equipmentCategory = equipmentCategory;
    return this;
  }
}
