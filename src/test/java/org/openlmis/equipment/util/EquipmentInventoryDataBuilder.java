/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.util;

import java.time.LocalDate;
import java.util.UUID;
import org.openlmis.equipment.domain.Equipment;
import org.openlmis.equipment.domain.EquipmentFundSource;
import org.openlmis.equipment.domain.EquipmentInventory;
import org.openlmis.equipment.domain.FunctionalStatus;
import org.openlmis.equipment.domain.ReasonNotFunctional;
import org.openlmis.equipment.domain.RegistrationStatus;
import org.openlmis.equipment.domain.Utilization;

@SuppressWarnings("PMD.TooManyMethods")
public class EquipmentInventoryDataBuilder {

  private UUID id = UUID.randomUUID();

  private Equipment equipment = new EquipmentDataBuilder().build();

  private UUID facilityId = UUID.randomUUID();

  private UUID programId = UUID.randomUUID();

  private EquipmentFundSource fundSource = new EquipmentFundSourceDataBuilder().build();

  private FunctionalStatus functionalStatus = FunctionalStatus.FUNCTIONING;

  private ReasonNotFunctional reasonNotFunctional;

  private Utilization utilization = Utilization.ACTIVE;

  private String additionalNotes = "some additional notes";

  private LocalDate decommissionDate = LocalDate.of(2020, 10, 10);

  private String equipmentTrackingId = "Equipment Tracking ID";

  private String source = "Source";

  private LocalDate dateOfInstallation = LocalDate.now().minusYears(10);

  private LocalDate dateOfWarrantyExpiry = LocalDate.now().plusYears(10);

  private double purchasePrice = 1234.56;

  private RegistrationStatus registrationStatus = RegistrationStatus.NEW;

  /**
   * Sets {@link Equipment}.
   */
  public EquipmentInventoryDataBuilder withEquipment(Equipment equipment) {
    this.equipment = equipment;
    return this;
  }

  /**
   * Sets inventory {@link UUID}.
   */
  public EquipmentInventoryDataBuilder withId(UUID id) {
    this.id = id;
    return this;
  }

  /**
   * Sets program {@link UUID}.
   */
  public EquipmentInventoryDataBuilder withProgramId(UUID programId) {
    this.programId = programId;
    return this;
  }

  /**
   * Sets facility {@link UUID}.
   */
  public EquipmentInventoryDataBuilder withFacilityId(UUID facilityId) {
    this.facilityId = facilityId;
    return this;
  }

  /**
   * Sets {@Link EquipmentFundSource}.
   */
  public EquipmentInventoryDataBuilder withFundSource(EquipmentFundSource fundSource) {
    this.fundSource = fundSource;
    return this;
  }

  /**
   * Sets {@link FunctionalStatus}.
   */
  public EquipmentInventoryDataBuilder withStatus(FunctionalStatus status) {
    this.functionalStatus = status;
    return this;
  }

  /**
   * Sets {@Link ReasonNotFunctional}.
   */
  public EquipmentInventoryDataBuilder withReasonNotFunctional(ReasonNotFunctional reason) {
    this.reasonNotFunctional = reason;
    return this;
  }

  public EquipmentInventoryDataBuilder withUtilization(Utilization utilization) {
    this.utilization = utilization;
    return this;
  }

  public EquipmentInventoryDataBuilder withAdditionalNotes(String additionalNotes) {
    this.additionalNotes = additionalNotes;
    return this;
  }

  public EquipmentInventoryDataBuilder withDecommissionDate(LocalDate decommissionDate) {
    this.decommissionDate = decommissionDate;
    return this;
  }

  public EquipmentInventoryDataBuilder withEquipmentTrackingId(String equipmentTrackingId) {
    this.equipmentTrackingId = equipmentTrackingId;
    return this;
  }

  public EquipmentInventoryDataBuilder withSource(String source) {
    this.source = source;
    return this;
  }

  public EquipmentInventoryDataBuilder withDateOfInstallation(LocalDate dateOfInstallation) {
    this.dateOfInstallation = dateOfInstallation;
    return this;
  }

  public EquipmentInventoryDataBuilder withDateOfWarrantyExpiry(LocalDate dateOfWarrantyExpiry) {
    this.dateOfWarrantyExpiry = dateOfWarrantyExpiry;
    return this;
  }

  public EquipmentInventoryDataBuilder withPurchasePrice(double purchasePrice) {
    this.purchasePrice = purchasePrice;
    return this;
  }

  public EquipmentInventoryDataBuilder withRegistrationStatus(RegistrationStatus status) {
    this.registrationStatus = status;
    return this;
  }

  public EquipmentInventoryDataBuilder withUnserviceableStatus() {
    this.functionalStatus = FunctionalStatus.UNSERVICEABLE;
    return this;
  }

  /**
   * Sets program and facility to random values.
   */
  public EquipmentInventoryDataBuilder withRandomProgramAndFacility() {
    facilityId = UUID.randomUUID();
    programId = UUID.randomUUID();

    return this;
  }

  /**
   * Builds instance of {@link EquipmentInventory}.
   */
  public EquipmentInventory build() {
    EquipmentInventory equipmentInventory =
        new EquipmentInventory(
            equipment, facilityId, programId,fundSource, functionalStatus, reasonNotFunctional,
            utilization, additionalNotes, decommissionDate, equipmentTrackingId, source,
            dateOfInstallation, dateOfWarrantyExpiry, purchasePrice, registrationStatus
        );
    equipmentInventory.setId(id);
    return equipmentInventory;
  }

}
