/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.util;

import java.util.UUID;

import org.openlmis.equipment.domain.EquipmentType;
import org.openlmis.equipment.domain.EquipmentTypeProgram;

public class EquipmentTypeProgramDataBuilder {

  private UUID id;
  private UUID programId;
  private EquipmentType equipmentType;

  private Integer displayOrder;
  private Boolean enableTestCount;
  private Boolean enableTotalColumn;

  /**
   * Builds instance of {@link EquipmentTypeProgramDataBuilder} with sample data.
   */
  public EquipmentTypeProgramDataBuilder() {

    id = UUID.randomUUID();
    programId = UUID.fromString("466b2e7f-5798-4027-a2ca-373627748ea6");
    equipmentType = new EquipmentTypeDataBuilder().build();
    displayOrder = 1;
    enableTestCount = true;
    enableTotalColumn = false;

  }
  /**
   * Builds instance of {@link EquipmentTypeProgram}.
   */

  public EquipmentTypeProgram build() {
    EquipmentTypeProgram equipmentTypeProgram = EquipmentTypeProgram.newEquipmentTypeProgram(
            equipmentType,programId,enableTestCount,enableTotalColumn,
            displayOrder);
    equipmentTypeProgram.setId(id);
    return equipmentTypeProgram;
  }

  /**
   * Builds instance of {@link EquipmentTypeProgram} without id.
   */
  public EquipmentTypeProgram buildAsNew() {
    return this.withoutId().build();
  }

  public EquipmentTypeProgramDataBuilder withoutId() {
    this.id = null;
    return this;
  }

  /**
   * Sets program {@link UUID}.
   */
  public EquipmentTypeProgramDataBuilder withProgramId(UUID programId) {
    this.programId = programId;
    return this;
  }

  public EquipmentTypeProgramDataBuilder withEquipmentType(EquipmentType equipmentType) {
    this.equipmentType = equipmentType;
    return this;
  }

  public EquipmentTypeProgramDataBuilder withDisplayOrder(Integer displayOrder) {
    this.displayOrder = displayOrder;
    return this;
  }

  public EquipmentTypeProgramDataBuilder withEnableTestCount(Boolean enableTestCount) {
    this.enableTestCount = enableTestCount;
    return this;
  }

  public EquipmentTypeProgramDataBuilder withEnableTotalColumn(Boolean enableTotalColumn) {
    this.enableTotalColumn = enableTotalColumn;
    return this;
  }
}
