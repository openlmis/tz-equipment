/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.util;

import java.util.UUID;
import org.openlmis.equipment.domain.EquipmentModel;
import org.openlmis.equipment.domain.EquipmentType;

public class EquipmentModelDataBuilder {
  private static int instanceNumber = 0;

  private UUID id;
  private String name;
  private String code;
  private EquipmentType type;

  /**
   * Builds instance of {@link EquipmentModelDataBuilder} with sample data.
   */
  public EquipmentModelDataBuilder() {
    instanceNumber++;

    id = UUID.randomUUID();
    name = "name" + instanceNumber;
    code = "code" + instanceNumber;
    type = new EquipmentTypeDataBuilder().build();
  }

  /**
   * Builds instance of {@link EquipmentModel}.
   */
  public EquipmentModel build() {
    EquipmentModel equipmentModel = new EquipmentModel(name, code, type);
    equipmentModel.setId(id);
    return equipmentModel;
  }

  /**
   * Builds instance of {@link EquipmentModel} without id.
   */
  public EquipmentModel buildAsNew() {
    return this.withoutId().build();
  }

  public EquipmentModelDataBuilder withoutId() {
    this.id = null;
    return this;
  }

  public EquipmentModelDataBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public EquipmentModelDataBuilder withCode(String code) {
    this.code = code;
    return this;
  }

  public EquipmentModelDataBuilder withEquipmentType(EquipmentType type) {
    this.type = type;
    return this;
  }
}