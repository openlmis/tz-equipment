/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.util;

import java.util.UUID;
import org.openlmis.equipment.domain.ReasonNotFunctional;

public class ReasonNotFunctionalDataBuilder {

  private static int instanceNumber = 0;

  private UUID id;

  private String name;

  private String code;

  private Integer displayOrder;

  /**
   * Builds instance of {@link ReasonNotFunctionalDataBuilder} with sample data.
   */
  public ReasonNotFunctionalDataBuilder() {
    instanceNumber++;

    id = UUID.randomUUID();
    name = "name" + instanceNumber;
    code = "code" + instanceNumber;
    displayOrder = instanceNumber;
  }

  /**
   * Builds instance of {@link ReasonNotFunctional}.
   */
  public ReasonNotFunctional build() {
    ReasonNotFunctional reasonNotFunctional = new ReasonNotFunctional(code, name, displayOrder);
    reasonNotFunctional.setId(id);
    return reasonNotFunctional;
  }

  /**
   * Builds instance of {@link ReasonNotFunctional} without id.
   */
  public ReasonNotFunctional buildAsNew() {
    return this.withoutId().build();
  }

  public ReasonNotFunctionalDataBuilder withoutId() {
    this.id = null;
    return this;
  }

  public ReasonNotFunctionalDataBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public ReasonNotFunctionalDataBuilder withCode(String code) {
    this.code = code;
    return this;
  }

  public ReasonNotFunctionalDataBuilder withDisplayOrder(int displayOrder) {
    this.displayOrder = displayOrder;
    return this;
  }
}