/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */


package org.openlmis.equipment.util;

import java.util.UUID;
import org.openlmis.equipment.domain.ServiceType;

public class ServiceTypeDataBuilder {

  private static int instanceNumber = 0;

  private UUID id;

  private String name;

  private String description;

  /**
   * Builds instance of {@link ServiceTypeDataBuilder} with sample data.
   */
  public ServiceTypeDataBuilder() {
    instanceNumber++;
    id = UUID.randomUUID();
    name = "name" + instanceNumber;
    description = "sample description";
  }

  /**
   * Builds instance of {@link ServiceType}.
   */
  public ServiceType build() {
    ServiceType serviceType = new ServiceType();
    serviceType.setId(id);
    serviceType.setName(name);
    serviceType.setDescription(description);
    return serviceType;
  }


  public ServiceTypeDataBuilder withoutId() {
    this.id = null;
    return this;
  }

}
