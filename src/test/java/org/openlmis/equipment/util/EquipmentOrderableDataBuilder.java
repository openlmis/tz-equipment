/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.util;

import java.util.UUID;
import org.openlmis.equipment.domain.Equipment;
import org.openlmis.equipment.domain.EquipmentOrderable;

public class EquipmentOrderableDataBuilder {

  private UUID id;
  private Equipment equipment;
  private UUID orderableId;

  /**
   * Builds instance of {@link EquipmentOrderableDataBuilder} with sample data.
   */
  public EquipmentOrderableDataBuilder() {

    id = UUID.randomUUID();
    equipment =  new EquipmentDataBuilder().build();
    orderableId = UUID.randomUUID();

  }

  /**
   * Builds instance of {@link EquipmentOrderable}.
   */
  public EquipmentOrderable build() {
    EquipmentOrderable equipmentOrderable = EquipmentOrderable.newEquipmentOrderable(equipment,
            orderableId);
    equipmentOrderable.setId(id);
    return equipmentOrderable;
  }

  /**
   * Builds instance of {@link EquipmentOrderable} without id.
   */
  public EquipmentOrderable buildAsNew() {
    return this.withoutId().build();
  }

  public EquipmentOrderableDataBuilder withoutId() {
    this.id = null;
    return this;
  }

  public EquipmentOrderableDataBuilder withEquipment(Equipment equipment) {
    this.equipment = equipment;
    return this;
  }

  public EquipmentOrderableDataBuilder withOrderableId(UUID orderableId) {
    this.orderableId = orderableId;
    return this;
  }


}
