/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */


package org.openlmis.equipment.util;

import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.With;
import org.javers.common.collections.Sets;
import org.openlmis.equipment.domain.Contract;
import org.openlmis.equipment.domain.Equipment;
import org.openlmis.equipment.domain.ServiceType;
import org.openlmis.equipment.domain.Vendor;

@With
@AllArgsConstructor
public class ContractDataBuilder {

  private static int instanceNumber = 0;

  private UUID id;


  private LocalDate date;

  private String identifier;

  private LocalDate startDate;

  private LocalDate endDate;

  private String description;

  private String terms;

  private String coverage;

  private Vendor vendor;

  private Set<ServiceType> serviceTypes;

  private Set<Equipment> equipments;

  private Set<UUID> facilities;

  /**
   * Builds instance of {@link ContractDataBuilder} with sample data.
   */
  public ContractDataBuilder() {
    instanceNumber++;
    id = UUID.randomUUID();
    date = LocalDate.now().minusYears(instanceNumber);
    identifier = "identifier " + instanceNumber;
    startDate = LocalDate.now().minusYears(1);
    endDate = LocalDate.now().plusYears(1);
    terms = "sample terms";
    description = "sample description";
    coverage = "sample coverage";
    vendor = new VendorDataBuilder().build();
    serviceTypes = Sets.asSet(new ServiceTypeDataBuilder().build());
    equipments = Sets.asSet(new EquipmentDataBuilder().build());
    facilities = Sets.asSet(UUID.randomUUID());
  }

  /**
   * Builds instance of {@link Vendor}.
   */
  public Contract build() {
    Contract contract = new Contract();
    contract.setId(id);
    contract.setDate(date);
    contract.setIdentifier(identifier);
    contract.setStartDate(startDate);
    contract.setEndDate(endDate);
    contract.setDescription(description);
    contract.setTerms(terms);
    contract.setCoverage(coverage);
    contract.setVendor(vendor);
    contract.setServiceTypes(serviceTypes);
    contract.setEquipments(equipments);
    contract.setFacilities(facilities);
    return contract;
  }


  public ContractDataBuilder withoutId() {
    this.id = null;
    return this;
  }

}
