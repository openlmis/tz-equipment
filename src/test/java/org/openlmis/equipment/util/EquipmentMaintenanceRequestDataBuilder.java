/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.util;

import java.time.LocalDate;
import java.util.UUID;
import org.openlmis.equipment.domain.EquipmentInventory;
import org.openlmis.equipment.domain.EquipmentMaintenanceRequest;
import org.openlmis.equipment.domain.EquipmentMaintenanceRequestReason;
import org.openlmis.equipment.domain.Vendor;

@SuppressWarnings("PMD.TooManyMethods")
public class EquipmentMaintenanceRequestDataBuilder {
  private UUID id = UUID.randomUUID();
  private EquipmentInventory inventory
      = new EquipmentInventoryDataBuilder().build();
  private Vendor vendor = new VendorDataBuilder().build();
  private EquipmentMaintenanceRequestReason
      reason = new EquipmentMaintenanceRequestReasonDataBuilder().build();
  private LocalDate requestDate = LocalDate.of(2020, 10, 10);

  private LocalDate recommendedDate = LocalDate.of(2020, 10, 10);

  private String comment = "some comments";

  private Boolean resolved = false;

  private String vendorComment = "some vendor commenets";

  private LocalDate breakDownDate
      = LocalDate.of(2020, 10, 10);

  /**
   * Sets {@link EquipmentInventory}.
   */
  public EquipmentMaintenanceRequestDataBuilder withInventory(
      EquipmentInventory equipment) {
    this.inventory = equipment;
    return this;
  }

  /**
   * Sets {@link Vendor}.
   */
  public EquipmentMaintenanceRequestDataBuilder
      withVendor(Vendor vendor) {
    this.vendor = vendor;
    return this;
  }

  /**
   * Sets inventory {@link UUID}.
   */
  public EquipmentMaintenanceRequestDataBuilder
      withId(UUID id) {
    this.id = id;
    return this;
  }

  /**
   * Sets inventory {@link LocalDate}.
   */
  public EquipmentMaintenanceRequestDataBuilder withRecommendedDate(
      LocalDate recommendedDate) {
    this.recommendedDate = recommendedDate;
    return this;
  }

  /**
   * Sets program {@link String}.
   */
  public EquipmentMaintenanceRequestDataBuilder withComment(String comment) {
    this.comment = comment;
    return this;
  }

  /**
   * Sets facility {@link Boolean}.
   */
  public EquipmentMaintenanceRequestDataBuilder
      withResolved(Boolean resolved) {
    this.resolved = resolved;
    return this;
  }

  /**
   * Sets {@Link String}.
   */
  public EquipmentMaintenanceRequestDataBuilder
      withVendorComment(String vendorComment) {
    this.vendorComment = vendorComment;
    return this;
  }

  /**
   * Sets inventory {@link LocalDate}.
   */
  public EquipmentMaintenanceRequestDataBuilder
      withBreakDownDate(LocalDate breakDownDate) {
    this.breakDownDate = breakDownDate;
    return this;
  }

  /**
   * Sets {@link Vendor}.
   */
  public EquipmentMaintenanceRequestDataBuilder
      withReason(EquipmentMaintenanceRequestReason reason) {
    this.reason = reason;
    return this;
  }

  /**
   * Sets inventory {@link LocalDate}.
   */
  public EquipmentMaintenanceRequestDataBuilder
      withRequestDate(LocalDate requestDate) {
    this.requestDate = requestDate;
    return this;
  }

  /**
   * Builds instance of {@link EquipmentMaintenanceRequest}.
   */
  public EquipmentMaintenanceRequest build() {
    EquipmentMaintenanceRequest equipmentInventory =
        new EquipmentMaintenanceRequest(
            inventory, vendor,
            requestDate, recommendedDate, comment, resolved,
            vendorComment, breakDownDate, reason
        );
    equipmentInventory.setId(id);
    return equipmentInventory;
  }

}
