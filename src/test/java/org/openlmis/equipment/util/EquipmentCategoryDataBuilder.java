/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */


package org.openlmis.equipment.util;

import com.google.common.collect.Sets;
import java.util.Set;
import java.util.UUID;
import org.openlmis.equipment.domain.Discipline;
import org.openlmis.equipment.domain.EquipmentCategory;
import org.openlmis.equipment.domain.EquipmentType;

public class EquipmentCategoryDataBuilder {
  private static int instanceNumber = 0;
  private UUID id;
  private String name;
  private String code;
  private Boolean enabled;
  private Integer displayOrder;

  private EquipmentType equipmentType;

  private Set<Discipline> disciplines;

  /**
   * Builds instance of {@link EquipmentCategoryDataBuilder} with sample data.
   */
  public EquipmentCategoryDataBuilder() {
    instanceNumber++;

    id = UUID.randomUUID();
    name = "name" + instanceNumber;
    code = "code" + instanceNumber;
    displayOrder = 1;
    enabled = true;
    equipmentType = new EquipmentTypeDataBuilder().build();
    disciplines = Sets.newHashSet(new DisciplineDataBuilder().build());

  }

  /**
   * Builds instance of {@link EquipmentCategory}.
   */
  public EquipmentCategory build() {
    EquipmentCategory equipmentCategory = EquipmentCategory.newEquipmentCategory(name, code,
        displayOrder, enabled, equipmentType, disciplines);
    equipmentCategory.setId(id);
    return equipmentCategory;
  }

  /**
   * Builds instance of {@link EquipmentCategory} without id.
   */
  public EquipmentCategory buildAsNew() {
    return this.withoutId().build();
  }

  public EquipmentCategoryDataBuilder withoutId() {
    this.id = null;
    return this;
  }

  public EquipmentCategoryDataBuilder withDisplayOrder(Integer displayOrder) {
    this.displayOrder = displayOrder;
    return this;
  }

  public EquipmentCategoryDataBuilder withName(String name) {
    this.name = name;
    return this;
  }

  public EquipmentCategoryDataBuilder withCode(String code) {
    this.code = code;
    return this;
  }

  public EquipmentCategoryDataBuilder withEnabled(Boolean enabled) {
    this.enabled = enabled;
    return this;
  }

  public EquipmentCategoryDataBuilder withDisciplines(Set<Discipline> disciplines) {
    this.disciplines = disciplines;
    return this;
  }

  public EquipmentCategoryDataBuilder withEquipmentType(EquipmentType equipmentType) {
    this.equipmentType = equipmentType;
    return this;
  }
}
