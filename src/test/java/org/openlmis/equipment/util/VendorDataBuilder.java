/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */


package org.openlmis.equipment.util;

import java.util.Set;
import java.util.UUID;
import org.javers.common.collections.Sets;
import org.openlmis.equipment.domain.Vendor;

public class VendorDataBuilder {

  private static int instanceNumber = 0;

  private UUID id;

  private String name;

  private String website;

  private String contactPerson;

  private String primaryPhone;

  private String email;

  private String description;

  private String specialization;

  private String geographicalCoverage;

  private Set<UUID> associatedUsers;

  /**
   * Builds instance of {@link VendorDataBuilder} with sample data.
   */
  public VendorDataBuilder() {
    instanceNumber++;
    id = UUID.randomUUID();
    name = "name" + instanceNumber;
    website = "www.example.com";
    contactPerson = "John Doe";
    primaryPhone = "+255717050609";
    email = "test@example.com";
    description = "sample description";
    specialization = "sample specialization";
    geographicalCoverage = "sample geographical coverage";
    associatedUsers = Sets.asSet(UUID.randomUUID());
  }

  /**
   * Builds instance of {@link Vendor}.
   */
  public Vendor build() {
    Vendor vendor = new Vendor();
    vendor.setId(id);
    vendor.setName(name);
    vendor.setWebsite(website);
    vendor.setContactPerson(contactPerson);
    vendor.setPrimaryPhone(primaryPhone);
    vendor.setEmail(email);
    vendor.setDescription(description);
    vendor.setSpecialization(specialization);
    vendor.setGeographicalCoverage(geographicalCoverage);
    vendor.setAssociatedUsers(associatedUsers);
    return vendor;
  }


  public VendorDataBuilder withoutId() {
    this.id = null;
    return this;
  }

}
