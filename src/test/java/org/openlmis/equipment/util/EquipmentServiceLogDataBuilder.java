/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.util;

import java.time.LocalDate;
import java.util.UUID;
import org.openlmis.equipment.domain.EquipmentMaintenanceRequest;
import org.openlmis.equipment.domain.EquipmentServiceLog;

public class EquipmentServiceLogDataBuilder {
  private static int instanceNumber = 0;

  private UUID id;
  private EquipmentMaintenanceRequest request;

  private LocalDate maintenanceDate;

  private String servicePerformed;

  private String finding;

  private String recommendation;

  private LocalDate nextVisitDate;

  private Boolean approved;

  /**
   * Builds instance of {@link EquipmentServiceLogDataBuilder} with sample data.
   */
  public EquipmentServiceLogDataBuilder() {
    instanceNumber++;

    id = UUID.randomUUID();
    request = new EquipmentMaintenanceRequestDataBuilder().build();
    request.setResolved(true);
    maintenanceDate = LocalDate.of(2020, 10, 10);
    servicePerformed = "service performed" + instanceNumber;
    finding = "device fault" + instanceNumber;
    recommendation = "some recommendations";
    nextVisitDate = LocalDate.of(2022, 10, 10);
  }

  /**
   * Builds instance of {@link EquipmentServiceLog}.
   */
  public EquipmentServiceLog build() {
    EquipmentServiceLog log = new EquipmentServiceLog(
        request, maintenanceDate,
        servicePerformed, finding,
        recommendation, nextVisitDate, approved);
    log.setId(id);
    return log;
  }

  /**
   * Builds instance of {@link EquipmentServiceLog} without id.
   */
  public EquipmentServiceLog buildAsNew() {
    return this.withoutId().build();
  }

  public EquipmentServiceLogDataBuilder withoutId() {
    this.id = null;
    return this;
  }

  /**
   * Sets {@link EquipmentMaintenanceRequest}.
   */
  public EquipmentServiceLogDataBuilder withRequest(EquipmentMaintenanceRequest request) {
    this.request = request;
    return this;
  }

  /**
   * Sets {@link LocalDate}.
   */
  public EquipmentServiceLogDataBuilder withMaintenanceDate(LocalDate maintenanceDate) {
    this.maintenanceDate = maintenanceDate;
    return this;
  }

  /**
   * Sets {@link String}.
   */
  public EquipmentServiceLogDataBuilder withServicePerformed(String servicePerformed) {
    this.servicePerformed = servicePerformed;
    return this;
  }

  /**
   * Sets {@link String}.
   */
  public EquipmentServiceLogDataBuilder withServiceFinding(String finding) {
    this.finding = finding;
    return this;
  }

  /**
   * Sets {@link String}.
   */
  public EquipmentServiceLogDataBuilder withRecommendation(String recommendation) {
    this.recommendation = recommendation;
    return this;
  }

  /**
   * Sets {@link LocalDate}.
   */
  public EquipmentServiceLogDataBuilder withNextVisitDate(LocalDate nextVisitDate) {
    this.nextVisitDate = nextVisitDate;
    return this;
  }

  /**
   * Sets facility {@link Boolean}.
   */
  public EquipmentServiceLogDataBuilder withApproved(Boolean approved) {
    this.approved = approved;
    return this;
  }
}
