/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.service;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.openlmis.equipment.service.PermissionService.EQUIPMENT_INVENTORY_VIEW;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.apache.commons.collections4.SetUtils;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.openlmis.equipment.domain.EquipmentInventory;
import org.openlmis.equipment.domain.EquipmentOrderable;
import org.openlmis.equipment.dto.PermissionStringDto;
import org.openlmis.equipment.exception.PermissionMessageException;
import org.openlmis.equipment.repository.EquipmentInventoryRepository;
import org.openlmis.equipment.repository.EquipmentOrderableRepository;
import org.openlmis.equipment.repository.custom.EquipmentInventoryRepositoryCustom.RepositorySearchParams;
import org.openlmis.equipment.service.EquipmentInventoryService.SearchParams;
import org.openlmis.equipment.util.EquipmentInventoryDataBuilder;
import org.openlmis.equipment.util.EquipmentOrderableDataBuilder;
import org.openlmis.equipment.util.PaginationUtil;
import org.openlmis.equipment.web.EquipmentInventorySearchParams;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public class EquipmentInventoryServiceTest {

  @Mock
  private EquipmentInventoryRepository repository;

  @Mock
  private EquipmentOrderableRepository equipmentOrderableRepository;

  @Mock
  private PermissionService permissionService;

  @Mock
  private PageRequest pageable;

  @InjectMocks
  private EquipmentInventoryService service;

  private EquipmentInventory equipmentInventory;

  private EquipmentOrderable equipmentOrderable;

  private EquipmentInventorySearchParams params;

  private Set<PermissionStringDto> permissionStrings;

  private Page<EquipmentInventory> expectedPage;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    permissionStrings = new HashSet<>();

    params = Mockito.mock(EquipmentInventorySearchParams.class);
    pageable = PageRequest.of(0, 1);

    PermissionStrings.Handler handler = mock(PermissionStrings.Handler.class);
    when(handler.get()).thenReturn(permissionStrings);
    when(permissionService.getPermissionStrings(any())).thenReturn(handler);

    equipmentInventory = new EquipmentInventoryDataBuilder().build();
    expectedPage = PaginationUtil.getPage(singletonList(equipmentInventory), pageable, 1);

    equipmentOrderable = new EquipmentOrderableDataBuilder().build();

  }

  @Test
  public void searchShouldReturnPageForValidParamsOnSearchEquipmentInventory() {
    UUID facilityId = UUID.randomUUID();

    UUID programId = UUID.randomUUID();
    addPermission(EQUIPMENT_INVENTORY_VIEW, facilityId, programId);

    SearchParams params = Mockito.mock(SearchParams.class);

    when(repository.search(
        any(RepositorySearchParams.class),
        eq(pageable)
    )).thenReturn(expectedPage);

    Page<EquipmentInventory> page = service.searchEquipmentInventory(
        facilityId, programId, params, pageable
    );

    assertEquals(expectedPage, page);
  }

  @Test
  public void searchShouldReturnOrderableEquipmentMapForValidParamsOnSearchEquipmentInventory() {
    UUID facilityId = UUID.randomUUID();

    UUID programId = UUID.randomUUID();
    UUID orderableId = UUID.randomUUID();

    addPermission(EQUIPMENT_INVENTORY_VIEW, facilityId, programId);

    SearchParams params = Mockito.mock(SearchParams.class);
    when(params.getOrderableIds())
        .thenReturn(SetUtils.hashSet(orderableId));

    when(repository.search(
        any(RepositorySearchParams.class),
        any()
    )).thenReturn(expectedPage);

    when(equipmentOrderableRepository.findAllByOrderableId(eq(orderableId)))
        .thenReturn(Arrays.asList(equipmentOrderable));

    Map<UUID, List<EquipmentInventory>> orderableEquipments =
        service.searchOrderableEquipmentInventories(facilityId, programId, params);

    Assertions.assertThat(orderableEquipments)
        .containsKey(orderableId);
    Assertions.assertThat(orderableEquipments)
        .containsValues(expectedPage.getContent());
  }

  @Test(expected = Exception.class)
  public void shouldFailSearchingIfUserHasNoRightsOnSearchEquipmentInventory() {
    UUID facilityId = UUID.randomUUID();
    UUID programId = UUID.randomUUID();

    doThrow(mock(PermissionMessageException.class))
        .when(permissionService)
        .canViewInventory(facilityId, programId);
    SearchParams params = Mockito.mock(SearchParams.class);

    service.searchEquipmentInventory(facilityId, programId, params, pageable);
  }


  @Test(expected = Exception.class)
  public void shouldFailSearchingIfUserHasNoCreateRequisitionRightsOnSearchOrderableEquipments() {
    UUID facilityId = UUID.randomUUID();
    UUID programId = UUID.randomUUID();

    doThrow(mock(PermissionMessageException.class))
        .when(permissionService)
        .canSubmitRequisition(facilityId, programId);
    SearchParams params = Mockito.mock(SearchParams.class);
    service.searchOrderableEquipmentInventories(facilityId, programId, params);
  }

  @Test(expected = Exception.class)
  public void searchShouldThrowExceptionForMissingPageableOnSearchEquipmentInventory() {
    UUID facilityId = UUID.randomUUID();
    UUID programId = UUID.randomUUID();
    when(repository.search(
        any(),
        eq(null)
    )).thenThrow(new Exception());

    service.searchEquipmentInventory(facilityId, programId, params, null);
  }

  @Test(expected = Exception.class)
  public void shouldThrowExceptionForMissingSearchParamsOnSearchEquipmentInventory() {
    UUID facilityId = UUID.randomUUID();
    UUID programId = UUID.randomUUID();

    service.searchEquipmentInventory(facilityId, programId, null, pageable);
  }

  @Test(expected = Exception.class)
  public void shouldThrowExceptionForMissingOrderableIdOnSearchOrderableEquipments() {
    UUID facilityId = UUID.randomUUID();
    UUID programId = UUID.randomUUID();

    SearchParams params = Mockito.mock(SearchParams.class);

    service.searchOrderableEquipmentInventories(facilityId, programId, params);
  }

  @Test(expected = Exception.class)
  public void shouldThrowExceptionForMissingFacilityIdAndProgramIdOnSearchEquipmentInventory() {
    service.searchEquipmentInventory(null, null, params, pageable);
  }

  @Test(expected = Exception.class)
  public void shouldThrowExceptionForMissingFacilityIdAndProgramIdOnSearchOrderableEquipments() {
    service.searchOrderableEquipmentInventories(null, null, params);
  }

  private void addPermission(String name, UUID facilityId, UUID programId) {
    PermissionStringDto permission = PermissionStringDto.create(
        name, facilityId, programId
    );
    permissionStrings.add(permission);
  }

}
