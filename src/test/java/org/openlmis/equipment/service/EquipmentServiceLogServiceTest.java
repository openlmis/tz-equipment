/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.openlmis.equipment.domain.EquipmentMaintenanceRequest;
import org.openlmis.equipment.domain.EquipmentServiceLog;
import org.openlmis.equipment.dto.EquipmentServiceLogDto;
import org.openlmis.equipment.repository.EquipmentMaintenanceRequestRepository;
import org.openlmis.equipment.repository.EquipmentServiceLogRepository;
import org.openlmis.equipment.util.EquipmentServiceLogDataBuilder;

@SuppressWarnings({"PMD.UnusedPrivateField", "PMD.TooManyMethods"})
public class EquipmentServiceLogServiceTest {

  @Mock
  private EquipmentServiceLogRepository equipmentServiceLogRepository;

  @Mock
  private EquipmentMaintenanceRequestRepository
      equipmentMaintenanceRequestRepository;
  @InjectMocks
  private EquipmentServiceLogService equipmentServiceLogService;

  private UUID equipmentId;

  private EquipmentServiceLogDto equipmentServiceLogDto;

  private EquipmentServiceLog serviceLog;

  private EquipmentMaintenanceRequest request;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    serviceLog = new EquipmentServiceLogDataBuilder().build();

    equipmentId = serviceLog.getId();
    request = serviceLog.getRequest();

    equipmentServiceLogDto
        = EquipmentServiceLogDto.newInstance(serviceLog);

    when(equipmentMaintenanceRequestRepository
        .findById(any())).thenReturn(Optional.of(request));
  }

  @Test
  public void shouldSaveNewEquipmentLogSuccessfulOnCreate() {
    when(equipmentServiceLogRepository
        .save(any(EquipmentServiceLog.class))).thenAnswer(invocation -> {
          EquipmentServiceLog equipment = invocation.getArgument(0);
          equipment.setId(equipmentId);
          return equipment;
        });


    EquipmentServiceLog savedEquipment = equipmentServiceLogService
        .create(equipmentServiceLogDto);
    verify(equipmentServiceLogRepository).save(any(EquipmentServiceLog.class));

    assertThat(savedEquipment).isEqualTo(serviceLog);
  }

  @Test
  public void shouldSaveEquipmentServiceLogSuccessfulOnUpdate() {
    when(equipmentServiceLogRepository
        .save(any(EquipmentServiceLog.class)))
        .then(AdditionalAnswers.returnsFirstArg());
    when(equipmentServiceLogRepository
        .existsById(serviceLog.getId())).thenReturn(true);

    EquipmentServiceLog savedEquipment = equipmentServiceLogService
        .update(
            serviceLog.getId(),
            equipmentServiceLogDto
        );
    verify(equipmentServiceLogRepository).save(any(EquipmentServiceLog.class));

    assertThat(savedEquipment).isEqualTo(serviceLog);
  }

  @Test
  public void shouldCreateWhenUpdatingWithIdThatDoesNotExists() {
    when(equipmentServiceLogRepository.findById(serviceLog.getId()))
        .thenReturn(Optional.empty());
    when(equipmentServiceLogRepository.save(any(EquipmentServiceLog.class)))
        .thenReturn(serviceLog);

    EquipmentServiceLog savedEquipment = equipmentServiceLogService.update(
        serviceLog.getId(),
        equipmentServiceLogDto
    );

    verify(equipmentServiceLogRepository).save(any(EquipmentServiceLog.class));
    assertThat(savedEquipment).isEqualTo(serviceLog);
  }

}
