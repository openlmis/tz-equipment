/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.openlmis.equipment.domain.ReasonNotFunctional;
import org.openlmis.equipment.dto.ReasonNotFunctionalDto;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.repository.ReasonNotFunctionalRepository;
import org.openlmis.equipment.service.ReasonNotFunctionalService.SearchParams;
import org.openlmis.equipment.util.PaginationUtil;
import org.openlmis.equipment.util.ReasonNotFunctionalDataBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@SuppressWarnings({"PMD.UnusedPrivateField", "PMD.TooManyMethods"})
public class ReasonNotFunctionalServiceTest {

  @Mock
  private ReasonNotFunctionalRepository reasonNotFunctionalRepository;
  @Mock
  private PermissionService permissionService;

  @InjectMocks
  private ReasonNotFunctionalService reasonNotFunctionalService;

  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  private ReasonNotFunctional reasonNotFunctional;
  private ReasonNotFunctionalDto reasonNotFunctionalDto;
  private UUID reasonNotFunctionalId;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    reasonNotFunctional = new ReasonNotFunctionalDataBuilder().build();
    reasonNotFunctionalId = reasonNotFunctional.getId();
    reasonNotFunctionalDto = ReasonNotFunctionalDto.newInstance(reasonNotFunctional);
  }

  @Test
  public void shouldSaveNewReasonSuccessfulOnCreate() {
    when(reasonNotFunctionalRepository.save(any(ReasonNotFunctional.class)))
        .thenAnswer(invocation -> {
          ReasonNotFunctional reason = invocation.getArgument(0);
          reason.setId(reasonNotFunctionalId);
          return reason;
        });

    ReasonNotFunctional savedReason = reasonNotFunctionalService.create(reasonNotFunctionalDto);
    verify(reasonNotFunctionalRepository).save(any(ReasonNotFunctional.class));

    assertThat(savedReason).isEqualTo(reasonNotFunctional);
  }

  @Test
  public void shouldSaveReasonSuccessfulOnUpdate() {
    when(reasonNotFunctionalRepository.save(any(ReasonNotFunctional.class)))
        .then(AdditionalAnswers.returnsFirstArg());
    when(reasonNotFunctionalRepository.existsById(reasonNotFunctional.getId())).thenReturn(true);

    ReasonNotFunctional savedReason = reasonNotFunctionalService.update(
        reasonNotFunctional.getId(),
        reasonNotFunctionalDto
    );
    verify(reasonNotFunctionalRepository).save(any(ReasonNotFunctional.class));

    assertThat(savedReason).isEqualTo(reasonNotFunctional);
  }

  @Test
  public void shouldCreateWhenUpdatingWithIdThatDoesNotExists() {
    when(reasonNotFunctionalRepository.findById(reasonNotFunctional.getId()))
        .thenReturn(Optional.empty());
    when(reasonNotFunctionalRepository.save(any(ReasonNotFunctional.class)))
        .thenReturn(reasonNotFunctional);

    ReasonNotFunctional savedReason = reasonNotFunctionalService.update(
        reasonNotFunctional.getId(),
        reasonNotFunctionalDto
    );

    verify(reasonNotFunctionalRepository).save(any(ReasonNotFunctional.class));
    assertThat(savedReason).isEqualTo(reasonNotFunctional);
  }

  @Test
  public void shouldGetReasonNotFunctionalIfExists() {
    when(reasonNotFunctionalRepository.findById(reasonNotFunctionalId))
        .thenReturn(Optional.of(reasonNotFunctional));
    ReasonNotFunctional foundReason = reasonNotFunctionalService.getReasonNotFunctional(
        reasonNotFunctionalId
    );

    verify(reasonNotFunctionalRepository).findById(reasonNotFunctional.getId());
    assertThat(foundReason).isEqualTo(reasonNotFunctional);
  }

  @Test(expected = NotFoundException.class)
  public void shouldFailGettingReasonNotFunctionalIfDoesNotExists() {
    when(reasonNotFunctionalRepository.findById(reasonNotFunctionalId)).thenReturn(
        Optional.empty()
    );
    reasonNotFunctionalService.getReasonNotFunctional(reasonNotFunctionalId);
  }

  @Test
  public void shouldFindAllReasonNotFunctional() {
    List<ReasonNotFunctional> expectedReasons = Lists.newArrayList(reasonNotFunctional);
    when(reasonNotFunctionalRepository.findAll(pageable))
        .thenReturn(PaginationUtil.getPage(expectedReasons, pageable));
    Page<ReasonNotFunctional> resultPage = reasonNotFunctionalService.findAll(pageable);

    verify(reasonNotFunctionalRepository).findAll(pageable);
    assertThat(expectedReasons).isEqualTo(resultPage.getContent());
  }

  @Test
  public void shouldSearchReasonNotFunctional() {
    List<ReasonNotFunctional> expectedReasons = Lists.newArrayList(reasonNotFunctional);
    when(reasonNotFunctionalRepository.search(any(), eq(pageable)))
        .thenReturn(PaginationUtil.getPage(expectedReasons, pageable));

    SearchParams searchParams = Mockito.mock(SearchParams.class);
    Page<ReasonNotFunctional> resultPage = reasonNotFunctionalService.search(
        searchParams,
        pageable
    );

    verify(reasonNotFunctionalRepository).search(searchParams, pageable);
    assertThat(expectedReasons).isEqualTo(resultPage.getContent());
  }

}
