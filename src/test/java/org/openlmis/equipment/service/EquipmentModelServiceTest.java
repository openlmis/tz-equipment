/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.openlmis.equipment.domain.EquipmentModel;
import org.openlmis.equipment.domain.EquipmentType;
import org.openlmis.equipment.dto.EquipmentModelDto;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.repository.EquipmentModelRepository;
import org.openlmis.equipment.repository.EquipmentTypeRepository;
import org.openlmis.equipment.service.EquipmentModelService.SearchParams;
import org.openlmis.equipment.util.EquipmentModelDataBuilder;
import org.openlmis.equipment.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@SuppressWarnings({"PMD.UnusedPrivateField", "PMD.TooManyMethods"})
public class EquipmentModelServiceTest {

  @Mock
  private EquipmentModelRepository equipmentModelRepository;
  @Mock
  private EquipmentTypeRepository equipmentTypeRepository;
  @Mock
  private PermissionService permissionService;

  @InjectMocks
  private EquipmentModelService equipmentModelService;

  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  private EquipmentModel equipmentModel;
  private EquipmentType equipmentType;
  private EquipmentModelDto equipmentModelDto;
  private UUID equipmentModelId;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    equipmentModel = new EquipmentModelDataBuilder().build();
    equipmentModelId = equipmentModel.getId();
    equipmentType = equipmentModel.getEquipmentType();
    equipmentModelDto = EquipmentModelDto.newInstance(equipmentModel);
  }

  @Test
  public void shouldSaveNewModelSuccessfulOnCreate() {
    when(equipmentModelRepository.save(any(EquipmentModel.class))).thenAnswer(invocation -> {
      EquipmentModel model = invocation.getArgument(0);
      model.setId(equipmentModelId);
      return model;
    });

    when(equipmentTypeRepository.findById(any())).thenReturn(Optional.of(equipmentType));

    EquipmentModel savedModel = equipmentModelService.create(equipmentModelDto);
    verify(equipmentModelRepository).save(any(EquipmentModel.class));

    assertThat(savedModel).isEqualTo(equipmentModel);
  }

  @Test
  public void shouldSaveModelSuccessfulOnUpdate() {
    when(equipmentModelRepository.save(any(EquipmentModel.class)))
        .then(AdditionalAnswers.returnsFirstArg());
    when(equipmentTypeRepository.findById(any())).thenReturn(Optional.of(equipmentType));
    when(equipmentModelRepository.existsById(equipmentModel.getId())).thenReturn(true);

    EquipmentModel savedModel = equipmentModelService.update(
        equipmentModel.getId(),
        equipmentModelDto
    );
    verify(equipmentModelRepository).save(any(EquipmentModel.class));

    assertThat(savedModel).isEqualTo(equipmentModel);
  }

  @Test
  public void shouldCreateWhenUpdatingWithIdThatDoesNotExists() {
    when(equipmentModelRepository.findById(equipmentModel.getId()))
        .thenReturn(Optional.empty());
    when(equipmentTypeRepository.findById(any()))
        .thenReturn(Optional.of(equipmentType));
    when(equipmentModelRepository.save(any(EquipmentModel.class)))
        .thenReturn(equipmentModel);

    EquipmentModel savedModel = equipmentModelService.update(
        equipmentModel.getId(),
        equipmentModelDto
    );

    verify(equipmentModelRepository).save(any(EquipmentModel.class));
    assertThat(savedModel).isEqualTo(equipmentModel);
  }

  @Test
  public void shouldGetEquipmentModelIfExists() {
    when(equipmentModelRepository.findById(equipmentModelId))
        .thenReturn(Optional.of(equipmentModel));
    EquipmentModel foundModel = equipmentModelService.getEquipmentModel(equipmentModelId);

    verify(equipmentModelRepository).findById(equipmentModel.getId());
    assertThat(foundModel).isEqualTo(equipmentModel);
  }

  @Test(expected = NotFoundException.class)
  public void shouldFailGettingEquipmentModelIfDoesNotExists() {
    when(equipmentModelRepository.findById(equipmentModelId)).thenReturn(Optional.empty());
    equipmentModelService.getEquipmentModel(equipmentModelId);
  }

  @Test
  public void shouldFindAllEquipmentModels() {
    List<EquipmentModel> expectedModels = Lists.newArrayList(equipmentModel);
    when(equipmentModelRepository.findAll(pageable))
        .thenReturn(PaginationUtil.getPage(expectedModels, pageable));
    Page<EquipmentModel> resultPage = equipmentModelService.findAll(pageable);

    verify(equipmentModelRepository).findAll(pageable);
    assertThat(expectedModels).isEqualTo(resultPage.getContent());
  }

  @Test
  public void shouldSearchEquipmentModels() {
    List<EquipmentModel> expectedModels = Lists.newArrayList(equipmentModel);
    when(equipmentModelRepository.search(any(), eq(pageable)))
        .thenReturn(PaginationUtil.getPage(expectedModels, pageable));

    SearchParams searchParams = Mockito.mock(SearchParams.class);
    Page<EquipmentModel> resultPage = equipmentModelService.search(
        searchParams,
        pageable
    );

    verify(equipmentModelRepository).search(searchParams, pageable);
    assertThat(expectedModels).isEqualTo(resultPage.getContent());
  }

}
