/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.service.referencedata;

import static org.codehaus.groovy.runtime.InvokerHelper.asList;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.openlmis.equipment.dto.RightDto;
import org.openlmis.equipment.dto.RightType;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestClientException;

public class RightReferenceDataServiceTest extends BaseReferenceDataServiceTest<RightDto> {

  private static final String RIGHT_NAME = "RIGHT_NAME";

  @InjectMocks
  private RightReferenceDataService service;

  @Override
  protected RightReferenceDataService getService() {
    return service;
  }

  @Override
  protected RightDto generateInstance() {
    RightDto dto = new RightDto();
    dto.setId(UUID.randomUUID());
    dto.setName(RIGHT_NAME);

    return dto;
  }

  @Test
  public void shouldFindRightByName() throws Exception {
    // given
    RightDto instance = generateInstance();

    // when
    mockArrayRequest(HttpMethod.GET, getArrayResultClass(service));
    mockArrayResponse(response -> when(response.getBody()).thenReturn(new Object[]{instance}));

    RightDto found = service.findRight(RIGHT_NAME);

    // then
    assertThat(found, equalTo(instance));

    URI uri = getUri();
    String url = getRequestUrl(service, "search?name=" + RIGHT_NAME);
    assertThat(uri.toString(), equalTo(url));

    HttpEntity entity = getEntity();
    assertThat(entity.getBody(), is(nullValue()));
  }

  @Test
  public void shouldReturnFirstIfThereIsMoreElements() throws Exception {
    // given
    RightDto instance1 = generateInstance();
    RightDto instance2 = generateInstance();

    // when
    mockArrayRequest(HttpMethod.GET, getArrayResultClass(service));
    mockArrayResponse(response ->
        when(response.getBody()).thenReturn(new Object[]{instance1, instance2}));

    RightDto found = service.findRight(RIGHT_NAME);

    // then
    assertThat(found, equalTo(instance1));

    URI uri = getUri();
    String url = getRequestUrl(service, "search?name=" + RIGHT_NAME);
    assertThat(uri.toString(), equalTo(url));

    HttpEntity entity = getEntity();
    assertThat(entity.getBody(), is(nullValue()));
  }

  @Test
  public void shouldReturnNullIfRightCouldNotBeFound() throws Exception {
    // when
    mockArrayRequest(HttpMethod.GET, getArrayResultClass(service));
    mockArrayResponse(response -> when(response.getBody()).thenReturn(new Object[0]));

    RightDto found = service.findRight(RIGHT_NAME);

    // then
    assertThat(found, is(nullValue()));

    URI uri = getUri();
    String url = getRequestUrl(service, "search?name=" + RIGHT_NAME);
    assertThat(uri.toString(), equalTo(url));

    HttpEntity entity = getEntity();
    assertThat(entity.getBody(), is(nullValue()));
  }

  @Test
  public void registerShouldHttpPutAndReturnTrueWhenRightDoesNotExist() throws Exception {
    // given
    mockArrayRequest(HttpMethod.GET, getArrayResultClass(service));
    mockArrayResponse(response -> when(response.getBody()).thenReturn(new Object[0]));

    // when
    Boolean result = service.register(RIGHT_NAME, RightType.SUPERVISION);

    // then
    assertThat(result, is(true));
    // verify that it has called put.
    verify(restClient, atLeastOnce())
        .exchange(uriCaptor.capture(), eq(HttpMethod.PUT), entityCaptor.capture(),
            eq(service.getResultClass()
            ));
  }

  @Test
  public void registerShouldHttpPutAndReturnFalseWhenPutThrowsError() throws Exception {
    // given
    Exception exception = new RestClientException("Not Found");
    mockArrayRequest(HttpMethod.GET, getArrayResultClass(service));
    mockArrayResponse(response -> when(response.getBody()).thenReturn(new Object[0]));
    when(restClient
        .exchange(any(URI.class), eq(HttpMethod.PUT), any(), eq(service.getResultClass())))
        .thenThrow(exception);

    // when
    Boolean result = service.register(RIGHT_NAME, RightType.SUPERVISION);

    // then
    assertThat(result, is(false));
    // verify that it has called put.
    verify(restClient, atLeastOnce())
        .exchange(uriCaptor.capture(), eq(HttpMethod.PUT), entityCaptor.capture(),
            eq(service.getResultClass()
            ));
  }

  @Test
  public void registerShouldNotPostRightIfItAlreadyExists() throws Exception {
    // given
    RightDto right = generateInstance();
    List<RightDto> list = asList(right);
    mockArrayRequest(HttpMethod.GET, getArrayResultClass(service));
    mockArrayResponse(response -> when(response.getBody()).thenReturn(list.toArray()));

    // when
    Boolean result = service.register(RIGHT_NAME, RightType.SUPERVISION);

    // then
    assertThat(result, is(true));

    verify(restClient, atLeastOnce()).exchange(
        uriCaptor.capture(), eq(HttpMethod.GET), any(),
        eq(service.getArrayResultClass())
    );

    verify(restClient, never())
        .exchange(uriCaptor.capture(), eq(HttpMethod.PUT), entityCaptor.capture(),
            eq(service.getResultClass()
            ));

  }
}