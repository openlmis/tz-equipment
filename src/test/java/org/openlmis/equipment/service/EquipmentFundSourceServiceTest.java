/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.openlmis.equipment.domain.EquipmentFundSource;
import org.openlmis.equipment.dto.EquipmentFundSourceDto;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.repository.EquipmentFundSourceRepository;
import org.openlmis.equipment.util.EquipmentFundSourceDataBuilder;
import org.openlmis.equipment.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@SuppressWarnings({"PMD.UnusedPrivateField", "PMD.TooManyMethods"})
public class EquipmentFundSourceServiceTest {
  public static final String TEST_NAME_QUERY = "TEST";
  public static final String TEST_CODE_QUERY = "TEST";

  @Mock
  private EquipmentFundSourceRepository fundSourceRepository;
  @Mock
  private PermissionService permissionService;
  @InjectMocks
  private EquipmentFundSourceService equipmentFundSourceService;

  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  private EquipmentFundSource equipmentFundSource;
  private EquipmentFundSourceDto equipmentFundSourceDto;
  private UUID equipmentFundSourceId;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    equipmentFundSource = new EquipmentFundSourceDataBuilder().build();
    equipmentFundSourceId = equipmentFundSource.getId();
    equipmentFundSourceDto = EquipmentFundSourceDto.newInstance(equipmentFundSource);
  }

  @Test
  public void shouldSaveNewFundSourceSuccessfulOnCreate() {
    when(fundSourceRepository.save(any(EquipmentFundSource.class))).thenAnswer(
        invocation -> {
          EquipmentFundSource fundSource = invocation.getArgument(0);
          fundSource.setId(equipmentFundSourceId);
          return fundSource;
        }
    );


    EquipmentFundSource savedFundSource = equipmentFundSourceService.create(equipmentFundSourceDto);
    verify(fundSourceRepository).save(any(EquipmentFundSource.class));

    assertThat(savedFundSource).isEqualTo(equipmentFundSource);
  }

  @Test
  public void shouldSaveFundSourceSuccessfulOnUpdate() {
    when(fundSourceRepository.save(any(EquipmentFundSource.class)))
        .then(AdditionalAnswers.returnsFirstArg());
    when(fundSourceRepository.existsById(equipmentFundSource.getId())).thenReturn(true);

    EquipmentFundSource savedFundSource = equipmentFundSourceService.update(
        equipmentFundSource.getId(),
        equipmentFundSourceDto
    );
    verify(fundSourceRepository).save(any(EquipmentFundSource.class));

    assertThat(savedFundSource).isEqualTo(equipmentFundSource);
  }

  @Test
  public void shouldCreateWhenUpdatingWithIdThatDoesNotExists() {
    when(fundSourceRepository.findById(equipmentFundSource.getId()))
        .thenReturn(Optional.empty());
    when(fundSourceRepository.save(any(EquipmentFundSource.class)))
        .thenReturn(equipmentFundSource);

    EquipmentFundSource savedFundSource = equipmentFundSourceService.update(
        equipmentFundSource.getId(),
        equipmentFundSourceDto
    );

    verify(fundSourceRepository).save(any(EquipmentFundSource.class));
    assertThat(savedFundSource).isEqualTo(equipmentFundSource);
  }

  @Test
  public void shouldGetEquipmentFundSourceIfExists() {
    when(fundSourceRepository.findById(equipmentFundSourceId))
        .thenReturn(Optional.of(equipmentFundSource));
    EquipmentFundSource foundFundSource = equipmentFundSourceService.getEquipmentFundSource(
        equipmentFundSourceId
    );

    verify(fundSourceRepository).findById(equipmentFundSource.getId());
    assertThat(foundFundSource).isEqualTo(equipmentFundSource);
  }

  @Test(expected = NotFoundException.class)
  public void shouldFailGettingEquipmentFundSourceIfDoesNotExists() {
    when(fundSourceRepository.findById(equipmentFundSourceId))
        .thenReturn(Optional.empty());
    equipmentFundSourceService.getEquipmentFundSource(equipmentFundSourceId);
  }

  @Test
  public void shouldSearchEquipmentFundSourcesByBothNameAndCodeWhenProvided() {
    List<EquipmentFundSource> expectedFundSources = Lists.newArrayList(equipmentFundSource);
    when(fundSourceRepository.findAllByNameContainingIgnoreCaseAndCodeContainingIgnoreCase(
        any(),
        any(),
        eq(pageable)
    )).thenReturn(PaginationUtil.getPage(expectedFundSources, pageable));

    Page<EquipmentFundSource> resultPage = equipmentFundSourceService.search(
        TEST_NAME_QUERY,
        TEST_CODE_QUERY,
        pageable
    );

    verify(fundSourceRepository).findAllByNameContainingIgnoreCaseAndCodeContainingIgnoreCase(
        TEST_NAME_QUERY,
        TEST_CODE_QUERY,
        pageable
    );
    assertThat(expectedFundSources).isEqualTo(resultPage.getContent());
  }

  @Test
  public void shouldSearchEquipmentFundSourcesByNameIfCodeNotProvided() {
    List<EquipmentFundSource> expectedFundSources = Lists.newArrayList(equipmentFundSource);
    when(fundSourceRepository.findAllByNameContainingIgnoreCase(
        any(),
        eq(pageable)
    )).thenReturn(PaginationUtil.getPage(expectedFundSources, pageable));

    Page<EquipmentFundSource> resultPage = equipmentFundSourceService.search(
        TEST_NAME_QUERY,
        "",
        pageable
    );

    verify(fundSourceRepository).findAllByNameContainingIgnoreCase(
        TEST_NAME_QUERY,
        pageable
    );
    assertThat(expectedFundSources).isEqualTo(resultPage.getContent());
  }

  @Test
  public void shouldSearchEquipmentFundSourcesByCodeIfNameNotProvided() {
    List<EquipmentFundSource> expectedFundSources = Lists.newArrayList(equipmentFundSource);
    when(fundSourceRepository.findAllByCodeContainingIgnoreCase(
        any(),
        eq(pageable)
    )).thenReturn(PaginationUtil.getPage(expectedFundSources, pageable));

    Page<EquipmentFundSource> resultPage = equipmentFundSourceService.search(
        "",
        TEST_CODE_QUERY,
        pageable
    );

    verify(fundSourceRepository).findAllByCodeContainingIgnoreCase(
        TEST_CODE_QUERY,
        pageable
    );
    assertThat(expectedFundSources).isEqualTo(resultPage.getContent());
  }


  @Test
  public void shouldReturnAllEquipmentFundSourcesIfBothNameAndCodeNotProvided() {
    List<EquipmentFundSource> expectedFundSources = Lists.newArrayList(equipmentFundSource);
    when(fundSourceRepository.findAll(eq(pageable)))
        .thenReturn(PaginationUtil.getPage(expectedFundSources, pageable));

    Page<EquipmentFundSource> resultPage = equipmentFundSourceService.search(
        "",
        "",
        pageable
    );

    verify(fundSourceRepository).findAll(pageable);
    assertThat(expectedFundSources).isEqualTo(resultPage.getContent());
  }
}
