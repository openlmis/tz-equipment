/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.openlmis.equipment.domain.EquipmentInventory;
import org.openlmis.equipment.domain.EquipmentMaintenanceRequest;
import org.openlmis.equipment.domain.EquipmentMaintenanceRequestReason;
import org.openlmis.equipment.domain.Vendor;
import org.openlmis.equipment.dto.EquipmentMaintenanceRequestDto;
import org.openlmis.equipment.repository.EquipmentInventoryRepository;
import org.openlmis.equipment.repository.EquipmentMaintenanceRequestReasonRepository;
import org.openlmis.equipment.repository.EquipmentMaintenanceRequestRepository;
import org.openlmis.equipment.repository.VendorRepository;
import org.openlmis.equipment.util.EquipmentMaintenanceRequestDataBuilder;

@SuppressWarnings({"PMD.UnusedPrivateField", "PMD.TooManyMethods"})
public class EquipmentMaintenanceRequestServiceTest {

  @Mock
  EquipmentMaintenanceRequestRepository equipmentMaintenanceRequestRepository;

  @Mock
  EquipmentInventoryRepository equipmentInventoryRepository;

  @Mock
  VendorRepository vendorRepository;

  @Mock
  EquipmentMaintenanceRequestReasonRepository maintenanceRequestReasonRepository;

  EquipmentInventory equipmentInventory;

  Vendor vendor;

  EquipmentMaintenanceRequestReason equipmentMaintenanceRequestReason;

  EquipmentMaintenanceRequestDto equipmentMaintenanceRequestDto;

  EquipmentMaintenanceRequest equipmentMaintenanceRequest;

  @InjectMocks
  EquipmentMaintenanceRequestService equipmentMaintenanceRequestService;

  private UUID maintenanceRequestId;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    equipmentMaintenanceRequest
        = new EquipmentMaintenanceRequestDataBuilder()
        .build();

    maintenanceRequestId = equipmentMaintenanceRequest.getId();
    vendor = equipmentMaintenanceRequest.getVendor();
    equipmentInventory = equipmentMaintenanceRequest.getInventory();
    equipmentMaintenanceRequestReason
        = equipmentMaintenanceRequest.getReason();

    equipmentMaintenanceRequestDto = EquipmentMaintenanceRequestDto
        .newInstance(equipmentMaintenanceRequest);

    when(equipmentInventoryRepository.findById(any()))
        .thenReturn(Optional.of(equipmentInventory));
    when(vendorRepository.findById(any())).thenReturn(Optional.of(vendor));
    when(maintenanceRequestReasonRepository.findById(any()))
        .thenReturn(Optional.of(equipmentMaintenanceRequestReason));

  }

  @Test
  public void shouldSaveNewEquipmentMaintenanceRequestSuccessfulOnCreate() {
    when(equipmentMaintenanceRequestRepository
        .save(any(EquipmentMaintenanceRequest.class))).thenAnswer(invocation -> {
          EquipmentMaintenanceRequest maintenanceRequest
              = invocation.getArgument(0);
          maintenanceRequest.setId(maintenanceRequestId);
          return maintenanceRequest;
        });


    EquipmentMaintenanceRequest savedEquipmentMaintenanceRequest
        = equipmentMaintenanceRequestService
        .create(equipmentMaintenanceRequestDto);
    verify(equipmentMaintenanceRequestRepository)
        .save(any(EquipmentMaintenanceRequest.class));

    assertThat(savedEquipmentMaintenanceRequest)
        .isEqualTo(equipmentMaintenanceRequest);
  }

  @Test
  public void shouldCreateWhenUpdatingWithIdThatDoesNotExists() {
    when(equipmentMaintenanceRequestRepository
        .findById(equipmentMaintenanceRequest.getId()))
        .thenReturn(Optional.empty());
    when(equipmentMaintenanceRequestRepository
        .save(any(EquipmentMaintenanceRequest.class)))
        .thenReturn(equipmentMaintenanceRequest);

    EquipmentMaintenanceRequest savedEquipment
        = equipmentMaintenanceRequestService.update(
        equipmentMaintenanceRequest.getId(),
        equipmentMaintenanceRequestDto
    );

    verify(equipmentMaintenanceRequestRepository)
        .save(any(EquipmentMaintenanceRequest.class));
    assertThat(savedEquipment).isEqualTo(equipmentMaintenanceRequest);
  }

}
