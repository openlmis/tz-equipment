/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.openlmis.equipment.domain.EquipmentEnergyType;
import org.openlmis.equipment.dto.EquipmentEnergyTypeDto;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.repository.EquipmentEnergyTypeRepository;
import org.openlmis.equipment.util.EquipmentEnergyTypeDataBuilder;
import org.openlmis.equipment.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@SuppressWarnings({"PMD.UnusedPrivateField", "PMD.TooManyMethods"})
public class EquipmentEnergyTypeServiceTest {
  public static final String TEST_NAME_QUERY = "TEST";
  public static final String TEST_CODE_QUERY = "TEST";

  @Mock
  private EquipmentEnergyTypeRepository energyTypeRepository;
  @Mock
  private PermissionService permissionService;
  @InjectMocks
  private EquipmentEnergyTypeService equipmentEnergyTypeService;

  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  private EquipmentEnergyType equipmentEnergyType;
  private EquipmentEnergyTypeDto equipmentEnergyTypeDto;
  private UUID equipmentEnergyTypeId;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    equipmentEnergyType = new EquipmentEnergyTypeDataBuilder().build();
    equipmentEnergyTypeId = equipmentEnergyType.getId();
    equipmentEnergyTypeDto = EquipmentEnergyTypeDto.newInstance(equipmentEnergyType);
  }

  @Test
  public void shouldSaveNewEnergyTypeSuccessfulOnCreate() {
    when(energyTypeRepository.save(any(EquipmentEnergyType.class))).thenAnswer(
        invocation -> {
          EquipmentEnergyType energyType = invocation.getArgument(0);
          energyType.setId(equipmentEnergyTypeId);
          return energyType;
        }
    );


    EquipmentEnergyType savedEnergyType = equipmentEnergyTypeService.create(equipmentEnergyTypeDto);
    verify(energyTypeRepository).save(any(EquipmentEnergyType.class));

    assertThat(savedEnergyType).isEqualTo(equipmentEnergyType);
  }

  @Test
  public void shouldSaveEnergyTypeSuccessfulOnUpdate() {
    when(energyTypeRepository.save(any(EquipmentEnergyType.class)))
        .then(AdditionalAnswers.returnsFirstArg());
    when(energyTypeRepository.existsById(equipmentEnergyType.getId())).thenReturn(true);

    EquipmentEnergyType savedEnergyType = equipmentEnergyTypeService.update(
        equipmentEnergyType.getId(),
        equipmentEnergyTypeDto
    );
    verify(energyTypeRepository).save(any(EquipmentEnergyType.class));

    assertThat(savedEnergyType).isEqualTo(equipmentEnergyType);
  }

  @Test
  public void shouldCreateWhenUpdatingWithIdThatDoesNotExists() {
    when(energyTypeRepository.findById(equipmentEnergyType.getId()))
        .thenReturn(Optional.empty());
    when(energyTypeRepository.save(any(EquipmentEnergyType.class)))
        .thenReturn(equipmentEnergyType);

    EquipmentEnergyType savedEnergyType = equipmentEnergyTypeService.update(
        equipmentEnergyType.getId(),
        equipmentEnergyTypeDto
    );

    verify(energyTypeRepository).save(any(EquipmentEnergyType.class));
    assertThat(savedEnergyType).isEqualTo(equipmentEnergyType);
  }

  @Test
  public void shouldGetEquipmentEnergyTypeIfExists() {
    when(energyTypeRepository.findById(equipmentEnergyTypeId))
        .thenReturn(Optional.of(equipmentEnergyType));
    EquipmentEnergyType foundEnergyType = equipmentEnergyTypeService.getEquipmentEnergyType(
        equipmentEnergyTypeId
    );

    verify(energyTypeRepository).findById(equipmentEnergyType.getId());
    assertThat(foundEnergyType).isEqualTo(equipmentEnergyType);
  }

  @Test(expected = NotFoundException.class)
  public void shouldFailGettingEquipmentEnergyTypeIfDoesNotExists() {
    when(energyTypeRepository.findById(equipmentEnergyTypeId))
        .thenReturn(Optional.empty());
    equipmentEnergyTypeService.getEquipmentEnergyType(equipmentEnergyTypeId);
  }

  @Test
  public void shouldSearchEquipmentEnergyTypesByBothNameAndCodeWhenProvided() {
    List<EquipmentEnergyType> expectedEnergyTypes = Lists.newArrayList(equipmentEnergyType);
    when(energyTypeRepository.findAllByNameContainingIgnoreCaseAndCodeContainingIgnoreCase(
        any(),
        any(),
        eq(pageable)
    )).thenReturn(PaginationUtil.getPage(expectedEnergyTypes, pageable));

    Page<EquipmentEnergyType> resultPage = equipmentEnergyTypeService.search(
        TEST_NAME_QUERY,
        TEST_CODE_QUERY,
        pageable
    );

    verify(energyTypeRepository).findAllByNameContainingIgnoreCaseAndCodeContainingIgnoreCase(
        TEST_NAME_QUERY,
        TEST_CODE_QUERY,
        pageable
    );
    assertThat(expectedEnergyTypes).isEqualTo(resultPage.getContent());
  }

  @Test
  public void shouldSearchEquipmentEnergyTypesByNameIfCodeNotProvided() {
    List<EquipmentEnergyType> expectedEnergyTypes = Lists.newArrayList(equipmentEnergyType);
    when(energyTypeRepository.findAllByNameContainingIgnoreCase(
        any(),
        eq(pageable)
    )).thenReturn(PaginationUtil.getPage(expectedEnergyTypes, pageable));

    Page<EquipmentEnergyType> resultPage = equipmentEnergyTypeService.search(
        TEST_NAME_QUERY,
        "",
        pageable
    );

    verify(energyTypeRepository).findAllByNameContainingIgnoreCase(
        TEST_NAME_QUERY,
        pageable
    );
    assertThat(expectedEnergyTypes).isEqualTo(resultPage.getContent());
  }

  @Test
  public void shouldSearchEquipmentEnergyTypesByCodeIfNameNotProvided() {
    List<EquipmentEnergyType> expectedEnergyTypes = Lists.newArrayList(equipmentEnergyType);
    when(energyTypeRepository.findAllByCodeContainingIgnoreCase(
        any(),
        eq(pageable)
    )).thenReturn(PaginationUtil.getPage(expectedEnergyTypes, pageable));

    Page<EquipmentEnergyType> resultPage = equipmentEnergyTypeService.search(
        "",
        TEST_CODE_QUERY,
        pageable
    );

    verify(energyTypeRepository).findAllByCodeContainingIgnoreCase(
        TEST_CODE_QUERY,
        pageable
    );
    assertThat(expectedEnergyTypes).isEqualTo(resultPage.getContent());
  }


  @Test
  public void shouldReturnAllEquipmentEnergyTypesIfBothNameAndCodeNotProvided() {
    List<EquipmentEnergyType> expectedEnergyTypes = Lists.newArrayList(equipmentEnergyType);
    when(energyTypeRepository.findAll(eq(pageable)))
        .thenReturn(PaginationUtil.getPage(expectedEnergyTypes, pageable));

    Page<EquipmentEnergyType> resultPage = equipmentEnergyTypeService.search(
        "",
        "",
        pageable
    );

    verify(energyTypeRepository).findAll(pageable);
    assertThat(expectedEnergyTypes).isEqualTo(resultPage.getContent());
  }
}
