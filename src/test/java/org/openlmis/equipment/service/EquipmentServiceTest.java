/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.equipment.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.common.collect.Lists;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.openlmis.equipment.domain.Equipment;
import org.openlmis.equipment.domain.EquipmentCategory;
import org.openlmis.equipment.domain.EquipmentEnergyType;
import org.openlmis.equipment.domain.EquipmentModel;
import org.openlmis.equipment.dto.EquipmentDto;
import org.openlmis.equipment.exception.NotFoundException;
import org.openlmis.equipment.repository.EquipmentCategoryRepository;
import org.openlmis.equipment.repository.EquipmentEnergyTypeRepository;
import org.openlmis.equipment.repository.EquipmentModelRepository;
import org.openlmis.equipment.repository.EquipmentRepository;
import org.openlmis.equipment.service.EquipmentService.SearchParams;
import org.openlmis.equipment.util.EquipmentDataBuilder;
import org.openlmis.equipment.util.PaginationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@SuppressWarnings({"PMD.UnusedPrivateField", "PMD.TooManyMethods"})
public class EquipmentServiceTest {

  @Mock
  private EquipmentRepository equipmentRepository;
  @Mock
  private EquipmentEnergyTypeRepository equipmentEnergyTypeRepository;
  @Mock
  private EquipmentModelRepository equipmentModelRepository;
  @Mock
  private EquipmentCategoryRepository equipmentCategoryRepository;
  @Mock
  private PermissionService permissionService;

  @InjectMocks
  private EquipmentService equipmentService;

  private PageRequest pageable = PageRequest.of(0, Integer.MAX_VALUE);

  private Equipment equipment;
  private EquipmentEnergyType energyType;
  private EquipmentModel model;
  private EquipmentCategory category;
  private EquipmentDto equipmentDto;
  private UUID equipmentId;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    equipment = new EquipmentDataBuilder().build();
    equipmentId = equipment.getId();
    energyType = equipment.getEnergyType();
    model = equipment.getModel();
    category = equipment.getCategory();
    equipmentDto = EquipmentDto.newInstance(equipment);


    when(equipmentEnergyTypeRepository.findById(any())).thenReturn(Optional.of(energyType));
    when(equipmentModelRepository.findById(any())).thenReturn(Optional.of(model));
    when(equipmentCategoryRepository.findById(any())).thenReturn(Optional.of(category));
  }

  @Test
  public void shouldSaveNewEquipmentSuccessfulOnCreate() {
    when(equipmentRepository.save(any(Equipment.class))).thenAnswer(invocation -> {
      Equipment equipment = invocation.getArgument(0);
      equipment.setId(equipmentId);
      return equipment;
    });


    Equipment savedEquipment = equipmentService.create(equipmentDto);
    verify(equipmentRepository).save(any(Equipment.class));

    assertThat(savedEquipment).isEqualTo(equipment);
  }

  @Test
  public void shouldSaveEquipmentSuccessfulOnUpdate() {
    when(equipmentRepository.save(any(Equipment.class)))
        .then(AdditionalAnswers.returnsFirstArg());
    when(equipmentRepository.existsById(equipment.getId())).thenReturn(true);

    Equipment savedEquipment = equipmentService.update(
        equipment.getId(),
        equipmentDto
    );
    verify(equipmentRepository).save(any(Equipment.class));

    assertThat(savedEquipment).isEqualTo(equipment);
  }

  @Test
  public void shouldCreateWhenUpdatingWithIdThatDoesNotExists() {
    when(equipmentRepository.findById(equipment.getId()))
        .thenReturn(Optional.empty());
    when(equipmentRepository.save(any(Equipment.class)))
        .thenReturn(equipment);

    Equipment savedEquipment = equipmentService.update(
        equipment.getId(),
        equipmentDto
    );

    verify(equipmentRepository).save(any(Equipment.class));
    assertThat(savedEquipment).isEqualTo(equipment);
  }

  @Test
  public void shouldGetEquipmentIfExists() {
    when(equipmentRepository.findById(equipmentId))
        .thenReturn(Optional.of(equipment));
    Equipment foundEquipment = equipmentService.getEquipment(equipmentId);

    verify(equipmentRepository).findById(equipment.getId());
    assertThat(foundEquipment).isEqualTo(equipment);
  }

  @Test(expected = NotFoundException.class)
  public void shouldFailGettingEquipmentIfDoesNotExists() {
    when(equipmentRepository.findById(equipmentId)).thenReturn(Optional.empty());
    equipmentService.getEquipment(equipmentId);
  }

  @Test
  public void shouldFindAllEquipments() {
    List<Equipment> expectedEquipments = Lists.newArrayList(equipment);
    when(equipmentRepository.findAll(pageable))
        .thenReturn(PaginationUtil.getPage(expectedEquipments, pageable));
    Page<Equipment> resultPage = equipmentService.findAll(pageable);

    verify(equipmentRepository).findAll(pageable);
    assertThat(expectedEquipments).isEqualTo(resultPage.getContent());
  }

  @Test
  public void shouldSearchEquipments() {
    List<Equipment> expectedEquipments = Lists.newArrayList(equipment);
    when(equipmentRepository.search(any(), eq(pageable)))
        .thenReturn(PaginationUtil.getPage(expectedEquipments, pageable));

    SearchParams searchParams = Mockito.mock(SearchParams.class);
    Page<Equipment> resultPage = equipmentService.search(
        searchParams,
        pageable
    );

    verify(equipmentRepository).search(searchParams, pageable);
    assertThat(expectedEquipments).isEqualTo(resultPage.getContent());
  }

}
