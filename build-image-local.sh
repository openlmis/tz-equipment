export VERSION=$(grep "serviceVersion" gradle.properties|cut -d'=' -f2)
curl -o .env -L https://raw.githubusercontent.com/OpenLMIS/openlmis-ref-distro/master/settings-sample.env
docker-compose -f docker-compose.builder.yml run builder
docker-compose -f docker-compose.builder.yml build image
docker tag openlmistz/equipment:latest openlmistz/equipment:$VERSION
docker login
docker push openlmistz/equipment:$VERSION
docker logout